package gov.lbl.nest.lazarus.process;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.lazarus.control.ControlOnlyExecutableProcess;
import gov.lbl.nest.lazarus.control.EndEventImpl;
import gov.lbl.nest.lazarus.control.FlowElementImpl;
import gov.lbl.nest.lazarus.control.FlowTestUtilities;
import gov.lbl.nest.lazarus.control.MockTerminationListener;
import gov.lbl.nest.lazarus.control.SequenceFlowImpl;
import gov.lbl.nest.lazarus.control.StartEventImpl;
import gov.lbl.nest.lazarus.data.DataInputAssociationImpl;
import gov.lbl.nest.lazarus.data.DataInputImpl;
import gov.lbl.nest.lazarus.data.DataObjectImpl;
import gov.lbl.nest.lazarus.data.DataOutputAssociationImpl;
import gov.lbl.nest.lazarus.data.DataOutputImpl;
import gov.lbl.nest.lazarus.data.InputOutputSpecificationImpl;
import gov.lbl.nest.lazarus.data.InputSetImpl;
import gov.lbl.nest.lazarus.data.MessageImpl;
import gov.lbl.nest.lazarus.data.OperationImpl;
import gov.lbl.nest.lazarus.data.OutputSetImpl;
import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.java.JavaInstantiationException;
import gov.lbl.nest.lazarus.java.JavaInterface;
import gov.lbl.nest.lazarus.java.JavaOperation;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;

/**
 * This is the set of test to be passed to ensure a minimal flow of data.
 *
 * @author patton
 */
@DisplayName("Minimal Service Task Tests")
public class MinimalServiceTaskTests extends
                                     FlowTestUtilities {

    private ProcessDefinitionImpl createMinimalFlowOfControl(DataObjectImpl inputData,
                                                             DataObjectImpl outputData) throws NoSuchMethodException,
                                                                                        JavaInstantiationException,
                                                                                        ClassNotFoundException {
        final List<FlowElementImpl> flowElements = new ArrayList<>();
        final StartEventImpl start = new StartEventImpl(START_NAME,
                                                        START_ID);
        flowElements.add(start);
        final ServiceTaskImpl activity = createServiceTask(inputData,
                                                           outputData);
        flowElements.add(activity);
        final EndEventImpl end = new EndEventImpl(END_NAME,
                                                  END_ID);
        flowElements.add(end);
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              start,
                                              activity));
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              activity,
                                              end));

        final List<DataObjectImpl> dataObjects = new ArrayList<>();
        dataObjects.add(inputData);
        dataObjects.add(outputData);
        final ProcessDefinitionImpl process = new ProcessDefinitionImpl(PROCESS_NAME,
                                                                        PROCESS_ID,
                                                                        start,
                                                                        flowElements,
                                                                        dataObjects,
                                                                        null);
        return process;
    }

    private ServiceTaskImpl createServiceTask(DataObjectImpl inputData,
                                              DataObjectImpl outputData) throws JavaInstantiationException,
                                                                         NoSuchMethodException,
                                                                         ClassNotFoundException {
        final DataInputImpl input = new DataInputImpl(INPUT_NAME,
                                                      INPUT_ID,
                                                      null,
                                                      null);
        final List<DataInputImpl> inputs = new ArrayList<>();
        inputs.add(input);
        final InputSetImpl inputSet = new InputSetImpl(INPUT_SET_NAME,
                                                       INPUT_SET_ID,
                                                       inputs);
        final List<InputSetImpl> inputSets = new ArrayList<>();
        inputSets.add(inputSet);

        final DataOutputImpl output = new DataOutputImpl(OUTPUT_NAME,
                                                         OUTPUT_ID,
                                                         null,
                                                         null);
        final List<DataOutputImpl> outputs = new ArrayList<>();
        outputs.add(output);
        final OutputSetImpl outputSet = new OutputSetImpl(OUTPUT_NAME,
                                                          OUTPUT_SET_ID,
                                                          outputs);
        final List<OutputSetImpl> outputSets = new ArrayList<>();
        outputSets.add(outputSet);

        final InputOutputSpecificationImpl ioSpec = new InputOutputSpecificationImpl(IOSPEC_ID,
                                                                                     inputs,
                                                                                     outputs,
                                                                                     inputSets,
                                                                                     outputSets);

        final List<DataObjectImpl> inputSources = new ArrayList<>();
        inputSources.add(inputData);
        final DataInputAssociationImpl inputAssoc = new DataInputAssociationImpl(INPUT_ASSOC_ID,
                                                                                 inputSources,
                                                                                 input);
        final List<DataInputAssociationImpl> inputAssocs = new ArrayList<>();
        inputAssocs.add(inputAssoc);

        final DataOutputAssociationImpl outputAssoc = new DataOutputAssociationImpl(OUTPUT_ASSOC_ID,
                                                                                    outputs,
                                                                                    outputData);
        final List<DataOutputAssociationImpl> outputAssocs = new ArrayList<>();
        outputAssocs.add(outputAssoc);

        final MessageImpl inMessage = new MessageImpl(IN_MESSAGE_NAME,
                                                      IN_MESSAGE_ID,
                                                      STRING_DEFINITION);
        final MessageImpl outMessage = new MessageImpl(OUT_MESSAGE_NAME,
                                                       OUT_MESSAGE_ID,
                                                       STRING_COLLECTION_DEFINITION);
        final OperationImpl operation = new JavaOperation(OPERATION_NAME,
                                                          OPERATION_ID,
                                                          OPERATION_METHOD,
                                                          inMessage,
                                                          outMessage);

        final List<OperationImpl> operations = new ArrayList<>();
        operations.add(operation);
        new JavaInterface(INTERFACE_NAME,
                          INTERFACE_ID,
                          operations,
                          INTERFACE_CLASS);

        final ServiceTaskBody serviceBody = new ServiceTaskBody(operation);
        final ServiceTaskImpl serviceTask = new ServiceTaskImpl(SERVICE_NAME,
                                                                SERVICE_ID,
                                                                serviceBody,
                                                                ioSpec,
                                                                inputAssocs,
                                                                outputAssocs,
                                                                null,
                                                                null,
                                                                null);
        return serviceTask;
    }

    /**
     * Test that the minimal data flow, process, activity input, activity output,
     * process can be executed.
     *
     * @throws DataObjectException
     *             when the data object can not be set.
     * @throws JavaInstantiationException
     *             when a suitable class of method can not be created.
     * @throws NoSuchMethodException
     *             when no matching {@link Method} instance can be found for the
     *             supplied {@link JavaInterface} instance.
     * @throws ExpressionEvaluationException
     *             when an outgoing {@link Expression} can not be evaluated.
     * @throws InterruptedException
     *             when the test is interrupted waiting for the workflow to
     *             terminate.
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    @Test
    @DisplayName("Execution")
    public void execution() throws DataObjectException,
                            JavaInstantiationException,
                            NoSuchMethodException,
                            ExpressionEvaluationException,
                            InterruptedException,
                            ClassNotFoundException {
        final DataObjectImpl inputData = new DataObjectImpl(INPUT_DATA_NAME,
                                                            INPUT_DATA_ID,
                                                            STRING_DEFINITION,
                                                            null);
        final DataObjectImpl outputData = new DataObjectImpl(OUTPUT_DATA_NAME,
                                                             OUTPUT_DATA_ID,
                                                             STRING_COLLECTION_DEFINITION,
                                                             null);

        final ProcessDefinitionImpl process = createMinimalFlowOfControl(inputData,
                                                                         outputData);

        final Map<String, Object> values = new HashMap<>();
        values.put(INPUT_DATA_NAME,
                   INPUT_STRING_VALUE);

        final ExecutableProcess executableProcess = process.createExecutableProcess(LABELS.get(0),
                                                                                    null,
                                                                                    values);

        ((ControlOnlyExecutableProcess) executableProcess).execute();
        confirmTermination(executableProcess.getTerminated());

        final Map<String, Object> results = executableProcess.getValues();
        assertEquals(2,
                     results.size());
        final List<?> result = (List<?>) results.get(OUTPUT_DATA_NAME);
        assertNotNull(result);
        assertEquals(OUTPUT_STRING_COLLECTION_VALUE.size(),
                     result.size());
    }

    /**
     * Test that the minimal data flow, process, activity input, activity output,
     * process can be executed.
     *
     * @throws DataObjectException
     *             when the data object can not be set.
     * @throws JavaInstantiationException
     *             when a suitable class of method can not be created.
     * @throws NoSuchMethodException
     *             when no matching {@link Method} instance can be found for the
     *             supplied {@link JavaInterface} instance.
     * @throws IOException
     *             when the recovery area can not be created.
     * @throws ExpressionEvaluationException
     *             when an outgoing {@link Expression} can not be evaluated.
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    @Test
    @DisplayName("Recovery")
    public void recovery() throws DataObjectException,
                           JavaInstantiationException,
                           NoSuchMethodException,
                           IOException,
                           ExpressionEvaluationException,
                           ClassNotFoundException {
        final File root = new File("lazarus");
        UnitTestUtility.deleteTree(root);
        Files.createDirectories(root.toPath());

        overlayDirectory(Paths.get("src/test/stores/minimal_6"),
                         Paths.get("lazarus/process"));

        final DataObjectImpl inputData = new DataObjectImpl(INPUT_DATA_NAME,
                                                            INPUT_DATA_ID,
                                                            STRING_DEFINITION,
                                                            null);
        final DataObjectImpl outputData = new DataObjectImpl(OUTPUT_DATA_NAME,
                                                             OUTPUT_DATA_ID,
                                                             STRING_COLLECTION_DEFINITION,
                                                             null);

        final ProcessDefinitionImpl process = createMinimalFlowOfControl(inputData,
                                                                         outputData);

        final MockTerminationListener listener = new MockTerminationListener();
        process.setDefaultTerminationListener(listener);
        final Collection<? extends ExecutableProcess> executableProcesses = process.recoverExecutableProcesses();
        assertEquals(1,
                     executableProcesses.size());
        final ExecutableProcess executableProcess = (executableProcesses.iterator()).next();
        ((ControlOnlyExecutableProcess) executableProcess).execute();

        final Map<String, Object> results = executableProcess.getValues();
        assertEquals(2,
                     results.size());
        final List<?> result = (List<?>) results.get(OUTPUT_DATA_NAME);
        assertNotNull(result);
        assertEquals(OUTPUT_STRING_COLLECTION_VALUE.size(),
                     result.size());
    }

    /**
     * Sets up the environment before each test.
     */
    @BeforeEach
    void setUp() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }

    /**
     * Tears down the environment after each test.
     */
    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }
}
