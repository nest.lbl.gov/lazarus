package gov.lbl.nest.lazarus.process;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.lazarus.control.ControlOnlyExecutableProcess;
import gov.lbl.nest.lazarus.control.EndEventImpl;
import gov.lbl.nest.lazarus.control.FlowElementImpl;
import gov.lbl.nest.lazarus.control.FlowTestUtilities;
import gov.lbl.nest.lazarus.control.SequenceFlowImpl;
import gov.lbl.nest.lazarus.control.StartEventImpl;
import gov.lbl.nest.lazarus.data.DataInputAssociationImpl;
import gov.lbl.nest.lazarus.data.DataInputImpl;
import gov.lbl.nest.lazarus.data.DataObjectImpl;
import gov.lbl.nest.lazarus.data.DataOutputAssociationImpl;
import gov.lbl.nest.lazarus.data.DataOutputImpl;
import gov.lbl.nest.lazarus.data.InputOutputSpecificationImpl;
import gov.lbl.nest.lazarus.data.InputSetImpl;
import gov.lbl.nest.lazarus.data.MessageImpl;
import gov.lbl.nest.lazarus.data.OperationImpl;
import gov.lbl.nest.lazarus.data.OutputSetImpl;
import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.java.JavaInstantiationException;
import gov.lbl.nest.lazarus.java.JavaInterface;
import gov.lbl.nest.lazarus.java.JavaOperation;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;

/**
 * This is the set of test to be passed to ensure a minimal flow of data.
 *
 * @author patton
 */
@DisplayName("Minimal Multiple Write Tests")
public class MinimalMultipleWriteTests extends
                                       FlowTestUtilities {

    private ProcessDefinitionImpl createDoubleActivityFlowOfControl(String operationName,
                                                                    String operationIdentity,
                                                                    String operationMethod,
                                                                    DataObjectImpl inputData,
                                                                    DataObjectImpl outputData,
                                                                    String operation1Name,
                                                                    String operation1Identity,
                                                                    String operation1Method,
                                                                    DataObjectImpl input1Data,
                                                                    DataObjectImpl output1Data) throws NoSuchMethodException,
                                                                                                JavaInstantiationException,
                                                                                                ClassNotFoundException {
        final List<FlowElementImpl> flowElements = new ArrayList<>();
        final StartEventImpl start = new StartEventImpl(START_NAME,
                                                        START_ID);
        flowElements.add(start);
        final ServiceTaskImpl activity = createServiceTask(operationName,
                                                           operationIdentity,
                                                           operationMethod,
                                                           inputData,
                                                           outputData);
        flowElements.add(activity);

        final ServiceTaskImpl activity1;
        if (null == input1Data) {
            activity1 = createServiceTask(operation1Name,
                                          operation1Identity,
                                          operation1Method,
                                          outputData,
                                          output1Data);
        } else {
            activity1 = createServiceTask(operation1Name,
                                          operation1Identity,
                                          operation1Method,
                                          input1Data,
                                          output1Data);
        }
        flowElements.add(activity1);

        final EndEventImpl end = new EndEventImpl(END_NAME,
                                                  END_ID);
        flowElements.add(end);
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              start,
                                              activity));
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              activity,
                                              activity1));
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              activity1,
                                              end));

        final List<DataObjectImpl> dataObjects = new ArrayList<>();
        dataObjects.add(inputData);
        if (null != outputData) {
            dataObjects.add(outputData);
        }
        if (null != input1Data) {
            dataObjects.add(input1Data);
        }
        if (null != output1Data) {
            dataObjects.add(output1Data);
        }
        final ProcessDefinitionImpl process = new ProcessDefinitionImpl(PROCESS_NAME,
                                                                        PROCESS_ID,
                                                                        start,
                                                                        flowElements,
                                                                        dataObjects,
                                                                        null);
        return process;
    }

    private ServiceTaskImpl createServiceTask(String operationName,
                                              String operationIdentity,
                                              String operationMethod,
                                              DataObjectImpl inputData,
                                              DataObjectImpl outputData) throws JavaInstantiationException,
                                                                         NoSuchMethodException,
                                                                         ClassNotFoundException {
        final DataInputImpl input = new DataInputImpl(INPUT_NAME,
                                                      INPUT_ID + operationIdentity,
                                                      null,
                                                      null);
        final List<DataInputImpl> inputs = new ArrayList<>();
        inputs.add(input);
        final InputSetImpl inputSet = new InputSetImpl(INPUT_SET_NAME,
                                                       INPUT_SET_ID + operationIdentity,
                                                       inputs);
        final List<InputSetImpl> inputSets = new ArrayList<>();
        inputSets.add(inputSet);

        final DataOutputImpl output = new DataOutputImpl(OUTPUT_NAME,
                                                         OUTPUT_ID + operationIdentity,
                                                         null,
                                                         null);
        final List<DataOutputImpl> outputs = new ArrayList<>();
        outputs.add(output);
        final OutputSetImpl outputSet = new OutputSetImpl(OUTPUT_NAME,
                                                          OUTPUT_SET_ID + operationIdentity,
                                                          outputs);
        final List<OutputSetImpl> outputSets = new ArrayList<>();
        outputSets.add(outputSet);

        final InputOutputSpecificationImpl ioSpec = new InputOutputSpecificationImpl(IOSPEC_ID + operationIdentity,
                                                                                     inputs,
                                                                                     outputs,
                                                                                     inputSets,
                                                                                     outputSets);

        final List<DataObjectImpl> inputSources = new ArrayList<>();
        inputSources.add(inputData);
        final DataInputAssociationImpl inputAssoc = new DataInputAssociationImpl(INPUT_ASSOC_ID + operationIdentity,
                                                                                 inputSources,
                                                                                 input);
        final List<DataInputAssociationImpl> inputAssocs = new ArrayList<>();
        inputAssocs.add(inputAssoc);

        final DataOutputAssociationImpl outputAssoc;
        if (null == outputData) {
            outputAssoc = new DataOutputAssociationImpl(OUTPUT_ASSOC_ID + operationIdentity,
                                                        outputs,
                                                        inputData);
        } else {
            outputAssoc = new DataOutputAssociationImpl(OUTPUT_ASSOC_ID + operationIdentity,
                                                        outputs,
                                                        outputData);
        }

        final List<DataOutputAssociationImpl> outputAssocs = new ArrayList<>();
        outputAssocs.add(outputAssoc);

        final MessageImpl inMessage = new MessageImpl(IN_MESSAGE_NAME,
                                                      IN_MESSAGE_ID + operationIdentity,
                                                      STRING_DEFINITION);
        final MessageImpl outMessage = new MessageImpl(OUT_MESSAGE_NAME,
                                                       OUT_MESSAGE_ID + operationIdentity,
                                                       STRING_COLLECTION_DEFINITION);
        final OperationImpl operation = new JavaOperation(operationName,
                                                          operationIdentity,
                                                          operationMethod,
                                                          inMessage,
                                                          outMessage);

        final List<OperationImpl> operations = new ArrayList<>();
        operations.add(operation);
        new JavaInterface(INTERFACE_NAME,
                          INTERFACE_ID + operationIdentity,
                          operations,
                          INTERFACE_CLASS);

        final ServiceTaskBody serviceBody = new ServiceTaskBody(operation);
        final ServiceTaskImpl serviceTask = new ServiceTaskImpl(SERVICE_NAME,
                                                                SERVICE_ID + operationIdentity,
                                                                serviceBody,
                                                                ioSpec,
                                                                inputAssocs,
                                                                outputAssocs,
                                                                null,
                                                                null,
                                                                null);
        return serviceTask;
    }

    /**
     * Test that the minimal data flow, process, activity input, activity output,
     * process can be executed.
     *
     * @throws DataObjectException
     *             when the data object can not be set.
     * @throws JavaInstantiationException
     *             when a suitable class of method can not be created.
     * @throws NoSuchMethodException
     *             when no matching {@link Method} instance can be found for the
     *             supplied {@link JavaInterface} instance.
     * @throws ExpressionEvaluationException
     *             when an outgoing {@link Expression} can not be evaluated.
     * @throws InterruptedException
     *             when the test is interrupted waiting for the workflow to
     *             terminate.
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    @Test
    @DisplayName("Execution")
    public void execution() throws DataObjectException,
                            JavaInstantiationException,
                            NoSuchMethodException,
                            ExpressionEvaluationException,
                            InterruptedException,
                            ClassNotFoundException {
        final DataObjectImpl inputData = new DataObjectImpl(INPUT_DATA_NAME,
                                                            INPUT_DATA_ID,
                                                            STRING_DEFINITION,
                                                            null);
        final DataObjectImpl outputData = new DataObjectImpl(OUTPUT_DATA_NAME,
                                                             OUTPUT_DATA_ID,
                                                             STRING_DEFINITION,
                                                             null);

        final ProcessDefinitionImpl process = createDoubleActivityFlowOfControl(OPERATION_1_NAME,
                                                                                OPERATION_1_ID,
                                                                                OPERATION_1_METHOD,
                                                                                inputData,
                                                                                outputData,
                                                                                OPERATION_2_NAME,
                                                                                OPERATION_2_ID,
                                                                                OPERATION_2_METHOD,
                                                                                null,
                                                                                null);

        final Map<String, Object> values = new HashMap<>();
        values.put(INPUT_DATA_NAME,
                   INPUT_STRING_VALUE);

        final ExecutableProcess executableProcess = process.createExecutableProcess(LABELS.get(0),
                                                                                    null,
                                                                                    values);

        ((ControlOnlyExecutableProcess) executableProcess).execute();
        confirmTermination(executableProcess.getTerminated());

        final Map<String, Object> results = executableProcess.getValues();
        assertEquals(2,
                     results.size());
        final Object output = results.get(OUTPUT_DATA_NAME);
        assertNotNull(output);
        final String result = (String) output;
        assertEquals(OUTPUT_ALT_STRING_VALUE,
                     result);
    }

    /**
     * Sets up the environment before each test.
     */
    @BeforeEach
    void setUp() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }

    /**
     * Tears down the environment after each test.
     */
    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }
}
