package gov.lbl.nest.lazarus.process;

import java.util.Arrays;
import java.util.List;

/**
 * This class is used to test {@link ServiceTaskBody} implementations.
 *
 * @author patton
 */
public class MockStringManipulation {

    /**
     * Returns the first word in a string.
     *
     * @param input
     *            the {@link String} instance from which to return the first word.
     *
     * @return the first word in a string.
     */
    public String firstWord(String input) {
        return split(input).get(0);
    }

    /**
     * Splits this string around matches of " ".
     *
     * @param input
     *            the {@link String} instance to split.
     *
     * @return the result of the split.
     */
    public List<String> split(String input) {
        return Arrays.asList(input.split(" "));
    }

    /**
     * Returns the string repeated two more times.
     *
     * @param input
     *            the {@link String} instance from which to create two more copies.
     *
     * @return the string containing three copies of the original.
     */
    public String triplicate(String input) {
        return input + " "
               + input
               + " "
               + input;
    }
}
