package gov.lbl.nest.lazarus.process;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.lazarus.control.ControlOnlyExecutableProcess;
import gov.lbl.nest.lazarus.control.ControlScope;
import gov.lbl.nest.lazarus.control.EndEventImpl;
import gov.lbl.nest.lazarus.control.FlowElementImpl;
import gov.lbl.nest.lazarus.control.FlowTestUtilities;
import gov.lbl.nest.lazarus.control.MockTerminationListener;
import gov.lbl.nest.lazarus.control.SequenceFlowImpl;
import gov.lbl.nest.lazarus.control.StartEventImpl;
import gov.lbl.nest.lazarus.control.TestTracker;
import gov.lbl.nest.lazarus.data.DataInputAssociationImpl;
import gov.lbl.nest.lazarus.data.DataInputImpl;
import gov.lbl.nest.lazarus.data.DataInputSourceImpl;
import gov.lbl.nest.lazarus.data.DataObjectImpl;
import gov.lbl.nest.lazarus.data.DataOutputAssociationImpl;
import gov.lbl.nest.lazarus.data.DataOutputImpl;
import gov.lbl.nest.lazarus.data.DataOutputSourceImpl;
import gov.lbl.nest.lazarus.data.InputOutputSpecificationImpl;
import gov.lbl.nest.lazarus.data.InputSetImpl;
import gov.lbl.nest.lazarus.data.MessageImpl;
import gov.lbl.nest.lazarus.data.MultiInstanceLoopCharacteristicsImpl;
import gov.lbl.nest.lazarus.data.OperationImpl;
import gov.lbl.nest.lazarus.data.OutputDataItem;
import gov.lbl.nest.lazarus.data.OutputSetImpl;
import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.java.JavaInstantiationException;
import gov.lbl.nest.lazarus.java.JavaInterface;
import gov.lbl.nest.lazarus.java.JavaOperation;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.NoSuchObjectException;

/**
 * This is the set of test to be passed to ensure a minimal flow of data.
 *
 * @author patton
 */
@DisplayName("Minimal Multiple Instance Tests")
public class MinimalMultipleInstancesTests extends
                                           FlowTestUtilities {

    /**
     * The labels of the recovered {@link ControlScope} instances.
     */
    private static List<String> RECOVERED_LABELS_IN_USE = Arrays.asList(new String[] { LABELS.get(0),
                                                                                       LABELS.get(1),
                                                                                       LABELS.get(2) });

    private ProcessDefinitionImpl createMinimalFlowOfControl(DataObjectImpl inputData,
                                                             DataObjectImpl outputData) throws NoSuchMethodException,
                                                                                        JavaInstantiationException,
                                                                                        ClassNotFoundException {
        final List<FlowElementImpl> flowElements = new ArrayList<>();
        final StartEventImpl start = new StartEventImpl(START_NAME,
                                                        START_ID);
        flowElements.add(start);
        final ServiceTaskImpl activity = createServiceTask(inputData,
                                                           outputData);
        flowElements.add(activity);
        final EndEventImpl end = new EndEventImpl(END_NAME,
                                                  END_ID);
        flowElements.add(end);
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              start,
                                              activity));
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              activity,
                                              end));

        final List<DataObjectImpl> dataObjects = new ArrayList<>();
        dataObjects.add(inputData);
        dataObjects.add(outputData);
        final ProcessDefinitionImpl process = new ProcessDefinitionImpl(PROCESS_NAME,
                                                                        PROCESS_ID,
                                                                        start,
                                                                        flowElements,
                                                                        dataObjects,
                                                                        null);
        return process;
    }

    private ServiceTaskImpl createServiceTask(DataObjectImpl inputData,
                                              DataObjectImpl outputData) throws JavaInstantiationException,
                                                                         NoSuchMethodException,
                                                                         ClassNotFoundException {
        final DataInputImpl input = new DataInputImpl(INPUT_NAME,
                                                      INPUT_ID,
                                                      null,
                                                      null);
        final List<DataInputImpl> inputs = new ArrayList<>();
        inputs.add(input);
        final InputDataItemImpl input_1 = new InputDataItemImpl(INPUT_1_NAME,
                                                                INPUT_1_ID,
                                                                input,
                                                                null,
                                                                null);
        final DataInputImpl input_2 = new DataInputImpl(INPUT_2_NAME,
                                                        INPUT_2_ID,
                                                        null,
                                                        null);
        inputs.add(input_2);
        final InputSetImpl inputSet = new InputSetImpl(INPUT_SET_NAME,
                                                       INPUT_SET_ID,
                                                       inputs);
        final List<InputSetImpl> inputSets = new ArrayList<>();
        inputSets.add(inputSet);

        final DataOutputImpl output = new DataOutputImpl(OUTPUT_NAME,
                                                         OUTPUT_ID,
                                                         null,
                                                         null);
        final List<DataOutputImpl> outputs = new ArrayList<>();
        outputs.add(output);
        final OutputDataItem output_1 = new OutputDataItem(OUTPUT_1_NAME,
                                                           OUTPUT_1_ID,
                                                           output,
                                                           null,
                                                           null);
        final DataOutputImpl output_2 = new DataOutputImpl(OUTPUT_2_NAME,
                                                           OUTPUT_2_ID,
                                                           null,
                                                           null);
        outputs.add(output_2);
        final OutputSetImpl outputSet = new OutputSetImpl(OUTPUT_NAME,
                                                          OUTPUT_SET_ID,
                                                          outputs);
        final List<OutputSetImpl> outputSets = new ArrayList<>();
        outputSets.add(outputSet);

        final InputOutputSpecificationImpl ioSpec = new InputOutputSpecificationImpl(IOSPEC_ID,
                                                                                     inputs,
                                                                                     outputs,
                                                                                     inputSets,
                                                                                     outputSets);

        final List<DataObjectImpl> inputSources = new ArrayList<>();
        inputSources.add(inputData);
        final DataInputAssociationImpl inputAssoc = new DataInputAssociationImpl(INPUT_ASSOC_ID,
                                                                                 inputSources,
                                                                                 input);
        final List<DataInputAssociationImpl> inputAssocs = new ArrayList<>();
        inputAssocs.add(inputAssoc);
        final List<DataInputSourceImpl> inputSources_1 = new ArrayList<>();
        inputSources_1.add(input_1);
        final DataInputAssociationImpl inputAssoc_1 = new DataInputAssociationImpl(INPUT_ASSOC_ID,
                                                                                   inputSources_1,
                                                                                   input_2);
        inputAssocs.add(inputAssoc_1);

        final List<DataOutputSourceImpl> outputSources_1 = new ArrayList<>();
        outputSources_1.add(output_2);
        final DataOutputAssociationImpl outputAssoc_1 = new DataOutputAssociationImpl(OUTPUT_ASSOC_ID,
                                                                                      outputSources_1,
                                                                                      output_1);
        final List<DataOutputAssociationImpl> outputAssocs = new ArrayList<>();
        outputAssocs.add(outputAssoc_1);

        final List<DataOutputSourceImpl> outputSources = new ArrayList<>();
        outputSources.add(output);
        final DataOutputAssociationImpl outputAssoc = new DataOutputAssociationImpl(OUTPUT_ASSOC_ID,
                                                                                    outputSources,
                                                                                    outputData);
        outputAssocs.add(outputAssoc);

        final MultiInstanceLoopCharacteristicsImpl loop = new MultiInstanceLoopCharacteristicsImpl(LOOP_ID,
                                                                                                   null,
                                                                                                   input,
                                                                                                   output,
                                                                                                   input_1,
                                                                                                   output_1,
                                                                                                   null);

        final MessageImpl inMessage = new MessageImpl(IN_MESSAGE_NAME,
                                                      IN_MESSAGE_ID,
                                                      STRING_DEFINITION);
        final MessageImpl outMessage = new MessageImpl(OUT_MESSAGE_NAME,
                                                       OUT_MESSAGE_ID,
                                                       STRING_COLLECTION_DEFINITION);
        final OperationImpl operation = new JavaOperation(OPERATION_NAME,
                                                          OPERATION_ID,
                                                          OPERATION_1_METHOD,
                                                          inMessage,
                                                          outMessage);

        final List<OperationImpl> operations = new ArrayList<>();
        operations.add(operation);
        new JavaInterface(INTERFACE_NAME,
                          INTERFACE_ID,
                          operations,
                          INTERFACE_CLASS);

        final ServiceTaskBody serviceBody = new ServiceTaskBody(operation);
        final ServiceTaskImpl serviceTask = new ServiceTaskImpl(SERVICE_NAME,
                                                                SERVICE_ID,
                                                                serviceBody,
                                                                ioSpec,
                                                                inputAssocs,
                                                                outputAssocs,
                                                                loop,
                                                                null,
                                                                null);

        return serviceTask;
    }

    /**
     * Test that the minimal data flow, process, activity input, activity output,
     * process can be executed.
     *
     * @throws DataObjectException
     *             when the data object can not be set.
     * @throws JavaInstantiationException
     *             when a suitable class of method can not be created.
     * @throws NoSuchMethodException
     *             when no matching {@link Method} instance can be found for the
     *             supplied {@link JavaInterface} instance.
     * @throws ExpressionEvaluationException
     *             when an outgoing {@link Expression} can not be evaluated.
     * @throws NoSuchObjectException
     *             when some part of the execution structure can not be created.
     * @throws InterruptedException
     *             then the workflow has not terminated correctly.
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    @Test
    @DisplayName("Execution")
    public void execution() throws DataObjectException,
                            JavaInstantiationException,
                            NoSuchMethodException,
                            ExpressionEvaluationException,
                            InterruptedException,
                            NoSuchObjectException,
                            ClassNotFoundException {
        final DataObjectImpl inputData = new DataObjectImpl(INPUT_DATA_NAME,
                                                            INPUT_DATA_ID,
                                                            STRING_COLLECTION_DEFINITION,
                                                            null);
        final DataObjectImpl outputData = new DataObjectImpl(OUTPUT_DATA_NAME,
                                                             OUTPUT_DATA_ID,
                                                             STRING_COLLECTION_DEFINITION,
                                                             null);

        final ProcessDefinitionImpl process = createMinimalFlowOfControl(inputData,
                                                                         outputData);
        final MockTerminationListener listener = new MockTerminationListener();

        final Map<String, Object> values = new HashMap<>();
        values.put(INPUT_DATA_NAME,
                   INPUT_STRING_COLLECTION_VALUE);

        final ExecutableProcess executableProcess = process.createExecutableProcess(LABELS.get(0),
                                                                                    null,
                                                                                    values,
                                                                                    listener);

        ((ControlOnlyExecutableProcess) executableProcess).execute();
        confirmTermination(executableProcess.getTerminated());

        final TestTracker tracker = new TestTracker();
        tracker.process = executableProcess;
        final String label = executableProcess.getLabel();
        assertTrue(RECOVERED_LABELS_IN_USE.contains(label));

        confirmTermination(tracker,
                           listener,
                           OUTPUT_DATA_NAME,
                           OUTPUT_1_STRING_COLLECTION_VALUE);
    }

    /**
     * Test that the minimal data flow, process, activity input, activity output,
     * process can be executed.
     *
     * @throws DataObjectException
     *             when the data object can not be set.
     * @throws JavaInstantiationException
     *             when a suitable class of method can not be created.
     * @throws NoSuchMethodException
     *             when no matching {@link Method} instance can be found for the
     *             supplied {@link JavaInterface} instance.
     * @throws ExpressionEvaluationException
     *             when an outgoing {@link Expression} can not be evaluated.
     * @throws IOException
     *             when the recovery area can not be created.
     * @throws NoSuchObjectException
     *             when some part of the execution structure can not be created.
     * @throws InterruptedException
     *             then the workflow has not terminated correctly.
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    @Test
    @DisplayName("Recovery")
    public void recovery() throws DataObjectException,
                           JavaInstantiationException,
                           NoSuchMethodException,
                           ExpressionEvaluationException,
                           IOException,
                           NoSuchObjectException,
                           InterruptedException,
                           ClassNotFoundException {
        final File root = new File("lazarus");
        UnitTestUtility.deleteTree(root);
        Files.createDirectories(root.toPath());

        overlayDirectory(Paths.get("src/test/stores/minimal_7"),
                         Paths.get("lazarus/process"));

        final DataObjectImpl inputData = new DataObjectImpl(INPUT_DATA_NAME,
                                                            INPUT_DATA_ID,
                                                            STRING_COLLECTION_DEFINITION,
                                                            null);
        final DataObjectImpl outputData = new DataObjectImpl(OUTPUT_DATA_NAME,
                                                             OUTPUT_DATA_ID,
                                                             STRING_COLLECTION_DEFINITION,
                                                             null);

        final ProcessDefinitionImpl process = createMinimalFlowOfControl(inputData,
                                                                         outputData);

        final MockTerminationListener listener = new MockTerminationListener();
        process.setDefaultTerminationListener(listener);
        final Collection<? extends ExecutableProcess> executableProcesses = process.recoverExecutableProcesses();
        assertEquals(RECOVERED_LABELS_IN_USE.size(),
                     executableProcesses.size());
        final List<TestTracker> trackers = new ArrayList<>();
        for (ExecutableProcess executableProcess : executableProcesses) {
            ((ControlOnlyExecutableProcess) executableProcess).execute();
            final TestTracker tracker = new TestTracker();
            tracker.process = executableProcess;
            final String label = executableProcess.getLabel();
            assertTrue(RECOVERED_LABELS_IN_USE.contains(label));
            trackers.add(tracker);
        }

        for (TestTracker tracker : trackers) {
            confirmTermination(tracker,
                               listener,
                               OUTPUT_DATA_NAME,
                               OUTPUT_1_STRING_COLLECTION_VALUE);
        }
    }

    /**
     * Sets up the environment before each test.
     */
    @BeforeEach
    void setUp() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }

    /**
     * Tears down the environment after each test.
     */
    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }
}
