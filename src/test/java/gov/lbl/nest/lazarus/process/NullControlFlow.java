package gov.lbl.nest.lazarus.process;

import java.util.List;

import gov.lbl.nest.lazarus.control.ControlFlow;
import gov.lbl.nest.lazarus.control.ControlOwner;
import gov.lbl.nest.lazarus.control.ControlScope;

/**
 * This implements the {@link ControlFlow} to be used instead of a
 * <code>null</code> value.
 *
 * @author patton
 *
 */
public class NullControlFlow implements
                             ControlFlow {

    @Override
    public void captureThrowable(Throwable cause) {
    }

    @Override
    public boolean changeOwner(ControlOwner owner) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ControlFlow createForkedFlow(int fanningIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ControlFlow createJoinedFlow() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void destroy() {
    }

    @Override
    public ControlScope getControlScope() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getFanningIndex() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer getPriority() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<ControlFlow> getReceived() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Integer> getRecoveredBranches() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void handleThrowable(Throwable cause) {
    }

    @Override
    public void setBranchesToRecover(List<Integer> branchesToExecute) {
    }

    @Override
    public void setJoiningOwner(ControlOwner owner,
                                int fanningIndex) {
    }

}
