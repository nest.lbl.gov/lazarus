package gov.lbl.nest.lazarus.structure;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This is the set of test to be passed by any implementation of the
 * {@link BaseElement} inferface.
 *
 * @author patton
 */
public abstract class AbstractBaseElementTests {

    /**
     * The identity to use in these tests.
     */
    protected static final String IDENTITY = "identity";

    /**
     * Create an {@link BaseElement} instance.
     *
     * @return the created {@link BaseElement} instance.
     */
    abstract protected BaseElement createBaseElement();

    /**
     * Test that the identity can be written and read.
     */
    @Test
    @DisplayName("Identity access")
    public void identityAccess() {
        BaseElement testObject = createBaseElement();
        assertEquals(IDENTITY,
                     testObject.getIdentity());
    }
}
