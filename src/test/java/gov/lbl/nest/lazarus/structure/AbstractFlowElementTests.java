package gov.lbl.nest.lazarus.structure;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This is the set of test to be passed by any implementation of the
 * {@link BaseElement} interface.
 *
 * @author patton
 */
public abstract class AbstractFlowElementTests extends
                                               AbstractBaseElementTests {

    /**
     * The name to use in these tests.
     */
    protected static final String NAME = "name";

    @Override
    protected BaseElement createBaseElement() {
        return createFlowElement();
    }

    /**
     * Create an {@link FlowElement} instance.
     *
     * @return the created {@link FlowElement} instance.
     */
    abstract protected FlowElement createFlowElement();

    /**
     * Test that the name can be written and read.
     */
    @Test
    @DisplayName("Name access")
    public void nameAccess() {
        FlowElement testObject = createFlowElement();
        assertEquals(NAME,
                     testObject.getName());
    }
}
