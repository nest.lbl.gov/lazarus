package gov.lbl.nest.lazarus.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import gov.lbl.nest.lazarus.control.FlowTestUtilities;

/**
 * This class implements the {@link DataOnlyActivityBody} in order to receive
 * and dispatch items from the data flow.
 *
 * @author patton
 */
public class MockActivityBody implements
                              DataOnlyActivityBody {

    @Override
    public void execute(Map<String, Object> inputs,
                        Map<String, Object> outputs) {
        final Integer input = (Integer) inputs.get(FlowTestUtilities.INPUT_NAME);
        assertNotNull(input);
        assertEquals(FlowTestUtilities.INPUT_INT_VALUE,
                     input.intValue());

        assertTrue(outputs.containsKey(FlowTestUtilities.OUTPUT_NAME));
        outputs.put(FlowTestUtilities.OUTPUT_NAME,
                    Integer.valueOf(FlowTestUtilities.OUTPUT_INT_VALUE));
    }

}
