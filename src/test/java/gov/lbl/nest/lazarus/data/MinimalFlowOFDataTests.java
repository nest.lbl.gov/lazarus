package gov.lbl.nest.lazarus.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.lazarus.control.FlowTestUtilities;
import gov.lbl.nest.lazarus.structure.DataObjectException;

/**
 * This is the set of test to be passed to ensure a minimal flow of data.
 *
 * @author patton
 */
@DisplayName("Minimal Flow of Data Tests")
public class MinimalFlowOFDataTests extends
                                    FlowTestUtilities {

    /**
     * The {@link ItemDefinitionImpl} instance representing an integer.
     */
    private static ItemDefinitionImpl INTEGER_DEFINITION;

    static {
        try {
            INTEGER_DEFINITION = new ItemDefinitionImpl(INT_ID,
                                                        Integer.class.getName(),
                                                        false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * The {@link DataFactory} instance used in these tests.
     */
    private DataFactory factory;

    private DataOnlyActivityImpl createActivity(final DataObjectImpl inputData,
                                                final DataObjectImpl outputData) throws ClassNotFoundException {
        final DataInputImpl input = new DataInputImpl(INPUT_NAME,
                                                      INPUT_ID,
                                                      null,
                                                      null);
        final List<DataInputImpl> inputs = new ArrayList<>();
        inputs.add(input);
        final InputSetImpl inputSet = new InputSetImpl(INPUT_SET_NAME,
                                                       INPUT_SET_ID,
                                                       inputs);
        final List<InputSetImpl> inputSets = new ArrayList<>();
        inputSets.add(inputSet);

        final DataOutputImpl output = new DataOutputImpl(OUTPUT_NAME,
                                                         OUTPUT_ID,
                                                         null,
                                                         null);
        final List<DataOutputImpl> outputs = new ArrayList<>();
        outputs.add(output);
        final OutputSetImpl outputSet = new OutputSetImpl(OUTPUT_SET_NAME,
                                                          OUTPUT_SET_ID,
                                                          outputs);
        final List<OutputSetImpl> outputSets = new ArrayList<>();
        outputSets.add(outputSet);

        final InputOutputSpecificationImpl ioSpec = new InputOutputSpecificationImpl(IOSPEC_ID,
                                                                                     inputs,
                                                                                     outputs,
                                                                                     inputSets,
                                                                                     outputSets);

        final List<DataObjectImpl> inputSources = new ArrayList<>();
        inputSources.add(inputData);
        final DataInputAssociationImpl inputAssoc = new DataInputAssociationImpl(INPUT_ASSOC_ID,
                                                                                 inputSources,
                                                                                 input);
        final List<DataInputAssociationImpl> inputAssocs = new ArrayList<>();
        inputAssocs.add(inputAssoc);

        final DataOutputAssociationImpl outputAssoc = new DataOutputAssociationImpl(OUTPUT_ASSOC_ID,
                                                                                    outputs,
                                                                                    outputData);
        final List<DataOutputAssociationImpl> outputAssocs = new ArrayList<>();
        outputAssocs.add(outputAssoc);

        final MockActivityBody activityBody = new MockActivityBody();
        final DataOnlyActivityImpl activity = new DataOnlyActivityImpl(ACTIVITY_ID,
                                                                       activityBody,
                                                                       ioSpec,
                                                                       inputAssocs,
                                                                       outputAssocs);
        return activity;
    }

    /**
     * Test that the minimal data flow, process, activity input, activity output,
     * process can be executed.
     *
     * @throws DataObjectException
     *             when the data object can not be set.
     * @throws DataOnlyExecutionException
     *             when the {@link DataOnlyActivityImpl#execute(DataCell)} fails.
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    @Test
    @DisplayName("Execution")
    public void execution() throws DataObjectException,
                            DataOnlyExecutionException,
                            ClassNotFoundException {
        final DataObjectImpl inputData = new DataObjectImpl(INPUT_DATA_NAME,
                                                            INPUT_DATA_ID,
                                                            INTEGER_DEFINITION,
                                                            null);
        final DataObjectImpl outputData = new DataObjectImpl(OUTPUT_DATA_NAME,
                                                             OUTPUT_DATA_ID,
                                                             INTEGER_DEFINITION,
                                                             null);

        final DataOnlyActivityImpl activity = createActivity(inputData,
                                                             outputData);

        final List<DataObjectImpl> dataObjects = new ArrayList<>();
        dataObjects.add(inputData);
        dataObjects.add(outputData);

        final DataScope scope = factory.createDataScope(dataObjects,
                                                        Paths.get("lazarus/active/1"));

        final Map<String, Object> values = new HashMap<>();
        values.put(INPUT_DATA_NAME,
                   Integer.valueOf(INPUT_INT_VALUE));
        scope.setDataObjectValues(values);

        activity.execute(scope.createDataCell(activity,
                                              Paths.get("lazarus//active/1/controls/1")));

        final Map<String, Object> results = scope.getValues();
        assertEquals(2,
                     results.size());
        final Integer result = (Integer) results.get(OUTPUT_DATA_NAME);
        assertEquals(OUTPUT_INT_VALUE,
                     result.intValue());
    }

    /**
     * Test that the minimal data flow, process, activity input, activity output,
     * process can be recovered.
     *
     * @throws DataObjectException
     *             when the data object can not be set.
     * @throws IOException
     *             when the recovery area can not be created.
     * @throws DataOnlyExecutionException
     *             when the {@link DataOnlyActivityImpl#execute(DataCell)} fails.
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    @Test
    @DisplayName("Recovery")
    public void recovery() throws DataObjectException,
                           IOException,
                           DataOnlyExecutionException,
                           ClassNotFoundException {
        final File root = new File("lazarus");
        UnitTestUtility.deleteTree(root);
        Files.createDirectories(root.toPath());

        overlayDirectory(Paths.get("src/test/stores/minimal_5"),
                         Paths.get("lazarus/process"));

        final DataObjectImpl inputData = new DataObjectImpl(INPUT_DATA_NAME,
                                                            INPUT_DATA_ID,
                                                            INTEGER_DEFINITION,
                                                            null);
        final DataObjectImpl outputData = new DataObjectImpl(OUTPUT_DATA_NAME,
                                                             OUTPUT_DATA_ID,
                                                             INTEGER_DEFINITION,
                                                             null);

        final DataOnlyActivityImpl activity = createActivity(inputData,
                                                             outputData);

        final List<DataObjectImpl> dataObjects = new ArrayList<>();
        dataObjects.add(inputData);
        dataObjects.add(outputData);

        final DataScope scope = factory.recoverDataScope(dataObjects,
                                                         Paths.get("lazarus/process/active/1"));

        activity.execute(scope.createDataCell(activity,
                                              Paths.get("lazarus/process/active/1/controls/1")));

        final Map<String, Object> results = scope.getValues();
        assertEquals(2,
                     results.size());
        final Integer result = (Integer) results.get(OUTPUT_DATA_NAME);
        assertEquals(OUTPUT_INT_VALUE,
                     result.intValue());
    }

    /**
     * Sets up the environment before each test.
     */
    @BeforeEach
    void setUp() {
        UnitTestUtility.deleteTree(new File("lazarus"));
        factory = DataFactory.getDataFactory();
    }

    /**
     * Tears down the environment after each test.
     */
    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }
}
