package gov.lbl.nest.lazarus.control;

import java.io.File;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.NoSuchObjectException;

/**
 * This is the set of test to be passed by the {@link ControlScope} class.
 *
 * @author patton
 */
@DisplayName("Minimal Failure Tests")
public class MinimalFailureTests extends
                                 FlowTestUtilities {

    /**
     * The {@link ControlFactory} used to build the control infrastructure to be
     * tested.
     */
    private ControlFactory controlFactory;

    /**
     * Test that the minimal flow of control, create, perform activity, destroy, can
     * be executed.
     *
     * @throws InterruptedException
     *             then the workflow has not terminated correctly.
     * @throws NoSuchObjectException
     *             when some part of the execution structure can not be created.
     * @throws ExpressionEvaluationException
     *             when an expression can not be evaluated.
     */
    @Test
    @DisplayName("Execution")
    public void execution() throws InterruptedException,
                            NoSuchObjectException,
                            ExpressionEvaluationException {
        final MockFailingBody body = new MockFailingBody();

        final ControlOnlyProcessImpl process = minimalActivity(body);

        final MockTerminationListener listener = new MockTerminationListener(true);
        final TestTracker tracker = new TestTracker();
        final ControlScope controlScope = controlFactory.createControlScope(process,
                                                                            LABELS.get(0),
                                                                            null,
                                                                            listener);
        final ControlOnlyExecutableProcess executableProcess = new ControlOnlyExecutableProcess(controlScope);

        tracker.process = executableProcess;
        executableProcess.execute();
        tracker.success = false;
        confirmTermination(executableProcess.getTerminated());
    }

    /**
     * Sets up the environment before each test.
     */
    @BeforeEach
    void setUp() {
        UnitTestUtility.deleteTree(new File("lazarus"));
        controlFactory = ControlFactory.getControlFactory();
    }

    /**
     * Tears down the environment after each test.
     */
    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }
}
