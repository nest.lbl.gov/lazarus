package gov.lbl.nest.lazarus.control;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.execution.TerminationListener;

/**
 * This class is the to check that the correct calls re being made to a
 * {@link TerminationListener}.
 *
 * @author patton
 */
public class MockTerminationListener implements
                                     TerminationListener {

    /**
     * The collection of {@link ControlScope} instance that failed.
     */
    private final List<ExecutableProcess> failures = new ArrayList<>();

    /**
     * <code>true</code> is a failed execution's data should be preserved.
     */
    private final boolean preserve;

    /**
     * The collection of {@link ControlScope} instance that terminated successfully.
     */
    private final List<ExecutableProcess> successes = new ArrayList<>();

    /**
     * Created an instance of this file.
     */
    public MockTerminationListener() {
        preserve = false;
    }

    /**
     * Created an instance of this file.
     *
     * @param preserve
     *            <code>true</code> is a failed execution's data should be
     *            preserved.
     */
    public MockTerminationListener(boolean preserve) {
        this.preserve = preserve;
    }

    @Override
    public boolean failure(ExecutableProcess process) {
        failures.add(process);
        return preserve;
    }

    /**
     * Returns the collection of {@link ControlScope} instance that failed.
     *
     * @return the collection of {@link ControlScope} instance that failed.
     */
    public Collection<ExecutableProcess> getFailures() {
        return failures;
    }

    /**
     * Returns the collection of {@link ControlScope} instance that terminated
     * successfully.
     *
     * @return the collection of {@link ControlScope} instance that terminated
     *         successfully.
     */
    public Collection<ExecutableProcess> getSuccesses() {
        return successes;
    }

    @Override
    public void success(ExecutableProcess process) {
        successes.add(process);

    }
}
