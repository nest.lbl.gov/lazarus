package gov.lbl.nest.lazarus.control;

import org.junit.jupiter.api.DisplayName;

import gov.lbl.nest.lazarus.structure.AbstractFlowElementTests;
import gov.lbl.nest.lazarus.structure.FlowElement;

/**
 * This is the set of test to be passed by the {@link FlowElementImpl} class.
 *
 * @author patton
 */
@DisplayName("FlowElementImpl Tests")
public class FlowElementImplTests extends
                                  AbstractFlowElementTests {

    @Override
    protected FlowElement createFlowElement() {
        return new FlowElementImpl(NAME,
                                   IDENTITY);
    }

}
