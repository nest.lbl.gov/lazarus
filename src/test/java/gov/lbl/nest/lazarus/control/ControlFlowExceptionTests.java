package gov.lbl.nest.lazarus.control;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This class test that a {@link ControlFlowException} instance fulfills its
 * requirements.
 *
 * @author patton
 */
@DisplayName("ControlFlowException class")
public class ControlFlowExceptionTests {

    /**
     * The {@link Throwable} instance used in tests.
     */
    private static final Throwable CAUSE = new Throwable();

    /**
     * The message used in tests.
     */
    private static final String MESSAGE = "This is the message for a NoSuchElementException instance";

    private static final String NULL_MESSAGE = "java.lang.Throwable";

    @Test
    @DisplayName("Construction")
    void construction() {

        final ControlFlowException exception = new ControlFlowException(MESSAGE);
        assertEquals(MESSAGE,
                     exception.getMessage());
        assertNull(exception.getCause());

        final ControlFlowException exception1 = new ControlFlowException(MESSAGE,
                                                                         CAUSE);
        assertEquals(MESSAGE,
                     exception1.getMessage());
        assertEquals(CAUSE,
                     exception1.getCause());

        final ControlFlowException exception2 = new ControlFlowException(CAUSE);
        assertEquals(NULL_MESSAGE,
                     exception2.getMessage());
        assertEquals(CAUSE,
                     exception2.getCause());
    }
}
