package gov.lbl.nest.lazarus.control;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.NoSuchObjectException;

/**
 * This is the set of test to be passed to ensure the minimal fork and join flow
 * of control.
 *
 * @author patton
 */
@DisplayName("Minimal Failure in parallel control flows Tests")
public class MinimalFailureInParallelControlFlowsTests extends
                                                       FlowTestUtilities {

    /**
     * THe number if test {@link ControlScope} instances to create.
     */
    private static final int INSTANTIATION_COUNT = 1;

    /**
     * The labels of the created {@link ControlScope} instances.
     */
    private static List<String> LABELS_IN_USE = Arrays.asList(new String[] { LABELS.get(0) });

    /**
     * The labels of the {@link ControlScope} instances that should pass through the
     * activity.
     */
    private static List<String> THROUGH_ACTIVITY = Arrays.asList(new String[] { LABELS.get(0) });

    /**
     * The labels of the {@link ControlScope} instances that should pass through the
     * activity_1.
     */
    private static List<String> THROUGH_ACTIVITY_1 = Arrays.asList(new String[] { LABELS.get(0) });

    /**
     * The {@link ControlFactory} used to build the control infrastructure to be
     * tested.
     */
    private ControlFactory controlFactory;

    /**
     * Test that the minimal flow of control, create, fork, join, destroy, can be
     * recovered.
     *
     * @throws InterruptedException
     *             then the workflow has not terminated correctly.
     * @throws NoSuchObjectException
     *             when some part of the execution structure can not be created.
     * @throws ExpressionEvaluationException
     *             when an expression can not be evaluated.
     */
    @Test
    @DisplayName("Execution")
    public void execution() throws InterruptedException,
                            NoSuchObjectException,
                            ExpressionEvaluationException {
        final MockFailingBody body = new MockFailingBody();
        final MockActivityBody body1 = new MockActivityBody();

        final ControlOnlyProcessImpl process = createMinimalForkAndJoin(body,
                                                                        body1);
        final MockTerminationListener listener = new MockTerminationListener(true);
        final List<TestTracker> trackers = new ArrayList<>();
        final Iterator<String> labels = LABELS_IN_USE.iterator();
        for (int count = 0;
             count != INSTANTIATION_COUNT;
             ++count) {
            final TestTracker tracker = new TestTracker();
            trackers.add(tracker);
            final String label = labels.next();
            final ControlScope controlScope = controlFactory.createControlScope(process,
                                                                                label,
                                                                                null,
                                                                                listener);
            tracker.success = false;
            if (THROUGH_ACTIVITY.contains(label)) {
                tracker.activity = true;
            }
            if (THROUGH_ACTIVITY_1.contains(label)) {
                tracker.activity_1 = true;
            }
            final ControlOnlyExecutableProcess executableProcess = new ControlOnlyExecutableProcess(controlScope);

            tracker.process = executableProcess;
            executableProcess.execute();
        }

        for (TestTracker tracker : trackers) {
            confirmTermination(tracker,
                               listener,
                               body1);
        }
    }

    /**
     * Sets up the environment before each test.
     */
    @BeforeEach
    void setUp() {
        UnitTestUtility.deleteTree(new File("lazarus"));
        controlFactory = ControlFactory.getControlFactory();
    }

    /**
     * Tears down the environment after each test.
     */
    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }
}
