package gov.lbl.nest.lazarus.control;

/**
 * This class implements the {@link ControlOnlyActivityBody} in order to mock up
 * a failing activity.
 *
 * @author patton
 */
public class MockFailingBody implements
                             ControlOnlyActivityBody {

    @Override
    public void execute(ControlFlow control,
                        BodyCompletion callback) {
        final Throwable cause = new Throwable();
        control.handleThrowable(cause);
    }

    @Override
    public void setRank(Integer rank) {
    }
}
