package gov.lbl.nest.lazarus.control;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.NoSuchObjectException;

/**
 * This is the set of test to be passed to ensure the minimal fork and join flow
 * of control.
 *
 * @author patton
 */
@DisplayName("Minimal Fork and Join Tests")
public class MinimalForkAndJoinTests extends
                                     FlowTestUtilities {

    /**
     * THe number if test {@link ControlScope} instances to create.
     */
    private static final int INSTANTIATION_COUNT = 1;

    /**
     * The labels of the created {@link ControlScope} instances.
     */
    private static List<String> LABELS_IN_USE = Arrays.asList(new String[] { LABELS.get(0) });

    /**
     * The labels of the {@link ControlScope} instances that should pass through the
     * activity.
     */
    private static List<String> THROUGH_ACTIVITY = Arrays.asList(new String[] { LABELS.get(0) });

    /**
     * The labels of the {@link ControlScope} instances that should pass through the
     * activity_1.
     */
    private static List<String> THROUGH_ACTIVITY_1 = Arrays.asList(new String[] { LABELS.get(0) });

    /**
     * The labels of the recovered {@link ControlScope} instances.
     */
    private static List<String> RECOVERED_LABELS_IN_USE = Arrays.asList(new String[] { LABELS.get(0),
                                                                                       LABELS.get(1),
                                                                                       LABELS.get(2),
                                                                                       LABELS.get(3),
                                                                                       LABELS.get(4),
                                                                                       LABELS.get(5),
                                                                                       LABELS.get(6),
                                                                                       LABELS.get(7),
                                                                                       LABELS.get(8),
                                                                                       LABELS.get(9),
                                                                                       LABELS.get(10),
                                                                                       LABELS.get(11),
                                                                                       LABELS.get(12),
                                                                                       LABELS.get(13),
                                                                                       LABELS.get(14),
                                                                                       LABELS.get(15),
                                                                                       LABELS.get(16),
                                                                                       LABELS.get(17),
                                                                                       LABELS.get(18) });
    /**
     * The labels of the recovered {@link ControlScope} instances that should pass
     * through the activity.
     */
    private static List<String> RECOVERED_THROUGH_ACTIVITY = Arrays.asList(new String[] { LABELS.get(0),
                                                                                          LABELS.get(1),
                                                                                          LABELS.get(2),
                                                                                          LABELS.get(3),
                                                                                          LABELS.get(4),
                                                                                          LABELS.get(5),
                                                                                          LABELS.get(6),
                                                                                          LABELS.get(7) });

    /**
     * The labels of the recovered {@link ControlScope} instances that should pass
     * through the activity_1.
     */
    private static List<String> RECOVERED_THROUGH_ACTIVITY_1 = Arrays.asList(new String[] { LABELS.get(0),
                                                                                            LABELS.get(1),
                                                                                            LABELS.get(2),
                                                                                            LABELS.get(3),
                                                                                            LABELS.get(4),
                                                                                            LABELS.get(5),
                                                                                            LABELS.get(6),
                                                                                            LABELS.get(7),
                                                                                            LABELS.get(8),
                                                                                            LABELS.get(9),
                                                                                            LABELS.get(10) });

    /**
     * The {@link ControlFactory} used to build the control infrastructure to be
     * tested.
     */
    private ControlFactory controlFactory;

    /**
     * Test that the minimal flow of control, create, fork, join, destroy, can be
     * recovered.
     *
     * @throws InterruptedException
     *             then the workflow has not terminated correctly.
     * @throws NoSuchObjectException
     *             when some part of the execution structure can not be created.
     * @throws ExpressionEvaluationException
     *             when an expression can not be evaluated.
     */
    @Test
    @DisplayName("Execution")
    public void execution() throws InterruptedException,
                            NoSuchObjectException,
                            ExpressionEvaluationException {
        final MockActivityBody body = new MockActivityBody();
        final MockActivityBody body1 = new MockActivityBody();

        final ControlOnlyProcessImpl process = createMinimalForkAndJoin(body,
                                                                        body1);
        final MockTerminationListener listener = new MockTerminationListener();
        final List<TestTracker> trackers = new ArrayList<>();
        final Iterator<String> labels = LABELS_IN_USE.iterator();
        for (int count = 0;
             count != INSTANTIATION_COUNT;
             ++count) {
            final TestTracker tracker = new TestTracker();
            trackers.add(tracker);
            final String label = labels.next();
            final ControlScope controlScope = controlFactory.createControlScope(process,
                                                                                label,
                                                                                null,
                                                                                listener);
            if (THROUGH_ACTIVITY.contains(label)) {
                tracker.activity = true;
            }
            if (THROUGH_ACTIVITY_1.contains(label)) {
                tracker.activity_1 = true;
            }
            final ControlOnlyExecutableProcess executableProcess = new ControlOnlyExecutableProcess(controlScope);

            tracker.process = executableProcess;
            executableProcess.execute();
        }

        for (TestTracker tracker : trackers) {
            confirmTermination(tracker,
                               listener,
                               body,
                               body1);
        }
    }

    /**
     * Test that the minimal flow of control, create, fork, join, destroy, can be
     * recovered.
     *
     * @throws InterruptedException
     *             then the workflow has not terminated correctly.
     * @throws NoSuchObjectException
     *             when some part of the execution structure can not be created.
     * @throws IOException
     *             when the recovery area can not be created.
     * @throws ExpressionEvaluationException
     *             when an expression can not be evaluated.
     */
    @Test
    @DisplayName("Recovery")
    public void recovery() throws InterruptedException,
                           NoSuchObjectException,
                           IOException,
                           ExpressionEvaluationException {
        final File root = new File("lazarus");
        UnitTestUtility.deleteTree(root);
        Files.createDirectories(root.toPath());

        overlayDirectory(Paths.get("src/test/stores/minimal_4"),
                         Paths.get("lazarus/process"));

        final MockActivityBody body = new MockActivityBody();
        final MockActivityBody body1 = new MockActivityBody();

        final ControlOnlyProcessImpl process = createMinimalForkAndJoin(body,
                                                                        body1);

        final MockTerminationListener listener = new MockTerminationListener();
        final Collection<? extends ControlScope> testObjects = controlFactory.recoverControlScopes(process,
                                                                                                   listener);
        assertEquals(RECOVERED_LABELS_IN_USE.size(),
                     testObjects.size());
        final List<TestTracker> trackers = new ArrayList<>();
        for (ControlScope controlScope : testObjects) {
            ControlOnlyExecutableProcess executableProcess = new ControlOnlyExecutableProcess(controlScope);
            executableProcess.execute();
            final TestTracker tracker = new TestTracker();
            tracker.process = executableProcess;
            final String label = executableProcess.getLabel();
            assertTrue(RECOVERED_LABELS_IN_USE.contains(label));
            if (RECOVERED_THROUGH_ACTIVITY.contains(label)) {
                tracker.activity = true;
            }
            if (RECOVERED_THROUGH_ACTIVITY_1.contains(label)) {
                tracker.activity_1 = true;
            }
            trackers.add(tracker);
        }

        for (TestTracker tracker : trackers) {
            confirmTermination(tracker,
                               listener,
                               body,
                               body1);
        }
    }

    /**
     * Sets up the environment before each test.
     */
    @BeforeEach
    void setUp() {
        UnitTestUtility.deleteTree(new File("lazarus"));
        controlFactory = ControlFactory.getControlFactory();
    }

    /**
     * Tears down the environment after each test.
     */
    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }
}
