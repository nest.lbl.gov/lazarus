package gov.lbl.nest.lazarus.control;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import gov.lbl.nest.lazarus.execution.ExecutableProcess;

/**
 * This class implements the {@link ControlOnlyActivityBody} in order to track
 * which activities have been executing in which context.
 *
 * @author patton
 */
public class MockActivityBody implements
                              ControlOnlyActivityBody {

    /**
     * The collection of {@link ControlScope} instances that have been seen by this
     * object.
     */
    final List<ExecutableProcess> processes = new ArrayList<>();

    @Override
    public void execute(ControlFlow control,
                        BodyCompletion callback) {
        processes.add((control.getControlScope()).getExecutableProcess());
        callback.bodyExecutionCompleted(control);
    }

    /**
     * Returns the collection of {@link ControlScope} instances that have been seen
     * by this object.
     *
     * @return the collection of {@link ControlScope} instances that have been seen
     *         by this object.
     */
    public Collection<ExecutableProcess> getSeen() {
        return processes;
    }

    @Override
    public void setRank(Integer rank) {
    }
}
