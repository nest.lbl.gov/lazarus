package gov.lbl.nest.lazarus.control;

import java.util.Collection;

import gov.lbl.nest.lazarus.structure.BaseElementImpl;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;

/**
 * This class implements the {@link ControlExpressionImpl} such that it returns
 * <code>true</code> when the label of the {@link ControlScope} instance is in
 * the list of selected {@link ControlScope} instances.
 *
 * @author patton
 */
public class SelectedControlScopesExpression extends
                                             BaseElementImpl implements
                                             ControlExpressionImpl<Boolean> {

    /**
     * The collection of labels for the {@link ControlScope} instances that should
     * return <code>true</code> when this expression is evaluated.
     */
    private Collection<String> labels;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param labels
     *            the collection of labels for the {@link ControlScope} instances
     *            that should return <code>true</code> when this expression is
     *            evaluated.
     */
    public SelectedControlScopesExpression(String identity,
                                           Collection<String> labels) {
        super(identity);
        this.labels = labels;
    }

    @Override
    public Boolean evaluate(ControlFlow controlFlow,
                            Class<Boolean> clazz) throws ExpressionEvaluationException {
        final String label = (controlFlow.getControlScope()).getLabel();
        return labels.contains(label);
    }

    @Override
    public Boolean evaluate(ControlFlow controlFlow,
                            Object[] arguments,
                            Class<Boolean> clazz) throws ExpressionEvaluationException {
        throw new UnsupportedOperationException();
    }
}
