package gov.lbl.nest.lazarus.control;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.NoSuchObjectException;
import gov.lbl.nest.lazarus.structure.StructureFactory;

/**
 * This is the set of test to be passed to ensure a minimal flow of control.
 *
 * @author patton
 */
@DisplayName("Minimal Flow of Control Tests")
public class MinimalFlowOfControlTests extends
                                       FlowTestUtilities {

    /**
     * The labels of the recovered {@link ControlScope} instances.
     */
    private static List<String> RECOVERED_LABELS_IN_USE = Arrays.asList(new String[] { LABELS.get(0),
                                                                                       LABELS.get(1),
                                                                                       LABELS.get(2),
                                                                                       LABELS.get(3),
                                                                                       LABELS.get(4) });
    /**
     * The {@link ControlFactory} used to build the control infrastructure to be
     * tested.
     */
    private ControlFactory controlFactory;

    /**
     * The {@link StructureFactory} instance used in these tests.
     */
//    private StructureFactory structureFactory;

    private ControlOnlyProcessImpl createMinimalFlowOfControl() {
        final List<FlowElementImpl> flowElements = new ArrayList<>();
        final StartEventImpl start = new StartEventImpl(START_NAME,
                                                        START_ID);
        flowElements.add(start);
        final EndEventImpl end = new EndEventImpl(END_NAME,
                                                  END_ID);
        flowElements.add(end);
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              start,
                                              end));
        final ControlOnlyProcessImpl process = new ControlOnlyProcessImpl(PROCESS_NAME,
                                                                          PROCESS_ID,
                                                                          start,
                                                                          flowElements);
        return process;
    }

    /**
     * Test that the minimal flow of control, create, transfer, destroy, can be
     * executed.
     *
     * @throws InterruptedException
     *             then the workflow has not terminated correctly.
     * @throws NoSuchObjectException
     *             when some part of the execution structure can not be created.
     * @throws ExpressionEvaluationException
     *             when an expression can not be evaluated.
     */
    @Test
    @DisplayName("Execution")
    public void execution() throws InterruptedException,
                            NoSuchObjectException,
                            ExpressionEvaluationException {
        final ControlOnlyProcessImpl process = createMinimalFlowOfControl();

        final MockTerminationListener listener = new MockTerminationListener();
        final TestTracker tracker = new TestTracker();
        final ControlScope controlScope = controlFactory.createControlScope(process,
                                                                            LABELS.get(0),
                                                                            null,
                                                                            listener);
        final ControlOnlyExecutableProcess executableProcess = new ControlOnlyExecutableProcess(controlScope);

        tracker.process = executableProcess;
        executableProcess.execute();
        confirmTermination(tracker,
                           listener);
    }

    /**
     * Test that the minimal flow of control, create, transfer, destroy, can be
     * recovered.
     *
     * @throws InterruptedException
     *             then the workflow has not terminated correctly.
     * @throws NoSuchObjectException
     *             when some part of the execution structure can not be created.
     * @throws IOException
     *             when the recovery area can not be created.
     * @throws ExpressionEvaluationException
     *             when an expression can not be evaluated.
     */
    @Test
    @DisplayName("Recovery")
    public void recovery() throws InterruptedException,
                           NoSuchObjectException,
                           IOException,
                           ExpressionEvaluationException {
        final File root = new File("lazarus");
        UnitTestUtility.deleteTree(root);
        Files.createDirectories(root.toPath());

        overlayDirectory(Paths.get("src/test/stores/minimal_1"),
                         Paths.get("lazarus/process"));

        final ControlOnlyProcessImpl process = createMinimalFlowOfControl();

        final MockTerminationListener listener = new MockTerminationListener();
        final Collection<? extends ControlScope> controlScopes = controlFactory.recoverControlScopes(process,
                                                                                                     listener);

        assertEquals(RECOVERED_LABELS_IN_USE.size(),
                     controlScopes.size());
        final List<TestTracker> trackers = new ArrayList<>();
        for (ControlScope controlScope : controlScopes) {
            ControlOnlyExecutableProcess executableProcess = new ControlOnlyExecutableProcess(controlScope);
            executableProcess.execute();
            final TestTracker tracker = new TestTracker();
            tracker.process = executableProcess;
            final String label = executableProcess.getLabel();
            assertTrue(RECOVERED_LABELS_IN_USE.contains(label));
            trackers.add(tracker);
        }

        for (TestTracker tracker : trackers) {
            confirmTermination(tracker,
                               listener);
        }
    }

    /**
     * Sets up the environment before each test.
     */
    @BeforeEach
    void setUp() {
        UnitTestUtility.deleteTree(new File("lazarus"));
        controlFactory = ControlFactory.getControlFactory();
    }

    /**
     * Tears down the environment after each test.
     */
    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }
}
