package gov.lbl.nest.lazarus.control;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.NoSuchObjectException;

/**
 * This is the set of test to be passed by the {@link ControlScope} class.
 *
 * @author patton
 */
@DisplayName("Minimal Activity Tests")
public class MinimalActivityTests extends
                                  FlowTestUtilities {

    /**
     * The labels of the recovered {@link ControlScope} instances.
     */
    private static List<String> RECOVERED_LABELS_IN_USE = Arrays.asList(new String[] { LABELS.get(0),
                                                                                       LABELS.get(1),
                                                                                       LABELS.get(2),
                                                                                       LABELS.get(3),
                                                                                       LABELS.get(4),
                                                                                       LABELS.get(5),
                                                                                       LABELS.get(6) });

    /**
     * The labels of the recovered {@link ControlScope} instances that should pass
     * through the activity.
     */
    private static List<String> RECOVERED_THROUGH_ACTIVITY = Arrays.asList(new String[] { LABELS.get(0),
                                                                                          LABELS.get(1),
                                                                                          LABELS.get(2),
                                                                                          LABELS.get(3) });

    /**
     * The {@link ControlFactory} used to build the control infrastructure to be
     * tested.
     */
    private ControlFactory factory;

    /**
     * Test that the minimal flow of control, create, perform activity, destroy, can
     * be executed.
     *
     * @throws InterruptedException
     *             then the workflow has not terminated correctly.
     * @throws NoSuchObjectException
     *             when some part of the execution structure can not be created.
     * @throws ExpressionEvaluationException
     *             when an expression can not be evaluated.
     */
    @Test
    @DisplayName("Execution")
    public void execution() throws InterruptedException,
                            NoSuchObjectException,
                            ExpressionEvaluationException {
        final MockActivityBody body = new MockActivityBody();

        final ControlOnlyProcessImpl process = minimalActivity(body);

        final MockTerminationListener listener = new MockTerminationListener();
        final TestTracker tracker = new TestTracker();
        final ControlScope controlScope = factory.createControlScope(process,
                                                                     LABELS.get(0),
                                                                     null,
                                                                     listener);

        final ControlOnlyExecutableProcess executableProcess = new ControlOnlyExecutableProcess(controlScope);

        tracker.process = executableProcess;
        executableProcess.execute();
        tracker.activity = true;
        confirmTermination(tracker,
                           listener,
                           body,
                           null);
        Collection<ExecutableProcess> processes = body.getSeen();
        assertEquals(1,
                     processes.size());
        assertTrue(processes.contains(executableProcess));
    }

    /**
     * Test that the minimal flow of control, create, perform activity, destroy, can
     * be recovered.
     *
     * @throws InterruptedException
     *             then the workflow has not terminated correctly.
     * @throws NoSuchObjectException
     *             when some part of the execution structure can not be created.
     * @throws IOException
     *             when the recovery area can not be created.
     * @throws ExpressionEvaluationException
     *             when an expression can not be evaluated.
     */
    @Test
    @DisplayName("Recovery")
    public void recovery() throws InterruptedException,
                           NoSuchObjectException,
                           IOException,
                           ExpressionEvaluationException {
        final File root = new File("lazarus");
        UnitTestUtility.deleteTree(root);
        Files.createDirectories(root.toPath());

        overlayDirectory(Paths.get("src/test/stores/minimal_2"),
                         Paths.get("lazarus/process"));

        final MockActivityBody body = new MockActivityBody();

        final ControlOnlyProcessImpl process = minimalActivity(body);

        final MockTerminationListener listener = new MockTerminationListener();
        final Collection<? extends ControlScope> testObjects = factory.recoverControlScopes(process,
                                                                                            listener);
        assertEquals(RECOVERED_LABELS_IN_USE.size(),
                     testObjects.size());
        final List<TestTracker> trackers = new ArrayList<>();
        for (ControlScope controlScope : testObjects) {
            ControlOnlyExecutableProcess executableProcess = new ControlOnlyExecutableProcess(controlScope);
            executableProcess.execute();
            final TestTracker tracker = new TestTracker();
            tracker.process = executableProcess;
            final String label = executableProcess.getLabel();
            assertTrue(RECOVERED_LABELS_IN_USE.contains(label));
            if (RECOVERED_THROUGH_ACTIVITY.contains(label)) {
                tracker.activity = true;
            }
            trackers.add(tracker);
        }

        for (TestTracker tracker : trackers) {
            confirmTermination(tracker,
                               listener,
                               body,
                               null);
        }
    }

    /**
     * Sets up the environment before each test.
     */
    @BeforeEach
    void setUp() {
        UnitTestUtility.deleteTree(new File("lazarus"));
        factory = ControlFactory.getControlFactory();
    }

    /**
     * Tears down the environment after each test.
     */
    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }
}
