package gov.lbl.nest.lazarus.digraph;

import java.util.Collection;

/**
 * This class is an implementation of a {@link Node} instance that delegates its
 * responsibilities to the {@link NodeImpl} class.
 *
 * @author patton
 */
public class MockDelegator implements
                           Node {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link NodeImpl} instance to which this Objects responsibilities are
     * delegated.
     */
    private final NodeImpl delegatee;

    /**
     * The identity of this Object.
     */
    private final String identity;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the identity of this Object.
     */
    public MockDelegator(final String identity) {
        delegatee = new NodeImpl(this);
        this.identity = identity;
    }

    // instance member method (alphabetic)

    @Override
    public DfsState depthFirstSearch(DfsState dfsState) {
        return delegatee.depthFirstSearch(dfsState);
    }

    /**
     * Returns the {@link NodeImpl} instance to which this Objects responsibilities
     * are delegated.
     *
     * @return the {@link NodeImpl} instance to which this Objects responsibilities
     *         are delegated.
     */
    public NodeImpl getDelegatee() {
        return delegatee;
    }

    @Override
    public Integer getDepth() {
        return delegatee.getDepth();
    }

    @Override
    public String getIdentity() {
        return identity;
    }

    @Override
    public void increaseDepth(int delta) {
        delegatee.increaseDepth(delta);
    }

    /**
     * Sets the {@link Collection} of {@link Node} instances that are directly
     * reachable from this one.
     *
     * @param successors
     *            the {@link Collection} of {@link Node} instances that are directly
     *            reachable from this one.
     */
    public void setSuccessors(final Collection<? extends Arc> successors) {
        delegatee.setSuccessors(successors);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
