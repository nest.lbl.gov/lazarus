package gov.lbl.nest.lazarus.digraph;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This class test that a depth first search for a digraph fulfills its
 * requirements.
 *
 * @author patton
 */
@DisplayName("Depth First Searchs")
public class DepthFirstSearchTests {

    /**
     * Depth of node in the asymmetric diamond graph.
     */
    private static final Map<String, Integer> ASYMMETRIC_DIAMOND_DEPTHS = new HashMap<>() {
        /**
         * Required for serialization
         */
        private static final long serialVersionUID = 1L;

        {
            put("A",
                0);
            put("B",
                1);
            put("C",
                2);
            put("D",
                3);
        }
    };

    /**
     * Depth of node in the branched graph.
     */
    private static final Map<String, Integer> BRANCHED_DEPTHS = new HashMap<>() {
        /**
         * Required for serialization
         */
        private static final long serialVersionUID = 1L;

        {
            put("A",
                0);
            put("B",
                1);
            put("C",
                1);
        }
    };

    /**
     * Depth of node in the linear graph.
     */
    private static final Map<String, Integer> DIAMOND_DEPTHS = new HashMap<>() {
        /**
         * Required for serialization
         */
        private static final long serialVersionUID = 1L;

        {
            put("A",
                0);
            put("B",
                1);
            put("C",
                1);
            put("D",
                2);
        }
    };

    /**
     * Depth of node in the linear graph.
     */
    private static final Map<String, Integer> LINEAR_DEPTHS = new HashMap<>() {
        /**
         * Required for serialization
         */
        private static final long serialVersionUID = 1L;

        {
            put("A",
                0);
            put("B",
                1);
        }
    };

    /**
     * Depth of node in the loop graph.
     */
    private static final Map<String, Integer> LONG_LOOP_DEPTHS = new HashMap<>() {
        /**
         * Required for serialization
         */
        private static final long serialVersionUID = 1L;

        {
            put("A",
                0);
            put("B",
                1);
            put("C",
                2);
        }
    };

    /**
     * Depth of node in the loop graph.
     */
    private static final Map<String, Integer> LOOP_DEPTHS = new HashMap<>() {
        /**
         * Required for serialization
         */
        private static final long serialVersionUID = 1L;

        {
            put("A",
                0);
            put("B",
                1);
        }
    };

    /**
     * Depth of node in the reversed asymmetric diamond graph.
     */
    private static final Map<String, Integer> REVERSED_ASYMMETRIC_DIAMOND_DEPTHS = new HashMap<>() {
        /**
         * Required for serialization
         */
        private static final long serialVersionUID = 1L;

        {
            put("A",
                0);
            put("B",
                1);
            put("C",
                2);
            put("D",
                3);
        }
    };

    /**
     * Creates a asymmetric diamond digraph.
     *
     * The 'D' node is needs to check that successor nodes get the correct depth
     * assigned to them.
     *
     * <pre>
     * A +------+> C +-> D
     *   +-> B -+
     * </pre>
     *
     * @return the created asymmetric digraph
     */
    private static Node createAsymmetricDiamondGraph() {
        final NodeImpl nodeD = new NodeImpl("D");
        final ArcImpl arc4 = new ArcImpl(nodeD);
        final NodeImpl nodeC = new NodeImpl("C");
        final List<ArcImpl> successorsC = new ArrayList<>();
        successorsC.add(arc4);
        nodeC.setSuccessors(successorsC);
        final ArcImpl arc3 = new ArcImpl(nodeC);
        final ArcImpl arc2 = new ArcImpl(nodeC);
        final NodeImpl nodeB = new NodeImpl("B");
        final List<ArcImpl> successorsB = new ArrayList<>();
        successorsB.add(arc3);
        nodeB.setSuccessors(successorsB);
        final ArcImpl arc1 = new ArcImpl(nodeB);
        final NodeImpl nodeA = new NodeImpl("A");
        final List<ArcImpl> successorsA = new ArrayList<>();
        successorsA.add(arc1);
        successorsA.add(arc2);
        nodeA.setSuccessors(successorsA);
        return nodeA;
    }

    /**
     * Creates a branched digraph.
     *
     * <pre>
     * A +-> B
     *   +-> C
     * </pre>
     *
     * @return the created branched digraph
     */
    private static Node createBranchedGraph() {
        final NodeImpl nodeC = new NodeImpl("C");
        final ArcImpl arc2 = new ArcImpl(nodeC);
        final NodeImpl nodeB = new NodeImpl("B");
        final ArcImpl arc1 = new ArcImpl(nodeB);
        final NodeImpl nodeA = new NodeImpl("A");
        final List<ArcImpl> successorsA = new ArrayList<>();
        successorsA.add(arc1);
        successorsA.add(arc2);
        nodeA.setSuccessors(successorsA);
        return nodeA;
    }

    /**
     * Creates a diamond digraph.
     *
     * <pre>
     * A +-> B -+> D
     *   +-> C -+
     * </pre>
     *
     * @return the created diamond digraph
     */
    private static Node createDiamondGraph() {
        final NodeImpl nodeD = new NodeImpl("D");
        final ArcImpl arc4 = new ArcImpl(nodeD);
        final ArcImpl arc3 = new ArcImpl(nodeD);
        final NodeImpl nodeC = new NodeImpl("C");
        final List<ArcImpl> successorsC = new ArrayList<>();
        successorsC.add(arc4);
        nodeC.setSuccessors(successorsC);
        final ArcImpl arc2 = new ArcImpl(nodeC);
        final NodeImpl nodeB = new NodeImpl("B");
        final List<ArcImpl> successorsB = new ArrayList<>();
        successorsB.add(arc3);
        nodeB.setSuccessors(successorsB);
        final ArcImpl arc1 = new ArcImpl(nodeB);
        final NodeImpl nodeA = new NodeImpl("A");
        final List<ArcImpl> successorsA = new ArrayList<>();
        successorsA.add(arc1);
        successorsA.add(arc2);
        nodeA.setSuccessors(successorsA);
        return nodeA;
    }

    /**
     * Creates a line digraph.
     *
     * <pre>
     * A-- > B
     * </pre>
     *
     * @return the created line digraph
     */
    private static Node createLinearGraph() {
        final NodeImpl nodeB = new NodeImpl("B");
        final ArcImpl arc1 = new ArcImpl(nodeB);
        final NodeImpl nodeA = new NodeImpl("A");
        final List<ArcImpl> successorsA = new ArrayList<>();
        successorsA.add(arc1);
        nodeA.setSuccessors(successorsA);
        return nodeA;
    }

    /**
     * Creates a long loop digraph.
     *
     * <pre>
     * A --> B --> C
     *       ^     |
     *       +-----+
     * </pre>
     *
     * @return the created long loop digraph
     */
    private static Node createLongLoopGraph() {
        final NodeImpl nodeC = new NodeImpl("C");
        final ArcImpl arc2 = new ArcImpl(nodeC);
        final NodeImpl nodeB = new NodeImpl("B");
        final List<ArcImpl> successorsB = new ArrayList<>();
        successorsB.add(arc2);
        nodeB.setSuccessors(successorsB);
        final ArcImpl arc3 = new ArcImpl(nodeB);
        final ArcImpl arc1 = new ArcImpl(nodeB);
        final List<ArcImpl> successorsC = new ArrayList<>();
        successorsC.add(arc3);
        nodeC.setSuccessors(successorsC);
        final NodeImpl nodeA = new NodeImpl("A");
        final List<ArcImpl> successorsA = new ArrayList<>();
        successorsA.add(arc1);
        nodeA.setSuccessors(successorsA);
        return nodeA;
    }

    /**
     * Creates a loop digraph.
     *
     * <pre>
     * A --> B -+
     *       ^  |
     *       +--+
     * </pre>
     *
     * @return the created loop digraph
     */
    private static Node createLoopGraph() {
        final NodeImpl nodeB = new NodeImpl("B");
        final ArcImpl arc2 = new ArcImpl(nodeB);
        final List<ArcImpl> successorsB = new ArrayList<>();
        successorsB.add(arc2);
        nodeB.setSuccessors(successorsB);
        final ArcImpl arc1 = new ArcImpl(nodeB);
        final NodeImpl nodeA = new NodeImpl("A");
        final List<ArcImpl> successorsA = new ArrayList<>();
        successorsA.add(arc1);
        nodeA.setSuccessors(successorsA);
        return nodeA;
    }

    /**
     * Creates a revered asymmetric diamond digraph.
     *
     * The 'D' node is needs to check that successor nodes get the correct depth
     * assigned to them.
     *
     * <pre>
     * A +-> B -+> C +-> D
     *   +------+
     * </pre>
     *
     * @return the created branched digraph
     */
    private static Node createReversedAsymmetricDiamondGraph() {
        final NodeImpl nodeD = new NodeImpl("D");
        final ArcImpl arc4 = new ArcImpl(nodeD);
        final NodeImpl nodeC = new NodeImpl("C");
        final List<ArcImpl> successorsC = new ArrayList<>();
        successorsC.add(arc4);
        nodeC.setSuccessors(successorsC);
        final ArcImpl arc3 = new ArcImpl(nodeC);
        final ArcImpl arc2 = new ArcImpl(nodeC);
        final NodeImpl nodeB = new NodeImpl("B");
        final List<ArcImpl> successorsB = new ArrayList<>();
        successorsB.add(arc3);
        nodeB.setSuccessors(successorsB);
        final ArcImpl arc1 = new ArcImpl(nodeB);
        final NodeImpl nodeA = new NodeImpl("A");
        final List<ArcImpl> successorsA = new ArrayList<>();
        successorsA.add(arc2);
        successorsA.add(arc1);
        nodeA.setSuccessors(successorsA);
        return nodeA;
    }

    /**
     * The {@link Node} instance that is the root of the digraph.
     */
    private Node testObject;

    /**
     * Test that a depth first search for a digraph fulfills its requirements.
     *
     */
    @Test
    @DisplayName("Asymmetric Diamond Graph")
    public void asymmetricDiamondGraph() {
        testObject = createAsymmetricDiamondGraph();
        DfsState dfsState = checkDepths(ASYMMETRIC_DIAMOND_DEPTHS);
        assertTrue(dfsState.isAcyclic(),
                   "This Graph was not marked as acyclic");
    }

    /**
     * Test that a depth first search for a digraph fulfills its requirements.
     *
     */
    @Test
    @DisplayName("Branched Graph")
    public void branchedGraph() {
        testObject = createBranchedGraph();
        DfsState dfsState = checkDepths(BRANCHED_DEPTHS);
        assertTrue(dfsState.isAcyclic(),
                   "This Graph was not marked as acyclic");
    }

    /**
     * Test that a depth first search for a digraph fulfills its requirements.
     *
     * @param depthMap
     *            the Map of Nodes to their executed depths.
     *
     * @return the resulting {@link DfsState} instance.
     */
    private DfsState checkDepths(final Map<String, Integer> depthMap) {
        final DfsState dfsState = testObject.depthFirstSearch(null);
        List<Node> nodes = dfsState.getReversePostordering();
        assertEquals(depthMap.size(),
                     nodes.size(),
                     "Incorrect number of nodes in postordering");
        for (Node node : nodes) {
            final String identity = node.getIdentity();
            assertEquals((int) depthMap.get(identity),
                         (node.getDepth()).intValue(),
                         "Incorrect depth for Node " + identity);
        }
        return dfsState;
    }

    /**
     * Test the responsibilities can be delegated to the {@link NodeImpl} class.
     */
    @Test
    @DisplayName("Delegation")
    public void delegation() {
        String identity2 = "B";
        final MockDelegator nodeB = new MockDelegator(identity2);
        assertEquals(identity2,
                     (nodeB.getDelegatee()).getIdentity(),
                     "Identity for delegatee is not correct");
        final ArcImpl arc1 = new ArcImpl(nodeB);
        String identity1 = "A";
        final MockDelegator nodeA = new MockDelegator(identity1);
        assertEquals(identity1,
                     (nodeA.getDelegatee()).getIdentity(),
                     "Identity for delegatee is not correct");
        final List<ArcImpl> successorsA = new ArrayList<>();
        successorsA.add(arc1);
        nodeA.setSuccessors(successorsA);
        testObject = nodeA;

        DfsState dfsState = checkDepths(LINEAR_DEPTHS);
        assertTrue(dfsState.isAcyclic(),
                   "This Graph was not marked as acyclic");
    }

    /**
     * Test that a depth first search for a digraph fulfills its requirements.
     *
     */
    @Test
    @DisplayName("Diamond Graph")
    public void diamondGraph() {
        testObject = createDiamondGraph();
        DfsState dfsState = checkDepths(DIAMOND_DEPTHS);
        assertTrue(dfsState.isAcyclic(),
                   "This Graph was not marked as acyclic");
    }

    /**
     * Test that a depth first search for a digraph fulfills its requirements.
     *
     */
    @Test
    @DisplayName("Linear Graph")
    public void linearGraph() {
        testObject = createLinearGraph();
        DfsState dfsState = checkDepths(LINEAR_DEPTHS);
        assertTrue(dfsState.isAcyclic(),
                   "This Graph was not marked as acyclic");
    }

    /**
     * Test that a depth first search for a digraph fulfills its requirements.
     *
     */
    @Test
    @DisplayName("Long Loop Graph")
    public void longLoopGraph() {
        testObject = createLongLoopGraph();
        DfsState dfsState = checkDepths(LONG_LOOP_DEPTHS);
        assertFalse(dfsState.isAcyclic(),
                    "This Graph was marked as acyclic");
    }

    /**
     * Test that a depth first search for a digraph fulfills its requirements.
     *
     */
    @Test
    @DisplayName("Loop Graph")
    public void loopGraph() {
        testObject = createLoopGraph();
        DfsState dfsState = checkDepths(LOOP_DEPTHS);
        assertFalse(dfsState.isAcyclic(),
                    "This Graph was marked as acyclic");
    }

    /**
     * Test that a {@link NodeImpl} given no delegator responsed correctly.
     */
    @Test
    @DisplayName("No Delegator")
    public void noDelegator() {
        final MockDelegator delegator = null;
        assertThrows(NullPointerException.class,
                     () -> {
                         new NodeImpl(delegator);
                     });
    }

    /**
     * Test that a depth first search for a digraph fulfills its requirements.
     *
     */
    @Test
    @DisplayName("Reversed Asymmetric Diamond Graph")
    public void reversedAsymmetricDiamondGraph() {
        testObject = createReversedAsymmetricDiamondGraph();
        DfsState dfsState = checkDepths(REVERSED_ASYMMETRIC_DIAMOND_DEPTHS);
        assertTrue(dfsState.isAcyclic(),
                   "This Graph was not marked as acyclic");
    }
}
