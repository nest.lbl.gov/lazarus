package gov.lbl.nest.lazarus.process;

import java.util.List;

import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.lazarus.data.DataInputAssociationImpl;
import gov.lbl.nest.lazarus.data.DataOutputAssociationImpl;
import gov.lbl.nest.lazarus.data.InputOutputSpecificationImpl;
import gov.lbl.nest.lazarus.data.LoopCharacteristicsImpl;
import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataInputAssociation;
import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.DataOutputAssociation;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;
import gov.lbl.nest.lazarus.structure.InputOutputSpecification;
import gov.lbl.nest.lazarus.structure.LoopCharacteristics;
import gov.lbl.nest.lazarus.structure.Operation;
import gov.lbl.nest.lazarus.structure.ServiceTask;

/**
 * This class implements the {@link ServiceTask} interface for this package.
 *
 * @author patton
 */
public class ServiceTaskImpl extends
                             SuspendableActivityImpl implements
                             ServiceTask {

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param dataBody
     *            the {@link ServiceTaskBody} instance used by this object to
     *            execute its responsibilities.
     * @param ioSpecification
     *            the {@link InputOutputSpecificationImpl} instance that maps data
     *            in and out of this object.
     * @param dataInputAssociations
     *            the list of {@link DataInputAssociation} instances used to fill
     *            the {@link DataInput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param dataOutputAssociations
     *            the list of {@link DataOutputAssociation} instances used to fill
     *            the {@link DataOutput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param loopCharacteristics
     *            the {@link LoopCharacteristics} instance, if any, defining who the
     *            created {@link ServiceTask} should loop.
     * @param taskRunner
     *            the {@link Supplier} instance that with run the
     *            {@link FlowNodeTask} instances created by this object.
     * @param suspendable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     */
    ServiceTaskImpl(String name,
                    String identity,
                    ServiceTaskBody dataBody,
                    InputOutputSpecificationImpl ioSpecification,
                    List<? extends DataInputAssociationImpl> dataInputAssociations,
                    List<? extends DataOutputAssociationImpl> dataOutputAssociations,
                    LoopCharacteristicsImpl loopCharacteristics,
                    Supplier<FlowNodeTask> taskRunner,
                    Suspendable suspendable) {
        super(name,
              identity,
              dataBody,
              ioSpecification,
              dataInputAssociations,
              dataOutputAssociations,
              loopCharacteristics,
              taskRunner,
              suspendable);
    }

    @Override
    public final Operation getOperation() {
        return ((ServiceTaskBody) getDataOnlyActivityBody()).getOperation();
    }
}
