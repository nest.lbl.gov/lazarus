package gov.lbl.nest.lazarus.process;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathFunctionResolver;
import javax.xml.xpath.XPathVariableResolver;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import gov.lbl.nest.lazarus.control.ControlExpressionImpl;
import gov.lbl.nest.lazarus.control.ControlFlow;
import gov.lbl.nest.lazarus.structure.BaseElementImpl;
import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.xpath.BpmnNamespaceContext;

/**
 * This class implements the {@link ControlExpressionImpl} interface so that is
 * can evaluate an XPath statement.
 *
 * @author patton
 *
 * @param <T>
 *            the type to which this object evaluates.
 */
public class XPathExpressionEvaluation<T extends Object> extends
                                      BaseElementImpl implements
                                      ControlExpressionImpl<T> {

    /**
     * The XML context to use to evaluate the expression.
     */
    private static Document DUMMY_CONTEXT;

    static {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            DUMMY_CONTEXT = builder.parse(new InputSource(new StringReader("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><dummy/>")));
        } catch (ParserConfigurationException
                 | SAXException
                 | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The {@link XPathFunctionResolver} used by this object.
     */
    private final DataObjectResolver dataObjectResolver;

    /**
     * The {@link XPath} instance to by used by this Object
     */
    private final XPathExpression xPathExpression;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param contents
     *            the {@link String} instance from which to create the
     *            {@link Expression} instance.
     * @param namespaceContext
     *            the fallback {@link NamespaceContext} to use, if any.
     * @param functionResolver
     *            the fallback {@link XPathFunctionResolver} to use, if any.
     * @param variableResolver
     *            the fallback {@link XPathVariableResolver} to use, if any.
     *
     * @throws XPathExpressionException
     *             when the supplied contents can not be compiled.
     */
    public XPathExpressionEvaluation(String identity,
                                     String contents,
                                     NamespaceContext namespaceContext,
                                     XPathFunctionResolver functionResolver,
                                     XPathVariableResolver variableResolver) throws XPathExpressionException {
        super(identity);
        final XPath xPath = (XPathFactory.newInstance()).newXPath();
        xPath.setNamespaceContext(new BpmnNamespaceContext(namespaceContext));
        dataObjectResolver = new DataObjectResolver(functionResolver);
        xPath.setXPathFunctionResolver(dataObjectResolver);
        if (null != variableResolver) {
            xPath.setXPathVariableResolver(variableResolver);
        }
        xPathExpression = xPath.compile(contents);
    }

    @Override
    public T evaluate(ControlFlow controlFlow,
                      Class<T> clazz) throws ExpressionEvaluationException {
        return evaluate(controlFlow,
                        null,
                        clazz);
    }

    @Override
    public T evaluate(ControlFlow controlFlow,
                      Object[] arguments,
                      Class<T> clazz) throws ExpressionEvaluationException {
        try {
            synchronized (dataObjectResolver) {
                final ControlledDataScope scope = (ControlledDataScope) (controlFlow.getControlScope());
                dataObjectResolver.setDataScope(scope.getDataScope());
                DUMMY_CONTEXT.getDocumentElement();
                @SuppressWarnings("unchecked")
                final T result = (T) xPathExpression.evaluate(DUMMY_CONTEXT,
                                                              XPathConstants.BOOLEAN);
                return result;
            }
        } catch (XPathExpressionException e) {
            throw new ExpressionEvaluationException(e);
        }
    }

}
