/**
 * This package provides the interfaces and classes that implement a complete
 * BPMN Process.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * <li>{@link java.lang.reflect}</li>
 * <li>{@link java.util.concurrent}</li>
 * <li>{@link org.slf4j}</li>
 * <li>{@link gov.lbl.nest.common.suspension}</li>
 * <li>{@link gov.lbl.nest.common.tasks}</li>
 * <li>{@link gov.lbl.nest.lazarus.xpath}</li>
 * <li>{@link gov.lbl.nest.lazarus.java}</li>
 * <li>{@link gov.lbl.nest.lazarus.digraph}</li>
 * <li>{@link gov.lbl.nest.lazarus.structure}</li>
 * <li>{@link gov.lbl.nest.lazarus.control}</li>
 * <li>{@link gov.lbl.nest.lazarus.data}</li>
 * </ul>
 */
package gov.lbl.nest.lazarus.process;