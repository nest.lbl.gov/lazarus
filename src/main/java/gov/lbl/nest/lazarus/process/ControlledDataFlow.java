package gov.lbl.nest.lazarus.process;

import gov.lbl.nest.lazarus.control.ControlFlow;
import gov.lbl.nest.lazarus.data.DataCell;
import gov.lbl.nest.lazarus.data.DataOnlyActivityImpl;
import gov.lbl.nest.lazarus.structure.DataObjectException;

/**
 * This interface is used to link a {@link ControlFlow} instance to a
 * {@link DataCell} instance.
 *
 * @author patton
 */
public interface ControlledDataFlow extends
                                    ControlFlow {

    /**
     * Returns the {@link DataCell} instance being bound to this object.
     *
     * @param activity
     *            the {@link DataOnlyActivityImpl} instance with which this object
     *            exists.
     *
     * @return the {@link DataCell} instance being bound to this object.
     * 
     * @throws DataObjectException
     *             when existing persisted data output value can not be written
     *             successfully.
     */
    public DataCell createDataCell(DataOnlyActivityImpl activity) throws DataObjectException;
}
