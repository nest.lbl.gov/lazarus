package gov.lbl.nest.lazarus.process;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathFunction;
import javax.xml.xpath.XPathFunctionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import gov.lbl.nest.lazarus.data.DataScope;

/**
 * Thius class implements the "bpmn2::getDataObject" function as an
 * {@link XPathFunction} instance.
 *
 * @author patton
 */
public class GetDataObjectFunction implements
                                   XPathFunction {

    /**
     * The {@link DataScope} instance in which this object will be evaluated.
     */
    private final DataScope scope;

    /**
     * Creates an instance of this class.
     *
     * @param scope
     *            the {@link DataScope} instance in which this object will be
     *            evaluated.
     */
    GetDataObjectFunction(DataScope scope) {
        this.scope = scope;
    }

    @Override
    public Object evaluate(List<?> args) throws XPathFunctionException {
        try {
            final Object dataObjectValue = (scope.getValues()).get(args.get(0));
            final StringWriter buffer = new StringWriter();
            JAXBContext content = JAXBContext.newInstance(dataObjectValue.getClass());
            Marshaller marshaller = content.createMarshaller();
            marshaller.marshal(dataObjectValue,
                               buffer);
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document document = builder.parse(new InputSource(new StringReader(buffer.toString())));
            final Element result = document.getDocumentElement();
            return result;
        } catch (JAXBException
                 | SAXException
                 | IOException
                 | ParserConfigurationException e) {
            throw new XPathFunctionException(e);
        }
    }

}
