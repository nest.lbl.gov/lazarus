package gov.lbl.nest.lazarus.process;

import java.util.Map;

import gov.lbl.nest.lazarus.control.ControlOnlyExecutableProcess;
import gov.lbl.nest.lazarus.data.DataScope;
import gov.lbl.nest.lazarus.execution.ExecutableProcess;

/**
 * This class implements the {@link ExecutableProcess} interface for this
 * package by extends the {@link ControlOnlyExecutableProcess} class to include
 * data handling.
 *
 * @author patton
 */
public class ExecutableProcessImpl extends
                                   ControlOnlyExecutableProcess {

    /**
     * Creates an instance of this class.
     *
     * @param controlScope
     *            the {@link ControlledDataScope} instance that defines the
     *            configuration of this object.
     */
    public ExecutableProcessImpl(ControlledDataScope controlScope) {
        super(controlScope);
    }

    @Override
    public Map<String, Object> getValues() {
        final ControlledDataScope controlScope = (ControlledDataScope) getControlScope();
        final DataScope scope = controlScope.getDataScope();
        return scope.getValues();
    }

}
