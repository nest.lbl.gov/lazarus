package gov.lbl.nest.lazarus.process;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gov.lbl.nest.lazarus.control.ControlOnlyProcessImpl;
import gov.lbl.nest.lazarus.control.ControlScope;
import gov.lbl.nest.lazarus.control.FlowElementImpl;
import gov.lbl.nest.lazarus.control.StartEventImpl;
import gov.lbl.nest.lazarus.data.DataObjectImpl;
import gov.lbl.nest.lazarus.data.DataScope;
import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.execution.ProcessDefinition;
import gov.lbl.nest.lazarus.execution.TerminationListener;
import gov.lbl.nest.lazarus.management.ProcessManagerImpl;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.FlowElement;
import gov.lbl.nest.lazarus.structure.Interface;
import gov.lbl.nest.lazarus.structure.ItemDefinition;
import gov.lbl.nest.lazarus.structure.Message;
import gov.lbl.nest.lazarus.structure.Process;
import gov.lbl.nest.lazarus.structure.SuspendableActivity;

/**
 * This interface extends the {@link Process} interface for this package.
 *
 * @author patton
 */
public class ProcessDefinitionImpl extends
                                   ControlOnlyProcessImpl implements
                                   ProcessDefinition {

    /**
     * The {@link ControlledDataFactory} instance used to create
     * {@link ExecutableProcess} instances.
     */
    private ControlledDataFactory controlledDatafactory = ControlledDataFactory.getControlledDataFactory();

    /**
     * The collection of {@link DataObjectImpl} instances in this objects.
     */
    private final Collection<? extends DataObjectImpl> dataObjects;

    /**
     * The {@link TerminationListener} instance the should be used by an
     * {@link ExecutableProcess} instance if not is explicitly specified.
     */
    private TerminationListener defaultListener;

    /**
     * The {@link ProcessManagerImpl} instance with which to control this object.
     */
    private ProcessManagerImpl processManager;

    /**
     * The collection of {@link SuspendableActivity} instances contained in this
     * object.
     */
    private List<SuspendableActivityImpl> suspendableActivities = new ArrayList<>();

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param start
     *            the {@link StartEventImpl} instance that begins execution of this
     *            object.
     * @param flowElements
     *            the collection of {@link FlowElement} instances that make up this
     *            object.
     * @param dataObjects
     *            the collection of {@link DataObjectImpl} instances in this object.
     * @param processManager
     *            the {@link ProcessManagerImpl} instance, if any, with which to
     *            control this object.
     */
    ProcessDefinitionImpl(String name,
                          String identity,
                          StartEventImpl start,
                          Collection<? extends FlowElementImpl> flowElements,
                          Collection<? extends DataObjectImpl> dataObjects,
                          ProcessManagerImpl processManager) {
        super(name,
              identity,
              start,
              flowElements);
        for (FlowElementImpl element : flowElements) {
            if (element instanceof SuspendableActivity) {
                suspendableActivities.add((SuspendableActivityImpl) element);
            }
        }
        this.dataObjects = dataObjects;
        this.processManager = processManager;
        if (null != processManager) {
            this.processManager.setSuspendableActivities(suspendableActivities);
        }
    }

    @Override
    public ExecutableProcess createExecutableProcess(String label,
                                                     Integer priority,
                                                     Map<String, Object> values) throws DataObjectException {
        return createExecutableProcess(label,
                                       priority,
                                       values,
                                       defaultListener);
    }

    @Override
    public ExecutableProcess createExecutableProcess(String label,
                                                     Integer priority,
                                                     Map<String, Object> values,
                                                     TerminationListener listener) throws DataObjectException {
        final ControlledDataScope controlScope = controlledDatafactory.createControlledDataScope(this,
                                                                                                 label,
                                                                                                 priority,
                                                                                                 listener);
        final DataScope scope = controlScope.getDataScope();
        scope.setDataObjectValues(values);
        return new ExecutableProcessImpl(controlScope);
    }

    /**
     * Returns the collection of {@link DataObjectImpl} instances in this objects.
     *
     * @return the collection of {@link DataObjectImpl} instances in this objects.
     */
    public Collection<? extends DataObjectImpl> getDataObjects() {
        return dataObjects;
    }

    @Override
    public Set<? extends Interface> getInterfaces() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<? extends ItemDefinition> getItemDefinitions() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<? extends Message> getMessages() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ProcessManagerImpl getProcessManager() {
        return processManager;
    }

    /**
     * Returns the collection of {@link SuspendableActivity} instances contained in
     * this object.
     *
     * @return the collection of {@link SuspendableActivity} instances contained in
     *         this object.
     */
    public Collection<? extends SuspendableActivityImpl> getSuspendableActivities() {
        return suspendableActivities;
    }

    @Override
    public Collection<? extends ExecutableProcess> recoverExecutableProcesses() {
        final Collection<? extends ControlledDataScope> controlScopes = controlledDatafactory.recoverControlledDataScopes(this,
                                                                                                                          defaultListener);
        return recoverExecutableProcesses(controlScopes);
    }

    /**
     * Recovers the collection of {@link ExecutableProcess} instances derived from
     * the supplied collection of {@link ControlledDataScope} instances.
     *
     * @param controlScopes
     *            the collection of {@link ControlledDataScope} instance from which
     *            to create the returned {@link ExecutableProcess} instances.
     *
     * @return the collection of {@link ExecutableProcess} instances derived from
     *         the supplied collection of {@link ControlledDataScope} instances.
     */
    private Collection<? extends ExecutableProcess> recoverExecutableProcesses(final Collection<? extends ControlledDataScope> controlScopes) {
        final List<ExecutableProcessImpl> results = new ArrayList<>();
        for (ControlScope controlScope : controlScopes) {
            results.add(new ExecutableProcessImpl((ControlledDataScope) controlScope));
        }
        return results;
    }

    @Override
    public Collection<? extends ExecutableProcess> rescueExecutableProcesses() {
        final Collection<? extends ControlledDataScope> controlScopes = controlledDatafactory.rescueControlledDataScopes(this,
                                                                                                                         defaultListener);
        return recoverExecutableProcesses(controlScopes);
    }

    @Override
    public void setDefaultTerminationListener(TerminationListener listener) {
        defaultListener = listener;
    }

}
