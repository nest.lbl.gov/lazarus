package gov.lbl.nest.lazarus.process;

import java.util.List;

import gov.lbl.nest.lazarus.data.DataCell;
import gov.lbl.nest.lazarus.data.DataInputImpl;
import gov.lbl.nest.lazarus.data.DataInputSourceImpl;
import gov.lbl.nest.lazarus.data.ItemDefinitionImpl;
import gov.lbl.nest.lazarus.data.MultiInstanceDataCell;
import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.InputDataItem;
import gov.lbl.nest.lazarus.structure.ItemDefinition;

/**
 * This class extends the {@link DataInputImpl} class in order to act as the
 * input of a branch of a multiple instance loop.
 *
 * @author patton
 *
 */
public class InputDataItemImpl extends
                               DataInputImpl implements
                               InputDataItem {

    /**
     * The collection of {@link DataInput} instances that will be used as inputs to
     * the loop instances.
     */
    private final DataInputSourceImpl loopDataInput;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param loopDataInput
     *            the collection of {@link DataInput} instances that will be used as
     *            inputs to the loop instances.
     * @param itemSubject
     *            the {@link ItemDefinition} whose structure defines the type of the
     *            item this object provides.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type. If <code>null</code> will use value from itemSubject.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public InputDataItemImpl(String name,
                             String identity,
                             DataInputSourceImpl loopDataInput,
                             ItemDefinitionImpl itemSubject,
                             Boolean collection) throws ClassNotFoundException {
        super(name,
              identity,
              itemSubject,
              collection);
        this.loopDataInput = loopDataInput;
    }

    @Override
    public Object getValue(DataCell cell) throws DataObjectException,
                                          ExpressionEvaluationException {
        List<?> collection = (List<?>) loopDataInput.getValueAsSource(cell);
        return collection.get(((MultiInstanceDataCell) cell).getIndex());
    }

}
