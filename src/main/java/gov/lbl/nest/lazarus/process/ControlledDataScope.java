package gov.lbl.nest.lazarus.process;

import gov.lbl.nest.lazarus.control.ControlOnlyProcessImpl;
import gov.lbl.nest.lazarus.control.ControlScope;
import gov.lbl.nest.lazarus.data.DataScope;
import gov.lbl.nest.lazarus.execution.TerminationListener;

/**
 * This interface is used to link a {@link ControlScope} instance to a
 * {@link DataScope} instance.
 *
 * @author patton
 */
public abstract class ControlledDataScope extends
                                          ControlScope {

    /**
     * The {@link DataScope} instance used by this object.
     */
    private DataScope scope;

    /**
     * Creates an instance of this class.
     */
    protected ControlledDataScope() {
        this(null,
             null,
             null,
             null,
             null);
    }

    /**
     * Creates an instance of this object.
     * 
     * @param label
     *            the {@link String} instance describing this object.
     * @param priority
     *            the {@link Integer} instance containing the priority this object.
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance of the workflow to be
     *            executed.
     * @param listener
     *            the {@link TerminationListener} instance, if any, to callback when
     *            this object has terminated.
     * @param scope
     *            the {@link DataScope} instance used by this object.
     */
    protected ControlledDataScope(String label,
                                  Integer priority,
                                  ControlOnlyProcessImpl process,
                                  TerminationListener listener,
                                  DataScope scope) {
        super(label,
              priority,
              process,
              listener);
        this.scope = scope;
    }

    /**
     * Returns the {@link DataScope} instance used by this object.
     *
     * @return the {@link DataScope} instance used by this object.
     */
    public final DataScope getDataScope() {
        return scope;
    }

    /**
     * Sets the {@link DataScope} instance used by this object.
     * 
     * @param scope
     *            the {@link DataScope} instance used by this object.
     */
    protected final void setDataScope(DataScope scope) {
        this.scope = scope;
    }
}
