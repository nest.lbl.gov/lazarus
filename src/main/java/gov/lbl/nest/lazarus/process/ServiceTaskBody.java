package gov.lbl.nest.lazarus.process;

import java.util.Map;

import gov.lbl.nest.lazarus.data.DataOnlyActivityBody;
import gov.lbl.nest.lazarus.data.OperationImpl;
import gov.lbl.nest.lazarus.structure.Operation;
import gov.lbl.nest.lazarus.structure.ServiceTask;

/**
 * This class implements the {@link DataOnlyActivityBody} interface for
 * {@link ServiceTask} instances.
 *
 * @author patton
 */
public class ServiceTaskBody implements
                             DataOnlyActivityBody {

    /**
     * The {@link Operation} instance that this object will call to execute its
     * responsibilities.
     */
    private final OperationImpl operation;

    /**
     * Creates an instance of this class.
     *
     * @param operation
     *            the {@link Operation} instance that this object will call to
     *            execute its responsibilities.
     */
    public ServiceTaskBody(OperationImpl operation) {
        if (null == operation) {
            throw new NullPointerException("operation can not be null");
        }
        this.operation = operation;
    }

    @Override
    public void execute(Map<String, Object> inputs,
                        Map<String, Object> outputs) {
        if (1 != inputs.size()) {
            throw new IllegalStateException("One and only one DataInput is allowed in a ServiceTask InputSet");
        }
        if (outputs.size() > 1) {
            throw new IllegalStateException("Only zero or one DataOutputs are allowed in a ServiceTask OutputSet");
        }
        if (operation.isVoid()) {
            operation.execute(inputs);
        } else {
            Object value = operation.execute(inputs);
            if (1 == outputs.size()) {
                outputs.put(((outputs.keySet()).iterator()).next(),
                            value);
            }
        }
    }

    /**
     * Returns the {@link Operation} instance that this object will call to execute
     * its responsibilities.
     *
     * @return the {@link Operation} instance that this object will call to execute
     *         its responsibilities.
     */
    OperationImpl getOperation() {
        return operation;
    }
}
