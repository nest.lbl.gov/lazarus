package gov.lbl.nest.lazarus.process;

import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.tasks.InspectorFactory;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.lazarus.control.BodyCompletion;
import gov.lbl.nest.lazarus.control.ControlFlow;
import gov.lbl.nest.lazarus.control.ControlFlowException;
import gov.lbl.nest.lazarus.control.FlowNodeInspectorFactory;
import gov.lbl.nest.lazarus.control.FlowNodeInspectorImpl;
import gov.lbl.nest.lazarus.data.DataCell;
import gov.lbl.nest.lazarus.data.DataOnlyActivityImpl;
import gov.lbl.nest.lazarus.data.FlowNodeTaskImpl;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.FlowNodeInspector;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;

/**
 * This class extends the {@link ControlledDataActivityBody} interface in order
 * to run a single instances of an {@link DataOnlyActivityImpl} implementation.
 *
 * @author patton
 */
public class SingleInstanceBody extends
                                ActivityBody {

    /**
     * This class executes the responsibilities of this object.
     *
     * @author patton
     */
    static class SingleInstanceTask extends
                                    FlowNodeTaskImpl {

        /**
         * The {@link BodyCompletion} instance to call back to once execution has been
         * completed.
         */
        private final BodyCompletion callback;

        /**
         * The {@link ControlFlow} instance in which to execute those responsibilities.
         */
        private final ControlFlow control;

        /**
         * Creates an instance of this class.
         *
         * @param activity
         *            the {@link DataOnlyActivityImpl} instance within which this object
         *            is executing.
         * @param cell
         *            the {@link DataCell} instance within which this object is
         *            executing.
         * @param factory
         *            the {@link InspectorFactory} used to create new
         *            {@link FlowNodeInspectorImpl} instances.
         * @param suspendable
         *            the {@link Suspendable} instance used by this object to know
         *            whether it is active or not.
         * @param rank
         *            the "rank" of this element, where object with a higher rank are
         *            preferred.
         * @param control
         *            the {@link ControlFlow} instance in which to execute those
         *            responsibilities.
         * @param callback
         *            the {@link BodyCompletion} instance to call back to once execution
         *            has been completed.
         */
        SingleInstanceTask(DataOnlyActivityImpl activity,
                           DataCell cell,
                           InspectorFactory<FlowNodeInspector> factory,
                           Suspendable suspendable,
                           Integer rank,
                           ControlFlow control,
                           BodyCompletion callback) {
            super(activity,
                  cell,
                  factory,
                  suspendable,
                  rank,
                  control.getPriority());
            this.callback = callback;
            this.control = control;

        }

        @Override
        public void run() {
            try {
                execute();
            } catch (Throwable e) {
                control.handleThrowable(e);
                return;
            }
            callback.bodyExecutionCompleted(control);
        }

    }

    /**
     * Creates an instance of this class.
     *
     * @param flowNode
     *            the name of the {@link ActivityImpl} instance in this this object
     *            is executing.
     * @param taskRunner
     *            the {@link Supplier} instance that with run the
     *            {@link FlowNodeTask} instances created by this object.
     * @param suspendable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     */
    public SingleInstanceBody(String flowNode,
                              Supplier<FlowNodeTask> taskRunner,
                              Suspendable suspendable) {
        super(flowNode,
              taskRunner,
              suspendable);
    }

    @Override
    public void execute(ControlFlow control,
                        BodyCompletion callback) {
        final DataCell cell;
        try {
            cell = ((ControlledDataFlow) control).createDataCell(getDataOnlyActivityImpl());
        } catch (DataObjectException e) {
            throw new ControlFlowException(e);
        }
        final String label = (control.getControlScope()).getLabel();
        final SingleInstanceTask task = new SingleInstanceTask(getDataOnlyActivityImpl(),
                                                               cell,
                                                               new FlowNodeInspectorFactory(label,
                                                                                            getFlowNodeExecutions()),
                                                               getSuspendable(),
                                                               getRank(),
                                                               control,
                                                               callback);
        final Supplier<FlowNodeTask> taskRunner = getTaskRunner();
        if (null == taskRunner) {
            task.run();
        } else {
            taskRunner.add(task);
        }
    }

}
