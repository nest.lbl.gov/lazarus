package gov.lbl.nest.lazarus.process;

import gov.lbl.nest.lazarus.control.ControlOnlyActivityBody;
import gov.lbl.nest.lazarus.data.DataOnlyActivityImpl;

/**
 * This class implements the {@link ControlOnlyActivityBody} interface.
 *
 * @author patton
 *
 */
public abstract class ControlledDataActivityBody implements
                                                 ControlOnlyActivityBody {

    /**
     * The {@link DataOnlyActivityImpl} instance that is using this object.
     */
    private DataOnlyActivityImpl activity;

    /**
     * Returns the {@link DataOnlyActivityImpl} instance this object is using.
     *
     * @return the {@link DataOnlyActivityImpl} instance this object is using.
     */
    DataOnlyActivityImpl getDataOnlyActivityImpl() {
        return activity;
    }

    /**
     * Set the {@link DataOnlyActivityImpl} instance this object is using.
     *
     * @param activity
     *            the {@link DataOnlyActivityImpl} instance this object is using.
     */
    void setDataOnlyActivityImpl(DataOnlyActivityImpl activity) {
        this.activity = activity;
    }
}
