package gov.lbl.nest.lazarus.process;

import java.util.List;

import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.lazarus.control.ControlOnlyActivityImpl;
import gov.lbl.nest.lazarus.control.FlowNodeImpl;
import gov.lbl.nest.lazarus.data.DataInputAssociationImpl;
import gov.lbl.nest.lazarus.data.DataOnlyActivityBody;
import gov.lbl.nest.lazarus.data.DataOnlyActivityImpl;
import gov.lbl.nest.lazarus.data.DataOutputAssociationImpl;
import gov.lbl.nest.lazarus.data.InputOutputSpecificationImpl;
import gov.lbl.nest.lazarus.data.MultiInstanceLoopCharacteristicsImpl;
import gov.lbl.nest.lazarus.execution.FlowNodeExecutions;
import gov.lbl.nest.lazarus.structure.Activity;
import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataInputAssociation;
import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.DataOutputAssociation;
import gov.lbl.nest.lazarus.structure.FlowNodeInspector;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;
import gov.lbl.nest.lazarus.structure.InputOutputSpecification;
import gov.lbl.nest.lazarus.structure.LoopCharacteristics;
import gov.lbl.nest.lazarus.structure.ServiceTask;

/**
 * This class implements the {@link Activity} interface for this package.
 *
 * @author patton
 */
public abstract class ActivityImpl extends
                                   ControlOnlyActivityImpl implements
                                   Activity,
                                   FlowNodeExecutions {

    /**
     * Creates the appropriate {@link ControlledDataActivityBody} body instance.
     *
     * @param name
     *            the name of this {@link FlowNodeImpl} instance.
     * @param loopCharacteristics
     *            the {@link LoopCharacteristics} instance, if any, defining who the
     *            created {@link ServiceTask} should loop.
     * @param taskRunner
     *            the {@link Supplier} instance that with run the
     *            {@link FlowNodeTask} instances created by this object.
     * @param suspenable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     *
     * @return the create {@link ControlledDataActivityBody} instance.
     */
    private static ControlledDataActivityBody createControlledDataActivityBody(String name,
                                                                               LoopCharacteristics loopCharacteristics,
                                                                               Supplier<FlowNodeTask> taskRunner,
                                                                               Suspendable suspenable) {
        if (null == loopCharacteristics) {
            return new SingleInstanceBody(name,
                                          taskRunner,
                                          suspenable);
        }
        if (loopCharacteristics instanceof MultiInstanceLoopCharacteristicsImpl) {
            return new MultiInstanceBody(name,
                                         (MultiInstanceLoopCharacteristicsImpl) loopCharacteristics,
                                         taskRunner,
                                         suspenable);
        }
        throw new UnsupportedOperationException();
    }

    /**
     * The {@link DataOnlyActivityImpl} used by this object to manage data.
     */
    private final DataOnlyActivityImpl dataOnly;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param dataBody
     *            the {@link DataOnlyActivityBody} instance used by this object to
     *            execute its responsibilities.
     * @param ioSpecification
     *            the {@link InputOutputSpecificationImpl} instance that maps data
     *            in and out of this object.
     * @param dataInputAssociations
     *            the list of {@link DataInputAssociation} instances used to fill
     *            the {@link DataInput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param dataOutputAssociations
     *            the list of {@link DataOutputAssociation} instances used to fill
     *            the {@link DataOutput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param loopCharacteristics
     *            the {@link LoopCharacteristics} instance, if any, defining who the
     *            created {@link ServiceTask} should loop.
     * @param taskRunner
     *            the {@link Supplier} instance that with run the
     *            {@link FlowNodeTask} instances created by this object.
     * @param suspendable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     */
    protected ActivityImpl(String name,
                           String identity,
                           DataOnlyActivityBody dataBody,
                           InputOutputSpecificationImpl ioSpecification,
                           List<? extends DataInputAssociationImpl> dataInputAssociations,
                           List<? extends DataOutputAssociationImpl> dataOutputAssociations,
                           LoopCharacteristics loopCharacteristics,
                           Supplier<FlowNodeTask> taskRunner,
                           Suspendable suspendable) {
        super(name,
              identity,
              createControlledDataActivityBody(name,
                                               loopCharacteristics,
                                               taskRunner,
                                               suspendable));
        dataOnly = new DataOnlyActivityImpl(identity,
                                            dataBody,
                                            ioSpecification,
                                            dataInputAssociations,
                                            dataOutputAssociations);
        final ActivityBody controlBody = (ActivityBody) getControlOnlyActivityBody();
        controlBody.setDataOnlyActivityImpl(dataOnly);
    }

    @Override
    public final List<? extends DataInputAssociationImpl> getDataInputAssociations() {
        return dataOnly.getDataInputAssociations();
    }

    /**
     * Returns the {@link DataOnlyActivityBody} instance that carries out this
     * object's responsibilities.
     *
     * @return the {@link DataOnlyActivityBody} instance that carries out this
     *         object's responsibilities.
     */
    DataOnlyActivityBody getDataOnlyActivityBody() {
        return dataOnly.getBody();
    }

    @Override
    public final List<? extends DataOutputAssociationImpl> getDataOutputAssociations() {
        return dataOnly.getDataOutputAssociations();
    }

    @Override
    public int getExecutingCount() {
        return getFlowNodeExecutions().getExecutingCount();
    }

    /**
     * Returns the {@link FlowNodeExecutions} instance used by this object.
     *
     * @return
     */
    private FlowNodeExecutions getFlowNodeExecutions() {
        return ((ActivityBody) getControlOnlyActivityBody()).getFlowNodeExecutions();
    }

    @Override
    public List<? extends FlowNodeInspector> getInspectors() {
        return getFlowNodeExecutions().getInspectors();
    }

    @Override
    public final InputOutputSpecification getIoSpecification() {
        return dataOnly.getIoSpecification();
    }

    @Override
    public int getPendingCount() {
        return getFlowNodeExecutions().getPendingCount();
    }

    @Override
    public int getTotalCount() {
        return getFlowNodeExecutions().getTotalCount();
    }
}
