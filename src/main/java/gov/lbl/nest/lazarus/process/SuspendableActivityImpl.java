package gov.lbl.nest.lazarus.process;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.suspension.SuspendableCollection;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.lazarus.data.DataInputAssociationImpl;
import gov.lbl.nest.lazarus.data.DataOnlyActivityBody;
import gov.lbl.nest.lazarus.data.DataOutputAssociationImpl;
import gov.lbl.nest.lazarus.data.InputOutputSpecificationImpl;
import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataInputAssociation;
import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.DataOutputAssociation;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;
import gov.lbl.nest.lazarus.structure.InputOutputSpecification;
import gov.lbl.nest.lazarus.structure.LoopCharacteristics;
import gov.lbl.nest.lazarus.structure.ServiceTask;
import gov.lbl.nest.lazarus.structure.SuspendableActivity;

/**
 * This class implements the {@link SuspendableActivity} interface by extending
 * the {@link ActivityImpl} class and adding the ability for it to be suspended.
 *
 * @author patton
 */
public abstract class SuspendableActivityImpl extends
                                              ActivityImpl implements
                                              SuspendableActivity {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SuspendableActivityImpl.class);

    /**
     * The {@link SuspendableCollection} instance, if any, to which this Object
     * belongs.
     */
    private SuspendableCollection collection;

    /**
     * <code>true</code> when the object supervising this one is active.
     */
    private final Suspendable suspendable;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param dataBody
     *            the {@link DataOnlyActivityBody} instance used by this object to
     *            execute its responsibilities.
     * @param ioSpecification
     *            the {@link InputOutputSpecificationImpl} instance that maps data
     *            in and out of this object.
     * @param dataInputAssociations
     *            the list of {@link DataInputAssociation} instances used to fill
     *            the {@link DataInput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param dataOutputAssociations
     *            the list of {@link DataOutputAssociation} instances used to fill
     *            the {@link DataOutput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param loopCharacteristics
     *            the {@link LoopCharacteristics} instance, if any, defining who the
     *            created {@link ServiceTask} should loop.
     * @param taskRunner
     *            the {@link Supplier} instance that with run the
     *            {@link FlowNodeTask} instances created by this object.
     * @param suspendable
     *            the {@link AtomicBoolean} that is <code>true</code> when this
     *            object is active.
     */
    protected SuspendableActivityImpl(String name,
                                      String identity,
                                      DataOnlyActivityBody dataBody,
                                      InputOutputSpecificationImpl ioSpecification,
                                      List<? extends DataInputAssociationImpl> dataInputAssociations,
                                      List<? extends DataOutputAssociationImpl> dataOutputAssociations,
                                      LoopCharacteristics loopCharacteristics,
                                      Supplier<FlowNodeTask> taskRunner,
                                      Suspendable suspendable) {
        super(name,
              identity,
              dataBody,
              ioSpecification,
              dataInputAssociations,
              dataOutputAssociations,
              loopCharacteristics,
              taskRunner,
              suspendable);
        this.suspendable = suspendable;
    }

    @Override
    public boolean isSuspended() throws InitializingException {
        return suspendable.isSuspended();
    }

    @Override
    public void setCollection(SuspendableCollection collection) {
        this.collection = collection;
    }

    @Override
    public boolean setSuspended(boolean suspend) throws InitializingException {
        // If already in the correct state, then no change.
        if (suspendable.isSuspended() == suspend) {
            return false;
        }
        synchronized (suspendable) {
            /*
             * Reconfirm after obtaining lock. If already in the correct state, then no
             * change.
             */
            if (suspendable.isSuspended() == suspend) {
                return false;
            }

            if (suspend) {
                LOG.info("Suspending activity \"" + getName()
                         + "\"");
            } else {
                LOG.info("Resumed activity \"" + getName()
                         + "\"");
            }
            suspendable.setSuspended(suspend);
            if (null != collection) {
                collection.update(this);
            }
            return true;
        }
    }

}
