package gov.lbl.nest.lazarus.process;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathFunction;
import javax.xml.xpath.XPathFunctionResolver;

import gov.lbl.nest.lazarus.data.DataScope;
import gov.lbl.nest.lazarus.xpath.BpmnNamespaceContext;

/**
 * This class implements the {@link XPathFunctionResolver} interface to
 * implement the "bpmn2:getDataObject" function.
 *
 * @author patton
 */
public class DataObjectResolver implements
                                XPathFunctionResolver {

    // public static final member data

    /**
     * The name of the function that returns a named data object.
     */
    public static final QName BPMN_GET_DATA_OBJECT = new QName(BpmnNamespaceContext.BPMN_NS,
                                                               "getDataObject");

    /**
     * The {@link DataScope} instance in which this object will be evaluated.
     */
    private DataScope scope;

    /**
     * The {@link XPathFunctionResolver} to use if this one does not resolve a
     * function.
     */
    private final XPathFunctionResolver fallback;

    /**
     * Creates an instance of this class.
     *
     * @param fallback
     *            the {@link XPathFunctionResolver} to use if this one does not
     *            resolve a function.
     */
    public DataObjectResolver(XPathFunctionResolver fallback) {
        this.fallback = fallback;
    }

    @Override
    public XPathFunction resolveFunction(QName functionName,
                                         int arity) {
        if (null == functionName) {
            throw new NullPointerException("Function name must not be null");
        }
        if (BPMN_GET_DATA_OBJECT.equals(functionName) && 1 == arity) {
            return new GetDataObjectFunction(scope);
        }
        if (null == fallback) {
            return null;
        }
        return fallback.resolveFunction(functionName,
                                        arity);
    }

    /**
     * Sets the {@link DataScope} instance in which this object will be evaluated.
     *
     * @param scope
     *            the {@link DataScope} instance in which this object will be
     *            evaluated.
     */
    public void setDataScope(DataScope scope) {
        this.scope = scope;
    }
}
