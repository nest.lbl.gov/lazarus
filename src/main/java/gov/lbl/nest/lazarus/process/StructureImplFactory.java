package gov.lbl.nest.lazarus.process;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFunctionResolver;
import javax.xml.xpath.XPathVariableResolver;

import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.lazarus.control.FlowElementImpl;
import gov.lbl.nest.lazarus.control.StartEventImpl;
import gov.lbl.nest.lazarus.data.DataInputAssociationImpl;
import gov.lbl.nest.lazarus.data.DataInputSourceImpl;
import gov.lbl.nest.lazarus.data.DataObjectImpl;
import gov.lbl.nest.lazarus.data.DataOutputAssociationImpl;
import gov.lbl.nest.lazarus.data.InputOutputSpecificationImpl;
import gov.lbl.nest.lazarus.data.ItemDefinitionImpl;
import gov.lbl.nest.lazarus.data.LoopCharacteristicsImpl;
import gov.lbl.nest.lazarus.data.MessageImpl;
import gov.lbl.nest.lazarus.data.OperationImpl;
import gov.lbl.nest.lazarus.java.JavaInstantiationException;
import gov.lbl.nest.lazarus.java.JavaInterface;
import gov.lbl.nest.lazarus.java.JavaOperation;
import gov.lbl.nest.lazarus.management.ProcessManager;
import gov.lbl.nest.lazarus.management.ProcessManagerImpl;
import gov.lbl.nest.lazarus.structure.DataInputAssociation;
import gov.lbl.nest.lazarus.structure.DataInputSource;
import gov.lbl.nest.lazarus.structure.DataInputTarget;
import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.DataOutputAssociation;
import gov.lbl.nest.lazarus.structure.DataOutputSource;
import gov.lbl.nest.lazarus.structure.DataOutputTarget;
import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.FlowElement;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;
import gov.lbl.nest.lazarus.structure.InputOutputSpecification;
import gov.lbl.nest.lazarus.structure.Interface;
import gov.lbl.nest.lazarus.structure.ItemDefinition;
import gov.lbl.nest.lazarus.structure.LoopCharacteristics;
import gov.lbl.nest.lazarus.structure.Message;
import gov.lbl.nest.lazarus.structure.Operation;
import gov.lbl.nest.lazarus.structure.Process;
import gov.lbl.nest.lazarus.structure.ServiceTask;
import gov.lbl.nest.lazarus.structure.StartEvent;
import gov.lbl.nest.lazarus.structure.StructureFactory;

/**
 * This class implements the parts of the {@link StructureFactory} interface
 * that exist in this package.
 *
 * @author patton
 */
public class StructureImplFactory extends
                                  gov.lbl.nest.lazarus.data.StructureImplFactory {

    /**
     * The URI for the XPath language.
     */
    private static final String XPATH_URI = "http://www.w3.org/1999/XPath";

    /**
     * The {@link NamespaceContext} instance if this object does not provide a
     * suitable one.
     */
    private NamespaceContext fallbackNamespaceContext;

    /**
     * The {@link XPathFunctionResolver} instance if this object does not provide a
     * suitable one.
     */
    private XPathFunctionResolver fallbackPathFunctionResolver;

    /**
     * The {@link XPathVariableResolver} instance if this object does not provide a
     * suitable one.
     */
    private XPathVariableResolver fallbackPathVariableResolver;

    @Override
    public <T> Expression<T> createExpression(String identity,
                                              String contents,
                                              String language,
                                              Class<T> clazz) {
        if (null == language || !language.equals(XPATH_URI)) {
            throw new UnsupportedOperationException("Current the only supported language is \"" + XPATH_URI
                                                    + "\"");
        }
        try {
            return new XPathExpressionEvaluation<>(identity,
                                                   contents,
                                                   fallbackNamespaceContext,
                                                   fallbackPathFunctionResolver,
                                                   fallbackPathVariableResolver);
        } catch (XPathExpressionException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public InputDataItemImpl createInputDataItem(String name,
                                                 String identity,
                                                 DataInputSource loopDataInput,
                                                 ItemDefinition itemSubject,
                                                 Boolean collection) throws ClassNotFoundException {
        return new InputDataItemImpl(name,
                                     identity,
                                     (DataInputSourceImpl) loopDataInput,
                                     (ItemDefinitionImpl) itemSubject,
                                     collection);
    }

    @Override
    public Interface createInterface(String name,
                                     String identity,
                                     Collection<? extends Operation> operations,
                                     String implementationRef) throws ClassNotFoundException,
                                                               NoSuchMethodException {
        final List<OperationImpl> ops = new ArrayList<>();
        for (Operation operation : operations) {
            ops.add((OperationImpl) operation);
        }
        try {
            return new JavaInterface(name,
                                     identity,
                                     ops,
                                     implementationRef);
        } catch (JavaInstantiationException e) {
            throw new ClassNotFoundException(null,
                                             e.getCause());
        }
    }

    @Override
    public Operation createOperation(String name,
                                     String identity,
                                     String implementationRef,
                                     Message inMessageRef,
                                     Message outMessageRef) throws NoSuchMethodException {
        try {
            return new JavaOperation(name,
                                     identity,
                                     implementationRef,
                                     (MessageImpl) inMessageRef,
                                     (MessageImpl) outMessageRef);
        } catch (JavaInstantiationException e) {
            throw (NoSuchMethodException) e.getCause();
        }
    }

    @Override
    public Process createProcess(String name,
                                 String identity,
                                 StartEvent start,
                                 Collection<? extends FlowElement> flowElements,
                                 Collection<? extends DataObject> dataObjects,
                                 ProcessManager processManager) {
        final List<FlowElementImpl> elements = new ArrayList<>();
        for (FlowElement flowElement : flowElements) {
            elements.add((FlowElementImpl) flowElement);
        }
        final List<DataObjectImpl> objs = new ArrayList<>();
        for (DataObject dataObject : dataObjects) {
            objs.add((DataObjectImpl) dataObject);
        }
        return new ProcessDefinitionImpl(name,
                                         identity,
                                         (StartEventImpl) start,
                                         elements,
                                         objs,
                                         (ProcessManagerImpl) processManager);
    }

    @Override
    public ServiceTask createServiceTask(String name,
                                         String identity,
                                         Operation operation,
                                         InputOutputSpecification ioSpecification,
                                         Collection<? extends DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget>> inputAssocs,
                                         Collection<? extends DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget>> outputAssocs,
                                         LoopCharacteristics loopCharacteristics,
                                         Supplier<FlowNodeTask> supplier,
                                         Suspendable suspendable) {
        final ServiceTaskBody dataBody = new ServiceTaskBody((OperationImpl) operation);

        final List<DataInputAssociationImpl> inputs = new ArrayList<>();
        for (DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget> inputAssoc : inputAssocs) {
            inputs.add((DataInputAssociationImpl) inputAssoc);
        }

        final List<DataOutputAssociationImpl> outputs = new ArrayList<>();
        for (DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget> outputAssoc : outputAssocs) {
            outputs.add((DataOutputAssociationImpl) outputAssoc);
        }

        return new ServiceTaskImpl(name,
                                   identity,
                                   dataBody,
                                   (InputOutputSpecificationImpl) ioSpecification,
                                   inputs,
                                   outputs,
                                   (LoopCharacteristicsImpl) loopCharacteristics,
                                   supplier,
                                   suspendable);
    }

    @Override
    public void setFallbackNamespaceContext(NamespaceContext context) {
        fallbackNamespaceContext = context;
    }

    @Override
    public void setFallbackXPathFunctionResolver(XPathFunctionResolver resolver) {
        fallbackPathFunctionResolver = resolver;
    }

    @Override
    public void setFallbackXPathVariableResolver(XPathVariableResolver resolver) {
        fallbackPathVariableResolver = resolver;
    }
}
