package gov.lbl.nest.lazarus.process;

import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.lazarus.control.FlowNodeExecutionsImpl;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;

/**
 * This class extends the {@link ControlledDataActivityBody} to include
 * information for {@link ActivityImpl} instances.
 *
 * @author patton
 */
public abstract class ActivityBody extends
                                   ControlledDataActivityBody {

    /**
     * The {@link FlowNodeExecutionsImpl} instance used by this object.
     */
    private final FlowNodeExecutionsImpl counts;

    /**
     * The "rank" of this element, where object with a higher rank are preferred.
     */
    private Integer rank;

    /**
     * The {@link Suspendable} instance used by this object to know whether it is
     * active or not.
     */
    private final Suspendable suspendable;

    /**
     * The {@link Supplier} instance that with run the {@link FlowNodeTask}
     * instances created by this object.
     */
    private Supplier<FlowNodeTask> taskRunner;

    /**
     * Creates an instance of this class.
     *
     * @param flowNode
     *            the name of the {@link ActivityImpl} instance in this this object
     *            is executing.
     * @param taskRunner
     *            the {@link Supplier} instance that with run the
     *            {@link FlowNodeTask} instances created by this object.
     * @param suspendable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     */
    protected ActivityBody(String flowNode,
                           Supplier<FlowNodeTask> taskRunner,
                           Suspendable suspendable) {
        this.counts = new FlowNodeExecutionsImpl(flowNode);
        this.suspendable = suspendable;
        this.taskRunner = taskRunner;
    }

    /**
     * Returns the {@link FlowNodeExecutionsImpl} instance used by this object.
     *
     * @return the {@link FlowNodeExecutionsImpl} instance used by this object.
     */
    protected final FlowNodeExecutionsImpl getFlowNodeExecutions() {
        return counts;
    }

    /**
     * Returns the "rank" of this element, where object with a higher rank are
     * preferred.
     *
     * @return the "rank" of this element, where object with a higher rank are
     *         preferred.
     */
    protected Integer getRank() {
        return rank;
    }

    /**
     * Returns the {@link Suspendable} instance used by this object to know whether
     * it is active or not.
     *
     * @return the {@link Suspendable} instance used by this object to know whether
     *         it is active or not.
     */
    protected final Suspendable getSuspendable() {
        return suspendable;
    }

    /**
     * Return the {@link Supplier} instance that with run the {@link FlowNodeTask}
     * instances created by this object.
     *
     * @return the {@link Supplier} instance that with run the {@link FlowNodeTask}
     *         instances created by this object.
     */
    public Supplier<FlowNodeTask> getTaskRunner() {
        return taskRunner;
    }

    @Override
    public void setRank(Integer rank) {
        this.rank = rank;
    }
}
