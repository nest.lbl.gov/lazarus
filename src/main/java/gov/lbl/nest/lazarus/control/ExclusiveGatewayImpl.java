package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.structure.ExclusiveGateway;

/**
 * This class implements the Exclusive Gateway logic for a workflow.
 *
 * @author patton
 */
public final class ExclusiveGatewayImpl extends
                                        GatewayImpl implements
                                        ExclusiveGateway {

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param direction
     *            the {@link GatewayDirection} instance of this object.
     */
    @SuppressWarnings("javadoc")
    public ExclusiveGatewayImpl(String name,
                                String identity,
                                GatewayDirection direction) {
        super(name,
              identity,
              new ExclusiveIncomingFlows(null),
              new ExclusiveOutgoingFlows(outgoingIdentity(identity)),
              direction);
        setInboundTarget();
    }

    @Override
    public void exercise(ControlFlow control) {
        toOutgoing(control);
    }

    @Override
    public SequenceFlowImpl getDefault() {
        return getOutgoingSequenceFlows().getDefault();
    }
}
