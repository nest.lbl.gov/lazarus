package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.structure.ControlOnlyActivity;

/**
 * This class implements the {@link ControlOnlyActivity} interface for this
 * package.
 *
 * @author patton
 */
public class ControlOnlyActivityImpl extends
                                     FlowNodeImpl implements
                                     ControlOnlyActivity,
                                     BodyCompletion {

    private final ControlOnlyActivityBody body;

    /**
     * Create an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param body
     *            the {@link ControlOnlyActivityBody} instance used by this object
     *            to execute its responsibilities.
     */
    public ControlOnlyActivityImpl(String name,
                                   String identity,
                                   ControlOnlyActivityBody body) {
        super(name,
              identity,
              new ExclusiveIncomingFlows(null),
              new ParallelOutgoingFlows(outgoingIdentity(identity)));
        setInboundTarget();
        this.body = body;
    }

    @Override
    public final void assignRank() {
        body.setRank(getDepth());
    }

    @Override
    public final void bodyExecutionCompleted(ControlFlow control) {
        toOutgoing(control);
    }

    @Override
    public final void exercise(ControlFlow control) {
        body.execute(control,
                     this);
    }

    /**
     * Returns the {@link ControlOnlyActivityBody} instance used by this object to
     * execute its responsibilities.
     *
     * @return the {@link ControlOnlyActivityBody} instance used by this object to
     *         execute its responsibilities.
     */
    protected final ControlOnlyActivityBody getControlOnlyActivityBody() {
        return body;
    }
}
