package gov.lbl.nest.lazarus.control;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.lazarus.structure.ControlOnlyProcess;
import gov.lbl.nest.lazarus.structure.FlowElement;
import gov.lbl.nest.lazarus.structure.NamedElement;
import gov.lbl.nest.lazarus.structure.Process;

/**
 * This class implements the {@link Process} interface for this package.
 *
 * @author patton
 *
 */
public class ControlOnlyProcessImpl extends
                                    NamedElement implements
                                    ControlOnlyProcess {

    private final AtomicInteger count = new AtomicInteger();

    /**
     * The collection of {@link FlowElement} instances that make up this object.
     */
    private final Collection<? extends FlowElementImpl> flowElements;

    /**
     * The collection of {@link ControlOwner} instance with this object, indexed by
     * their identities.
     */
    private final Map<String, ControlOwner> owners;

    /**
     * The {@link StartEventImpl} instance that begins execution of this object.
     */
    private final StartEventImpl start;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param start
     *            the {@link StartEventImpl} instance that begins execution of this
     *            object.
     * @param flowElements
     *            the collection of {@link FlowElement} instances that make up this
     *            object.
     */
    public ControlOnlyProcessImpl(String name,
                                  String identity,
                                  StartEventImpl start,
                                  Collection<? extends FlowElementImpl> flowElements) {
        super(name,
              identity);
        this.flowElements = flowElements;
        start.depthFirstSearch(null);
        owners = new HashMap<>();
        for (FlowElementImpl element : flowElements) {
            if (element instanceof FlowNodeImpl) {
                final FlowNodeImpl flowNode = (FlowNodeImpl) element;
                flowNode.assignRank();
                owners.putAll(flowNode.getOwners());
            }
        }
        this.start = start;
    }

    @Override
    public AtomicInteger getCount() {
        return count;
    }

    @Override
    public Collection<? extends FlowElementImpl> getFlowElements() {
        return flowElements;
    }

    /**
     * Returns the collection of {@link ControlOwner} instance with this object,
     * indexed by their identities.
     *
     * @return the collection of {@link ControlOwner} instance with this object,
     *         indexed by their identities.
     */
    public Map<String, ControlOwner> getOwners() {
        return owners;
    }

    /**
     * Returns the {@link StartEventImpl} instance that begins execution of this
     * object.
     *
     * @return the {@link StartEventImpl} instance that begins execution of this
     *         object.
     */
    public StartEventImpl getStart() {
        return start;
    }

}
