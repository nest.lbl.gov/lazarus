package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.control.SequenceFlowImpl.SourceImpl;

/**
 * This class collects together all outgoing {@link SequenceFlowImpl} instances
 * for a {@link FlowNodeImpl} instance.
 *
 * @author patton
 */
abstract class OutgoingSequenceFlows extends
                                     MultipleSequenceFlows implements
                                     SourceImpl {

    private SequenceFlowImpl defaultFlow;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the {@link String} instance used to identify this owner from all
     *            the other one in the workflow.
     */
    protected OutgoingSequenceFlows(String identity) {
        super(identity);
    }

    @Override
    public final void addOutboundSequenceFlowImpl(SequenceFlowImpl flow) {
        if (null == flow.getExpressionImpl() && null == defaultFlow) {
            defaultFlow = flow;
        }
        addSequenceFlowImpl(flow);
    }

    /**
     * Returns the default {@link SequenceFlowImpl} instance for output if no other
     * is selected.
     *
     * @return the default {@link SequenceFlowImpl} instance for output if no other
     *         is selected.
     */
    SequenceFlowImpl getDefault() {
        return defaultFlow;
    }

    /**
     * Transfers out the {@link ControlFlow} instance through the outgoing sequence
     * flows.
     *
     * @param control
     *            the {@link ControlFlow} instance being transfered.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store or, for
     *             loops, the size of the loop can not be returned.
     */
    void transferControl(ControlFlow control) {
        // The following is a test to see if the control scope is still executing.
        if (control.changeOwner(this)) {
            exercise(control);
        }
    }
}
