package gov.lbl.nest.lazarus.control;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import gov.lbl.nest.lazarus.digraph.Arc;
import gov.lbl.nest.lazarus.structure.SequenceFlow;

/**
 * This class collects together all {@link SequenceFlowImpl} instances either in
 * _or_ out of a for a {@link FlowNodeImpl} instance.
 *
 * @author patton
 */
abstract class MultipleSequenceFlows implements
                                     ControlOwner {

    /**
     * The {@link String} instance used to identify this owner from all the other
     * one in the workflow.
     */
    private String identity;

    /**
     * The single {@link SequenceFlow} instance used when there are not multiple
     * instances.
     *
     * (This is used to simplify non-gateway behavior.)
     */
    private SequenceFlowImpl singleFlow;

    /**
     * The sequence of {@link SequenceFlow} instances ordered by when they were
     * added.
     */
    private List<SequenceFlowImpl> multipleFlows;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the {@link String} instance used to identify this owner from all
     *            the other one in the workflow.
     */
    protected MultipleSequenceFlows(String identity) {
        this.identity = identity;
    }

    /**
     * Adds the specified {@link SequenceFlowImpl} instance to this object.
     *
     * @param flow
     *            the {@link SequenceFlowImpl} instance to be added to this object.
     */
    protected final void addSequenceFlowImpl(SequenceFlowImpl flow) {
        if (null == multipleFlows) {
            if (null == singleFlow) {
                singleFlow = flow;
            } else {
                multipleFlows = new ArrayList<>();
                multipleFlows.add(singleFlow);
                singleFlow = null;
                multipleFlows.add(flow);
            }
        } else {
            multipleFlows.add(flow);
        }
    }

    /**
     * Returns all of the {@link Arc} instances contained in this object.
     *
     * @return all of the {@link Arc} instances contained in this object.
     */
    Collection<? extends Arc> getArcs() {
        if (null == multipleFlows) {
            if (null == singleFlow) {
                return Collections.emptyList();
            } else {
                final List<SequenceFlowImpl> arcs = new ArrayList<>();
                arcs.add(singleFlow);
                return arcs;
            }
        }
        return multipleFlows;
    }

    /**
     * Returns the sequence of {@link SequenceFlow} instances ordered by when they
     * were added.
     *
     * @return the sequence of {@link SequenceFlow} instances ordered by when they
     *         were added.
     */
    protected final List<SequenceFlowImpl> getMultipleFlows() {
        return multipleFlows;
    }

    @Override
    public String getOwnerIdentity() {
        return identity;
    }

    /**
     * Returns the single {@link SequenceFlow} instance used when there are not
     * multiple instances.
     *
     * @return the single {@link SequenceFlow} instance used when there are not
     *         multiple instances.
     */
    protected final SequenceFlowImpl getSingleFlow() {
        return singleFlow;
    }
}
