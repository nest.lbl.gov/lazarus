package gov.lbl.nest.lazarus.control;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import gov.lbl.nest.lazarus.control.SequenceFlowImpl.SourceImpl;
import gov.lbl.nest.lazarus.control.SequenceFlowImpl.TargetImpl;
import gov.lbl.nest.lazarus.digraph.DfsState;
import gov.lbl.nest.lazarus.digraph.NodeImpl;
import gov.lbl.nest.lazarus.structure.FlowNode;
import gov.lbl.nest.lazarus.structure.SequenceFlow;

/**
 * This class implements the {@link FlowNode} interface for this package.
 *
 * @author patton
 */
public abstract class FlowNodeImpl extends
                                   FlowElementImpl implements
                                   FlowNode,
                                   SourceImpl,
                                   TargetImpl,
                                   ControlOwner {

    /**
     * The prefix added to a {@link FlowNode} identifier to create an Incoming
     * identifier.
     */
    public final static String INCOMING_PREFIX = "*";

    /**
     * The prefix added to a {@link FlowNode} identifier to create an Outgoing
     * identifier.
     */
    public final static String OUTGOING_PREFIX = "+";

    /**
     * Returns the identity for the {@link IncomingSequenceFlows} instance use by
     * the {@link FlowNodeImpl} instance with the supplied identity.
     *
     * @param identity
     *            the {@link FlowNodeImpl} instance using the
     *            {@link IncomingSequenceFlows} instance.
     *
     * @return the identity for the {@link IncomingSequenceFlows} instance use by
     *         the {@link FlowNodeImpl} instance with the supplied identity.
     */
    final static String incomingIdentity(String identity) {
        return INCOMING_PREFIX + identity;
    }

    /**
     * Returns the identity for the {@link OutgoingSequenceFlows} instance use by
     * the {@link FlowNodeImpl} instance with the supplied identity.
     *
     * @param identity
     *            the {@link FlowNodeImpl} instance using the
     *            {@link OutgoingSequenceFlows} instance.
     *
     * @return the identity for the {@link OutgoingSequenceFlows} instance use by
     *         the {@link FlowNodeImpl} instance with the supplied identity.
     */
    final static String outgoingIdentity(String identity) {
        return OUTGOING_PREFIX + identity;
    }

    /**
     * The {@link IncomingSequenceFlows} through which control flows are received.
     */
    private final IncomingSequenceFlows incoming;

    /**
     * The {@link NodeImpl} instance used by this object to create a digraph.
     */
    private NodeImpl node;

    /**
     * The {@link OutgoingSequenceFlows} instance through which control flows are
     * sent downstream.
     */
    private final OutgoingSequenceFlows outgoing;

    /**
     * The {@link Map} of {@link ControlOwner} instances that are in this object,
     * indexed by their owner identity.
     */
    private final Map<String, ControlOwner> owners = new HashMap<>();

    /**
     * True if the {@link NodeImpl} instance's successors have been set.
     */
    private boolean successorsSet = false;

    /**
     * Create an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param incoming
     *            the {@link IncomingSequenceFlows} through which control flows are
     *            received
     * @param outgoing
     *            the {@link OutgoingSequenceFlows} instance through which control
     *            flows are sent downstream.
     */
    protected FlowNodeImpl(String name,
                           String identity,
                           IncomingSequenceFlows incoming,
                           OutgoingSequenceFlows outgoing) {
        super(name,
              identity);
        this.node = new NodeImpl(identity);
        this.incoming = incoming;
        this.outgoing = outgoing;
        owners.put(identity,
                   this);
        if (null != incoming && null != incoming.getOwnerIdentity()) {
            owners.put(incoming.getOwnerIdentity(),
                       incoming);
        }
        if (null != outgoing) {
            owners.put(outgoing.getOwnerIdentity(),
                       outgoing);
        }
    }

    @Override
    public final TargetImpl addInboundSequenceFlowImpl(SequenceFlowImpl flow) {
        return incoming.addInboundSequenceFlowImpl(flow);
    }

    @Override
    public final void addOutboundSequenceFlowImpl(SequenceFlowImpl flow) {
        outgoing.addOutboundSequenceFlowImpl(flow);
    }

    /**
     * Assigns the rank of this object base on it place in the DiGraph of the
     * workflow.
     */
    public abstract void assignRank();

    @Override
    public final DfsState depthFirstSearch(DfsState dfsState) {
        if (!successorsSet) {
            if (null != outgoing) {
                node.setSuccessors(outgoing.getArcs());
            }
            successorsSet = true;
        }
        return node.depthFirstSearch(dfsState);
    }

    /**
     * Receive the {@link ControlFlow} instance from the incoming sequence flows.
     *
     * @param control
     *            the {@link ControlFlow} instance being transfered.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store or, for
     *             loops, the size of the loop can not be returned.
     */
    final void fromIncoming(ControlFlow control) {
        // The following is a test to see if the control scope is still executing.
        if (control.changeOwner(this)) {
            exercise(control);
        }
    }

    @Override
    public final Integer getDepth() {
        return node.getDepth();
    }

    @Override
    public final Collection<? extends SequenceFlow> getIncoming() {
        return incoming.getMultipleFlows();
    }

    @Override
    public final Collection<? extends SequenceFlow> getOutgoing() {
        return outgoing.getMultipleFlows();
    }

    /**
     * Returns the {@link OutgoingSequenceFlows} instance used by this object.
     *
     * @return the {@link OutgoingSequenceFlows} instance used by this object.
     */
    final OutgoingSequenceFlows getOutgoingSequenceFlows() {
        return outgoing;
    }

    @Override
    public final String getOwnerIdentity() {
        return getIdentity();
    }

    /**
     * Returns the {@link Map} of {@link ControlOwner} instances that are in this
     * object, indexed by their owner identity.
     *
     * @return the {@link Map} of {@link ControlOwner} instances that are in this
     *         object, indexed by their owner identity.
     */
    public final Map<String, ControlOwner> getOwners() {
        return owners;
    }

    @Override
    public final void increaseDepth(int delta) {
        node.increaseDepth(delta);
    }

    @Override
    public final void receiveControl(ControlFlow control) {
        incoming.receiveControl(control);
    }

    /**
     * Sets this objects to be the target of its own {@link IncomingSequenceFlows}
     * instance.
     */
    protected final void setInboundTarget() {
        if (null != incoming) {
            incoming.setTarget(this);
        }
    }

    /**
     * Transfers out the {@link ControlFlow} instance through the outgoing sequence
     * flows.
     *
     * @param control
     *            the {@link ControlFlow} instance being transfered.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store or, for
     *             loops, the size of the loop can not be returned.
     */
    protected final void toOutgoing(ControlFlow control) {
        // The following is a test to see if the control scope is still executing.
        if (control.changeOwner(outgoing)) {
            outgoing.transferControl(control);
        }
    }

}
