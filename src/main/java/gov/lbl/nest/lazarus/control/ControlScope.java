package gov.lbl.nest.lazarus.control;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import jakarta.xml.bind.annotation.XmlTransient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.execution.TerminationListener;
import gov.lbl.nest.lazarus.execution.ThrownFailure;

/**
 * This class encapsulates the flows of control of a single instance of
 * workflow.
 *
 * @author patton
 */
public abstract class ControlScope {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ControlScope.class);

    /**
     * The {@link AtomicInteger} instance used to keep count of the number of
     * parallel {@link ControlScope} instances that exist.
     */
    private AtomicInteger count;

    /**
     * The sequence of {@link ThrownFailure} instances thrown within this workflow.
     * The first one being the {@link ThrownFailure} instance that cause this object
     * to terminate.
     */
    private List<ThrownFailure> failures = new ArrayList<>();

    /**
     * The collection of {@link ControlFlow} instances that currently exist.
     */
    private final Collection<ControlFlowImpl> flows = new ArrayList<>();

    /**
     * The Collections of {@link ControlFlow} instances that had been received by a
     * {@link ParallelIncomingFlows} instance, index by identity.
     */
    private final Map<String, List<ControlFlow>> receivedFlows = new HashMap<>();
    /**
     * The {@link String} instance describing this object.
     */
    private String label;

    /**
     * The {@link TerminationListener} instance, if any, to callback when this
     * object has terminated.
     */
    private TerminationListener listener;
    /**
     * The {@link Integer} instance containing the priority this object.
     */
    private Integer priority;

    /**
     * The {@link ExecutableProcess} instance whose flow of control is managed by
     * this object.
     */
    private ExecutableProcess process;

    /**
     * The {@link StartEventImpl} instance of the workflow to be executed.
     */
    private StartEventImpl start;

    /**
     * <code>true</code> when this workflow contained in this object has terminated.
     * The definition of termination being no control flows exist.
     */
    private final AtomicBoolean terminated = new AtomicBoolean();

    /**
     * Creates an instance of this object.
     * 
     * @param label
     *            the {@link String} instance describing this object.
     * @param priority
     *            the {@link Integer} instance containing the priority this object.
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance of the workflow to be
     *            executed.
     * @param listener
     *            the {@link TerminationListener} instance, if any, to callback when
     *            this object has terminated.
     */
    protected ControlScope(String label,
                           Integer priority,
                           ControlOnlyProcessImpl process,
                           TerminationListener listener) {
        this.label = label;
        this.priority = priority;
        setControlElements(process,
                           listener);

    }

    /**
     * Add a created {@link ControlFlowImpl} instance to this object.
     *
     * @param control
     *            the {@link ControlFlowImpl} instance to be added.
     */
    public final void addControlFlow(ControlFlowImpl control) {
        synchronized (flows) {
            flows.add(control);
        }
    }

    /**
     * Captures any {@link Throwable} instance created by by within a
     * {@link ControlFlow} instance belonging to this object. Unlike
     * {@link #handleThrowable(ControlFlowImpl, Throwable)}, this method does not
     * remove the {@link ControlFlow} instance from this object.
     *
     * @param control
     *            the {@link ControlFlowImpl} instance within which the
     *            {@link Throwable} instance was created.
     * @param cause
     *            the {@link Throwable} instance that was thrown by the owner of
     *            this object.
     */
    public final void captureThrowable(ControlFlowImpl control,
                                       Throwable cause) {
        synchronized (failures) {
            failures.add(createThrownFailure(control,
                                             cause));
            if (null == System.getProperty("gov.lbl.nest.lazarus.suppressTracebacks")) {
                LOG.error(cause.toString());
            }
            updateFailures(failures);
        }
    }

    /**
     * Creates a new {@link ControlFlowImpl} instance within the scope of this
     * object.
     *
     * @return the new {@link ControlFlowImpl} instance.
     *
     * @throws ControlFlowException
     *             when the backing store for the new object could not be created.
     */
    public abstract ControlFlowImpl createControlFlow();

    /**
     * Creates a {@link ThrownFailure} instance from the supplied objects.
     *
     * @param control
     *            the {@link ControlFlowImpl} instance within which the
     *            {@link Throwable} instance was created.
     * @param cause
     *            the {@link Throwable} instance that was thrown by the owner of
     *            this object.
     *
     * @return the {@link ThrownFailure} instance from the supplied objects.
     */
    protected abstract ThrownFailure createThrownFailure(ControlFlowImpl control,
                                                         Throwable cause);

    /**
     * Destroys the specified {@link ControlFlowImpl} instance.
     *
     * @param control
     *            the {@link ControlFlowImpl} instance to destroy.
     */
    void destroyControlFlow(ControlFlowImpl control) {
        if (removeControlFlow(control)) {
            control.purge();
        }
    }

    /**
     * Discontinues the specified {@link ControlFlowImpl} instance.
     *
     * @param control
     *            the {@link ControlFlowImpl} instance to discontinue.
     */
    void discontinueControlFlow(ControlFlowImpl control) {
        removeControlFlow(control);
    }

    /**
     * Execute the workflow contained in this object.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store or, for
     *             loops, the size of the loop can not be returned.
     */
    public final void execute() {
        if (flows.isEmpty()) {
            if (isTerminating()) {
                terminate();
            } else {
                final ControlFlowImpl control = createControlFlow();
                addControlFlow(control);
                final boolean proceed = control.changeOwner(start);
                setExecuting();
                // The following is a test to see if the control scope is still executing.
                if (proceed) {
                    control.exercise();
                }
            }
        } else {
            /**
             * As a flow can destroy itself before returning from its #exercise method, the
             * following loop needs to be done over a copy of the flows collection so that
             * the original can be manipulated without causing a concurrency issue.
             */
            final Collection<ControlFlowImpl> tmpCopy = new ArrayList<>(flows);
            for (ControlFlowImpl flow : tmpCopy) {
                // The following is a test to see if the control scope is still executing.
                if (flow.changeOwner(flow.getOwner())) {
                    flow.exercise();
                }
            }
        }
    }

    /**
     * Returns the {@link ExecutableProcess} instance whose flow of control is
     * managed by this object.
     *
     * @return the {@link ExecutableProcess} instance whose flow of control is
     *         managed by this object.
     */
    @XmlTransient
    public ExecutableProcess getExecutableProcess() {
        return process;
    }

    /**
     * Returns the sequence of {@link Throwable} instances thrown within this
     * workflow. The first one being the {@link ThrownFailure} instance that cause
     * this object to terminate.
     *
     * @return the sequence of {@link Throwable} instances thrown within this
     *         workflow. The first one being the {@link ThrownFailure} instance that
     *         cause this object to terminate.
     */
    public List<? extends ThrownFailure> getFailures() {
        return failures;
    }

    /**
     * Returns the {@link String} instance describing this object.
     *
     * @return the {@link String} instance describing this object.
     */
    public final String getLabel() {
        return label;
    }

    /**
     * Returns the {@link Integer} instance containing the priority this object.
     *
     * @return the {@link Integer} instance containing the priority this object.
     */
    public final Integer getPriority() {
        return priority;
    }

    /**
     * Returns the "live" List of {@link ControlFlow} instances that had been
     * received by a {@link ParallelIncomingFlows} instance.
     *
     * @param owner
     *            the Identity of owner that contains the
     *            {@link ParallelIncomingFlows} instance.
     *
     * @return the "live" List of {@link ControlFlow} instances that had been
     *         received by a {@link ParallelIncomingFlows} instance.
     */
    List<ControlFlow> getReceived(String owner) {
        final List<ControlFlow> result = receivedFlows.get(owner);
        if (null != result) {
            return result;
        }
        final List<ControlFlow> created = new ArrayList<>();
        receivedFlows.put(owner,
                          created);
        return created;
    }

    /**
     * Returns the {@link AtomicBoolean} instance that will be <code>true</code>
     * when this workflow contained in this object has terminated. The
     * {@link #wait()} method of the returned instance can be used to detect changes
     * in its value.
     *
     * @return the {@link AtomicBoolean} instance that will be <code>true</code>
     *         when this workflow contained in this object has terminated.
     */
    public final AtomicBoolean getTerminated() {
        return terminated;
    }

    /**
     * Handles any {@link Throwable} instance created by by within a
     * {@link ControlFlow} instance belonging to this object. Unlike
     * {@link #capttureThrowable(Throwable)}, this method does s the
     * {@link ControlFlow} instance from this object.
     *
     * @param control
     *            the {@link ControlFlowImpl} instance within which the
     *            {@link Throwable} instance was created.
     * @param cause
     *            the {@link Throwable} instance that was thrown within a
     *            {@link ControlFlow} instance belonging to this object.
     */
    void handleThrowable(ControlFlowImpl control,
                         Throwable cause) {
        captureThrowable(control,
                         cause);
        discontinueControlFlow(control);
    }

    /**
     * Returns <code>true</code> when there are no {@link ControlFlowImpl} instances
     * executing after a failure and the {@link TerminationListener} instances, if
     * any, have been called.
     * 
     * @return <code>true</code> when there are no {@link ControlFlowImpl} instances
     *         executing after a failure.
     */
    protected abstract boolean isFailed();

    /**
     * Returns <code>true</code> if a {@link ControlFlowImpl} instance belonging to
     * this object has created a {@link Throwable} instance(), but there are still
     * executing {@link ControlFlowImpl} instances or, if they has all completed,
     * the {@link TerminationListener} instances, if any, have not been called.
     *
     * @return <code>true</code> if a {@link ControlFlowImpl} instance belonging to
     *         this object has created a {@link Throwable} instance();
     */
    boolean isFailing() {
        return !failures.isEmpty() && !isFailed();
    }

    /**
     * Returns <code>true</code> if this object has completed its responsibilities,
     * but the "outside" world has been completely notified.
     *
     * @return <code>true</code> if this object has completed its responsibilities,
     *         but the "outside" world has been completely notified.
     */
    protected abstract boolean isTerminating();

    /**
     * Preserves any backing store of information about this object.
     *
     * This is called when no {@link ControlFlow} instances belong to this object,
     * and after the "outside" world has be notified.
     */
    protected abstract void preserve();

    /**
     * Purges any backing store of information about this object.
     *
     * This is called when no {@link ControlFlow} instances belong to this object,
     * and after the "outside" world has be notified.
     */
    protected abstract void purge();

    /**
     * Removes the specified {@link ControlFlowImpl} instance from this object.
     *
     * @param control
     *            the {@link ControlFlowImpl} instance to remove.
     *
     * @return <code>true</code> is the specified {@link ControlFlowImpl} instance
     *         should be purged after this method.
     */
    private boolean removeControlFlow(ControlFlowImpl control) {
        final boolean empty;
        synchronized (flows) {
            if (flows.remove(control)) {
                empty = flows.isEmpty();
            } else {
                throw new IllegalStateException("Destroying a control flow that does not belong to this Control Scope");
            }
        }
        if (empty) {
            setTerminating();
            terminate();
            return false;
        }
        return true;
    }

    /**
     * Sets the control elements of this object
     * 
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance of the workflow to be
     *            executed.
     * @param listener
     *            the {@link TerminationListener} instance, if any, to callback when
     *            this object has terminated.
     */
    protected final void setControlElements(ControlOnlyProcessImpl process,
                                            TerminationListener listener) {
        if (null != process) {
            count = process.getCount();
            count.incrementAndGet();
            start = process.getStart();
        } else {
            count = null;
            this.start = null;
        }
        this.listener = listener;
    }

    /**
     * This is called when all of the initial information needs to execute this
     * object has been stored.
     */
    protected abstract void setCreated();

    /**
     * Sets the {@link ExecutableProcess} instance whose flow of control is managed
     * by this object.
     *
     * @param process
     *            the {@link ExecutableProcess} instance whose flow of control is
     *            managed by this object.
     */
    public void setExecutableProcess(ExecutableProcess process) {
        this.process = process;
    }

    /**
     * This is called when the first {@link ControlFlow} instances is bound to its
     * {@link ControlOwner} instance.
     */
    protected abstract void setExecuting();

    /**
     * Sets the {@link String} instance describing this object.
     * 
     * @param label
     *            the {@link String} instance describing this object.
     */
    protected void setLabel(String label) {
        this.label = label;
    }

    /**
     * Sets the {@link Integer} instance containing the priority this object.
     * 
     * @param priority
     *            the {@link Integer} instance containing the priority this object.
     */
    protected void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * This is called when no {@link ControlFlow} instances belong to this object,
     * but before the "outside" world has be notified.
     */
    protected abstract void setTerminating();

    /**
     * Tells the "outside" world that this object has terminated its execution.
     */
    private void terminate() {
        final boolean preserve;
        if (null != listener) {
            if (failures.isEmpty()) {
                listener.success(process);
                preserve = false;
            } else {
                if (!isFailed()) {
                    preserve = listener.failure(process);
                    updatedFailingToFailed();
                } else {
                    /*
                     * Technically there should be a check to see if preservation was required, but
                     * given the difficultly of accessing that information from here and the low
                     * probability of this statement running, the conservative choice is to simply
                     * preserve the state.
                     */
                    // TODO: Add some sort of default setting.
                    preserve = true;
                }
            }
        } else {
            preserve = false;
        }
        synchronized (terminated) {
            terminated.set(true);
            terminated.notifyAll();
        }
        if (preserve) {
            preserve();
        } else {
            purge();
        }
        count.decrementAndGet();
    }

    /**
     * Moves a failing file to be a failed one.
     *
     * @throws ControlFlowException
     *             when the file can not be moved.
     */
    protected abstract void updatedFailingToFailed();

    /**
     * Replaces any existing sequence of {@link ThrownFailure} with an updated copy.
     * The first one being the {@link ThrownFailure} instance that cause this object
     * to terminate.
     *
     * @param failures
     *            the {@link Throwable} instances that have been thrown within any
     *            {@link ControlFlow} instance belonging to this object.
     *
     * @throws ControlFlowException
     *             when the backing store for the new object could not be created.
     */
    protected abstract void updateFailures(List<? extends ThrownFailure> failures);
}
