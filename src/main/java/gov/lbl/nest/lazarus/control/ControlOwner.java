package gov.lbl.nest.lazarus.control;

/**
 * This interface is used to assign ownership of a {@link ControlFlow} instance
 * to an {@link Object} within an instance of a workflow.
 *
 * During normal execution, when ownership is assigned to this object, the
 * {@link #exercise(ControlFlow)} method is immediately called.
 *
 * During recovery, the most recent owner of a recovered {@link ControlFlow}
 * instances is given ownership, but the {@link #exercise(ControlFlow)} method
 * is not immediately called, rather that happens then the recovered
 * {@link ControlFlow} is restarted.
 *
 * @author patton
 */
public interface ControlOwner {

    /**
     * Exercises control in order to fulfill the object's responsibilities.
     *
     * @param control
     *            the {@link ControlFlow} instance being exercised.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store or, for
     *             loops, the size of the loop can not be returned.
     */
    void exercise(ControlFlow control);

    /**
     * Returns the {@link String} instance used to identify this owner from all the
     * other one in the workflow.
     *
     * @return the {@link String} instance used to identify this owner from all the
     *         other one in the workflow.
     */
    String getOwnerIdentity();

}
