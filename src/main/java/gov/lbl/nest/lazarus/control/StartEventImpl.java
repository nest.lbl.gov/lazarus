package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.structure.StartEvent;

/**
 * This class implements the {@link StartEvent} interface for this package.
 *
 * @author patton
 */
public final class StartEventImpl extends
                                  FlowNodeImpl implements
                                  StartEvent {

    /**
     * Creates an instance for this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     */
    public StartEventImpl(String name,
                          String identity) {
        super(name,
              identity,
              null,
              new ExclusiveOutgoingFlows(outgoingIdentity(identity)));
        setInboundTarget();
    }

    @Override
    public final void assignRank() {
        // Start Events do not currently support Ranking.
    }

    @Override
    public void exercise(ControlFlow control) {
        toOutgoing(control);
    }
}
