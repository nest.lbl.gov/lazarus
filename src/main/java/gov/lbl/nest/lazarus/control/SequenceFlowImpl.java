package gov.lbl.nest.lazarus.control;

import java.beans.Expression;

import gov.lbl.nest.lazarus.digraph.ArcImpl;
import gov.lbl.nest.lazarus.digraph.Node;
import gov.lbl.nest.lazarus.structure.FlowNode;
import gov.lbl.nest.lazarus.structure.SequenceFlow;

/**
 * This class implements the {@link SequenceFlow} interface for this package.
 *
 * @author patton
 */
public class SequenceFlowImpl extends
                              FlowElementImpl implements
                              SequenceFlow {

    /**
     * This interface defines the source of a flow of control.
     *
     * @author patton
     */
    public interface SourceImpl extends
                                Source {

        /**
         * Adds the specified {@link SequenceFlowImpl} instance to be an outbound one
         * for this Object.
         *
         * @param flow
         *            the {@link SequenceFlowImpl} to be set as an outbound one for this
         *            Object.
         */
        void addOutboundSequenceFlowImpl(SequenceFlowImpl flow);
    }

    /**
     * This interface defines the source of a flow of control.
     *
     * @author patton
     */
    public interface TargetImpl extends
                                Target {

        /**
         * Adds the specified {@link SequenceFlowImpl} instance to be an inbound one for
         * this Object.
         *
         * @param flow
         *            the {@link SequenceFlowImpl} to be set as an inbound one for this
         *            Object.
         *
         * @return TODO
         */
        TargetImpl addInboundSequenceFlowImpl(SequenceFlowImpl flow);

        /**
         * Receives the control flow through the inbound sequence flows.
         *
         * @param control
         *            the {@link ControlFlow} instance being received.
         *
         * @throws ControlFlowException
         *             when there is a problem accessing the backing store or, for
         *             loops, the size of the loop can not be returned.
         */
        void receiveControl(ControlFlow control);
    }

    /**
     * The {@link ArcImpl} instance used by this object to create a digraph.
     */
    private ArcImpl arc;

    /**
     * The {@link Expression}, if any, associated with this Object.
     */
    private final ControlExpressionImpl<Boolean> expression;

    /**
     * The source of the flow of control passing through this object.
     */
    private final SourceImpl source;

    /**
     * The target of the flow of control passing through this object.
     */
    private final TargetImpl target;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param source
     *            the source of a flow of control.
     * @param target
     *            the target of a flow of control.
     */
    public SequenceFlowImpl(String name,
                            String identity,
                            SourceImpl source,
                            TargetImpl target) {
        this(name,
             identity,
             source,
             target,
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param source
     *            the source of a flow of control.
     * @param target
     *            the target of a flow of control.
     * @param expression
     *            the {@link Expression}, if any, associated with this Object.
     */
    public SequenceFlowImpl(String name,
                            String identity,
                            SourceImpl source,
                            TargetImpl target,
                            ControlExpressionImpl<Boolean> expression) {
        super(name,
              identity);
        this.arc = new ArcImpl((FlowNode) target);
        this.expression = expression;
        this.source = source;
        source.addOutboundSequenceFlowImpl(this);
        this.target = target.addInboundSequenceFlowImpl(this);
    }

    @Override
    public ControlExpressionImpl<Boolean> getExpressionImpl() {
        return expression;
    }

    @Override
    public Node getHead() {
        return arc.getHead();
    }

    @Override
    public SourceImpl getSource() {
        return source;
    }

    @Override
    public TargetImpl getTarget() {
        return target;
    }

    /**
     * Transfers the control flow from the source to the target.
     *
     * @param control
     *            the {@link ControlFlow} instance being transfered.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store or, for
     *             loops, the size of the loop can not be returned.
     */
    void transferControl(ControlFlow control) {
        target.receiveControl(control);
    }

}
