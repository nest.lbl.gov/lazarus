package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.common.tasks.ActiveCollectionProducer;
import gov.lbl.nest.common.tasks.Consumer;
import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.common.tasks.ordinal.PriorityRankedActiveCollection;
import gov.lbl.nest.lazarus.structure.FlowNodeInspector;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;

/**
 * This class is a convenience class that provides a simple name of the Producer
 * of {@link FlowNodeTask} instances.
 *
 * <b>Remember:</b> A {@link Producer} instance produces {@link Task} instances
 * <em>for</em> a {@link Consumer} instance. It does not create the
 * {@link FlowNodeTask} instances itself, but rather they are added to the
 * {@link Producer} instance via an associated {@link Supplier} instance.
 *
 * @author patton
 */
public class FlowNodeTaskProducer extends
                                  ActiveCollectionProducer<FlowNodeTask, FlowNodeInspector> {

    /**
     * Creates an instance of this class.
     */
    public FlowNodeTaskProducer() {
        super(new PriorityRankedActiveCollection<FlowNodeTask, FlowNodeInspector>());
    }
}
