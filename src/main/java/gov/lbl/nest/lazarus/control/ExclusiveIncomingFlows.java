package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.control.SequenceFlowImpl.TargetImpl;

/**
 * This class extends the {@link IncomingSequenceFlows} class to implement the
 * inbound side of an exclusive gateway.
 *
 * @author patton
 */
final class ExclusiveIncomingFlows extends
                                   IncomingSequenceFlows {

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the {@link String} instance used to identify this owner from all
     *            the other one in the workflow.
     */
    protected ExclusiveIncomingFlows(String identity) {
        super(identity);
    }

    @Override
    public final TargetImpl addInboundSequenceFlowImpl(SequenceFlowImpl flow) {
        addSequenceFlowImpl(flow);
        return this;
    }

    @Override
    public void exercise(ControlFlow control) {
        transferControl(control);
    }

    @Override
    public final void receiveControl(ControlFlow control) {
        /*
         * As this is effectively a "pass-through", there is nothing to be gained by
         * this object owning the ControlFlow instance. Therefore ownership of the
         * ControlFlow instance is immediately given to the target object.
         */
        // The following is a test to see if the control scope is still executing.
        if (control.changeOwner(getTarget())) {
            exercise(control);
        }
    }
}
