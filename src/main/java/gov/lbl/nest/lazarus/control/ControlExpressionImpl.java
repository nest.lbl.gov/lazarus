package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;

/**
 * This interface extends the {@link Expression} interface for this package.
 *
 * @author patton
 *
 * @param <T>
 *            the type to which this Object evaluates.
 */
public interface ControlExpressionImpl<T extends Object> extends
                                      Expression<T> {

    /**
     * Evaluates this expression for the provided {@link ControlFlow} instance.
     *
     * @param controlFlow
     *            the {@link ControlFlow} instance for which this Object should be
     *            evaluated.
     * @param clazz
     *            the {@link Class} which should be returned by this method.
     *
     * @return the result of evaluating this expression for the provided
     *         {@link ControlScope} instance.
     *
     * @throws ExpressionEvaluationException
     *             when the expression can not be evaluated for some reason.
     */
    T evaluate(ControlFlow controlFlow,
               Class<T> clazz) throws ExpressionEvaluationException;

    /**
     * Evaluates this expression for the provided {@link ControlFlow} instance.
     *
     * @param controlFlow
     *            the {@link ControlFlow} instance for which this Object should be
     *            evaluated.
     * @param arguments
     *            the arguments, if any, to use when evaluating this Object.
     * @param clazz
     *            the {@link Class} which should be returned by this method.
     *
     * @return the result of evaluating this expression for the provided
     *         {@link ControlScope} instance.
     *
     * @throws ExpressionEvaluationException
     *             when the expression can not be evaluated for some reason.
     */
    T evaluate(ControlFlow controlFlow,
               Object[] arguments,
               Class<T> clazz) throws ExpressionEvaluationException;
}
