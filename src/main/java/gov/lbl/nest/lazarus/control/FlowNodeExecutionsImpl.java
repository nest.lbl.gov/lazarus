package gov.lbl.nest.lazarus.control;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.common.tasks.FoundationTask;
import gov.lbl.nest.lazarus.execution.FlowNodeExecutions;
import gov.lbl.nest.lazarus.structure.FlowNodeInspector;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;

/**
 * This class inplements the {@link FlowNodeExecutions} interface.
 *
 * @author patton
 */
public class FlowNodeExecutionsImpl implements
                                    FlowNodeExecutions {

    /**
     * The number of executing tasks currently executing for the
     * {@link FlowNodeImpl} instance this object is monitoring.
     */
    private final AtomicInteger executingCount = new AtomicInteger();

    /**
     * The name of the {@link FlowNodeImpl} instance this object is monitoring.
     */
    private final String name;

    /**
     * The collection of {@link FlowNodeTask} instances which this Object is
     * tracking.
     */
    private final List<FoundationTask<? extends FlowNodeInspector>> tasks = new ArrayList<>();

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of the {@link FlowNodeImpl} instance this object is
     *            monitoring.
     */
    public FlowNodeExecutionsImpl(String name) {
        this.name = name;
    }

    @Override
    public int getExecutingCount() {
        return executingCount.get();
    }

    @Override
    public List<? extends FlowNodeInspector> getInspectors() {
        final List<FlowNodeInspector> result = new ArrayList<>();
        synchronized (tasks) {
            for (FoundationTask<? extends FlowNodeInspector> task : tasks) {
                result.add(task.inspect());
            }
        }
        return result;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPendingCount() {
        synchronized (tasks) {
            return tasks.size() - executingCount.get();
        }
    }

    @Override
    public int getTotalCount() {
        synchronized (tasks) {
            return tasks.size();
        }
    }

    /**
     * Called when a task in {@link FlowNodeImpl} instance this object is monitoring
     * ends execution.
     *
     * @param task
     *            the {@link FlowNodeTask} that is finishing.
     */
    void taskFinishing(FoundationTask<? extends FlowNodeInspector> task) {
        synchronized (tasks) {
            tasks.remove(task);
            executingCount.decrementAndGet();
        }
    }

    /**
     * Called when a task in {@link FlowNodeImpl} instance this object is monitoring
     * is ready for execution.
     *
     * @param task
     *            the {@link FlowNodeTask} that is being queued.
     */
    void taskQueued(FoundationTask<? extends FlowNodeInspector> task) {
        synchronized (tasks) {
            tasks.add(task);
        }
    }

    /**
     * Called when a task in {@link FlowNodeImpl} instance this object is monitoring
     * begins execution.
     *
     * @param task
     *            the {@link FlowNodeTask} that is starting.
     */
    void taskStarting(FoundationTask<? extends FlowNodeInspector> task) {
        executingCount.incrementAndGet();
    }
}
