package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.common.tasks.FoundationTask;
import gov.lbl.nest.common.tasks.InspectorFactory;
import gov.lbl.nest.lazarus.structure.FlowNodeInspector;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;

/**
 * This class is used to create {@link FlowNodeInspectorImpl} instances.
 *
 * @author patton
 */
public class FlowNodeInspectorFactory implements
                                      InspectorFactory<FlowNodeInspector> {

    /**
     * The {@link FlowNodeExecutionsImpl} instance used by this object.
     */
    private final FlowNodeExecutionsImpl counts;

    /**
     * The label of the {@link ControlScope} instance to which the
     * {@link FlowNodeTask} instance belongs.
     */
    private final String label;

    /**
     * Creates an instance of this class.
     *
     * @param label
     *            the label of the {@link ControlScope} instance to which the
     *            {@link FlowNodeTask} instance belongs.
     * @param counts
     *            the {@link FlowNodeExecutionsImpl} instance to be used by this
     *            object.
     */
    public FlowNodeInspectorFactory(String label,
                                    FlowNodeExecutionsImpl counts) {
        this.counts = counts;
        this.label = label;
    }

    @Override
    public FlowNodeInspector creatCreatingInspector(FoundationTask<FlowNodeInspector> task) {
        counts.taskQueued(task);
        return new FlowNodeInspectorImpl(label,
                                         counts.getName(),
                                         null);
    }

    @Override
    public FlowNodeInspectorImpl createBeginningInspector(FoundationTask<FlowNodeInspector> task) {
        counts.taskStarting(task);
        return new FlowNodeInspectorImpl(label,
                                         counts.getName(),
                                         (Thread.currentThread()).getName());
    }

    @Override
    public FlowNodeInspectorImpl createEndingInspector(FoundationTask<FlowNodeInspector> task) {
        counts.taskFinishing(task);
        return new FlowNodeInspectorImpl(null,
                                         counts.getName(),
                                         null);
    }

}
