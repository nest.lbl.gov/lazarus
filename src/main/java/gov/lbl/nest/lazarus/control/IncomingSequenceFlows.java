package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.control.SequenceFlowImpl.TargetImpl;

/**
 * This class collects together all incoming {@link SequenceFlowImpl} instances
 * for a {@link FlowNodeImpl} instance.
 *
 * @author patton
 */
abstract class IncomingSequenceFlows extends
                                     MultipleSequenceFlows implements
                                     TargetImpl {

    /**
     * The object to which to object is going to pass control.
     */
    private FlowNodeImpl target;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the {@link String} instance used to identify this owner from all
     *            the other one in the workflow.
     */
    protected IncomingSequenceFlows(String identity) {
        super(identity);
    }

    /**
     * Returns the object to which to object is going to pass control.
     *
     * @return the object to which to object is going to pass control.
     */
    protected final ControlOwner getTarget() {
        return target;
    }

    /**
     * Sets the object to which to object is going to pass control.
     *
     * @param target
     *            the object to which to object is going to pass control.
     */
    protected final void setTarget(FlowNodeImpl target) {
        this.target = target;
    }

    /**
     * Transfers out the control flow to the main body of this object's
     * {@link FlowNodeImpl} instance..
     *
     * @param control
     *            the {@link ControlFlow} instance being transfered.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store or, for
     *             loops, the size of the loop can not be returned.
     */
    protected final void transferControl(ControlFlow control) {
        target.fromIncoming(control);
    }
}
