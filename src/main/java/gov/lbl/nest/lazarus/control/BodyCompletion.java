package gov.lbl.nest.lazarus.control;

/**
 * This interface is used by the {@link ControlOnlyActivityBody} implementations
 * to signal when they have completed execution.
 *
 * @author patton
 *
 */
public interface BodyCompletion {

    /**
     * Callback method used to signal that a {@link ControlOnlyActivityBody} has
     * completed their execution.
     *
     * @param control
     *            the {@link ControlFlow} instance whose
     *            {@link ControlOnlyActivityBody} has completed.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store or, for
     *             loops, the size of the loop can not be returned.
     */
    void bodyExecutionCompleted(ControlFlow control);
}
