package gov.lbl.nest.lazarus.control;

import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.lazarus.control.SequenceFlowImpl.TargetImpl;

/**
 * This class extends the {@link IncomingSequenceFlows} class to implement the
 * inbound side of an parallel gateway.
 *
 * @author patton
 */
final class ParallelIncomingFlows extends
                                  IncomingSequenceFlows {

    /**
     * This class is used to track which inbound {@link SequenceFlowImpl} instance
     * provided which {@link ControlFlow} instance.
     *
     * @author patton
     */
    class InboundTarget implements
                        TargetImpl {

        /**
         * The index into the sequence of {@link ControlFlow} instances when fanning-out
         * or fanning-in flows, otherwise -1.
         */
        private final int fanningIndex;

        /**
         * Sets the index of the {@link SequenceFlowImpl} instance that is passing
         * {@link ControlFlow} instances to this object.
         *
         * @param fanningIndex
         *            the index into the sequence of {@link ControlFlow} instances when
         *            fanning-out or fanning-in flows, otherwise -1.
         */
        private InboundTarget(int fanningIndex) {
            this.fanningIndex = fanningIndex;
        }

        @Override
        public TargetImpl addInboundSequenceFlowImpl(SequenceFlowImpl flow) {
            throw new UnsupportedOperationException("This method should never be called");
        }

        @Override
        public void receiveControl(ControlFlow control) {
            control.setJoiningOwner(ParallelIncomingFlows.this,
                                    fanningIndex);
            exercise(control);
        }

    }

    /**
     * The sequence of {@link InboundTarget} instances in the order they were made.
     */
    private List<InboundTarget> inboundTargets = new ArrayList<>();

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the {@link String} instance used to identify this owner from all
     *            the other one in the workflow.
     */
    protected ParallelIncomingFlows(String identity) {
        super(identity);
    }

    @Override
    public final TargetImpl addInboundSequenceFlowImpl(SequenceFlowImpl flow) {
        final InboundTarget inboundTarget = new InboundTarget(inboundTargets.size());
        inboundTargets.add(inboundTarget);
        addSequenceFlowImpl(flow);
        return inboundTarget;
    }

    @Override
    public void exercise(ControlFlow control) {
        List<SequenceFlowImpl> flows = getMultipleFlows();
        // If join has already happened, i.e. we are in recovery, simply transfer
        // control.
        if (null == flows || -1 == control.getFanningIndex()) {
            transferControl(control);
            return;
        }

        final List<ControlFlow> received = control.getReceived();
        received.add(control);
        if (flows.size() != received.size()) {
            // Wait until all sequence flows have delivered a ControlFlow.
            return;
        }

        /**
         * When the new token has been created and is in the ready state then the join
         * is officially complete. After that is just cleaning up.
         */
        final ControlFlow joinedFlow = control.createJoinedFlow();
        if (null != joinedFlow) {
            for (ControlFlow receive : received) {
                receive.destroy();
            }
            transferControl(joinedFlow);
        }
    }

    @Override
    public final void receiveControl(ControlFlow control) {
        // The following is a test to see if the control scope is still executing.
        if (control.changeOwner(this)) {
            exercise(control);
        }
    }

}
