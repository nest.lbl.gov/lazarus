package gov.lbl.nest.lazarus.control;

import java.util.List;

/**
 * This class is used to manage a single flow of control. It must always be
 * owned by one and only one {@link ControlOwner} instance.
 *
 * @author patton
 */
public interface ControlFlow {

    /**
     * Captures any {@link Throwable} instance created by the owner of this object.
     * Unlike {@link #handleThrowable(Throwable)}, this method does not removed this
     * object from its containing {@link ControlScope} instance.
     *
     * @param cause
     *            the {@link Throwable} instance that was thrown by the owner of
     *            this object.
     */
    void captureThrowable(Throwable cause);

    /**
     * Changes the current owning object.
     *
     * @param owner
     *            the new owner of this object.
     *
     * @return <code>true</code> if the flow of control for this object should
     *         continue. If <code>false</code> is returned, then the entire
     *         {@link ControlScope} is not longer continuing and so this flow of
     *         control should stop.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store.
     */
    boolean changeOwner(ControlOwner owner);

    /**
     * Creates a new {@link ControlFlow} instance which will start then this object
     * is destroyed.
     *
     * @param fanningIndex
     *            the index into the sequence of {@link ControlFlow} instances when
     *            fanning-out.
     *
     * @return the new {@link ControlFlow} instance.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store.
     */
    ControlFlow createForkedFlow(int fanningIndex);

    /**
     * Creates a new {@link ControlFlow} instance which supersedes one or more
     * existing {@link ControlFlow} instances.
     *
     * @return the new {@link ControlFlow} instance.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store.
     */
    ControlFlow createJoinedFlow();

    /**
     * Destroys this object.
     */
    void destroy();

    /**
     * Returns the {@link ControlScope} instance to which this object belongs.
     *
     * @return the {@link ControlScope} instance to which this object belongs.
     */
    ControlScope getControlScope();

    /**
     * Returns the index, if this object is a forked instance, of the
     * {@link SequenceFlowImpl} to which this object should be transferred,
     * otherwise returns -1.
     *
     * @return the index, if this object is a forked instance, of the
     *         {@link SequenceFlowImpl} to which this object should be transferred.
     */
    int getFanningIndex();

    /**
     * Returns the priority of this object with respect to others in the same
     * Activity.
     *
     * @return the priority of this object with respect to others in the same
     *         Activity.
     */
    Integer getPriority();

    /**
     * Returns the "live" List of {@link ControlFlow} instances that had been
     * received by this object's owner.
     *
     * @return the "live" List of {@link ControlFlow} instances that had been
     *         received by this object's owner
     */
    List<ControlFlow> getReceived();

    /**
     * Returns the collection of flow indexes for the branches, if any, that have
     * been recovered by this object if it is executing a multi-instance activity.
     *
     * @return the collection of flow indexes for the branches, if any, that have
     *         been recovered by this object if it is executing a multi-instance
     *         activity.
     */
    List<Integer> getRecoveredBranches();

    /**
     * Handles the {@link Throwable} instance created by the owner of this object.
     * Unlike {@link #captureThrowable(Throwable)}, this method does remove this
     * object from its containing {@link ControlScope} instance.
     *
     * @param cause
     *            the {@link Throwable} instance that was thrown by the owner of
     *            this object.
     */
    void handleThrowable(Throwable cause);

    /**
     * Sets the collection of flow indexes for the branches, if any, that will be
     * recovered by this object if it is executing a multi-instance activity.
     *
     * @param branchesToExecute
     *            the collection of flow indexes for the branches, if any, that will
     *            be recovered by this object if it is executing a multi-instance
     *            activity.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store.
     */
    void setBranchesToRecover(List<Integer> branchesToExecute) throws ControlFlowException;

    /**
     * Sets the current owner of a joining instance
     *
     * @param owner
     *            the {@link ControlOwner} instance that is becoming the owner of
     *            this object.
     * @param fanningIndex
     *            the index into the sequence of {@link ControlFlow} instances when
     *            fanning-in.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store.
     */
    void setJoiningOwner(ControlOwner owner,
                         int fanningIndex);

}
