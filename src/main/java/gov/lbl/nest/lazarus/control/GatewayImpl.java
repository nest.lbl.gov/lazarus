package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.structure.Gateway;

/**
 * This class is the base from which all other gateways inherit.
 *
 * @author patton
 */
public abstract class GatewayImpl extends
                                  FlowNodeImpl implements
                                  Gateway {

    /**
     * The {@link GatewayDirection} instance of this object.
     */
    private final GatewayDirection direction;

    /**
     * Create an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param incoming
     *            the {@link IncomingSequenceFlows} through which control flows are
     *            received
     * @param outgoing
     *            the {@link OutgoingSequenceFlows} instance through which control
     *            flows are sent downstream.
     * @param direction
     *            the {@link GatewayDirection} instance of this object.
     */
    @SuppressWarnings("javadoc")
    protected GatewayImpl(String name,
                          String identity,
                          IncomingSequenceFlows incoming,
                          OutgoingSequenceFlows outgoing,
                          GatewayDirection direction) {
        super(name,
              identity,
              incoming,
              outgoing);
        this.direction = direction;
    }

    @Override
    public final void assignRank() {
        // Gateways do not currently support Ranking.
    }

    @Override
    public final GatewayDirection getGatewayDirection() {
        return direction;
    }

}
