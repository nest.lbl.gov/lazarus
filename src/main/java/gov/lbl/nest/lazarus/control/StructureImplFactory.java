package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.control.SequenceFlowImpl.SourceImpl;
import gov.lbl.nest.lazarus.control.SequenceFlowImpl.TargetImpl;
import gov.lbl.nest.lazarus.structure.EndEvent;
import gov.lbl.nest.lazarus.structure.ExclusiveGateway;
import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.Gateway.GatewayDirection;
import gov.lbl.nest.lazarus.structure.ParallelGateway;
import gov.lbl.nest.lazarus.structure.SequenceFlow;
import gov.lbl.nest.lazarus.structure.SequenceFlow.Source;
import gov.lbl.nest.lazarus.structure.SequenceFlow.Target;
import gov.lbl.nest.lazarus.structure.StartEvent;
import gov.lbl.nest.lazarus.structure.StructureFactory;

/**
 *
 * @author patton
 *
 */
public abstract class StructureImplFactory extends
                                           StructureFactory {

    @Override
    public EndEvent createEndEvent(String name,
                                   String identity) {
        return new EndEventImpl(name,
                                identity);
    }

    @Override
    public ExclusiveGateway createExclusiveGateway(String name,
                                                   String identity,
                                                   GatewayDirection direction) {
        return new ExclusiveGatewayImpl(name,
                                        identity,
                                        direction);
    }

    @Override
    public ParallelGateway createParallelGateway(String name,
                                                 String identity,
                                                 GatewayDirection direction) {
        return new ParallelGatewayImpl(name,
                                       identity,
                                       direction);
    }

    @Override
    public SequenceFlow createSequenceFlow(String name,
                                           String identity,
                                           Source source,
                                           Target target,
                                           Expression<Boolean> expression) {
        return new SequenceFlowImpl(name,
                                    identity,
                                    (SourceImpl) source,
                                    (TargetImpl) target,
                                    (ControlExpressionImpl<Boolean>) expression);
    }

    @Override
    public StartEvent createStartEvent(String name,
                                       String identity) {
        return new StartEventImpl(name,
                                  identity);
    }
}
