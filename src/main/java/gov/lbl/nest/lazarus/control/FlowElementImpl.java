package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.structure.BaseElementImpl;
import gov.lbl.nest.lazarus.structure.FlowElement;

/**
 * This class implements the {@link FlowElement} interface for this package.
 *
 * @author patton
 *
 */
public class FlowElementImpl extends
                             BaseElementImpl implements
                             FlowElement {

    /**
     * The name of this object.
     */
    private String name;

    /**
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     */
    public FlowElementImpl(String name,
                           String identity) {
        super(identity);
        this.name = name;
    }

    @Override
    public final String getName() {
        return name;
    }
}
