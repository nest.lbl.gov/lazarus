package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.structure.FlowNodeInspector;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;

/**
 * This class implements the {@link FlowNodeInspector} interface.
 *
 * @author patton
 */
public class FlowNodeInspectorImpl implements
                                   FlowNodeInspector {

    /**
     * The name of the {@link FlowNodeImpl} instance whose responsibilities this
     * object is fulfilling.
     */
    private final String flowNode;

    /**
     * The label of the {@link ControlScope} instance to which the
     * {@link FlowNodeTask} instance belongs.
     */
    private final String label;

    /**
     * The name of the {@link Thread} instance, if any, that is currently executing
     * the {@link FlowNodeTask} instance.
     */
    private final String thread;

    /**
     * Creates an instance of this class.
     *
     * @param label
     *            the label of the {@link ControlScope} instance to which the
     *            {@link FlowNodeTask} instance belongs.
     * @param flowNode
     *            the name of the {@link FlowNodeImpl} instance whose
     *            responsibilities this object is fulfilling.
     * @param thread
     *            the name of the {@link Thread} instance, if any, that is currently
     *            executing the {@link FlowNodeTask} instance.
     */
    FlowNodeInspectorImpl(String label,
                          String flowNode,
                          String thread) {
        this.flowNode = flowNode;
        this.label = label;
        this.thread = thread;
    }

    @Override
    public String getFlowNode() {
        return flowNode;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String getThread() {
        return thread;
    }

}
