package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.structure.ParallelGateway;

/**
 * This class implements the Exclusive Gateway logic for a workflow.
 *
 * @author patton
 */
public final class ParallelGatewayImpl extends
                                       GatewayImpl implements
                                       ParallelGateway {

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param direction
     *            the {@link GatewayDirection} instance of this object.
     */
    @SuppressWarnings("javadoc")
    public ParallelGatewayImpl(String name,
                               String identity,
                               GatewayDirection direction) {
        super(name,
              identity,
              new ParallelIncomingFlows(incomingIdentity(identity)),
              new ParallelOutgoingFlows(outgoingIdentity(identity)),
              direction);
        setInboundTarget();
    }

    @Override
    public void exercise(ControlFlow control) {
        toOutgoing(control);
    }
}
