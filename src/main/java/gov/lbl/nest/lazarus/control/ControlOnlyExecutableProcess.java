package gov.lbl.nest.lazarus.control;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.execution.ThrownFailure;

/**
 * This class implements the {@link ExecutableProcess} interface for this
 * package.
 *
 * @author patton
 */
public class ControlOnlyExecutableProcess implements
                                          ExecutableProcess {

    /**
     * The {@link ControlScope} instance that defines the configuration of this
     * object.
     */
    private final ControlScope controlScope;

    /**
     * Creates an instance of this class.
     *
     * @param controlScope
     *            the {@link ControlScope} instance that defines the configuration
     *            of this object.
     */
    public ControlOnlyExecutableProcess(ControlScope controlScope) {
        this.controlScope = controlScope;
    }

    /**
     * Start the execution of this object.
     */
    public void execute() {
        controlScope.setExecutableProcess(this);
        controlScope.execute();
    }

    /**
     * Returns the {@link ControlScope} instance that defines the configuration of
     * this object.
     *
     * @return the {@link ControlScope} instance that defines the configuration of
     *         this object.
     */
    protected ControlScope getControlScope() {
        return controlScope;
    }

    @Override
    public ThrownFailure getFailure() {
        final List<? extends ThrownFailure> failures = controlScope.getFailures();
        if (null == failures || failures.isEmpty()) {
            return null;
        }
        return failures.get(0);
    }

    @Override
    public String getLabel() {
        return controlScope.getLabel();
    }

    @Override
    public AtomicBoolean getTerminated() {
        return controlScope.getTerminated();
    }

    @Override
    public Map<String, Object> getValues() {
        return null;
    }

}
