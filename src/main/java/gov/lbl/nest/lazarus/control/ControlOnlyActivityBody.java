package gov.lbl.nest.lazarus.control;

/**
 * This interface is used by the {@link ControlOnlyActivityImpl} class to
 * execute its responsibilities.
 *
 * @author patton
 */
public interface ControlOnlyActivityBody {

    /**
     * Execute its responsibilities.
     *
     * @param control
     *            the {@link ControlFlow} instance in which to execute those
     *            responsibilities.
     * @param callback
     *            The {@link ControlOnlyActivityImpl} instance to call back to once
     *            execution has been completed.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store or, for
     *             loops, the size of the loop can not be returned.
     */
    void execute(ControlFlow control,
                 BodyCompletion callback);

    /**
     * Sets the "rank" of this element, where object with a higher rank are
     * preferred.
     *
     * @param rank
     *            the "rank" of this element, where object with a higher rank are
     *            preferred.
     */
    void setRank(Integer rank);
}
