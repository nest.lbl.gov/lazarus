package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.structure.EndEvent;

/**
 * This class implements the {@link EndEvent} interface for this package.
 *
 * @author patton
 */
public final class EndEventImpl extends
                                FlowNodeImpl implements
                                EndEvent {

    /**
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     */
    public EndEventImpl(String name,
                        String identity) {
        super(name,
              identity,
              new ExclusiveIncomingFlows(null),
              null);
        setInboundTarget();
    }

    @Override
    public final void assignRank() {
        // End Events do not currently support Ranking.
    }

    @Override
    public void exercise(ControlFlow control) {
        control.destroy();
    }
}
