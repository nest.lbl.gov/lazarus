package gov.lbl.nest.lazarus.control;

/**
 * This class throw when there is a problem access the backing store within this
 * package.
 *
 * @author patton
 */
public class ControlFlowException extends
                                  RuntimeException {

    /**
     * Used by serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     */
    public ControlFlowException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public ControlFlowException(String message,
                                Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Creates an instance of this class.
     *
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public ControlFlowException(Throwable cause) {
        super(cause);
    }
}
