package gov.lbl.nest.lazarus.control;

import java.util.List;

import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;

/**
 * This class extends the {@link IncomingSequenceFlows} class to implement the
 * outbound side of an exclusive gateway.
 *
 * @author patton
 */
final class ExclusiveOutgoingFlows extends
                                   OutgoingSequenceFlows {

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the {@link String} instance used to identify this owner from all
     *            the other one in the workflow.
     */
    protected ExclusiveOutgoingFlows(String identity) {
        super(identity);
    }

    @Override
    public void exercise(ControlFlow control) {
        List<SequenceFlowImpl> flows = getMultipleFlows();
        if (null == flows) {
            getSingleFlow().transferControl(control);
            return;
        }
        selectOutboundFlow(control).transferControl(control);
    }

    /**
     * Returns the outbound {@link SequenceFlowImpl} instance to use for the
     * supplied {@link ControlFlow} instance.
     *
     * @param control
     *            the {@link ControlFlow} instance being transfered.
     *
     * @return the outbound {@link SequenceFlowImpl} instance to use for the
     *         supplied {@link ControlFlow} instance.
     *
     * @throws ControlFlowException
     *             when an expression can not be evaluated.
     */
    private SequenceFlowImpl selectOutboundFlow(ControlFlow control) {
        SequenceFlowImpl defaultFlow = null;
        for (SequenceFlowImpl flow : getMultipleFlows()) {
            final ControlExpressionImpl<Boolean> expression = flow.getExpressionImpl();
            if (null == expression) {
                if (null == defaultFlow) {
                    defaultFlow = flow;
                }
            } else
                try {
                    if (expression.evaluate(control,
                                            Boolean.class)) {
                        return flow;
                    }
                } catch (ExpressionEvaluationException e) {
                    throw new ControlFlowException(e);
                }
        }
        return defaultFlow;
    }
}
