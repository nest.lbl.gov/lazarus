package gov.lbl.nest.lazarus.control;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This class extends the {@link IncomingSequenceFlows} class to implement the
 * outbound side of an exclusive gateway.
 *
 * @author patton
 */
final class ParallelOutgoingFlows extends
                                  OutgoingSequenceFlows {

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the {@link String} instance used to identify this owner from all
     *            the other one in the workflow.
     */
    protected ParallelOutgoingFlows(String identity) {
        super(identity);
    }

    @Override
    public final void exercise(ControlFlow control) {
        List<SequenceFlowImpl> flows = getMultipleFlows();
        if (null == flows) {
            getSingleFlow().transferControl(control);
            return;
        }

        /**
         * If the provided ControlFlow is a forked, then transfer it out with its
         * associated SequenceFlow.
         */
        final int fanningIndex = control.getFanningIndex();
        if (-1 < fanningIndex) {
            (flows.get(fanningIndex)).transferControl(control);
            return;
        }

        final int finished = flows.size();
        final List<ControlFlow> forked = new ArrayList<>();
        for (int i = 0;
             i != finished;
             ++i) {
            forked.add(control.createForkedFlow(i));
        }
        control.destroy();
        final Iterator<ControlFlow> iterator = forked.iterator();
        for (SequenceFlowImpl flow : flows) {
            flow.transferControl(iterator.next());
        }
    }
}
