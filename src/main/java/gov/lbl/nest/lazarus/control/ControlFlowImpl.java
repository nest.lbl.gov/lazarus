package gov.lbl.nest.lazarus.control;

import java.util.List;

import jakarta.xml.bind.annotation.XmlTransient;

/**
 * This class is used to manage a single flow of control. It must always be
 * owned by one and only one {@link ControlOwner} instance.
 *
 * @author patton
 */
public abstract class ControlFlowImpl implements
                                      ControlFlow {

    /**
     * The index into the sequence of {@link ControlFlow} instances when fanning-out
     * or fanning-in flows, otherwise -1.
     */
    private int fanningIndex;

    /**
     * The {@link ControlScope} instance to which this object belongs.
     */
    private ControlScope controlScope;

    /**
     * The {@link ControlOwner} instance that owns this object.
     */
    private ControlOwner owner;

    /**
     * Creates an instance of this class.
     *
     * @param controlScope
     *            the {@link ControlScope} instance to which this object belongs.
     * @param owner
     *            the {@link ControlOwner} that owns this recovered object.
     * @param fanningIndex
     *            the index into the sequence of {@link ControlFlow} instances when
     *            fanning-out or fanning-in flows, otherwise -1.
     */
    protected ControlFlowImpl(ControlScope controlScope,
                              ControlOwner owner,
                              int fanningIndex) {
        this.fanningIndex = fanningIndex;
        this.controlScope = controlScope;
        this.owner = owner;
    }

    @Override
    public final void captureThrowable(Throwable cause) {
        controlScope.captureThrowable(this,
                                      cause);
    }

    @Override
    public final boolean changeOwner(ControlOwner owner) {
        if (this.owner != owner) {
            this.owner = owner;
            this.fanningIndex = -1;
            updateOwner(owner);
        }
        if (controlScope.isFailing()) {
            controlScope.discontinueControlFlow(this);
            return false;
        }
        return true;
    }

    @Override
    public final ControlFlowImpl createForkedFlow(int fanningIndex) {
        final ControlFlowImpl result = controlScope.createControlFlow();
        controlScope.addControlFlow(result);
        result.setForkedOwner(owner,
                              fanningIndex);
        return result;
    }

    @Override
    public final ControlFlowImpl createJoinedFlow() {
        final ControlFlowImpl result = controlScope.createControlFlow();
        controlScope.addControlFlow(result);
        // The following is a test to see if the control scope is still executing.
        if (result.changeOwner(owner)) {
            return result;
        }
        return null;
    }

    @Override
    public final void destroy() {
        controlScope.destroyControlFlow(this);
    }

    /**
     * Exercises control in order to fulfill the current owning object's
     * responsibilities.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store or, for
     *             loops, the size of the loop can not be returned.
     */
    void exercise() {
        owner.exercise(this);
    }

    @Override
    @XmlTransient
    public final ControlScope getControlScope() {
        return controlScope;
    }

    @Override
    public final int getFanningIndex() {
        return fanningIndex;
    }

    /**
     * Returns the {@link ControlOwner} instance that owns this object.
     *
     * @return the {@link ControlOwner} instance that owns this object.
     */
    public final ControlOwner getOwner() {
        return owner;
    }

    @Override
    public final Integer getPriority() {
        final ControlScope controlScope = getControlScope();
        return controlScope.getPriority();
    }

    @Override
    public final List<ControlFlow> getReceived() {
        return controlScope.getReceived(owner.getOwnerIdentity());
    }

    @Override
    public final void handleThrowable(Throwable cause) {
        controlScope.handleThrowable(this,
                                     cause);
    }

    /**
     * Purges any backing store of information about this object.
     */
    protected abstract void purge();

    /**
     * Sets the control elements of this object
     * 
     * @param controlScope
     *            the {@link ControlScope} instance to which this object belongs.
     * @param owner
     *            the {@link ControlOwner} that owns this recovered object.
     */
    public final void setControlElements(ControlScope controlScope,
                                         ControlOwner owner) {
        this.controlScope = controlScope;
        this.owner = owner;
    }

    /**
     * Sets the {@link ControlScope} instance to which this object belongs.
     * 
     * @param controlScope
     *            the {@link ControlScope} instance to which this object belongs.
     */
    public void setControlScope(ControlScope controlScope) {
        this.controlScope = controlScope;
    }

    /**
     * Sets the index, if this object is a forked instance, of the
     * {@link SequenceFlowImpl} to which this object should be transferred.
     * 
     * @param index
     *            the index, if this object is a forked instance, of the
     *            {@link SequenceFlowImpl} to which this object should be
     *            transferred.
     */
    protected void setFanningIndex(int index) {
        this.fanningIndex = index;
    }

    /**
     * Sets the current owner of a forked instance
     *
     * @param owner
     *            the {@link ControlOwner} instance that is becoming the owner of
     *            this object.
     * @param fanningIndex
     *            the index into the sequence of {@link ControlFlow} instances when
     *            fanning-out.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store.
     */
    private void setForkedOwner(ControlOwner owner,
                                int fanningIndex) {
        this.owner = owner;
        this.fanningIndex = fanningIndex;
        updateForkedOwner(owner,
                          fanningIndex);
    }

    @Override
    public final void setJoiningOwner(ControlOwner owner,
                                      int fanningIndex) {
        this.owner = owner;
        this.fanningIndex = fanningIndex;
        updateJoiningOwner(owner,
                           fanningIndex);
    }

    /**
     * Updates the backing store to record the new 0wner.
     *
     * @param owner
     *            the {@link ControlOwner} instance that is becoming the owner of
     *            this object.
     * @param fanningIndex
     *            the index into the sequence of {@link ControlFlow} instances when
     *            fanning-out or fanning-in flows, otherwise -1.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store.
     */
    protected abstract void updateForkedOwner(ControlOwner owner,
                                              int fanningIndex);

    /**
     * Updates the backing store to record the new 0wner. Sets the current owner of
     * a joining instance
     *
     * @param owner
     *            the {@link ControlOwner} instance that is becoming the owner of
     *            this object.
     * @param fanningIndex
     *            the index into the sequence of {@link ControlFlow} instances when
     *            fanning-out or fanning-in flows, otherwise -1.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store.
     */
    protected abstract void updateJoiningOwner(ControlOwner owner,
                                               int fanningIndex);

    /**
     * Updates the backing store to record the new 0wner.
     *
     * @param owner
     *            the identity of {@link ControlOwner} instance that is becoming the
     *            owner of this object.
     *
     * @throws ControlFlowException
     *             when there is a problem accessing the backing store.
     */
    protected abstract void updateOwner(ControlOwner owner);
}
