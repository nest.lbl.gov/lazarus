package gov.lbl.nest.lazarus.control;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.lazarus.execution.TerminationListener;

/**
 * This class needs to be extends to create appropriate instances of the
 * {@link ControlScope} class.
 *
 * @author patton
 */
public abstract class ControlFactory {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ControlFactory.class);

    /**
     * true if the implementation class of this Object has already been logged.
     */
    private static AtomicBoolean LOGGING = new AtomicBoolean();

    /**
     * The {@link ServiceLoader} instance that will load the {@link ControlFactory}
     * implementation.
     */
    private final static ServiceLoader<ControlFactory> CONTROL_FACTORY_LOADER = ServiceLoader.load(ControlFactory.class,
                                                                                                   ControlFactory.class.getClassLoader());

    /**
     * Creates an instance of the {@link ControlFactory} class.
     *
     * @return the created instance of the {@link ControlFactory} class.
     */
    public static ControlFactory getControlFactory() {
        for (ControlFactory factory : CONTROL_FACTORY_LOADER) {
            final Class<?> clazz = factory.getClass();
            try {
                final Constructor<?> constructor = clazz.getConstructor();
                ControlFactory result = (ControlFactory) constructor.newInstance();
                if (!(LOGGING.get())) {
                    LOG.debug("Using \"" + clazz.getCanonicalName()
                              + "\" as "
                              + ControlFactory.class.getName()
                              + " implementation");
                    LOGGING.set(true);
                }
                return result;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    /**
     * Create an instance of the {@link ControlScope} class.
     * 
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance of the workflow to be
     *            executed.
     * @param label
     *            A {@link String} instance uniquely describing this
     * @param priority
     *            the {@link Integer} instance containing the priority this object.
     *
     * @return the instance of the {@link ControlScope} class.
     *
     * @throws ControlFlowException
     *             when there is a problem with the the backing store so the
     *             requested {@link ControlScope} instance can not be created.
     */
    public final ControlScope createControlScope(ControlOnlyProcessImpl process,
                                                 String label,
                                                 Integer priority) {
        return createControlScope(process,
                                  label,
                                  priority,
                                  null);
    }

    /**
     * Create an instance of the {@link ControlScope} class.
     * 
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance of the workflow to be
     *            executed.
     * @param label
     *            A {@link String} instance describing this object.
     * @param priority
     *            the {@link Integer} instance containing the priority this object.
     * @param listener
     *            the {@link TerminationListener} instance to callback when this
     *            object has terminated.
     *
     * @return the instance of the {@link ControlScope} class.
     *
     * @throws ControlFlowException
     *             when there is a problem with the the backing store so the
     *             requested {@link ControlScope} instance can not be created.
     */
    public abstract ControlScope createControlScope(ControlOnlyProcessImpl process,
                                                    String label,
                                                    Integer priority,
                                                    TerminationListener listener);

    /**
     * Recovers any {@link ControlScope} instances that were in the process of being
     * executed when the previous instance of this application stops.
     *
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance for which to recover
     *            instances.
     * @param listener
     *            the {@link TerminationListener} instance to callback when this
     *            object has terminated.
     *
     * @return the collection of recovered {@link ControlScope} instances.
     *
     * @throws ControlFlowException
     *             when there is a problem with the the backing store so that at
     *             least one {@link ControlScope} instance can not be recovered.
     */
    public abstract Collection<? extends ControlScope> recoverControlScopes(ControlOnlyProcessImpl process,
                                                                            TerminationListener listener);

}
