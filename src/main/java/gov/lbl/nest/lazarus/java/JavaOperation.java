package gov.lbl.nest.lazarus.java;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import gov.lbl.nest.lazarus.data.InterfaceImpl;
import gov.lbl.nest.lazarus.data.MessageImpl;
import gov.lbl.nest.lazarus.data.OperationImpl;
import gov.lbl.nest.lazarus.structure.Message;
import gov.lbl.nest.lazarus.structure.Operation;

/**
 * This class implements the {@link Operation} interface that is back by a Java
 * method.
 *
 * @author patton
 */
public class JavaOperation extends
                           OperationImpl {

    /**
     * The {@link Object} instance on which to execute the supplied method.
     */
    private Object instance;

    /**
     * The {@link Method} instance to execute on this object's {@link #instance}.
     */
    private Method method;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param implementationRef
     *            the name of the method within the external system that performs
     *            this operation.
     * @param inMessageRef
     *            the {@link Message} instance that defines the input to this
     *            object.
     * @param outMessageRef
     *            the {@link Message} instance, if any, that defines the output to
     *            this object.
     *
     * @throws JavaInstantiationException
     *             when a suitable class of method can not be created.
     */
    public JavaOperation(String name,
                         String identity,
                         String implementationRef,
                         MessageImpl inMessageRef,
                         MessageImpl outMessageRef) throws JavaInstantiationException {
        super(name,
              identity,
              implementationRef,
              inMessageRef,
              outMessageRef);
    }

    @Override
    public Object execute(Map<String, Object> inputs) {
        final Object argument = ((inputs.values()).iterator()).next();
        try {
            return method.invoke(instance,
                                 new Object[] { argument });
        } catch (IllegalArgumentException
                 | IllegalAccessException
                 | InvocationTargetException e) {
            throw new OperationException(null,
                                         e);
        }
    }

    @Override
    public boolean isVoid() {
        return Void.TYPE == method.getReturnType();
    }

    @Override
    protected void setInterface(InterfaceImpl iface) throws NoSuchMethodException {
        super.setInterface(iface);
        final JavaInterface javaInterface = (JavaInterface) getInterface();
        this.instance = javaInterface.getInstance();
        method = (javaInterface.getJavaInstanceFactory()).getMethod(instance,
                                                                    getImplementationRef(),
                                                                    (getInMessageRef().getItemRef()).getStructure());
    }
}
