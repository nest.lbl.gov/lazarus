/**
 * This package provides the interfaces and classes that implement BPMN
 * operations as methods of Java classes.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * <li>{@link java.lang.reflect}</li>
 * <li>{@link java.util.concurrent}</li>
 * <li>{@link org.slf4j}</li>
 * <li>{@link gov.lbl.nest.lazarus.digraph}</li>
 * <li>{@link gov.lbl.nest.lazarus.structure}</li>
 * <li>{@link gov.lbl.nest.lazarus.data}</li>
 * </ul>
 */
package gov.lbl.nest.lazarus.java;