package gov.lbl.nest.lazarus.java;

/**
 * This class is used to pass {@link Throwable} instance thrown by an external
 * system back into this one.
 *
 * @author patton
 */
public class OperationException extends
                                RuntimeException {

    /**
     * Used by serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     */
    public OperationException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public OperationException(String message,
                              Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Creates an instance of this class.
     *
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public OperationException(Throwable cause) {
        super(cause);
    }
}
