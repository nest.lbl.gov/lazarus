package gov.lbl.nest.lazarus.java;

import java.lang.reflect.Method;
import java.util.Collection;

import gov.lbl.nest.lazarus.data.InterfaceImpl;
import gov.lbl.nest.lazarus.data.OperationImpl;
import gov.lbl.nest.lazarus.structure.Interface;
import gov.lbl.nest.lazarus.structure.Operation;

/**
 * This class implements the {@link Interface} interface that is back by a Java
 * class.
 *
 * @author patton
 */
public class JavaInterface extends
                           InterfaceImpl {

    /**
     * The {@link Object} instance on which to execute the {@link Operation}
     * instances.
     */
    private final Object instance;

    /**
     * The {@link JavaInstanceFactory} instance used to create Java instances.
     */
    private final JavaInstanceFactory factory = JavaInstanceFactory.getJavaInstanceFactory();

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param operations
     *            the collection of {@link OperationImpl} instances that can be
     *            performed by the external system.
     * @param implementationRef
     *            the name of the external system to which call outs will be made.
     *
     * @throws JavaInstantiationException
     *             when a suitable class can not be created.
     * @throws NoSuchMethodException
     *             when no matching {@link Method} instance can be found for the
     *             supplied {@link InterfaceImpl} instance.
     */
    public JavaInterface(String name,
                         String identity,
                         Collection<OperationImpl> operations,
                         String implementationRef) throws JavaInstantiationException,
                                                   NoSuchMethodException {
        super(name,
              identity,
              operations,
              implementationRef);
        instance = factory.createInstance(implementationRef);
        for (OperationImpl operation : operations) {
            ((JavaOperation) operation).setInterface(this);
        }
    }

    /**
     * Returns the {@link Object} instance on which to execute the {@link Operation}
     * instances.
     *
     * @return the {@link Object} instance on which to execute the {@link Operation}
     *         instances.
     */
    Object getInstance() {
        return instance;
    }

    /**
     * Returns the {@link JavaInstanceFactory} instance on which to execute the
     * {@link Operation} instances.
     *
     * @return the {@link JavaInstanceFactory} instance on which to execute the
     *         {@link Operation} instances.
     */
    JavaInstanceFactory getJavaInstanceFactory() {
        return factory;
    }
}
