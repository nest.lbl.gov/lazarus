package gov.lbl.nest.lazarus.java;

/**
 * This class throw when a requested java instance can not be created.
 *
 * @author patton
 */
public class JavaInstantiationException extends
                                        Exception {

    /**
     * Used by serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     */
    public JavaInstantiationException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public JavaInstantiationException(String message,
                                      Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Creates an instance of this class.
     *
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public JavaInstantiationException(Throwable cause) {
        super(cause);
    }
}
