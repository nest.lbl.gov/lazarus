package gov.lbl.nest.lazarus.java;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This interface is used by the {@link JavaInterface} class to create the
 * necessary {@link Object} instance for that class to use.
 *
 * @author patton
 */
public abstract class JavaInstanceFactory {

    /**
     * The {@link JavaInstanceFactory} implementation to use if none is explicitly
     * specified.
     */
    public final static JavaInstanceFactory DEFAULT_JAVA_INSTANCE_FACTORY = new PojoInstanceFactory();

    /**
     * The {@link ServiceLoader} instance that will load the {@link UriHandler}
     * implementation.
     */
    private final static ServiceLoader<JavaInstanceFactory> JAVA_INSTANCE_FACTORY_LOADER = ServiceLoader.load(JavaInstanceFactory.class,
                                                                                                              JavaInstanceFactory.class.getClassLoader());

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(JavaInstanceFactory.class);

    /**
     * true if the implementation class of this Object has already been logged.
     */
    private static AtomicBoolean LOGGING = new AtomicBoolean();

    /**
     * Creates an instance of the {@link JavaInstanceFactory} class.
     *
     * @return the created instance of the {@link JavaInstanceFactory} class.
     */
    public static JavaInstanceFactory getJavaInstanceFactory() {
        for (JavaInstanceFactory instrumentation : JAVA_INSTANCE_FACTORY_LOADER) {
            final Class<?> clazz = instrumentation.getClass();
            try {
                final Constructor<?> constructor = clazz.getConstructor();
                JavaInstanceFactory result = (JavaInstanceFactory) constructor.newInstance();
                if (!(LOGGING.get())) {
                    LOG.debug("Using \"" + clazz.getCanonicalName()
                              + "\" as JavaInstanceFactory implementation");
                    LOGGING.set(true);
                }
                return result;
            } catch (Exception e) {
                return null;
            }
        }
        if (!(LOGGING.get())) {
            LOG.info("Using default, " + DEFAULT_JAVA_INSTANCE_FACTORY.getClass()
                     + " as JavaInstanceFactory implementation");
            LOGGING.set(true);
        }
        return DEFAULT_JAVA_INSTANCE_FACTORY;
    }

    /**
     * Create a new instance of the Java class backing this object.
     *
     * @param implementation
     *            the name of the class to instantiate.
     *
     * @return the new instance of the Java class backing this object.
     *
     * @throws JavaInstantiationException
     *             when a suitable class can not be created.
     */
    protected abstract Object createInstance(String implementation) throws JavaInstantiationException;

    /**
     * Returns the {@link Method} instance to execute on the supplied {@link Object}
     * instance.
     *
     * @param instance
     *            the {@link Object} instance on which to execute the supplied
     *            method.
     * @param methodName
     *            the name of the method within the external system that performs
     *            this operation.
     * @param input
     *            the {@link Class} instance for the type of parameters for the
     *            method.
     *
     * @return the {@link Method} instance to execute on the supplied {@link Object}
     *         instance.
     *
     * @throws NoSuchMethodException
     *             when no matching {@link Method} instance can be found for the
     *             supplied {@link Object} instance.
     */
    protected abstract Method getMethod(Object instance,
                                        String methodName,
                                        Class<?> input) throws NoSuchMethodException;
}
