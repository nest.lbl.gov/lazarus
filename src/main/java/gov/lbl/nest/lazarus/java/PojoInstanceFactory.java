package gov.lbl.nest.lazarus.java;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * This class implements the {@link JavaInstanceFactory} interface to create
 * simple Plain of Java Objects.
 *
 * @author patton
 */
public class PojoInstanceFactory extends
                                 JavaInstanceFactory {

    @Override
    public Object createInstance(String implementation) throws JavaInstantiationException {
        try {
            Class<?> clazz = (getClass().getClassLoader()).loadClass(implementation);
            return (clazz.getDeclaredConstructor()).newInstance();
        } catch (ClassNotFoundException
                 | IllegalAccessException
                 | InstantiationException
                 | InvocationTargetException
                 | NoSuchMethodException e) {
            throw new JavaInstantiationException(e);
        }
    }

    @Override
    public Method getMethod(final Object instance,
                            String methodName,
                            Class<?> input) throws NoSuchMethodException {
        final Method method = (instance.getClass()).getMethod(methodName,
                                                              new Class<?>[] { input });
        return method;
    }
}
