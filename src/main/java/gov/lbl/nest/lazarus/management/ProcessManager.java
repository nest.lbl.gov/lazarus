package gov.lbl.nest.lazarus.management;

import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.common.suspension.SuspendableCollection;
import gov.lbl.nest.lazarus.structure.SuspendableActivity;

/**
 * This interface is used to manage a workflow.
 *
 * @author patton
 */
public interface ProcessManager extends
                                SuspendableCollection {

    /**
     * Increased by one the maximum number of tasks instances that can be executing
     * at the same time.
     *
     * @return the resulting the maximum number of tasks instances that can be
     *         executing at the same time, or <code>null</code> if this can not be
     *         modified.
     */
    Integer addThread();

    /**
     * Returns the {@link AtomicInteger} instance to contains the number of tasks
     * currently executing for the workflow this object is monitoring.
     *
     * @return the {@link AtomicInteger} instance to contains the number of tasks
     *         currently executing for the workflow this object is monitoring.
     */
    AtomicInteger getExecutingCount();

    /**
     * Returns the maximum number of task that can be executing at the same time.
     *
     * @return the maximum number of task that can be executing at the same time.
     */
    int getExecutingLimit();

    /**
     * Returns the number of pending tasks currently awaiting executing for the
     * workflow this object is monitoring.
     *
     * @return the number of pending tasks currently awaiting executing for the
     *         workflow this object is monitoring.
     */
    int getPendingCount();

    /**
     * Returns the {@link SuspendableActivity} instance within the {@link Process}
     * instance that has the supplied name.
     *
     * @param name
     *            the name of the {@link SuspendableActivity} instance within the
     *            workflow to return.
     *
     * @return the {@link SuspendableActivity} instance within the {@link Process}
     *         instance that has the supplied name.
     */
    SuspendableActivity getSuspendableActivity(String name);

    /**
     * Temporarily stop the consumption of tasks.
     *
     * @return true when the consumption of tasks has been paused.
     */
    boolean pause();

    /**
     * Continue the consumption of tasks after it has been temporarily stopped.
     *
     * @return true when the consumption of tasks is no longer paused.
     */
    boolean proceed();

    /**
     * Decreased by one the maximum number of tasks instances that can be executing
     * at the same time, but this can not go below zero.
     *
     * @return the resulting the maximum number of tasks instances that can be
     *         executing at the same time, or <code>null</code> if this can not be
     *         modified.
     */
    Integer removeThread();
}
