package gov.lbl.nest.lazarus.management;

import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.CollectableSuspendable;
import gov.lbl.nest.common.suspension.FileSuspendedWhileRunning;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.suspension.SuspendableCollection;
import gov.lbl.nest.common.suspension.SuspendableCollectionImpl;
import gov.lbl.nest.common.suspension.SuspendableCollectionImpl.SuspendedWhileRunningImpl;
import gov.lbl.nest.common.tasks.Consumer;
import gov.lbl.nest.common.tasks.ParallelConsumer;
import gov.lbl.nest.common.tasks.StatefulSupplier;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.lazarus.process.SuspendableActivityImpl;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;
import gov.lbl.nest.lazarus.structure.SuspendableActivity;

/**
 * This class implements the {@link ProcessManager} interface
 *
 * @author patton
 */
public class ProcessManagerImpl implements
                                ProcessManager {

    class SuspendableFlowNodeCollection extends
                                        SuspendableCollectionImpl {

        /**
         * Creates an instance of this class.
         *
         * @param suspendable
         *            the external {@link Suspendable} instance that contains this
         *            Object's state.
         * @param path
         *            the {@link Path} instance, if any, to the file containing the set
         *            of names of suspended {@link CollectableSuspendable} instances.
         */
        SuspendableFlowNodeCollection(final Suspendable suspendable,
                                      final Path path) {
            super(suspendable,
                  getSuspendedWhileRunning(path));
            try {
                if (null != suspendable) {
                    if (suspendable.isSuspended()) {
                        LOG.info("Workflow \"" + suspendable.getName()
                                 + "\" is initially suspended");
                    } else {
                        LOG.info("Workflow \"" + suspendable.getName()
                                 + "\" is initially running");
                    }
                }
            } catch (InitializingException e) {
                // Do nothing as this must be being done elsewhere.
            }

        }

        @Override
        public Collection<? extends CollectableSuspendable> getCollectableSuspendables() {
            return activities.values();
        }

        @Override
        public boolean setSuspended(final boolean suspend) throws InitializingException {
            boolean result = super.setSuspended(suspend);
            if (result) {
                if (isSuspended()) {
                    LOG.info("Suspending workflow \"" + getName()
                             + "\"");
                } else {
                    LOG.info("Resuming workflow \"" + getName()
                             + "\"");
                }
            }
            return result;
        }
    }

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProcessManagerImpl.class);

    private static SuspendableCollectionImpl.SuspendedWhileRunning getSuspendedWhileRunning(Path path) {
        if (null == path) {
            return new SuspendedWhileRunningImpl();
        }
        return new FileSuspendedWhileRunning(path.toFile());
    }

    /**
     * The collection of {@link SuspendableActivity} instances, index by their name.
     */
    Map<String, SuspendableActivityImpl> activities = new HashMap<>();

    /**
     * The {@link Consumer} instance used by this object.
     */
    private final Consumer consumer;

    /**
     * The {@link StatefulSupplier} instance, if any, associated with the
     * {@link Consumer} instance.
     */
    private final StatefulSupplier<FlowNodeTask> statefulSupplier;

    /**
     * The {@link SuspendableCollection} instance to which this object's
     * implementation is delegated.
     */
    private final SuspendableCollection suspendableCollection;

    /**
     * Creates an instance of this class.
     *
     * @param consumer
     *            the {@link Consumer} instance to be used by this object.
     * @param supplier
     *            the {@link Supplier} instance to be used by this object, which
     *            should be a decorated instance of the {@link Consumer} instance's
     *            own {@link Supplier}.
     * @param suspendable
     *            the {@link Suspendable} instance this created instance should use
     *            as the application's {@link Suspendable}.
     * @param path
     *            the {@link Path} instance, if any, to the file containing the set
     *            of names of suspended {@link CollectableSuspendable} instances.
     */
    @SuppressWarnings("unchecked")
    public ProcessManagerImpl(Consumer consumer,
                              Supplier<FlowNodeTask> supplier,
                              Suspendable suspendable,
                              Path path) {
        this.consumer = consumer;
        final Supplier<FlowNodeTask> supplierToUse;
        if (null == supplier && null != consumer) {
            supplierToUse = (Supplier<FlowNodeTask>) consumer.getSupplier();
        } else {
            supplierToUse = supplier;
        }
        if (supplierToUse instanceof StatefulSupplier) {
            statefulSupplier = (StatefulSupplier<FlowNodeTask>) supplierToUse;
        } else {
            statefulSupplier = null;
        }
        suspendableCollection = new SuspendableFlowNodeCollection(suspendable,
                                                                  path);
    }

    /**
     * Creates an instance of this class.
     *
     * @param consumer
     *            the {@link Consumer} instance to be used by this object.
     * @param suspendable
     *            the {@link Suspendable} instance this created instance should use
     *            as the application's {@link Suspendable}.
     * @param path
     *            the {@link Path} instance, if any, to the file containing the set
     *            of names of suspended {@link CollectableSuspendable} instances.
     */
    public ProcessManagerImpl(Consumer consumer,
                              Suspendable suspendable,
                              Path path) {
        this(consumer,
             null,
             suspendable,
             path);
    }

    @Override
    public Integer addThread() {
        if (consumer instanceof ParallelConsumer) {
            return ((ParallelConsumer) consumer).addThread();
        }
        return null;
    }

    @Override
    public Collection<? extends CollectableSuspendable> getCollectableSuspendables() {
        return suspendableCollection.getCollectableSuspendables();
    }

    @Override
    public AtomicInteger getExecutingCount() {
        return consumer.getExecutingCount();
    }

    @Override
    public int getExecutingLimit() {
        return consumer.getExecutionLimit();
    }

    @Override
    public String getName() {
        return suspendableCollection.getName();
    }

    @Override
    public int getPendingCount() {
        return statefulSupplier.size();
    }

    @Override
    public SuspendableActivityImpl getSuspendableActivity(String name) {
        return activities.get(name);
    }

    @Override
    public boolean isSuspended() throws InitializingException {
        return suspendableCollection.isSuspended();
    }

    @Override
    public boolean isSuspended(String name) throws InitializingException {
        return suspendableCollection.isSuspended(name);
    }

    @Override
    public boolean pause() {
        return consumer.pause();
    }

    @Override
    public boolean proceed() {
        return consumer.proceed();
    }

    @Override
    public Integer removeThread() {
        if (consumer instanceof ParallelConsumer) {
            return ((ParallelConsumer) consumer).removeThread();
        }
        return null;
    }

    @Override
    public void setCollection(SuspendableCollection collection) {
        suspendableCollection.setCollection(collection);
    }

    /**
     * Sets the collection of {@link SuspendableActivity} instances contained in the
     * Process instance this object is managing.
     *
     * @param suspendableActivities
     *            the collection of {@link SuspendableActivity} instances contained
     *            in the Process instance this object is managing.
     */
    public void setSuspendableActivities(Collection<? extends SuspendableActivityImpl> suspendableActivities) {
        for (SuspendableActivityImpl activity : suspendableActivities) {
            activity.setCollection(this);
            activities.put(activity.getName(),
                           activity);
        }
    }

    @Override
    public boolean setSuspended(boolean suspend) throws InitializingException {
        boolean result = suspendableCollection.setSuspended(suspend);
        if (result && !suspend) {
            if (null == statefulSupplier) {
                consumer.flush();
            } else {
                statefulSupplier.restart();
            }
        }
        return result;
    }

    @Override
    public void update(CollectableSuspendable suspendable) throws InitializingException {
        suspendableCollection.update(suspendable);
        if (!suspendable.isSuspended()) {
            if (null != statefulSupplier) {
                statefulSupplier.reset();
            }
        }
    }
}
