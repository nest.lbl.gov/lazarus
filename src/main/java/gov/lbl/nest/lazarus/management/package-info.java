/**
 * This package provides the classes to persist the state of execution workflows
 * on disk.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * <li>{@link java.util.concurrent}</li>
 * <li>{@link java.nio.file}</li>
 * <li>{@link org.slf4j}</li>
 * <li>{@link gov.lbl.nest.common.configure}</li>
 * <li>{@link gov.lbl.nest.common.suspension}</li>
 * <li>{@link gov.lbl.nest.common.tasks}</li>
 * <li>{@link gov.lbl.nest.lazarus.structure}</li>
 * <li>{@link gov.lbl.nest.lazarus.process}</li>
 * </ul>
 */
package gov.lbl.nest.lazarus.management;
