package gov.lbl.nest.lazarus.data;

import java.util.List;

import gov.lbl.nest.lazarus.structure.BaseElementImpl;
import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.InputOutputSpecification;
import gov.lbl.nest.lazarus.structure.InputSet;
import gov.lbl.nest.lazarus.structure.OutputSet;

/**
 * This class implements the {@link InputOutputSpecification} interface for this
 * package.
 *
 * @author patton
 */
public class InputOutputSpecificationImpl extends
                                          BaseElementImpl implements
                                          InputOutputSpecification {

    /**
     * The list of {@link DataInput} instances that define possible input data.
     */
    private final List<? extends DataInput> dataInputs;

    /**
     * The list of {@link DataOutput} instances that define possible output data.
     */
    private final List<? extends DataOutput> dataOutputs;

    /**
     * The list of {@link InputSetImpl} instances that define possible sets of input
     * data.
     */
    private final List<InputSetImpl> inputSets;

    /**
     * The list of {@link OutputSetImpl} instances that define possible sets of
     * output data.
     */
    private final List<OutputSetImpl> outputSets;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param dataInputs
     *            the list of {@link DataInput} instances that define possible input
     *            data.
     * @param dataOutputs
     *            the list of {@link DataOutput} instances that define possible
     *            output data.
     * @param inputSets
     *            the list of {@link InputSet} instances that define possible sets
     *            of input data.
     * @param outputSets
     *            the list of {@link OutputSet} instances that define possible sets
     *            of output data.
     */
    public InputOutputSpecificationImpl(String identity,
                                        List<? extends DataInput> dataInputs,
                                        List<? extends DataOutput> dataOutputs,
                                        List<InputSetImpl> inputSets,
                                        List<OutputSetImpl> outputSets) {
        super(identity);
        this.dataInputs = dataInputs;
        this.dataOutputs = dataOutputs;
        this.inputSets = inputSets;
        this.outputSets = outputSets;

    }

    @Override
    public List<? extends DataInput> getDataInputs() {
        return dataInputs;
    }

    @Override
    public List<? extends DataOutput> getDataOutputs() {
        return dataOutputs;
    }

    /**
     * Returns the {@link InputSet} that is associated with the specified
     * {@link DataCell} instance.
     *
     * @param cell
     *            the {@link DataCell} instance for which the {@link DataInput}
     *            instances should be returned.
     *
     * @return the {@link InputSet} that is associated with the specified
     *         {@link DataCell} instance.
     */
    InputSetImpl getInputSet(DataCell cell) {
        // Only a single InputSet is currently implemented.
        return inputSets.get(0);
    }

    @Override
    public List<? extends InputSet> getInputSets() {
        return inputSets;
    }

    /**
     * Returns the {@link OutputSetImpl} instance for the supplied {@link DataCell}
     * instance and {@link InputSet} instance.
     *
     * @param cell
     *            the {@link DataCell} instance for which the {@link DataInput}
     *            instances should be returned.
     * @param inputSet
     *            the {@link InputSet} instance associated with the
     *            {@link OutputSet} to return.
     *
     * @return the {@link OutputSetImpl} instance for the supplied {@link DataCell}
     *         instance and {@link InputSet} instance.
     */
    OutputSetImpl getOutputSet(DataCell cell,
                               InputSet inputSet) {
        // Only single OutputSet currently allowed.
        return outputSets.get(0);
    }

    @Override
    public List<? extends OutputSet> getOutputSets() {
        return outputSets;
    }
}
