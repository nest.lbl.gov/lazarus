package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.DataOutputTarget;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;

/**
 * This interface extends the {@link DataOutputTarget} interface so that is can
 * be used to fill in its value when required.
 *
 * @author patton
 */
public interface DataOutputTargetImpl extends
                                      DataOutputTarget {

    /**
     * Requests that this object pull its value for the specified {@link DataCell}
     * instance and store a deep copy of it.
     *
     * @param cell
     *            the {@link DataCell} instance for which to pull this object's
     *            value.
     *
     * @throws DataObjectException
     *             when the value can not be pulled.
     * @throws ExpressionEvaluationException
     *             when the Loop Cardinality can not be evaluated.
     */
    void pullValue(DataCell cell) throws DataObjectException,
                                  ExpressionEvaluationException;

    /**
     * Sets the {@link DataOutputAssociationImpl} instance that is used to fill this
     * object.
     *
     * @param outputAssoc
     *            the {@link DataOutputAssociationImpl} instance that is used to
     *            fill this object.
     * @param activity
     *            the {@link DataOnlyActivityImpl} instance to which the
     *            {@link DataOutputAssociationImpl} instance belongs.
     */
    void setDataOutputAssociation(DataOutputAssociationImpl outputAssoc,
                                  DataOnlyActivityImpl activity);
}
