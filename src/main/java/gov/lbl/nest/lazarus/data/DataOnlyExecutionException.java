package gov.lbl.nest.lazarus.data;

/**
 * This class throw when the {@link DataOnlyActivityImpl#execute(DataCell)}
 * method fails.
 *
 * @author patton
 */
public class DataOnlyExecutionException extends
                                        Exception {

    /**
     * Used by serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     */
    public DataOnlyExecutionException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public DataOnlyExecutionException(String message,
                                      Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Creates an instance of this class.
     *
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public DataOnlyExecutionException(Throwable cause) {
        super(cause);
    }
}
