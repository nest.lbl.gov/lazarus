package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.ItemAwareElement;
import gov.lbl.nest.lazarus.structure.ItemDefinition;
import gov.lbl.nest.lazarus.structure.NamedElement;

/**
 * This class implements the {@link ItemAwareElement} interface for this
 * package.
 *
 * @author patton
 */
public class ItemAwareElementImpl extends
                                  NamedElement implements
                                  ItemAwareElement {

    /**
     * The {@link ItemDefinition} whose structure defines the type of the item this
     * object provides.
     */
    private final ItemDefinitionImpl itemSubject;

    /**
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param itemSubject
     *            the {@link ItemDefinition} whose structure defines the type of the
     *            item this object provides.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type. If <code>null</code> will use value from itemSubject.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public ItemAwareElementImpl(String name,
                                String identity,
                                ItemDefinitionImpl itemSubject,
                                Boolean collection) throws ClassNotFoundException {
        super(name,
              identity);
        if (null == collection || null == itemSubject) {
            this.itemSubject = itemSubject;
        } else {
            boolean local = collection.booleanValue();
            if (local != itemSubject.isCollection()) {
                throw new IllegalStateException("Mismatch in isCollection attribute between \"" + name
                                                + "\" and its itemSubject");
            }
            this.itemSubject = itemSubject;
        }
    }

    @Override
    public final ItemDefinitionImpl getItemSubject() {
        return itemSubject;
    }
}
