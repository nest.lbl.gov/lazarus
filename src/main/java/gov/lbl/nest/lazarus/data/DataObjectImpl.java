package gov.lbl.nest.lazarus.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;

/**
 * This class implements the {@link DataObject} interface for this package.
 *
 * @author patton
 */
public class DataObjectImpl extends
                            ItemAwareElementImpl implements
                            DataObject,
                            DataInputSourceImpl,
                            DataOutputTargetImpl {

    /**
     * The collection of @link DataInputAssociationImpl} instances that use this
     * object as a source.
     */
    List<DataInputAssociationImpl> outgoingAssocs = new ArrayList<>();

    /**
     * The collection of {@link DataoutputAssociationImpl} instances that is used to
     * fill this object, indexed by the identity of their
     * {@link DataOnlyActivityImpl} instance.
     */
    HashMap<String, DataOutputAssociationImpl> incomingAssocs = new HashMap<>();

    /**
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param itemSubject
     *            the {@link ItemDefinitionImpl} whose structure defines the type of
     *            the item this object provides.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type. If <code>null</code> will use value from itemSubject.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public DataObjectImpl(String name,
                          String identity,
                          ItemDefinitionImpl itemSubject,
                          Boolean collection) throws ClassNotFoundException {
        super(name,
              identity,
              itemSubject,
              collection);
    }

    @Override
    public void addInputAssociation(DataInputAssociationImpl inputAssoc) {
        outgoingAssocs.add(inputAssoc);
    }

    @Override
    public Object getValueAsSource(DataCell cell) {
        return cell.getValue(getIdentity());
    }

    @Override
    public void pullValue(DataCell cell) throws DataObjectException,
                                         ExpressionEvaluationException {
        final DataOutputAssociationImpl incomingAssoc = incomingAssocs.get((cell.getActivity()).getIdentity());
        cell.setValue(getIdentity(),
                      incomingAssoc.getValue(cell));
    }

    @Override
    public void setDataOutputAssociation(DataOutputAssociationImpl outputAssoc,
                                         DataOnlyActivityImpl activity) {
        final String identity = activity.getIdentity();
        if (incomingAssocs.containsKey(identity)) {
            throw new IllegalStateException("Data Output Association already set for this activity");
        }
        incomingAssocs.put(identity,
                           outputAssoc);
    }

}
