package gov.lbl.nest.lazarus.data;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.NamedElement;
import gov.lbl.nest.lazarus.structure.OutputSet;

/**
 * This class implements the {@link OutputSet} interface for this package.
 *
 * @author patton
 */
public class OutputSetImpl extends
                           NamedElement implements
                           OutputSet {

    /**
     * The mapping to return for an empty InputSet or an empty OutputSet.
     */
    private static final Map<String, Object> EMPTY_IO = Collections.emptyMap();

    /**
     * The collection of {@link DataOutputImpl} instances that make up this object.
     */
    private final List<DataOutputImpl> dataOutputs;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param dataOutputs
     *            the collection of {@link DataOutputImpl} instances that make up
     *            this object.
     */
    public OutputSetImpl(String name,
                         String identity,
                         List<DataOutputImpl> dataOutputs) {
        super(name,
              identity);
        this.dataOutputs = dataOutputs;
    }

    /**
     * Returns a map of <code>null</code> values, index by their {@link DataOutput}
     * instance name.
     *
     * @param cell
     *            the {@link DataCell} instance for which the {@link DataInput}
     *            instances should be returned.
     *
     * @return the map of <code>null</code> values, index by their
     *         {@link DataOutput} instance name.
     */
    Map<String, Object> getActiveOutputs(DataCell cell) {
        if (dataOutputs.isEmpty()) {
            return EMPTY_IO;
        }
        final Map<String, Object> results = new HashMap<>(dataOutputs.size());
        final List<DataOutputImpl> activeOutputs = cell.getActiveOutputs(dataOutputs);
        for (DataOutput dataOutput : activeOutputs) {
            final DataOutput dataOutputToUse;
            dataOutputToUse = dataOutput;
            results.put(dataOutputToUse.getName(),
                        null);
        }
        return results;
    }

    @Override
    public List<DataOutputImpl> getDataOutputs() {
        return dataOutputs;
    }

    /**
     * Sets the {@link DataOutputImpl} instances to those values supplied.
     *
     * @param cell
     *            the {@link DataCell} instance for which the {@link DataOutput}
     *            instances should be filled.
     * @param outputs
     *            the {@link Object} instances containing the values with which to
     *            fill the {@link DataOutputImpl} instances.
     *
     * @throws DataObjectException
     *             when the outputs can not be set.
     * @throws ExpressionEvaluationException
     *             when the Loop Cardinality can not be evaluated.
     */
    void setActiveOutputs(DataCell cell,
                          Map<String, Object> outputs) throws DataObjectException,
                                                       ExpressionEvaluationException {
        if (outputs.isEmpty()) {
            return;
        }

        final Set<DataOutputTargetImpl> targets = new HashSet<>();
        for (DataOutputImpl dataOutput : dataOutputs) {
            targets.addAll(dataOutput.getTargets());

            final Object value = outputs.get(dataOutput.getName());
            if (null != value) {
                final DataOutputImpl dataOutputToUse;
                dataOutputToUse = dataOutput;
                dataOutputToUse.setValue(cell,
                                         value);
            }
        }

        for (DataOutputTargetImpl target : targets) {
            target.pullValue(cell);
        }
    }
}
