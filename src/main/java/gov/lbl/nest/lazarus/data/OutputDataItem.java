package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.ItemDefinition;

/**
 * This class extends the {@link DataOutputImpl} class in order to act as the
 * output of a branch of a multiple instance loop.
 *
 * @author patton
 *
 */
public class OutputDataItem extends
                            DataOutputImpl {

    /**
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param loopDataOutput
     *            the collection of {@link DataOutput} instances that will be used
     *            as outputs to the loop instances.
     * @param itemSubject
     *            the {@link ItemDefinition} whose structure defines the type of the
     *            item this object provides.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type. If <code>null</code> will use value from itemSubject.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public OutputDataItem(String name,
                          String identity,
                          DataOutputSourceImpl loopDataOutput,
                          ItemDefinitionImpl itemSubject,
                          Boolean collection) throws ClassNotFoundException {
        super(name,
              identity,
              itemSubject,
              collection);
    }

}
