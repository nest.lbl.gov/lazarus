/**
 * This package provides the interfaces and classes that implement the flow of
 * date within a BPMN workflow.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * <li>{@link org.slf4j}</li>
 * <li>{@link gov.lbl.nest.common.tasks}</li>
 * <li>{@link gov.lbl.nest.lazarus.digraph}</li>
 * <li>{@link gov.lbl.nest.lazarus.structure}</li>
 * </ul>
 */
package gov.lbl.nest.lazarus.data;