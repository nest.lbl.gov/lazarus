package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;

/**
 * This interface extends the {@link Expression} interface for this package.
 *
 * @author patton
 *
 * @param <T>
 *            the type to which this Object evaluates.
 */
public interface DataExpressionImpl<T extends Object> extends
                                   Expression<T> {

    /**
     * Returns the evaluation of this object, with the supplied {@link DataCell}
     * instance, using the specified arguments.
     *
     * @param cell
     *            the {@link DataCell} instance within which to return this object's
     *            value.
     * @param clazz
     *            the {@link Class} which should be returned by this method.
     *
     * @return the evaluation of this object, with the supplied {@link DataCell}
     *         instance, using the specified arguments.
     *
     * @throws ExpressionEvaluationException
     *             when the is a problem evaluation this object.
     */
    T evaluate(DataCell cell,
               Class<T> clazz) throws ExpressionEvaluationException;

    /**
     * Returns the evaluation of this object, with the supplied {@link DataCell}
     * instance, using the specified arguments.
     *
     * @param cell
     *            the {@link DataCell} instance within which to return this object's
     *            value.
     * @param arguments
     *            the arguments, if any, to use when evaluating this Object.
     * @param clazz
     *            the {@link Class} which should be returned by this method.
     *
     * @return the evaluation of this object, with the supplied {@link DataCell}
     *         instance, using the specified arguments.
     *
     * @throws ExpressionEvaluationException
     *             when the is a problem evaluation this object.
     */
    T evaluate(DataCell cell,
               Object[] arguments,
               Class<T> clazz) throws ExpressionEvaluationException;
}
