package gov.lbl.nest.lazarus.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataInputSource;
import gov.lbl.nest.lazarus.structure.DataInputTarget;
import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.DataOutputSource;
import gov.lbl.nest.lazarus.structure.DataOutputTarget;
import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.InputSet;
import gov.lbl.nest.lazarus.structure.ItemDefinition;
import gov.lbl.nest.lazarus.structure.OutputSet;

/**
 *
 * @author patton
 *
 */
public abstract class StructureImplFactory extends
                                           gov.lbl.nest.lazarus.control.StructureImplFactory {

    @Override
    public DataInputImpl createDataInput(String name,
                                         String identity,
                                         ItemDefinition itemSubject,
                                         Boolean collection) throws ClassNotFoundException {
        return new DataInputImpl(name,
                                 identity,
                                 (ItemDefinitionImpl) itemSubject,
                                 collection);
    }

    @Override
    public DataInputAssociationImpl createDataInputAssociation(String identity,
                                                               Collection<? extends DataInputSource> sources,
                                                               DataInputTarget target) {
        final List<DataInputSourceImpl> inputs = new ArrayList<>();
        for (DataInputSource source : sources) {
            inputs.add((DataInputSourceImpl) source);
        }
        return new DataInputAssociationImpl(identity,
                                            inputs,
                                            (DataInputTargetImpl) target);
    }

    @Override
    public DataObjectImpl createDataObject(String name,
                                           String identity,
                                           ItemDefinition itemSubject,
                                           Boolean collection) throws ClassNotFoundException {
        return new DataObjectImpl(name,
                                  identity,
                                  (ItemDefinitionImpl) itemSubject,
                                  collection);
    }

    @Override
    public DataOutputImpl createDataOutput(String name,
                                           String identity,
                                           ItemDefinition itemSubject,
                                           Boolean collection) throws ClassNotFoundException {
        return new DataOutputImpl(name,
                                  identity,
                                  (ItemDefinitionImpl) itemSubject,
                                  collection);
    }

    @Override
    public DataOutputAssociationImpl createDataOutputAssociation(String identity,
                                                                 Collection<? extends DataOutputSource> sources,
                                                                 DataOutputTarget target) {
        final List<DataOutputSourceImpl> outputs = new ArrayList<>();
        for (DataOutputSource source : sources) {
            outputs.add((DataOutputSourceImpl) source);
        }
        return new DataOutputAssociationImpl(identity,
                                             outputs,
                                             (DataOutputTargetImpl) target);
    }

    @Override
    public InputOutputSpecificationImpl createInputOutputSpecification(String identity,
                                                                       Collection<? extends DataInput> dataInputs,
                                                                       Collection<? extends DataOutput> dataOutputs,
                                                                       Collection<? extends InputSet> inputSets,
                                                                       Collection<? extends OutputSet> outputSets) {

        final List<DataInputImpl> inputs = new ArrayList<>();
        for (DataInput dataInput : dataInputs) {
            inputs.add((DataInputImpl) dataInput);
        }

        final List<DataOutputImpl> outputs = new ArrayList<>();
        for (DataOutput dataOutput : dataOutputs) {
            outputs.add((DataOutputImpl) dataOutput);
        }

        final List<InputSetImpl> inSets = new ArrayList<>();
        for (InputSet inputSet : inputSets) {
            inSets.add((InputSetImpl) inputSet);
        }

        final List<OutputSetImpl> outSets = new ArrayList<>();
        for (OutputSet outputSet : outputSets) {
            outSets.add((OutputSetImpl) outputSet);
        }

        return new InputOutputSpecificationImpl(identity,
                                                inputs,
                                                outputs,
                                                inSets,
                                                outSets);
    }

    @Override
    public InputSetImpl createInputSet(String name,
                                       String identity,
                                       Collection<? extends DataInput> dataInputs) {
        final List<DataInputImpl> inputs = new ArrayList<>();
        for (DataInput dataInput : dataInputs) {
            inputs.add((DataInputImpl) dataInput);
        }
        return new InputSetImpl(name,
                                identity,
                                inputs);
    }

    @Override
    public ItemDefinitionImpl createItemDefinition(String identity,
                                                   String structure,
                                                   Boolean isCollection) throws ClassNotFoundException {
        return new ItemDefinitionImpl(identity,
                                      structure,
                                      isCollection);
    }

    @Override
    public MessageImpl createMessage(String name,
                                     String identity,
                                     ItemDefinition itemRef) {
        return new MessageImpl(name,
                               identity,
                               (ItemDefinitionImpl) itemRef);
    }

    @Override
    public MultiInstanceLoopCharacteristicsImpl createMultiInstanceLoopCharacteristics(String identity,
                                                                                       Expression<Integer> loopCardinality,
                                                                                       DataInputTarget loopDataInputRef,
                                                                                       DataOutputSource loopDataOutputRef,
                                                                                       DataInput inputDataItem,
                                                                                       DataOutput outputDataItem,
                                                                                       Expression<Boolean> completionCondition) {
        return new MultiInstanceLoopCharacteristicsImpl(identity,
                                                        (DataExpressionImpl<Integer>) loopCardinality,
                                                        (DataInputTargetImpl) loopDataInputRef,
                                                        (DataOutputSourceImpl) loopDataOutputRef,
                                                        (DataInputImpl) inputDataItem,
                                                        (DataOutputImpl) outputDataItem,
                                                        (DataExpressionImpl<Boolean>) completionCondition);
    }

    @Override
    public OutputSetImpl createOutputSet(String name,
                                         String identity,
                                         Collection<? extends DataOutput> dataOutputs) {
        final List<DataOutputImpl> outputs = new ArrayList<>();
        for (DataOutput dataOutput : dataOutputs) {
            outputs.add((DataOutputImpl) dataOutput);
        }
        return new OutputSetImpl(name,
                                 identity,
                                 outputs);
    }
}
