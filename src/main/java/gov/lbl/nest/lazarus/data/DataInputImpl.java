package gov.lbl.nest.lazarus.data;

import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.ItemDefinition;

/**
 * This class implements the {@link DataInput} interface for this package.
 *
 * @author patton
 */
public class DataInputImpl extends
                           ItemAwareElementImpl implements
                           DataInput,
                           DataInputSourceImpl,
                           DataInputTargetImpl {

    /**
     * The collection of @link DataInputAssociationImpl} instances that use this
     * object as a source.
     */
    List<DataInputAssociationImpl> outgoingAssocs = new ArrayList<>();

    /**
     * The {@link DataInputAssociationImpl} instance that is used to fill this
     * object.
     */
    DataInputAssociationImpl incomingAssoc;

    /**
     * The effective {@link ItemDefinition} instance of this object. If none has
     * explicitly specified this is derived from the
     * {@link DataInputAssociationImpl} targeting this object.
     */
    ItemDefinition effectiveItem;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param itemSubject
     *            the {@link ItemDefinition} whose structure defines the type of the
     *            item this object provides.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type. If <code>null</code> will use value from itemSubject.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public DataInputImpl(String name,
                         String identity,
                         ItemDefinitionImpl itemSubject,
                         Boolean collection) throws ClassNotFoundException {
        super(name,
              identity,
              itemSubject,
              collection);
    }

    @Override
    public final void addInputAssociation(DataInputAssociationImpl inputAssoc) {
        outgoingAssocs.add(inputAssoc);
    }

    @Override
    public Object getValue(DataCell cell) throws DataObjectException,
                                          ExpressionEvaluationException {
        if (null == incomingAssoc) {
            return null;
        }
        Object result = incomingAssoc.getValue(cell);
        return result;
    }

    @Override
    public final Object getValueAsSource(DataCell cell) throws DataObjectException,
                                                        ExpressionEvaluationException {
        return getValue(cell);
    }

    @Override
    public final void setDataInputAssociation(DataInputAssociationImpl inputAssoc) {
        this.incomingAssoc = inputAssoc;
        final ItemDefinition itemSubject = getItemSubject();
        if (null != itemSubject) {
            effectiveItem = itemSubject;
        } else if (null == inputAssoc.getAssignment() && null == inputAssoc.getTransformation()) {
            List<? extends DataInputSourceImpl> sources = inputAssoc.getSources();
            if (1 == sources.size()) {
                DataInputSourceImpl source = sources.get(0);
                effectiveItem = source.getItemSubject();
            }
        }
    }
}
