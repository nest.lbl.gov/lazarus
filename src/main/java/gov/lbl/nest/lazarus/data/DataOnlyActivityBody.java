package gov.lbl.nest.lazarus.data;

import java.util.Map;

import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataOutput;

/**
 * This interface is used by any object that consumes and produces data within a
 * workflow.
 *
 * @author patton
 */
public interface DataOnlyActivityBody {

    /**
     * Consumes the input data in order to produces the output data.
     *
     * @param inputs
     *            the collection of {@link DataInput} instances that are the inputs
     *            to performing the responsibilities of this Object. If this is
     *            <code>null</code> then no InputOutputSpecification was specified
     *            for this Object.
     * @param outputs
     *            the collection of {@link DataOutput} instances that are to be
     *            filled with the outputs from performing the responsibilities of
     *            this Object.
     */
    void execute(Map<String, Object> inputs,
                 Map<String, Object> outputs);
}
