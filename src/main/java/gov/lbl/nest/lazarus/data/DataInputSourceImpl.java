package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.DataInputSource;

/**
 * This interface extends the {@link DataInputSource} interface so that is can
 * be used to read its value when required.
 *
 * @author patton
 */
public interface DataInputSourceImpl extends
                                     DataSource,
                                     DataInputSource {

    /**
     * Adds the specified {@link DataInputAssociationImpl} instance to this object.
     *
     * @param inputAssoc
     *            the {@link DataInputAssociationImpl} instance to be added.
     */
    void addInputAssociation(DataInputAssociationImpl inputAssoc);

}
