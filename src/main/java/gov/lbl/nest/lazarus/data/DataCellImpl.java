package gov.lbl.nest.lazarus.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.DataOutput;

/**
 * This class is used to isolate a data flow within a {@link DataScope}
 * instance.
 *
 * @author patton
 */
public abstract class DataCellImpl implements
                                   DataCell {

    /**
     * The {@link DataOnlyActivityImpl} instance with which this object exists.
     */
    private final DataOnlyActivityImpl activity;

    /**
     * The cached values of {@link DataOutputImpl} objects.
     */
    private final Map<String, Object> dataOutputs = new HashMap<>();

    /**
     * The collection of identities of {@link DataOutputImpl} instances that are
     * recoverable.
     *
     * Currently only collections need be recoverable. This may change in the
     * future.
     */
    private List<String> recoverableCollections;

    /**
     * The {@link DataScope} instance to which the object belongs.
     */
    private final DataScope scope;

    /**
     * Creates an instance of this class.
     *
     * @param scope
     *            the {@link DataScope} instance to which the object belongs.
     * @param activity
     *            the {@link DataOnlyActivityImpl} instance with which this object
     *            exists.
     */
    public DataCellImpl(DataScope scope,
                        DataOnlyActivityImpl activity) {
        this.activity = activity;
        this.scope = scope;
    }

    @Override
    public final void clear(String identity) {
        dataOutputs.clear();
    }

    @Override
    public final List<DataInputImpl> getActiveInputs(List<DataInputImpl> dataInputs) {
        return dataInputs;
    }

    @Override
    public final List<DataOutputImpl> getActiveOutputs(List<DataOutputImpl> dataOutputs) {
        return dataOutputs;
    }

    @Override
    public DataOnlyActivityImpl getActivity() {
        return activity;
    }

    /**
     * Returns the {@link DataScope} instance to which the object belongs.
     *
     * @return the {@link DataScope} instance to which the object belongs.
     */
    protected final DataScope getDataScope() {
        return scope;
    }

    @Override
    public final Object getOutput(String identity) {
        synchronized (dataOutputs) {
            return dataOutputs.get(identity);
        }
    }

    @Override
    public final Object getValue(String identity) {
        return scope.getValueByIdentity(identity);
    }

    @Override
    public void setOutput(String identity,
                          Object value) throws DataObjectException {
        synchronized (dataOutputs) {
            dataOutputs.put(identity,
                            value);
        }
    }

    @Override
    public void setRecoverableOutput(String identity,
                                     Collection<?> values) throws DataObjectException {
        addRecoveredOutput(identity,
                           values);
        createOrReplaceRecoverableOutputValues(identity,
                                               values);
    }

    /**
     * Adds a recovered value for a {@link DataOutput} instance.
     *
     * @param identity
     *            the identity of the {@link DataOutput} instance that was recovered
     * @param values
     *            the collection of Values of the {@link DataOutput} instance that
     *            was recovered.
     *
     * @throws DataObjectException
     *             when the value can not be written successfully.
     */
    protected void addRecoveredOutput(String identity,
                                      Collection<?> values) throws DataObjectException {
        if (null == recoverableCollections) {
            recoverableCollections = new ArrayList<>();
        }
        if (!recoverableCollections.contains(identity)) {
            recoverableCollections.add(identity);
        }
        setOutput(identity,
                  values);
    }

    @Override
    public void setValue(String identity,
                         Object value) throws DataObjectException {
        scope.setValue(identity,
                       value);
    }

    /**
     * Stores the set values in persistent storage.
     *
     * @param identity
     *            the identity of the {@link DataObject} instances being stored.
     * @param values
     *            the collection of Values of the {@link DataObject} instances being
     *            stored.
     *
     * @throws DataObjectException
     *             when the value can not be written successfully.
     */
    protected abstract void createOrReplaceRecoverableOutputValues(String identity,
                                                                   Collection<?> values) throws DataObjectException;
}
