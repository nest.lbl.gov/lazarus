package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.BaseElementImpl;
import gov.lbl.nest.lazarus.structure.ItemDefinition;

/**
 * This class implements the {@link ItemDefinition} interface for this package.
 *
 * @author patton
 */
public class ItemDefinitionImpl extends
                                BaseElementImpl implements
                                ItemDefinition {

    /**
     * Returns the {@link Class} instance for the supplied class name.
     *
     * @param structure
     *            the name of the {@link Class} instance to return.
     *
     * @return the {@link Class} instance for the supplied class name.
     *
     * @throws ClassNotFoundException
     */
    private static Class<?> getClass(String structure) throws ClassNotFoundException {
        if (null == structure) {
            return null;
        }
        return ((ItemDefinitionImpl.class).getClassLoader()).loadClass(structure);
    }

    /**
     * <code>true</code> if this object is a collection of its structure type.
     */
    private final boolean collection;

    /**
     * The {@link Class} whose structure defines the type of the item this object
     * represents.
     */
    private final Class<?> structure;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the identity of this Object.
     * @param structure
     *            the {@link Class} whose structure defines the type of the item
     *            this object represents.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    ItemDefinitionImpl(String identity,
                       Class<?> structure,
                       Boolean collection) throws ClassNotFoundException {
        super(identity);
        this.collection = (null != collection && collection.equals(Boolean.TRUE));
        this.structure = structure;
    }

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the identity of this Object.
     * @param structure
     *            the name of the {@link Class} whose structure defines the type of
     *            the item this object represents.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public ItemDefinitionImpl(String identity,
                              String structure,
                              Boolean collection) throws ClassNotFoundException {
        this(identity,
             getClass(structure),
             collection);
    }

    @Override
    public Class<?> getStructure() {
        return structure;
    }

    @Override
    public boolean isCollection() {
        return collection;
    }

}
