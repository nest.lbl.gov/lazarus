package gov.lbl.nest.lazarus.data;

import java.util.List;

import gov.lbl.nest.lazarus.structure.Assignment;
import gov.lbl.nest.lazarus.structure.BaseElementImpl;
import gov.lbl.nest.lazarus.structure.DataAssociation;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.DataOutputSource;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.FormalExpression;

/**
 * This class implements the {@link DataAssociation} interface for this package.
 *
 * @param <S>
 *            the class which defines the type of the sources.
 * @param <T>
 *            the class which defines the type of the target.
 *
 * @author patton
 */
public class DataAssociationImpl<S extends DataSource, T> extends
                                BaseElementImpl implements
                                DataAssociation<S, T> {

    /**
     * The {@link Assignment} instance, if any, that maps the data from the source
     * to the target.
     */
    private final Assignment assignment;

    /**
     * The collection of source instances that this object uses to create the target
     * data.
     */
    private final List<? extends S> sources;

    /**
     * The target instance of this object.
     */
    private final T target;

    /**
     * The {@link FormalExpressionImpl} instance, if any, that transforms the
     * sources into the target.
     */
    private final FormalExpressionImpl<Object> transformation;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of {@link DataOutputSource} instance that this
     *            object uses to create the target data.
     * @param target
     *            the target instance of this object.
     * @param assignment
     *            the {@link Assignment} instance, if any, that maps the data from
     *            the source to the target.
     * @param transformation
     *            the {@link FormalExpression} instance, if any, that transforms the
     *            sources into the target.
     */
    protected DataAssociationImpl(String identity,
                                  List<? extends S> sources,
                                  T target,
                                  Assignment assignment,
                                  FormalExpressionImpl<Object> transformation) {
        super(identity);
        this.assignment = assignment;
        this.target = target;
        this.sources = sources;
        this.transformation = transformation;
    }

    @Override
    public Assignment getAssignment() {
        return assignment;
    }

    @Override
    public List<? extends S> getSources() {
        return sources;
    }

    @Override
    public T getTarget() {
        return target;
    }

    @Override
    public FormalExpression<Object> getTransformation() {
        return transformation;
    }

    /**
     * Returns the target value for this object.
     *
     * @param cell
     *            the {@link DataCell} instance within which to return this object's
     *            value.
     *
     * @return the target value for this object.
     *
     * @throws DataObjectException
     *             when a data object can not be accessed.
     * @throws ExpressionEvaluationException
     *             when the Loop Cardinality can not be evaluated.
     */
    Object getValue(DataCell cell) throws DataObjectException,
                                   ExpressionEvaluationException {
        if (null == assignment && null == transformation) {
            if (null == sources || sources.isEmpty()) {
                return null;
            }
            // Limited to a single source if there is no transformation.
            return (sources.get(0)).getValueAsSource(cell);
        }
        if (null == transformation) {
            throw new UnsupportedOperationException("There is no support for assignment at present");
        }
        FormalExpressionImpl<Object> typedTransformation = transformation;
        final Object[] arguments = new Object[sources.size()];
        int index = 0;
        for (S source : sources) {
            arguments[index++] = source.getValueAsSource(cell);
        }
        try {
            return typedTransformation.evaluate(cell,
                                                arguments,
                                                Object.class);
        } catch (ExpressionEvaluationException e) {
            throw new DataObjectException(e);
        }
    }
}
