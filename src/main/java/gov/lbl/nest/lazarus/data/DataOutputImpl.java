package gov.lbl.nest.lazarus.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.ItemDefinition;
import gov.lbl.nest.lazarus.structure.OutputSet;

/**
 * This class implements the {@link OutputSet} interface for this package.
 *
 * @author patton
 */
public class DataOutputImpl extends
                            ItemAwareElementImpl implements
                            DataOutput,
                            DataOutputTargetImpl,
                            DataOutputSourceImpl {

    /**
     * The {@link DataoutputAssociationImpl} instance that is used to fill this
     * object.
     */
    DataOutputAssociationImpl incomingAssoc;

    /**
     * The collection of @link DataOutputAssociationImpl} instances that use this
     * object as a source.
     */
    List<DataOutputAssociationImpl> outgoingAssocs = new ArrayList<>();

    /**
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param itemSubject
     *            the {@link ItemDefinition} whose structure defines the type of the
     *            item this object provides.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type. If <code>null</code> will use value from itemSubject.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public DataOutputImpl(String name,
                          String identity,
                          ItemDefinitionImpl itemSubject,
                          Boolean collection) throws ClassNotFoundException {
        super(name,
              identity,
              itemSubject,
              collection);
    }

    @Override
    public final void addOutputAssociation(DataOutputAssociationImpl outputAssoc) {
        outgoingAssocs.add(outputAssoc);
    }

    @Override
    public final Set<? extends DataOutputTargetImpl> getTargets() {
        final Set<DataOutputTargetImpl> results = new HashSet<>();
        for (DataOutputAssociationImpl outputAssoc : outgoingAssocs) {
            results.add(outputAssoc.getTarget());
        }
        return results;
    }

    @Override
    public Object getValueAsSource(DataCell cell) throws DataObjectException,
                                                  ExpressionEvaluationException {
        return cell.getOutput(getIdentity());
    }

    @Override
    public void pullValue(DataCell cell) throws DataObjectException,
                                         ExpressionEvaluationException {
        cell.setOutput(getIdentity(),
                       incomingAssoc.getValue(cell));
    }

    @Override
    public final void setDataOutputAssociation(DataOutputAssociationImpl outputAssoc,
                                               DataOnlyActivityImpl activity) {
        incomingAssoc = outputAssoc;
    }

    /**
     * Sets the value for this object for the specified {@link DataCell} instance.
     *
     * @param cell
     *            the {@link DataCell} instance for which to set this object's
     *            value.
     * @param value
     *            the cached value for this object to be cached.
     *
     * @throws DataObjectException
     *             when the value of the {@link DataOutputImpl} instance can not be
     *             set.
     */
    void setValue(DataCell cell,
                  Object value) throws DataObjectException {
        cell.setOutput(getIdentity(),
                       value);
    }
}
