package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.DataInputAssociation;
import gov.lbl.nest.lazarus.structure.DataInputTarget;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.DataOutputTarget;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;

/**
 * This interface extends the {@link DataOutputTarget} interface so that is can
 * be used to fill in its value when required.
 *
 * @author patton
 */
public interface DataInputTargetImpl extends
                                     DataInputTarget {

    /**
     * Returns the value of this object after being resolved through its
     * {@link DataInputAssociation} instance.
     *
     * @param cell
     *            the {@link DataCell} instance with which to return this object's
     *            value.
     *
     * @return the value of this object after being resolved through its
     *         {@link DataInputAssociation} instance.
     *
     * @throws DataObjectException
     *             when a data object can not be set.
     * @throws ExpressionEvaluationException
     *             when the Loop Cardinality can not be evaluated.
     */
    Object getValue(DataCell cell) throws DataObjectException,
                                   ExpressionEvaluationException;

    /**
     * Returns the value of this object with the specified {@link DataCell} instance
     *
     * @param cell
     *            the {@link DataCell} instance with which to return this object's
     *            value.
     *
     * @return the value of this object with the specified {@link DataCell} instance
     *
     * @throws DataObjectException
     *             when the value can not be returned.
     * @throws ExpressionEvaluationException
     *             when the Loop Cardinality can not be evaluated.
     */
    Object getValueAsSource(DataCell cell) throws DataObjectException,
                                           ExpressionEvaluationException;

    /**
     * Sets the {@link DataInputAssociationImpl} instance that is used to fill this
     * object.
     *
     * @param inputAssoc
     *            the {@link DataInputAssociationImpl} instance that is used to fill
     *            this object.
     */
    void setDataInputAssociation(DataInputAssociationImpl inputAssoc);
}
