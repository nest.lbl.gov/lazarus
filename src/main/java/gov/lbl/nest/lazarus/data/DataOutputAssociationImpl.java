package gov.lbl.nest.lazarus.data;

import java.util.List;

import gov.lbl.nest.lazarus.structure.Assignment;
import gov.lbl.nest.lazarus.structure.DataOutputAssociation;
import gov.lbl.nest.lazarus.structure.DataOutputSource;
import gov.lbl.nest.lazarus.structure.FormalExpression;

/**
 * This class implements the {@link DataOutputAssociation} interface for this
 * package.
 *
 * @author patton
 */
public class DataOutputAssociationImpl extends
                                       DataAssociationImpl<DataOutputSourceImpl, DataOutputTargetImpl> implements
                                       DataOutputAssociation<DataOutputSourceImpl, DataOutputTargetImpl> {

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of sources that this object uses to create the
     *            target data.
     * @param target
     *            the {@link DataOutputTargetImpl} instance of the target of this
     *            object.
     */
    public DataOutputAssociationImpl(String identity,
                                     List<? extends DataOutputSourceImpl> sources,
                                     DataOutputTargetImpl target) {
        this(identity,
             sources,
             target,
             null,
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of {@link DataOutputSource} instance that this
     *            object uses to create the target data.
     * @param target
     *            the {@link DataOutputTargetImpl} instance of the target of this
     *            object.
     * @param assignment
     *            the {@link Assignment} instance, if any, that maps the data from
     *            the source to the target.
     */
    public DataOutputAssociationImpl(String identity,
                                     List<? extends DataOutputSourceImpl> sources,
                                     DataOutputTargetImpl target,
                                     Assignment assignment) {
        this(identity,
             sources,
             target,
             assignment,
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of {@link DataOutputSource} instance that this
     *            object uses to create the target data.
     * @param target
     *            the {@link DataOutputTargetImpl} instance of the target of this
     *            object.
     * @param assignment
     *            the {@link Assignment} instance, if any, that maps the data from
     *            the source to the target.
     * @param transformation
     *            the {@link FormalExpression} instance, if any, that transforms the
     *            sources into the target.
     */
    private DataOutputAssociationImpl(String identity,
                                      List<? extends DataOutputSourceImpl> sources,
                                      DataOutputTargetImpl target,
                                      Assignment assignment,
                                      FormalExpressionImpl<Object> transformation) {
        super(identity,
              sources,
              target,
              assignment,
              transformation);
        for (DataOutputSourceImpl source : sources) {
            source.addOutputAssociation(this);
        }
    }

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of sources that this object uses to create the
     *            target data.
     * @param target
     *            the {@link DataOutputTargetImpl} instance of the target of this
     *            object.
     * @param transformation
     *            the {@link FormalExpression} instance, if any, that transforms the
     *            sources into the target.
     */
    public DataOutputAssociationImpl(String identity,
                                     List<? extends DataOutputSourceImpl> sources,
                                     DataOutputTargetImpl target,
                                     FormalExpressionImpl<Object> transformation) {
        this(identity,
             sources,
             target,
             null,
             transformation);
    }

    /**
     * Sets the {@link DataOnlyActivityImpl} instance to which this object belongs.
     *
     * @param activity
     *            the {@link DataOnlyActivityImpl} instance to which this object
     *            belongs.
     */
    void setActivity(DataOnlyActivityImpl activity) {
        getTarget().setDataOutputAssociation(this,
                                             activity);
    }

}
