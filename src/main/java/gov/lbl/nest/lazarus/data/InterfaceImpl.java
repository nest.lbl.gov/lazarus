package gov.lbl.nest.lazarus.data;

import java.util.Collection;

import gov.lbl.nest.lazarus.structure.Interface;
import gov.lbl.nest.lazarus.structure.NamedElement;

/**
 * This class implements the {@link Interface} interface for this package.
 *
 * @author patton
 */
public class InterfaceImpl extends
                           NamedElement implements
                           Interface {

    /**
     * The name of the external system to which call outs will be made.
     */
    private final String implementationRef;

    /**
     * The collection of {@link OperationImpl} instances that can be performed by
     * the external system.
     */
    private final Collection<OperationImpl> operations;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param operations
     *            the collection of {@link OperationImpl} instances that can be
     *            performed by the external system.
     * @param implementationRef
     *            the name of the external system to which call outs will be made.
     */
    public InterfaceImpl(String name,
                         String identity,
                         Collection<OperationImpl> operations,
                         String implementationRef) {
        super(name,
              identity);
        this.implementationRef = implementationRef;
        this.operations = operations;
    }

    @Override
    public String getImplementationRef() {
        return implementationRef;
    }

    @Override
    public Collection<OperationImpl> getOperations() {
        return operations;
    }

}
