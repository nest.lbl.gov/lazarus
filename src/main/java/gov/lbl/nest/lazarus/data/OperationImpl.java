package gov.lbl.nest.lazarus.data;

import java.lang.reflect.Method;
import java.util.Map;

import gov.lbl.nest.lazarus.structure.Interface;
import gov.lbl.nest.lazarus.structure.Message;
import gov.lbl.nest.lazarus.structure.NamedElement;
import gov.lbl.nest.lazarus.structure.Operation;

/**
 * This class implements the {@link Operation} interface for this package.
 *
 * @author patton
 */
public abstract class OperationImpl extends
                                    NamedElement implements
                                    Operation {

    /**
     * The name of the method within the external system that performs this
     * operation.
     */
    private final String implementationRef;

    /**
     * The {@link Message} instance that defines the input to this object.
     */
    private final MessageImpl inMessageRef;

    /**
     * The {@link Interface} instance to which this object belongs.
     */
    private InterfaceImpl iface;

    /**
     * The {@link Message} instance, if any, that defines the output to this object.
     */
    private MessageImpl outMessageRef;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param implementationRef
     *            the name of the method within the external system that performs
     *            this operation.
     * @param inMessageRef
     *            the {@link Message} instance that defines the input to this
     *            object.
     * @param outMessageRef
     *            the {@link Message} instance, if any, that defines the output to
     *            this object.
     */
    protected OperationImpl(String name,
                            String identity,
                            String implementationRef,
                            MessageImpl inMessageRef,
                            MessageImpl outMessageRef) {
        super(name,
              identity);
        this.implementationRef = implementationRef;
        this.inMessageRef = inMessageRef;
        this.outMessageRef = outMessageRef;

    }

    /**
     * Executes this operation and returns a result.
     *
     * @param inputs
     *            the inputs to use in execution.
     *
     * @return the result of this operation.
     */
    public abstract Object execute(Map<String, Object> inputs);

    @Override
    public final String getImplementationRef() {
        return implementationRef;
    }

    @Override
    public final MessageImpl getInMessageRef() {
        return inMessageRef;
    }

    @Override
    public final InterfaceImpl getInterface() {
        return iface;
    }

    @Override
    public final MessageImpl getOutMessageRef() {
        return outMessageRef;
    }

    /**
     * Returns <code>true</code> when the operation does not return a value.
     *
     * @return <code>true</code> when the operation does not return a value.
     */
    public abstract boolean isVoid();

    /**
     * Adds the supplied {@link InterfaceImpl} instance as the he {@link Object}
     * instance on which to execute this object's method.
     *
     * @param iface
     *            the {@link InterfaceImpl} instance on which to execute the
     *            supplied method.
     *
     * @throws NoSuchMethodException
     *             when no matching {@link Method} instance can be found for the
     *             supplied {@link InterfaceImpl} instance.
     */
    protected void setInterface(InterfaceImpl iface) throws NoSuchMethodException {
        this.iface = iface;
    }
}
