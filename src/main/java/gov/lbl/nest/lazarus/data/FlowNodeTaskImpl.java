package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.tasks.InspectorFactory;
import gov.lbl.nest.common.tasks.ordinal.PriorityRankedTask;
import gov.lbl.nest.lazarus.structure.FlowNodeInspector;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;

/**
 * This class extends the {@link PriorityRankedTask} to include data access for
 * a flow node.
 *
 * @author patton
 */
public abstract class FlowNodeTaskImpl extends
                                       PriorityRankedTask<FlowNodeInspector> implements
                                       FlowNodeTask {

    /**
     * The {@link DataOnlyActivityImpl} instance within which this object is
     * executing.
     */
    private final DataOnlyActivityImpl activity;

    /**
     * The {@link DataCell} instance within which this object is executing.
     */
    private final DataCell cell;

    /**
     * Creates an instance of this class.
     *
     * @param activity
     *            the {@link DataOnlyActivityImpl} instance within which this object
     *            is executing.
     * @param cell
     *            the {@link MultiInstanceDataCell} instance within which this
     *            object is executing.
     * @param factory
     *            the {@link InspectorFactory} used to create new
     *            {@link FlowNodeInspector} instances.
     * @param suspendable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     * @param rank
     *            the "rank" of this element, where object with a higher rank are
     *            preferred.
     * @param priority
     *            the "priority" of this element, where object with are higher
     *            priority a preferred.
     */
    public FlowNodeTaskImpl(DataOnlyActivityImpl activity,
                            DataCell cell,
                            InspectorFactory<FlowNodeInspector> factory,
                            Suspendable suspendable,
                            Integer rank,
                            Integer priority) {
        super(factory,
              suspendable,
              rank,
              priority);
        this.activity = activity;
        this.cell = cell;
    }

    /**
     * Executes this objects' responsibilities.
     *
     * @throws DataOnlyExecutionException
     *             when this method fails.
     */
    protected void execute() throws DataOnlyExecutionException {
        activity.execute(cell);
    }

    /**
     * Returns the {@link DataCell} instance within which this object is executing.
     *
     * @return the {@link DataCell} instance within which this object is executing.
     */
    protected DataCell getCell() {
        return cell;
    }
}
