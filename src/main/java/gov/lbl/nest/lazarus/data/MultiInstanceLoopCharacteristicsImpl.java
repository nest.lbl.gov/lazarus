package gov.lbl.nest.lazarus.data;

import java.util.List;

import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.MultiInstanceLoopCharacteristics;

/**
 * This class implements the {@link MultiInstanceLoopCharacteristics} interface
 * for this package.
 *
 * @author patton
 */
public class MultiInstanceLoopCharacteristicsImpl extends
                                                  LoopCharacteristicsImpl implements
                                                  MultiInstanceLoopCharacteristics {

    /**
     * The {@link DataExpressionImpl} instance that defines the number of loops to
     * be executed.
     */
    private final DataExpressionImpl<Integer> loopCardinality;

    /**
     * The collection of {@link DataInput} instance that will be used as inputs to
     * the loop instances.
     */
    private final DataInputTargetImpl loopDataInputRef;

    /**
     * The collection of {@link DataOutput} instance that will be used a outputs to
     * the loop instances.
     */
    private final DataOutputSourceImpl loopDataOutputRef;

    /**
     * The {@link DataInput} instance that will provide the input to each loop
     * instance and is an element of the loop data input collection.
     */
    private final DataInputImpl inputDataItem;

    /**
     * The {@link DataOutput} instance into which each loop instance with write its
     * output and is an element of the loop data output collection.
     */
    private final DataOutputImpl outputDataItem;

    /**
     * The {@link Expression} which, when <code>true</code>, will terminate the
     * remaining loop instances.
     */
    private final DataExpressionImpl<Boolean> completionCondition;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     *
     *
     * @param loopCardinality
     *            The {@link DataExpressionImpl} instance that defines the number of
     *            loops to be executed.
     *
     * @param loopDataInputRef
     *            The collection of {@link DataInput} instance that will be used as
     *            inputs to the loop instances.
     *
     * @param loopDataOutputRef
     *            The collection of {@link DataOutput} instance that will be used a
     *            outputs to the loop instances.
     *
     * @param inputDataItem
     *            The {@link DataInput} instance that will provide the input to each
     *            loop instance and is an element of the loop data input collection.
     *
     * @param outputDataItem
     *            The {@link DataOutput} instance into which each loop instance with
     *            write its output and is an element of the loop data output
     *            collection.
     *
     * @param completionCondition
     *            The {@link Expression} which, when <code>true</code>, will
     *            terminate the remaining loop instances.
     */
    public MultiInstanceLoopCharacteristicsImpl(String identity,
                                                DataExpressionImpl<Integer> loopCardinality,
                                                DataInputTargetImpl loopDataInputRef,
                                                DataOutputSourceImpl loopDataOutputRef,
                                                DataInputImpl inputDataItem,
                                                DataOutputImpl outputDataItem,
                                                DataExpressionImpl<Boolean> completionCondition) {
        super(identity);
        this.loopCardinality = loopCardinality;
        this.loopDataInputRef = loopDataInputRef;
        this.loopDataOutputRef = loopDataOutputRef;
        this.inputDataItem = inputDataItem;
        this.outputDataItem = outputDataItem;
        this.completionCondition = completionCondition;
    }

    @Override
    public DataExpressionImpl<Boolean> getCompletionCondition() {
        return completionCondition;
    }

    @Override
    public DataInputImpl getInputDataItem() {
        return inputDataItem;
    }

    @Override
    public DataExpressionImpl<Integer> getLoopCardinality() {
        return loopCardinality;
    }

    @Override
    public DataInputTargetImpl getLoopDataInputRef() {
        return loopDataInputRef;
    }

    @Override
    public DataOutputSourceImpl getLoopDataOutputRef() {
        return loopDataOutputRef;
    }

    @Override
    public DataOutputImpl getOutputDataItem() {
        return outputDataItem;
    }

    /**
     * @param cell
     *            the {@link DataCell} instance in which to evaluate the size.
     *
     * @return the size of the loop.
     *
     * @throws DataFlowException
     *             when the size of this loop instance can not be returned.
     */
    public int size(DataCell cell) {
        if (null == loopDataInputRef) {
            try {
                return loopCardinality.evaluate(cell,
                                                Integer.class);
            } catch (ExpressionEvaluationException e) {
                throw new DataFlowException("Could not evaluate Expressions",
                                            e);
            }
        }
        Object object;
        try {
            object = loopDataInputRef.getValueAsSource(cell);
        } catch (DataObjectException
                 | ExpressionEvaluationException e) {
            throw new DataFlowException("Could not get the size of the input collection",
                                        e);
        }
        return ((List<?>) object).size();
    }
}
