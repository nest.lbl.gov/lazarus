package gov.lbl.nest.lazarus.data;

import java.util.Set;

import gov.lbl.nest.lazarus.structure.DataOutputSource;

/**
 * This interface extends the {@link DataOutputSource} interface so that is can
 * be used to fill in its value when required.
 *
 * @author patton
 */
public interface DataOutputSourceImpl extends
                                      DataSource,
                                      DataOutputSource {

    /**
     * Adds the specified {@link DataOutputAssociationImpl} instance to this object.
     *
     * @param dataOutputAssociationImpl
     *            the {@link DataOutputAssociationImpl} instance to be added.
     */
    void addOutputAssociation(DataOutputAssociationImpl dataOutputAssociationImpl);

    /**
     * Returns the set of {@link DataOutputTargetImpl} instances that are connected
     * to this object by one or more {@link DataOutputAssociationImpl} instances.
     *
     * @return the set of {@link DataOutputTargetImpl} instances that are connected
     *         to this object by one or more {@link DataOutputAssociationImpl}
     *         instances.
     */
    Set<? extends DataOutputTargetImpl> getTargets();
}
