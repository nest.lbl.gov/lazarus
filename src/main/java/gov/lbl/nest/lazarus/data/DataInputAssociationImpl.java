package gov.lbl.nest.lazarus.data;

import java.util.List;

import gov.lbl.nest.lazarus.structure.Assignment;
import gov.lbl.nest.lazarus.structure.DataInputAssociation;
import gov.lbl.nest.lazarus.structure.DataInputSource;
import gov.lbl.nest.lazarus.structure.FormalExpression;

/**
 * This class implements the {@link DataInputAssociation} interface for this
 * package.
 *
 * @author patton
 */
public class DataInputAssociationImpl extends
                                      DataAssociationImpl<DataInputSourceImpl, DataInputTargetImpl> implements
                                      DataInputAssociation<DataInputSourceImpl, DataInputTargetImpl> {

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of sources that this object uses to create the
     *            target data.
     * @param target
     *            the {@link DataInputTargetImpl} instance of the target of this
     *            object.
     */
    public DataInputAssociationImpl(String identity,
                                    List<? extends DataInputSourceImpl> sources,
                                    DataInputTargetImpl target) {
        this(identity,
             sources,
             target,
             null,
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of {@link DataInputSource} instance that this
     *            object uses to create the target data.
     * @param target
     *            the {@link DataInputTargetImpl} instance of the target of this
     *            object.
     * @param assignment
     *            the {@link Assignment} instance, if any, that maps the data from
     *            the source to the target.
     */
    public DataInputAssociationImpl(String identity,
                                    List<? extends DataInputSourceImpl> sources,
                                    DataInputTargetImpl target,
                                    Assignment assignment) {
        this(identity,
             sources,
             target,
             assignment,
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of {@link DataInputSource} instance that this
     *            object uses to create the target data.
     * @param target
     *            the {@link DataInputTargetImpl} instance of the target of this
     *            object.
     * @param assignment
     *            the {@link Assignment} instance, if any, that maps the data from
     *            the source to the target.
     * @param transformation
     *            the {@link FormalExpression} instance, if any, that transforms the
     *            sources into the target.
     */
    private DataInputAssociationImpl(String identity,
                                     List<? extends DataInputSourceImpl> sources,
                                     DataInputTargetImpl target,
                                     Assignment assignment,
                                     FormalExpressionImpl<Object> transformation) {
        super(identity,
              sources,
              target,
              assignment,
              transformation);
        for (DataInputSourceImpl source : sources) {
            source.addInputAssociation(this);
        }
        target.setDataInputAssociation(this);
    }

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of sources that this object uses to create the
     *            target data.
     * @param target
     *            the {@link DataInputTargetImpl} instance of the target of this
     *            object.
     * @param transformation
     *            the {@link FormalExpression} instance, if any, that transforms the
     *            sources into the target.
     */
    public DataInputAssociationImpl(String identity,
                                    List<? extends DataInputSourceImpl> sources,
                                    DataInputTargetImpl target,
                                    FormalExpressionImpl<Object> transformation) {
        this(identity,
             sources,
             target,
             null,
             transformation);
    }

}
