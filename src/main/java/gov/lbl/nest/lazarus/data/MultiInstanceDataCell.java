package gov.lbl.nest.lazarus.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;

/**
 * This class extends the {@link DataCell} interface be used in a single branch
 * of a multiple instance execution.
 *
 * @author patton
 */
public class MultiInstanceDataCell implements
                                   DataCell {

    /**
     * The {@link DataCell} instance that this object is wrapping.
     */
    private final DataCell cell;

    /**
     * The {@link MultiInstanceLoopCharacteristicsImpl} instance characterizing the
     * behaviour of this object.
     */
    private final MultiInstanceLoopCharacteristicsImpl characteristics;

    /**
     * The cached values of {@link DataOutputImpl} objects.
     */
    private final Map<String, Object> dataOutputs = new HashMap<>();

    /**
     * The index of the branch within the the multiple instantiation.
     */
    private final int index;

    /**
     * Creates an instance of this class.
     *
     * @param characteristics
     *            the {@link MultiInstanceLoopCharacteristicsImpl} instance
     *            characterizing the behaviour of this object. collection.
     * @param cell
     *            the {@link DataCell} instance that this object is wrapping.
     * @param index
     *            the index of the branch within the the multiple instantiation.
     */
    public MultiInstanceDataCell(MultiInstanceLoopCharacteristicsImpl characteristics,
                                 DataCell cell,
                                 int index) {
        this.cell = cell;
        this.characteristics = characteristics;
        this.index = index;
    }

    @Override
    public void clear(String identity) {
        cell.clear(identity);
    }

    @Override
    public List<DataInputImpl> getActiveInputs(List<DataInputImpl> dataInputs) {
        final DataInputTargetImpl loopDataInputRef = characteristics.getLoopDataInputRef();
        final List<DataInputImpl> results = new ArrayList<>();
        for (DataInputImpl dataInput : dataInputs) {
            if (loopDataInputRef != dataInput) {
                results.add(dataInput);
            }
        }
        return results;
    }

    @Override
    public List<DataOutputImpl> getActiveOutputs(List<DataOutputImpl> dataOutputs) {
        final DataOutputSourceImpl loopDataOutputRef = characteristics.getLoopDataOutputRef();
        final List<DataOutputImpl> results = new ArrayList<>();
        for (DataOutputImpl dataOutput : dataOutputs) {
            if (loopDataOutputRef != dataOutput) {
                results.add(dataOutput);
            }
        }
        return results;
    }

    @Override
    public DataOnlyActivityImpl getActivity() {
        return cell.getActivity();
    }

    /**
     * Returns the index of the branch within the the multiple instantiation.
     *
     * @return the index of the branch within the the multiple instantiation.
     */
    public int getIndex() {
        return index;
    }

    @Override
    public Object getOutput(String identity) throws DataObjectException,
                                             ExpressionEvaluationException {
        DataOutputImpl outputDataItem = characteristics.getOutputDataItem();
        if (identity.equals(outputDataItem.getIdentity())) {
            synchronized (dataOutputs) {
                return dataOutputs.get(identity);
            }
        }

        final DataOutputSourceImpl loopDataOutputRef = characteristics.getLoopDataOutputRef();

        if (identity.equals(loopDataOutputRef.getIdentity())) {

            outputDataItem.pullValue(this);

            @SuppressWarnings("unchecked")
            final List<Object> loopOutput = (List<Object>) (cell.getOutput(identity));
            final List<Object> loopOutputToUse;
            if (null == loopOutput) {
                @SuppressWarnings("unchecked")
                final List<Object> recoveredLoop = (List<Object>) cell.getOutput(identity);
                if (null == recoveredLoop) {
                    final int size = characteristics.size(this);
                    final List<Object> nulls = Collections.nCopies(size,
                                                                   null);
                    loopOutputToUse = new ArrayList<>(size);
                    loopOutputToUse.addAll(nulls);
                } else {
                    loopOutputToUse = recoveredLoop;
                }
            } else {
                loopOutputToUse = loopOutput;
            }
            loopOutputToUse.set(index,
                                outputDataItem.getValueAsSource(this));
            cell.setRecoverableOutput(identity,
                                      loopOutputToUse);
            return loopOutputToUse;
        }

        synchronized (dataOutputs) {
            return cell.getOutput(identity);
        }
    }

    @Override
    public Object getValue(String identity) {
        return cell.getValue(identity);
    }

    @Override
    public void setOutput(String identity,
                          Object value) throws DataObjectException {
        DataOutputImpl outputDataItem = characteristics.getOutputDataItem();
        if (identity.equals(outputDataItem.getIdentity())) {
            synchronized (dataOutputs) {
                dataOutputs.put(identity,
                                value);
                return;
            }
        }
        cell.setOutput(identity,
                       value);
    }

    @Override
    public void setRecoverableOutput(String identity,
                                     Collection<?> values) throws DataObjectException {
        cell.setRecoverableOutput(identity,
                                  values);
    }

    @Override
    public void setValue(String identity,
                         Object value) throws DataObjectException {
        cell.setValue(identity,
                      value);
    }
}
