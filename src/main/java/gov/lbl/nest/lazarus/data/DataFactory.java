package gov.lbl.nest.lazarus.data;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.lazarus.structure.DataObject;

/**
 * This class needs to be extends to create appropriate instances of the
 * {@link DataScope} class.
 *
 * @author patton
 */
public abstract class DataFactory {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DataFactory.class);

    /**
     * true if the implementation class of this Object has already been logged.
     */
    private static AtomicBoolean LOGGING = new AtomicBoolean();

    /**
     * The {@link ServiceLoader} instance that will load the {@link DataFactory}
     * implementation.
     */
    private final static ServiceLoader<DataFactory> DATA_FACTORY_LOADER = ServiceLoader.load(DataFactory.class,
                                                                                             DataFactory.class.getClassLoader());

    /**
     * Creates an instance of the {@link DataFactory} class.
     *
     * @return the created instance of the {@link DataFactory} class.
     */
    public static DataFactory getDataFactory() {
        for (DataFactory factory : DATA_FACTORY_LOADER) {
            final Class<?> clazz = factory.getClass();
            try {
                final Constructor<?> constructor = clazz.getConstructor();
                DataFactory result = (DataFactory) constructor.newInstance();
                if (!(LOGGING.get())) {
                    LOG.debug("Using \"" + clazz.getCanonicalName()
                              + "\" as "
                              + DataFactory.class.getName()
                              + " implementation");
                    LOGGING.set(true);
                }
                return result;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    /**
     * Create an instance of the {@link DataScope} class.
     *
     * @param dataObjects
     *            the {@link DataObject} instances for the process, indexed by their
     *            name.
     * @param guidance
     *            the {@link Object} instance that provides guidance to the
     *            persistence system on the storage of data items.
     *
     * @return the instance of the {@link DataScope} class.
     *
     * @throws DataFlowException
     *             when there is a problem with the the backing store so the
     *             requested {@link DataScope} instance can not be created.
     */
    public abstract DataScope createDataScope(Collection<? extends DataObjectImpl> dataObjects,
                                              Object guidance);

    /**
     * Create an instance of the {@link DataScope} class.
     *
     * @param dataObjects
     *            the {@link DataObject} instances for the process, indexed by their
     *            name.
     * @param guidance
     *            the {@link Object} instance that provides guidance to the
     *            persistence system on the storage of data items.
     *
     * @return the instance of the {@link DataScope} class.
     *
     * @throws DataFlowException
     *             when there is a problem with the the backing store so the
     *             requested {@link DataScope} instance can not be created.
     */
    public abstract DataScope recoverDataScope(Collection<? extends DataObjectImpl> dataObjects,
                                               Object guidance);
}
