package gov.lbl.nest.lazarus.data;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.InputSet;
import gov.lbl.nest.lazarus.structure.NamedElement;

/**
 * This class implements the {@link InputSet} interface for this package.
 *
 * @author patton
 */
public class InputSetImpl extends
                          NamedElement implements
                          InputSet {

    /**
     * The mapping to return for an empty InputSet or an empty OutputSet.
     */
    private static final Map<String, Object> EMPTY_IO = Collections.emptyMap();

    /**
     * The collection of {@link DataInputImpl} instances that make up this object.
     */
    private final List<DataInputImpl> dataInputs;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param dataInputs
     *            the collection of {@link ItemAwareElementImpl} instances that make
     *            up this object.
     */
    public InputSetImpl(String name,
                        String identity,
                        List<DataInputImpl> dataInputs) {
        super(name,
              identity);
        this.dataInputs = dataInputs;
    }

    /**
     * Returns the map of active input Objects, index by their {@link DataInput}
     * instance name.
     *
     * @param cell
     *            the {@link DataCell} instance for which the {@link DataInput}
     *            instances should be returned.
     *
     * @return the map of active input Objects, index by their {@link DataInput}
     *         instance id.
     *
     * @throws DataObjectException
     *             when the data can not be returned.
     * @throws ExpressionEvaluationException
     *             when the Loop Cardinality can not be evaluated.
     */
    Map<String, Object> getActiveInputs(DataCell cell) throws DataObjectException,
                                                       ExpressionEvaluationException {
        if (dataInputs.isEmpty()) {
            return EMPTY_IO;
        }
        final Map<String, Object> results = new HashMap<>(dataInputs.size());
        final List<DataInputImpl> activeInputs = cell.getActiveInputs(dataInputs);
        for (final DataInputImpl dataInput : activeInputs) {
            results.put(dataInput.getName(),
                        dataInput.getValueAsSource(cell));
        }
        return results;

    }

    @Override
    public List<DataInputImpl> getDataInputs() {
        return dataInputs;
    }
}
