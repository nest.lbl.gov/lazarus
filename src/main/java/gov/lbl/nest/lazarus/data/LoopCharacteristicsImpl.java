package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.BaseElementImpl;
import gov.lbl.nest.lazarus.structure.LoopCharacteristics;

/**
 * This class implements the {@link LoopCharacteristics} interface for this
 * package.
 *
 * @author patton
 */
public class LoopCharacteristicsImpl extends
                                     BaseElementImpl implements
                                     LoopCharacteristics {

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     */
    public LoopCharacteristicsImpl(String identity) {
        super(identity);
    }

}
