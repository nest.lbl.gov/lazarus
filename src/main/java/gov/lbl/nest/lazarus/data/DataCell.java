package gov.lbl.nest.lazarus.data;

import java.util.Collection;
import java.util.List;

import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.FlowNode;

/**
 * This interface is used to isolate a data flow within a {@link FlowNode}
 * instance.
 *
 * @author patton
 */
public interface DataCell {

    /**
     * Clears the cached value for the identified {@link DataOutputImpl} instance.
     *
     * @param identity
     *            the identity of the {@link DataOutputImpl} whose value should be
     *            cleared.
     */
    void clear(String identity);

    /**
     * Returns the collection of active {@link DataInputImpl} instances supplied set
     * of instances.
     *
     * @param dataInputs
     *            the collection of {@link DataInputImpl} instance from which to
     *            select the active ones.
     *
     * @return the collection of active {@link DataInputImpl} instances supplied set
     *         of instances.
     */
    List<DataInputImpl> getActiveInputs(List<DataInputImpl> dataInputs);

    /**
     * Returns the collection of active {@link DataOutputImpl} instances supplied
     * set of instances.
     *
     * @param dataOutputs
     *            the collection of {@link DataOutputImpl} instance from which to
     *            select the active ones.
     *
     * @return the collection of active {@link DataOutputImpl} instances supplied
     *         set of instances.
     */
    List<DataOutputImpl> getActiveOutputs(List<DataOutputImpl> dataOutputs);

    /**
     * Returns the {@link DataOnlyActivityImpl} instance with which this object
     * exists.
     *
     * @return the {@link DataOnlyActivityImpl} instance with which this object
     *         exists.
     */
    DataOnlyActivityImpl getActivity();

    /**
     * Returns the value for the identified {@link DataOutputImpl} instance.
     *
     * @param identity
     *            the identity of the {@link DataOutputImpl} whose value should be
     *            returned.
     *
     * @return the value for the identified {@link DataOutputImpl} instance.
     *
     * @throws DataObjectException
     *             when the value of the {@link DataOutputImpl} instance can not be
     *             returned.
     * @throws ExpressionEvaluationException
     *             when the Loop Cardinality can not be evaluatedwith this object.
     */
    Object getOutput(String identity) throws DataObjectException,
                                      ExpressionEvaluationException;

    /**
     * Returns the value of the {@link DataObject} instance whose identity is
     * supplied.
     *
     * @param identity
     *            the identity of the {@link DataObject} instance whose value should
     *            be returned.
     *
     * @return the value of the {@link DataObject} instance whose identity is
     *         supplied.
     */
    Object getValue(String identity);

    /**
     * Sets the value for the identified {@link DataOutputImpl} instance.
     *
     * @param identity
     *            the identity of the {@link DataOutputImpl} whose value should be
     *            set.
     * @param value
     *            the value to be set.
     *
     * @throws DataObjectException
     *             when the value of the {@link DataOutputImpl} instance can not be
     *             set.
     */
    void setOutput(String identity,
                   Object value) throws DataObjectException;

    /**
     * Stores the set values in persistent storage.
     *
     * @param identity
     *            the identity of the {@link DataObject} instances being stored.
     * @param values
     *            the collection of Values of the {@link DataObject} instances being
     *            stored.
     *
     * @throws DataObjectException
     *             when the value can not be written successfully.
     */
    void setRecoverableOutput(String identity,
                              Collection<?> values) throws DataObjectException;

    /**
     * Sets the value for the identified {@link DataObject} instance.
     *
     * @param identity
     *            the identity of the {@link DataObject} whose value should be set.
     * @param value
     *            the value to be set.
     *
     * @throws DataObjectException
     *             when the value of the {@link DataObject} instance can not be set.
     */
    void setValue(String identity,
                  Object value) throws DataObjectException;
}
