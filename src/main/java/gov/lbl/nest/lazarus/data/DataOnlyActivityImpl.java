package gov.lbl.nest.lazarus.data;

import java.util.List;
import java.util.Map;

import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataInputAssociation;
import gov.lbl.nest.lazarus.structure.DataOnlyActivity;
import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.DataOutputAssociation;
import gov.lbl.nest.lazarus.structure.InputOutputSpecification;

/**
 * This class implements the {@link DataOnlyActivity} interface for this
 * package.
 *
 * @author patton
 *
 */
public class DataOnlyActivityImpl implements
                                  DataOnlyActivity {

    /**
     * The {@link DataOnlyActivityBody} instance that carries out this object's
     * responsibilities.
     */
    private final DataOnlyActivityBody body;

    /**
     * The list of {@link DataInputAssociation} instances used to fill the
     * {@link DataInput} elements of the {@link InputOutputSpecification} instance.
     */
    private final List<? extends DataInputAssociationImpl> dataInputAssociations;

    /**
     * The list of {@link DataOutputAssociation} instances used to fill the
     * {@link DataOutput} elements of the {@link InputOutputSpecification} instance.
     */
    private final List<? extends DataOutputAssociationImpl> dataOutputAssociations;

    /**
     * The unique identity of this object with in the context of the structure.
     */
    private final String identity;

    /**
     * The {@link InputOutputSpecification} instance that maps data in and out of
     * this object.
     */
    private final InputOutputSpecificationImpl ioSpecification;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param body
     *            the {@link DataOnlyActivityBody} instance that carries out this
     *            object's responsibilities.
     * @param ioSpecification
     *            the {@link InputOutputSpecificationImpl} instance that maps data
     *            in and out of this object.
     * @param dataInputAssociations
     *            the list of {@link DataInputAssociation} instances used to fill
     *            the {@link DataInput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param dataOutputAssociations
     *            the list of {@link DataOutputAssociation} instances used to fill
     *            the {@link DataOutput} elements of the
     *            {@link InputOutputSpecification} instance.
     */
    public DataOnlyActivityImpl(String identity,
                                DataOnlyActivityBody body,
                                InputOutputSpecificationImpl ioSpecification,
                                List<? extends DataInputAssociationImpl> dataInputAssociations,
                                List<? extends DataOutputAssociationImpl> dataOutputAssociations) {
        this.body = body;
        this.dataInputAssociations = dataInputAssociations;
        this.dataOutputAssociations = dataOutputAssociations;
        this.identity = identity;
        this.ioSpecification = ioSpecification;

        // Loop _must_ be called after the identity has been established.
        for (DataOutputAssociationImpl association : dataOutputAssociations) {
            association.setActivity(this);
        }
    }

    /**
     * Execute this object's responsibilities.
     *
     * @param cell
     *            The {@link DataCell} instance within which to execute those - *
     *            responsibilities.
     *
     * @throws DataOnlyExecutionException
     *             when this method fails.
     */
    public void execute(DataCell cell) throws DataOnlyExecutionException {
        final Map<String, Object> activeInputs;
        final Map<String, Object> outputs;
        final OutputSetImpl outputSet;
        if (null != ioSpecification) {
            final InputSetImpl inputSet = ioSpecification.getInputSet(cell);
            outputSet = ioSpecification.getOutputSet(cell,
                                                     inputSet);
            outputs = outputSet.getActiveOutputs(cell);
            try {
                activeInputs = inputSet.getActiveInputs(cell);
                body.execute(activeInputs,
                             outputs);
                outputSet.setActiveOutputs(cell,
                                           outputs);
            } catch (Throwable t) {
                throw new DataOnlyExecutionException(t);
            }
        }

    }

    /**
     * Returns the {@link DataOnlyActivityBody} instance that carries out this
     * object's responsibilities.
     *
     * @return the {@link DataOnlyActivityBody} instance that carries out this
     *         object's responsibilities.
     */
    public DataOnlyActivityBody getBody() {
        return body;
    }

    @Override
    public List<? extends DataInputAssociationImpl> getDataInputAssociations() {
        return dataInputAssociations;
    }

    @Override
    public List<? extends DataOutputAssociationImpl> getDataOutputAssociations() {
        return dataOutputAssociations;
    }

    /**
     * Returns the unique identity of this object with in the context of the
     * structure.
     *
     * @return the unique identity of this object with in the context of the
     *         structure.
     */
    String getIdentity() {
        return identity;
    }

    @Override
    public InputOutputSpecification getIoSpecification() {
        return ioSpecification;
    }
}
