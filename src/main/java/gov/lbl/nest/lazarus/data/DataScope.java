package gov.lbl.nest.lazarus.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.DataObjectException;

/**
 * This class encapsulates the data for a single instance of workflow.
 *
 * @author patton
 */
public abstract class DataScope {

    /**
     * The collection of identities of {@link DataObject} instances that are
     * collections.
     */
    private final List<String> collections = new ArrayList<>();

    /**
     * The collection {@link DataObject} instances contained in this object, indexed
     * by their name.
     */
    private final Map<String, DataObjectImpl> dataObjects = new HashMap<>();

    /**
     * The value of each {@link DataObject} instance within this object.
     */
    private final Map<String, Object> values = new HashMap<>();

    /**
     * Creates an instance of this class.
     *
     * @param dataObjects
     *            the {@link DataObject} instances for the process, indexed by their
     *            name.
     */
    public DataScope(Collection<? extends DataObjectImpl> dataObjects) {
        if (null == dataObjects || dataObjects.isEmpty()) {
            return;
        }
        setDataObjects(dataObjects);
    }

    /**
     * Returns a new {@link DataCell} instance within the scope of this object.
     *
     * <b>Note:</b> This should only be used when no control scope is present. If
     * one does exist it will have its own method for creating a {@link DataCell}
     * instance appropriate for itself.
     *
     * @param activity
     *            the {@link DataOnlyActivityImpl} instance with which this object
     *            exists.
     * @param guidance
     *            the {@link Object} instance that provides guidance to the
     *            persistence system on the storage of data items.
     *
     * @return the new {@link DataCell} instance within the scope of this object.
     * 
     * @throws DataObjectException
     *             when existing persisted data output value can not be written
     *             successfully.
     */
    public abstract DataCell createDataCell(DataOnlyActivityImpl activity,
                                            Object guidance) throws DataObjectException;

    /**
     * Stores the set values in persistent storage.
     *
     * @param identity
     *            the identity of the {@link DataObject} instances being stored.
     * @param values
     *            the collection of Values of the {@link DataObject} instances being
     *            stored.
     *
     * @throws DataObjectException
     *             when the value can not be written successfully.
     */
    protected abstract void createOrReplaceCollectionValues(String identity,
                                                            Collection<?> values) throws DataObjectException;

    /**
     * Stores the set value in persistent storage.
     *
     * @param identity
     *            the identity of the {@link DataObject} instance being stored.
     * @param value
     *            the Value of the {@link DataObject} instance being stored.
     *
     * @throws DataObjectException
     *             when the value can not be written successfully.
     */
    protected abstract void createOrReplaceValue(String identity,
                                                 Object value) throws DataObjectException;

    /**
     * Returns the collection {@link DataObject} instances contained in this object.
     *
     * @return the collection {@link DataObject} instances contained in this object.
     */
    protected final Collection<? extends DataObjectImpl> getDataObjects() {
        return dataObjects.values();
    }

    /**
     * Returns the value of the {@link DataObject} instance whose identity is
     * supplied.
     *
     * @param identity
     *            the identity of the {@link DataObject} instance whose value should
     *            be returned.
     *
     * @return the value of the {@link DataObject} instance whose identity is
     *         supplied.
     */
    public final Object getValueByIdentity(String identity) {
        synchronized (values) {
            return values.get(identity);
        }
    }

    /**
     * Returns the current values for the {@link DataObject} instances contained in
     * this object.
     *
     * @return the current values for the {@link DataObject} instances contained in
     *         this object.
     */
    public final Map<String, Object> getValues() {
        final Map<String, Object> results = new HashMap<>();
        for (DataObjectImpl dataObject : dataObjects.values()) {
            results.put(dataObject.getName(),
                        getValueByIdentity(dataObject.getIdentity()));
        }
        return results;
    }

    /**
     * Sets the collection {@link DataObject} instances contained in this object
     * 
     * @param dataObjects
     *            the collection {@link DataObject} instances contained in this
     *            object.
     */
    protected final void setDataObjects(Collection<? extends DataObjectImpl> dataObjects) {
        for (DataObjectImpl dataObject : dataObjects) {
            final ItemDefinitionImpl itemDefinition = dataObject.getItemSubject();
            if (itemDefinition.isCollection()) {
                collections.add(dataObject.getIdentity());
            }
            this.dataObjects.put(dataObject.getName(),
                                 dataObject);
        }
    }

    /**
     * Sets the initial values for the {@link DataObject} instances contained in
     * this object.
     *
     * @param objectValues
     *            the initial values for {@link DataObject} instances, index by
     *            their names.
     *
     * @throws DataObjectException
     *             when the supplied {@link DataObject} instance can not be set.
     *             This must <b>not</b> be thrown when there are no values supplied,
     *             either by a <code>null</code> value or an empty {@link Map}.
     */
    public final void setDataObjectValues(Map<String, Object> objectValues) throws DataObjectException {
        if (null != objectValues) {
            for (String name : objectValues.keySet()) {
                final Object value = objectValues.get(name);
                final DataObjectImpl dataObject = dataObjects.get(name);
                if (null == dataObject) {
                    throw new DataObjectException("There is no DataObject named \"" + name
                                                  + "\"");
                }
                setValue(dataObject.getIdentity(),
                         value);
            }
        }
    }

    /**
     * Recovers the values for the {@link DataObject} instances contained in this
     * object. This method does <b>not</b> persist these values as they have been
     * previously persisted.
     *
     * @param objectValues
     *            the recovered values for {@link DataObject} instances, index by
     *            their identities.
     */
    protected final void setRecoveredValues(Map<String, Object> objectValues) {
        if (null != objectValues) {
            for (String identity : objectValues.keySet()) {
                final Object value = objectValues.get(identity);
                values.put(identity,
                           value);
            }
        }
    }

    /**
     * Sets the value for the identified {@link DataObject} instance.
     *
     * @param identity
     *            the identity of the {@link DataObject} whose value should be set.
     * @param value
     *            the value to be set.
     *
     * @throws DataObjectException
     *             when the value of the {@link DataObject} instance can not be set.
     */
    void setValue(String identity,
                  Object value) throws DataObjectException {
        synchronized (values) {
            if (collections.contains(identity)) {
                createOrReplaceCollectionValues(identity,
                                                (Collection<?>) value);
            } else {
                createOrReplaceValue(identity,
                                     value);
            }
            values.put(identity,
                       value);
        }
    }
}
