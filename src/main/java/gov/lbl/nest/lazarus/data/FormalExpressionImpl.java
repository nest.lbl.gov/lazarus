package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.FormalExpression;
import gov.lbl.nest.lazarus.structure.ItemDefinition;

/**
 * This interface extends the {@link DataExpressionImpl} interface so that its
 * return type can be queried.
 *
 * @author patton
 *
 * @param <T>
 *            the type to which this Object evaluates.
 */
public interface FormalExpressionImpl<T> extends
                                     DataExpressionImpl<T>,
                                     FormalExpression<T> {

    /**
     * Returns the {@link ItemDefinition} whose structure defines the type of the
     * item this object provides.
     *
     * @return the {@link ItemDefinition} whose structure defines the type of the
     *         item this object provides.
     */
    ItemDefinition getItemSubject();
}
