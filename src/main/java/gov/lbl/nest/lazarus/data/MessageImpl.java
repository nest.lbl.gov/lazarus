package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.ItemDefinition;
import gov.lbl.nest.lazarus.structure.Message;
import gov.lbl.nest.lazarus.structure.NamedElement;

/**
 * This interface extends the {@link Message} interface for this package.
 *
 * @author patton
 */
public class MessageImpl extends
                         NamedElement implements
                         Message {

    /**
     * The {@link ItemDefinitionImpl} instance that defines the content of this
     * object.
     */
    private final ItemDefinitionImpl itemRef;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param itemRef
     *            the {@link ItemDefinitionImpl} instance that defines the content
     *            of this object.
     */
    public MessageImpl(String name,
                       String identity,
                       ItemDefinitionImpl itemRef) {
        super(name,
              identity);
        this.itemRef = itemRef;
    }

    @Override
    public ItemDefinition getItemRef() {
        return itemRef;
    }

}
