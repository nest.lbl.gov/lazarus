package gov.lbl.nest.lazarus.data;

import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.ExpressionEvaluationException;
import gov.lbl.nest.lazarus.structure.ItemAwareElement;

/**
 * This interface extends the {@link ItemAwareElement} interface so that its
 * value can be returned.
 *
 * @author patton
 */
public interface DataSource extends
                            ItemAwareElement {

    /**
     * Returns the value of this object with the context of the supplied
     * {@link DataCell} instance.
     *
     * @param cell
     *            the {@link DataCell} instance within which to return this object's
     *            value.
     *
     * @return the value of this object with the context of the supplied
     *         {@link DataCell} instance.
     *
     * @throws DataObjectException
     *             when the requested value can not be returned.
     * @throws ExpressionEvaluationException
     *             when the Loop Cardinality can not be evaluated.
     */
    Object getValueAsSource(DataCell cell) throws DataObjectException,
                                           ExpressionEvaluationException;

}
