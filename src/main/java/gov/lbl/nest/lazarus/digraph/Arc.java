package gov.lbl.nest.lazarus.digraph;

/**
 * This interface defines the methods a arc, or edges,of a digraph must support.
 *
 * @author patton
 */
public interface Arc {

    /**
     * Returns the {@link Node} instance to which this Object is directed.
     *
     * @return the {@link Node} instance to which this Object is directed.
     */
    Node getHead();

}
