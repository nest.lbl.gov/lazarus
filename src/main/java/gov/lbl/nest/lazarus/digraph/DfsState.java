package gov.lbl.nest.lazarus.digraph;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;

/**
 * A current state of the depth first search of the {@link Node} instances.
 *
 * @author patton
 */
public class DfsState {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * True is no loops have been discovered in the graph, that is to say it is a
     * DAG.
     */
    private boolean acyclic = true;

    /**
     * The collection of discovered {@link Node} instances.
     */
    private Collection<Node> discovered = new HashSet<>();

    /**
     * The current "path" from the root node of the graph.
     */
    private Queue<Node> path = Collections.asLifoQueue(new ArrayDeque<Node>());

    /**
     * The sequence in which the {@link Node} instances were visited.
     */
    private List<Node> visited = new ArrayList<>();

    // constructors

    /**
     * Creates an instance of this class.
     */
    DfsState() {
    }

    // instance member method (alphabetic)

    /**
     * Attempts to add the specified {@link Node} to the set of discovered nodes.
     * Automatically adds the specified {@link Node} instance to the list of visited
     * nodes if had not been discovered before.
     *
     * @param node
     *            the {@link Node} instance to attempt to add.
     *
     * @return true if the {@link Node} instance is added, i.e. has not already been
     *         discovered.
     */
    boolean addDiscovered(Node node) {
        if (discovered.add(node)) {
            path.add(node);
            visited.add(node);
            return true;
        }
        return false;
    }

    /**
     * Returns the current depth on the node in the graph.
     *
     * @return the current depth on the node in the graph.
     */
    int getDepth() {
        return path.size() - 1;
    }

    /**
     * Returns the reverse postordering of the Nodes discovered. That is to say a
     * natural linearization of the control flow.
     *
     * @return the reverse postordering of the Nodes discovered.
     */
    public List<Node> getReversePostordering() {
        final List<Node> result = new ArrayList<>();
        for (Node node : visited) {
            if (!result.contains(node)) {
                result.add(node);
            }
        }
        return result;
    }

    /**
     * Returns true is no loops have been discovered in the graph
     *
     * @return true is no loops have been discovered in the graph
     */
    public boolean isAcyclic() {
        return acyclic;
    }

    /**
     * Returns true when the
     *
     * @param node
     *
     * @return
     */
    boolean isInPath(final Node node) {
        boolean result = path.contains(node);
        if (acyclic && result) {
            acyclic = false;
        }
        return result;
    }

    /**
     * Signals to this Object that the specified {@link Node} instance has no
     * successors.
     *
     * @param node
     *            the {@link Node} instance that has no successors.
     */
    void noSuccessors(final Node node) {
        path.remove();
    }

    /**
     * Signals this Object that the visit to the specified {@link Node} instance in
     * completed
     *
     * @param node
     *            the {@link Node} instance whose visit is compete.
     */
    void visitComplete(final Node node) {
        visited.add(node);
        path.remove();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
