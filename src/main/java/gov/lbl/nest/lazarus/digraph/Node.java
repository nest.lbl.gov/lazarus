package gov.lbl.nest.lazarus.digraph;

/**
 * This interface defines the methods a node, or vertex, of a digraph must
 * support.
 *
 * @author patton
 */
public interface Node {

    /**
     *
     * @param dfsState
     *            the current state of the depth first search.
     *
     * @return the updated {@link DfsState} instance.
     */
    DfsState depthFirstSearch(final DfsState dfsState);

    /**
     * Returns the depth of this Object within it Graph, after the Graph's depth
     * first search has completed.
     *
     * @return the depth of this Object within it Graph.
     */
    Integer getDepth();

    /**
     * Returns the identity of this Object.
     *
     * @return the identity of this Object.
     */
    String getIdentity();

    /**
     * Changes the Object's depth by increasing it by the specified amount.
     *
     * @param delta
     *            the amount by which this Object's depth should be increased.
     */
    void increaseDepth(final int delta);
}
