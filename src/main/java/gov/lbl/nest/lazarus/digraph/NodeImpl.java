package gov.lbl.nest.lazarus.digraph;

import java.util.Collection;

/**
 * This class is a basic implementation of the {@link Arc} interface.
 *
 * @author patton
 */
public class NodeImpl implements
                      Node {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Node} implementation that is delegating its responsibilities to
     * this class.
     */
    private final Node delegator;

    /**
     * The depth of this Object within it Graph.
     */
    private Integer depth;

    /**
     * The identity of this Object.
     */
    private final String identity;

    /**
     * The {@link Collection} of {@link Node} instances that are directly reachable
     * from this one.
     */
    private Collection<? extends Arc> successors;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param delegator
     *            the {@link Node} implementation, if any, that is delegating its
     *            responsibilities to this class.
     */
    public NodeImpl(final Node delegator) {
        if (null == delegator) {
            throw new NullPointerException();
        }
        this.delegator = delegator;
        this.identity = null;
    }

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the identity of this Object.
     */
    public NodeImpl(final String identity) {
        this.identity = identity;
        this.delegator = this;
    }

    // instance member method (alphabetic)

    @Override
    public DfsState depthFirstSearch(final DfsState dfsState) {
        final DfsState dfsStateToUse;
        if (null == dfsState) {
            dfsStateToUse = new DfsState();
        } else {
            dfsStateToUse = dfsState;
        }
        if (!dfsStateToUse.addDiscovered(delegator)) {
            if (dfsStateToUse.isInPath(delegator)) {
                return dfsStateToUse;
            }
            int effectiveDepth = dfsStateToUse.getDepth() + 1;
            int delta = effectiveDepth - depth;
            if (0 < delta) {
                delegator.increaseDepth(delta);
            }
            return dfsStateToUse;
        }
        depth = dfsStateToUse.getDepth();
        if (null == successors || successors.isEmpty()) {
            dfsStateToUse.noSuccessors(delegator);
            return dfsStateToUse;
        }
        DfsState current = dfsStateToUse;
        for (Arc arc : successors) {
            current = (arc.getHead()).depthFirstSearch(current);
        }
        current.visitComplete(delegator);
        return current;
    }

    @Override
    public Integer getDepth() {
        return depth;
    }

    @Override
    public String getIdentity() {
        if (this == delegator) {
            return identity;
        }
        return delegator.getIdentity();
    }

    @Override
    public void increaseDepth(int delta) {
        depth = depth + delta;
        if (null == successors || successors.isEmpty()) {
            return;
        }
        for (Arc arc : successors) {
            (arc.getHead()).increaseDepth(delta);
        }
    }

    /**
     * Sets the {@link Collection} of {@link Node} instances that are directly
     * reachable from this one.
     *
     * @param successors
     *            the {@link Collection} of {@link Node} instances that are directly
     *            reachable from this one.
     */
    public void setSuccessors(final Collection<? extends Arc> successors) {
        this.successors = successors;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
