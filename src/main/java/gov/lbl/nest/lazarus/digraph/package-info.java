/**
 * This package provides the interfaces and classes to characterize an DIrected
 * GRAPH.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * </ul>
 */
package gov.lbl.nest.lazarus.digraph;
