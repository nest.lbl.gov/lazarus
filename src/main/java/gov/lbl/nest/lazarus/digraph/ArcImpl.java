package gov.lbl.nest.lazarus.digraph;

/**
 * This class is a basic implementation of the {@link Arc} interface.
 *
 * @author patton
 */
public class ArcImpl implements
                     Arc {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Node} instance to which this Object is directed.
     */
    final private Node head;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param head
     *            the {@link Node} instance to which this Object is directed.
     */
    public ArcImpl(final Node head) {
        this.head = head;
    }

    // instance member method (alphabetic)

    @Override
    public Node getHead() {
        return head;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
