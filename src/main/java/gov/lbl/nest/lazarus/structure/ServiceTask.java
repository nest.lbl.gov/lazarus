package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to make a direct call to a system to execute its
 * responsibilities.
 *
 * @author patton
 *
 */
public interface ServiceTask extends
                             Task {

    /**
     * Returns the {@link Operation} instance that this object will call to execute
     * its responsibilities.
     *
     * @return the {@link Operation} instance that this object will call to execute
     *         its responsibilities.
     */
    Operation getOperation();

}
