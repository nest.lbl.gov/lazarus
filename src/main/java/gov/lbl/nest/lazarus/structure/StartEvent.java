package gov.lbl.nest.lazarus.structure;

/**
 * This class is used to create a new control flow.
 *
 * @author patton
 */
public interface StartEvent extends
                            FlowNode {

}
