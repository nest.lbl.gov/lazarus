package gov.lbl.nest.lazarus.structure;

/**
 * This class represents an expression that can be evaluated.
 *
 * @author patton
 *
 * @param <T>
 *            the type to which this Object evaluates.
 */
public interface Expression<T extends Object> extends
                           BaseElement {

}
