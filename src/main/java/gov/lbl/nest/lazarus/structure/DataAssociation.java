package gov.lbl.nest.lazarus.structure;

import java.util.List;

/**
 * This interface is used to map a set of data onto another data element.
 *
 * @param <S>
 *            the class which defines the type of the sources.
 * @param <T>
 *            the class which defines the type of the target.
 *
 * @author patton
 */
public interface DataAssociation<S, T> extends
                                BaseElement {

    /**
     * Returns the {@link Assignment} instance, if any, that maps the data from the
     * source to the target.
     *
     * @return the {@link Assignment} instance, if any, that maps the data from the
     *         source to the target.
     */
    Assignment getAssignment();

    /**
     * Returns the collection of source instances that this object uses to create
     * the target data.
     *
     * @return the collection of source instances that this object uses to create
     *         the target data.
     */
    List<? extends S> getSources();

    /**
     * Returns the target instance of this object.
     *
     * @return the target instance of this object.
     */
    T getTarget();

    /**
     * Returns the {@link FormalExpression} instance, if any, that transforms the
     * sources into the target.
     *
     * @return the {@link FormalExpression} instance, if any, that transforms the
     *         sources into the target.
     */
    FormalExpression<Object> getTransformation();

}
