package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to manage the execution of a single task that is part
 * of the workflow.
 *
 * @author patton
 *
 */
public interface Task extends
                      Activity {

}
