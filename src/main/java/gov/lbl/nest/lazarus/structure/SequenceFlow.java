package gov.lbl.nest.lazarus.structure;

import gov.lbl.nest.lazarus.digraph.Arc;

/**
 * This is any element in the structure that solely passes a control flow from
 * one element to another.
 *
 * @author patton
 */
public interface SequenceFlow extends
                              Arc,
                              FlowElement {

    /**
     * This interface defines the source of a flow of control.
     *
     * @author patton
     */
    public interface Source {
    }

    /**
     * This interface defines the target of a flow of control.
     *
     * @author patton
     */
    public interface Target {
    }

    /**
     * Returns the boolean {@link Expression} instance, that guards this object.
     *
     * @return the boolean {@link Expression} instance, that guards this object.
     */
    Expression<Boolean> getExpressionImpl();

    /**
     * Returns the source of the flow of control passing through this object.
     *
     * @return the source of the flow of control passing through this object.
     */
    Source getSource();

    /**
     * Returns the target of the flow of control passing through this object.
     *
     * @return the target of the flow of control passing through this object.
     */
    Target getTarget();
}
