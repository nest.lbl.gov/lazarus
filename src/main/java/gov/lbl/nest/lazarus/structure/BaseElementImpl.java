package gov.lbl.nest.lazarus.structure;

/**
 * This class implements the {@link BaseElement} interface for this package.
 *
 * @author patton
 *
 */
public class BaseElementImpl implements
                             BaseElement {

    /**
     * The unique identity of this object with in the context of the structure.
     */
    private String identity;

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     */
    public BaseElementImpl(String identity) {
        this.identity = identity;
    }

    @Override
    public final String getIdentity() {
        return identity;
    }

    /**
     * Sets the unique identity of this object with in the context of the structure.
     * 
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     */
    public void setIdentity(String identity) {
        this.identity = identity;
    }
}
