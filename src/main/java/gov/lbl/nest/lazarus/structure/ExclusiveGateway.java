package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to label gateways that deal with a single flow of
 * control.
 *
 * @author patton
 *
 */
public interface ExclusiveGateway extends
                                  Gateway {

    /**
     * Returns the default {@link SequenceFlow} instance for output if no other is
     * selected.
     *
     * @return the default {@link SequenceFlow} instance for output if no other is
     *         selected.
     */
    SequenceFlow getDefault();
}
