package gov.lbl.nest.lazarus.structure;

/**
 * This class throw when a requested object can not be found or created.
 *
 * @author patton
 */
public class NoSuchObjectException extends
                                   Exception {

    /**
     * Used by serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     */
    public NoSuchObjectException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public NoSuchObjectException(String message,
                                 Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Creates an instance of this class.
     *
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public NoSuchObjectException(Throwable cause) {
        super(cause);
    }
}
