package gov.lbl.nest.lazarus.structure;

/**
 * This interface must be implemented by every concrete element with the
 * structure to give is a unique identifier.
 *
 * @author patton
 */
public interface BaseElement {

    /**
     * Returns the unique identity of this object with in the context of the
     * structure.
     *
     * @return the unique identity of this object with in the context of the
     *         structure.
     */
    String getIdentity();
}