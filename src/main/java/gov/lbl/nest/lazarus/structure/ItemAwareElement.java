package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used by and element that contains a data item.
 *
 * @author patton
 *
 */
public interface ItemAwareElement extends
                                  BaseElement {

    /**
     * Returns the {@link ItemDefinition} whose structure defines the type of the
     * item this object provides.
     *
     * @return the {@link ItemDefinition} whose structure defines the type of the
     *         item this object provides.
     */
    ItemDefinition getItemSubject();

    /**
     * Returns the name of this object.
     *
     * @return the name of this object.
     */
    String getName();
}
