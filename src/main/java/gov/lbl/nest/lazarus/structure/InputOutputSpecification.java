package gov.lbl.nest.lazarus.structure;

import java.util.List;

/**
 * This interface us used to specified the inputs and outputs of an
 * {@link DataOnlyActivity} instance.
 *
 * @author patton
 */
public interface InputOutputSpecification extends
                                          BaseElement {

    /**
     * Returns the list of {@link DataInput} instances that define possible input
     * data.
     *
     * @return the list of {@link DataInput} instances that define possible input
     *         data.
     */
    List<? extends DataInput> getDataInputs();

    /**
     * Returns the list of {@link DataOutput} instances that define possible output
     * data.
     *
     * @return the list of {@link DataOutput} instances that define possible output
     *         data.
     */
    List<? extends DataOutput> getDataOutputs();

    /**
     * Returns the list of {@link InputSet} instances that define possible sets of
     * input data.
     *
     * @return the list of {@link InputSet} instances that define possible sets of
     *         input data.
     */
    List<? extends InputSet> getInputSets();

    /**
     * Returns the list of {@link OutputSet} instances that define possible sets of
     * output data.
     *
     * @return the list of {@link OutputSet} instances that define possible sets of
     *         output data.
     */
    List<? extends OutputSet> getOutputSets();
}
