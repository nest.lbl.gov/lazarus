package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used by any {@link FlowNode} that does something.
 *
 * @author patton
 */
public interface Activity extends
                          ControlOnlyActivity,
                          DataOnlyActivity {

}
