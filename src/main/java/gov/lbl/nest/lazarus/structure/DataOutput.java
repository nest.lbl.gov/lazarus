package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to map data out of an {@link DataOnlyActivity}
 * instance.
 *
 * @author patton
 */
public interface DataOutput extends
                            DataOutputSource,
                            DataOutputTarget,
                            ItemAwareElement {
}
