package gov.lbl.nest.lazarus.structure;

/**
 * This class adds the name property to the {@link BaseElementImpl} class.
 *
 * @author patton
 */
public class NamedElement extends
                          BaseElementImpl {

    /**
     * The name of this object.
     */
    private String name;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     */
    public NamedElement(String name,
                        String identity) {
        super(identity);
        this.name = name;
    }

    /**
     * Returns the name of this object.
     *
     * @return the name of this object.
     */
    public final String getName() {
        return name;
    }

    /**
     * Sets the name of this object.
     *
     * @param name
     *            the name of this object.
     */
    public final void setName(String name) {
        this.name = name;
    }
}
