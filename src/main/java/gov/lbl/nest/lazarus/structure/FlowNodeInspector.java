package gov.lbl.nest.lazarus.structure;

import gov.lbl.nest.common.tasks.Inspector;
import gov.lbl.nest.lazarus.control.ControlScope;
import gov.lbl.nest.lazarus.control.FlowNodeImpl;

/**
 * This interface is used to inspect the execution {@link FlowNodeTask}
 * instances.
 *
 * @author patton
 */
public interface FlowNodeInspector extends
                                   Inspector {

    /**
     * Returns the name of the {@link FlowNode} instance in which the
     * {@link FlowNodeTask} instance is scheduled to execute or is executing.
     *
     * @return the name of the {@link FlowNode} instance in which the
     *         {@link FlowNodeTask} instance is scheduled to execute or is
     *         executing.
     */
    String getFlowNode();

    /**
     * Returns the label of the {@link ControlScope} instance to which the
     * {@link FlowNodeTask} instance belongs.
     *
     * @return the label of the {@link ControlScope} instance to which the
     *         {@link FlowNodeTask} instance belongs.
     */
    String getLabel();

    /**
     * Returns the name of the {@link Thread} instance, if any, that is currently
     * executing the {@link FlowNodeImpl} instance.
     *
     * @return the name of the {@link Thread} instance, if any, that is currently
     *         executing the {@link FlowNode} instance.
     */
    String getThread();
}
