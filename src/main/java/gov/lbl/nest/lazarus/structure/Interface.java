package gov.lbl.nest.lazarus.structure;

import java.util.Collection;

/**
 * This interface defines an external system to which call outs can be made.
 *
 * @author patton
 */
public interface Interface extends
                           RootElement {

    /**
     * Returns the name of the external to which call outs can be made.
     *
     * @return the name of the external to which call outs can be made.
     */
    String getImplementationRef();

    /**
     * Returns the name of this object.
     *
     * @return the name of this object.
     */
    String getName();

    /**
     * Returns the collection of {@link Operation} instances that can be performed
     * by the external system.
     *
     * @return the collection of {@link Operation} instances that can be performed
     *         by the external system.
     */
    Collection<? extends Operation> getOperations();
}
