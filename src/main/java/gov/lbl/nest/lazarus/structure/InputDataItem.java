package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to as the data item in a multi-instance loop.
 *
 * @author patton
 */
public interface InputDataItem extends
                               DataInput {

}
