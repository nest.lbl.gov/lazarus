package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to determine the execution of a loop.
 *
 * @author patton
 */
public interface LoopCharacteristics extends
                                     BaseElement {

}
