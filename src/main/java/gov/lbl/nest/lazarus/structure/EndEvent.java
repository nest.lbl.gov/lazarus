package gov.lbl.nest.lazarus.structure;

/**
 * This class is used to destroy a existing control flow.
 *
 * @author patton
 */
public interface EndEvent extends
                          FlowNode {

}
