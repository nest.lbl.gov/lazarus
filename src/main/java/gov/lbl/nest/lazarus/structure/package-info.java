/**
 * This package provides the interfaces and classes to characterize the
 * structure of a BPMN 2.0 XML document.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * <li>{@link org.slf4j}</li>
 * <li>{@link gov.lbl.nest.common.suspension}</li>
 * <li>{@link gov.lbl.nest.common.tasks}</li>
 * <li>{@link gov.lbl.nest.lazarus.digraph}</li>
 * </ul>
 */
package gov.lbl.nest.lazarus.structure;