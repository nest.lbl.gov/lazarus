package gov.lbl.nest.lazarus.structure;

import java.util.List;

/**
 * This interface is used to map a set of data into an {@link DataOnlyActivity}
 * instance.
 *
 * @author patton
 */
public interface InputSet extends
                          BaseElement {

    /**
     * Returns the collection of {@link DataInput} instances that make up this
     * object.
     *
     * @return the collection of {@link DataInput} instances that make up this
     *         object.
     */
    List<? extends DataInput> getDataInputs();

}
