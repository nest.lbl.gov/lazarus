package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to define how data is passed.
 *
 * @author patton
 */
public interface Message extends
                         RootElement {
    /**
     * Returns The {@link ItemDefinition} instance that defines the content of this
     * object.
     *
     * @return The {@link ItemDefinition} instance that defines the content of this
     *         object.
     */
    ItemDefinition getItemRef();

    /**
     * Returns the name of this object.
     *
     * @return the name of this object.
     */
    String getName();
}
