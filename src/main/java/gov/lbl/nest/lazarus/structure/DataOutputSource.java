package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to label objects that can act as a source for
 * {@link DataOutputAssociation}.
 *
 * @author patton
 */
public interface DataOutputSource extends
                                  ItemAwareElement {

}
