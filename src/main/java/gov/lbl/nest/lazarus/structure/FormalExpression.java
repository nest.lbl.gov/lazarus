package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to describe a formal expression.
 *
 * @author patton
 *
 * @param <T>
 *            the type to which this Object evaluates.
 */
public interface FormalExpression<T> extends
                                 Expression<T> {

}
