package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to define the structure of a DataObject.
 *
 * @author patton
 */
public interface ItemDefinition extends
                                RootElement {

    /**
     * Returns the {@link Class} whose structure defines the type of the item this
     * object represents.
     *
     * @return the {@link Class} whose structure defines the type of the item this
     *         object represents.
     *
     */
    Class<?> getStructure();

    /**
     * Returns <code>true</code> if this object is a collection of its structure
     * type.
     *
     * @return <code>true</code> if this object is a collection of its structure
     *         type.
     */
    boolean isCollection();
}
