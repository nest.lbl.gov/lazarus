package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to determine the execution of a set of multiple loops.
 *
 * @author patton
 */
public interface MultiInstanceLoopCharacteristics extends
                                                  LoopCharacteristics {

    /**
     * Returns the {@link Expression} which, when <code>true</code>, will terminate
     * the remaining loop instances.
     *
     * @return the {@link Expression} which, when <code>true</code>, will terminate
     *         the remaining loop instances.
     */
    Expression<Boolean> getCompletionCondition();

    /**
     * Returns the {@link DataInput} instance that will provide the input to each
     * loop instance and is an element of the loop data input collection.
     *
     * @return the {@link DataInput} instance that will provide the input to each
     *         loop instance and is an element of the loop data input collection.
     */
    DataInput getInputDataItem();

    /**
     * Returns the {@link Expression} instance that defines the number of loops to
     * be executed.
     *
     * @return the {@link Expression} instance that defines the number of loops to
     *         be executed.
     */
    Expression<Integer> getLoopCardinality();

    /**
     * Returns the collection of {@link DataInput} instance that will be used as
     * inputs to the loop instances.
     *
     * @return the collection of {@link DataInput} instance that will be used as
     *         inputs to the loop instances.
     */
    DataInputTarget getLoopDataInputRef();

    /**
     * Returns the collection of {@link DataOutput} instance that will be used a
     * outputs to the loop instances.
     *
     * @return the collection of {@link DataOutput} instance that will be used a
     *         outputs to the loop instances.
     */
    DataOutputSource getLoopDataOutputRef();

    /**
     * Returns the {@link DataOutput} instance into which each loop instance with
     * write its output and is an element of the loop data output collection.
     *
     * @return the {@link DataOutput} instance into which each loop instance with
     *         write its output and is an element of the loop data output
     *         collection.
     */
    DataOutput getOutputDataItem();
}
