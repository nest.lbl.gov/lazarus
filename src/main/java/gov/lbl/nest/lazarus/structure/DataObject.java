package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to hold data within a workflow instance.
 *
 * @author patton
 */
public interface DataObject extends
                            DataInputSource,
                            DataOutputTarget,
                            ItemAwareElement {

}
