package gov.lbl.nest.lazarus.structure;

import java.util.List;

/**
 * This interface is used to map a set of data out from an
 * {@link DataOnlyActivity} instance.
 *
 * @author patton
 */
public interface OutputSet extends
                           BaseElement {

    /**
     * Returns the collection of {@link DataOutput} instances that make up this
     * object.
     *
     * @return the collection of {@link DataOutput} instances that make up this
     *         object.
     */
    List<? extends DataOutput> getDataOutputs();

}
