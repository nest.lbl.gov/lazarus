package gov.lbl.nest.lazarus.structure;

/**
 * This class throw when there is an issue with handling a DataObject.
 *
 * @author patton
 */
public class DataObjectException extends
                                 Exception {

    /**
     * Used by serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     */
    public DataObjectException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public DataObjectException(String message,
                               Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Creates an instance of this class.
     *
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public DataObjectException(Throwable cause) {
        super(cause);
    }
}
