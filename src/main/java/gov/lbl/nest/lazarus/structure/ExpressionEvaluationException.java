package gov.lbl.nest.lazarus.structure;

/**
 * This class throw when there is an issue with evaluating an {@link Expression}
 * instance.
 *
 * @author patton
 */
public class ExpressionEvaluationException extends
                                           Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by serializable.
     */
    private static final long serialVersionUID = 1L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     */
    public ExpressionEvaluationException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public ExpressionEvaluationException(String message,
                                         Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Creates an instance of this class.
     *
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public ExpressionEvaluationException(Throwable cause) {
        super(cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
