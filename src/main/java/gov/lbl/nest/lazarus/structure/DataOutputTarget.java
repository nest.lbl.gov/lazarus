package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to label objects that can act as a target for
 * {@link DataOutputAssociation}.
 *
 * @author patton
 */
public interface DataOutputTarget extends
                                  ItemAwareElement {

}
