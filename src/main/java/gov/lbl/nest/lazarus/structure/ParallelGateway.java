package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to label gateways that deal with multiple flows of
 * control.
 *
 * @author patton
 *
 */
public interface ParallelGateway extends
                                 Gateway {

}
