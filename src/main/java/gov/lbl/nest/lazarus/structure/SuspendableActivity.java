package gov.lbl.nest.lazarus.structure;

import gov.lbl.nest.common.suspension.CollectableSuspendable;
import gov.lbl.nest.lazarus.execution.SuspendableFlowNodeExecutions;

/**
 * This interface extends the {@link Activity} interface by adding the ability
 * for it to be suspended.
 *
 * @author patton
 */
public interface SuspendableActivity extends
                                     Activity,
                                     CollectableSuspendable,
                                     SuspendableFlowNodeExecutions {
}
