package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used by any element in the structure though which flow of
 * control can pass.
 *
 * @author patton
 */
public interface FlowElement extends
                             BaseElement {

    /**
     * Returns the name of this object.
     *
     * @return the name of this object.
     */
    String getName();
}