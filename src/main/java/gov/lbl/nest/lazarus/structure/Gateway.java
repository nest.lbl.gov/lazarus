package gov.lbl.nest.lazarus.structure;

import gov.lbl.nest.lazarus.structure.SequenceFlow.Source;
import gov.lbl.nest.lazarus.structure.SequenceFlow.Target;

/**
 * This interface is used by any element in the structure that, when a control
 * flow arrives, makes a decision and then the control flow is passed on.
 *
 * @author patton
 */
public interface Gateway extends
                         FlowNode,
                         Target,
                         Source {

    /**
     * The possible direction that can be assigned to this class.
     *
     * @author patton
     */
    enum GatewayDirection {
                           /**
                            * The direction is unspecified.
                            */
                           UNSPECIFIED("Unspecified"),

                           /**
                            * The direction is inbound.
                            */
                           CONVERGING("Converging"),

                           /**
                            * The direction is outgoing.
                            */
                           DIVERGING("Diverging"),

                           /**
                            * The directions are both inbound and outgoing.
                            */
                           MIXED("Mixed");

        /**
         * The label to use for the object's value.
         */
        public final String label;

        private GatewayDirection(String label) {
            this.label = label;
        }
    }

    /**
     * Returns the {@link GatewayDirection} instance of this object.
     *
     * @return the {@link GatewayDirection} instance of this object.
     */
    GatewayDirection getGatewayDirection();
}
