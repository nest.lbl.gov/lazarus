package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to map data into an {@link DataOnlyActivity} instance.
 *
 * @author patton
 */
public interface DataInput extends
                           DataInputSource,
                           DataInputTarget,
                           ItemAwareElement {
}
