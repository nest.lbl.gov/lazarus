package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to assign values as part of a
 * {@link DataInputAssociation} instance.
 *
 * @author patton
 */
public interface Assignment extends
                            BaseElement {

    /**
     * Returns the {@link Expression} instance defining from where the data will be
     * taken.
     *
     * @return the {@link Expression} instance defining from where the data will be
     *         taken.
     */
    Expression<Object> getFrom();

    /**
     * Returns the {@link Expression} instance defining to where the data will be
     * placed.
     *
     * @return the {@link Expression} instance defining to where the data will be
     *         placed.
     */
    Expression<Object> getTo();
}
