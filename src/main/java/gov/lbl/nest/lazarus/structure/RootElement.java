package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used by any {@link BaseElement} that can appear at the
 * Definition level of a BPMN diagram.
 *
 * @author patton
 */
public interface RootElement extends
                             BaseElement {
}
