package gov.lbl.nest.lazarus.structure;

import java.util.Set;

/**
 * This interface is used to capture the structure of a workflow and created new
 * instances that can be executed.
 *
 * @author patton
 */
public interface Process extends
                         ControlOnlyProcess {

    /**
     * Returns the set of {@link Interface} instances used by this object.
     *
     * @return the set of {@link Interface} instances used by this object.
     */
    Set<? extends Interface> getInterfaces();

    /**
     * Returns the set of {@link ItemDefinition} instances used by this object.
     *
     * @return the set of {@link ItemDefinition} instances used by this object.
     */
    Set<? extends ItemDefinition> getItemDefinitions();

    /**
     * Returns the set of {@link Message} instances used by this object.
     *
     * @return the set of {@link Message} instances used by this object.
     */
    Set<? extends Message> getMessages();
}
