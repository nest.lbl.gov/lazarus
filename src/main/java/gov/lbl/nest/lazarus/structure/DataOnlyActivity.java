package gov.lbl.nest.lazarus.structure;

import java.util.List;

/**
 * This interface is used by any element in the structure that does something
 * with data when it has ownership of a control flow.
 *
 * @author patton
 *
 */
public interface DataOnlyActivity {

    /**
     * Returns the list of {@link DataInputAssociation} instances used to fill the
     * {@link DataInput} elements of the {@link InputOutputSpecification} instance.
     *
     * @return the list of {@link DataInputAssociation} instances used to fill the
     *         {@link DataInput} elements of the {@link InputOutputSpecification}
     *         instance.
     */
    @SuppressWarnings("rawtypes")
    List<? extends DataInputAssociation> getDataInputAssociations();

    /**
     * Returns the list of {@link DataOutputAssociation} instances used to fill the
     * {@link DataOutput} elements of the {@link InputOutputSpecification} instance.
     *
     * @return the list of {@link DataOutputAssociation} instances used to fill the
     *         {@link DataOutput} elements of the {@link InputOutputSpecification}
     *         instance.
     */
    @SuppressWarnings("rawtypes")
    List<? extends DataOutputAssociation> getDataOutputAssociations();

    /**
     * Returns the {@link InputOutputSpecification} instance that maps data in and
     * out of this object.
     *
     * @return the {@link InputOutputSpecification} instance that maps data in and
     *         out of this object.
     */
    InputOutputSpecification getIoSpecification();

}
