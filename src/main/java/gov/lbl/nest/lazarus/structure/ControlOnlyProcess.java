package gov.lbl.nest.lazarus.structure;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This interface is used to capture the control structure of a workflow and
 * created new instances that can be executed.
 *
 * @author patton
 */
public interface ControlOnlyProcess extends
                                    RootElement {

    /**
     * Returns the {@link AtomicInteger} instance used to keep count of the number
     * of parallel {@link ControlOnlyProcess} instances that exist.
     *
     * @return the {@link AtomicInteger} instance used to keep count of the number
     *         of parallel {@link ControlOnlyProcess} instances that exist.
     */
    AtomicInteger getCount();

    /**
     * Returns the collection of {@link FlowElement} instances that make up this
     * object.
     *
     * @return the collection of {@link FlowElement} instances that make up this
     *         object.
     */
    Collection<? extends FlowElement> getFlowElements();

    /**
     * Returns the name of this object.
     *
     * @return the name of this object.
     */
    String getName();

}
