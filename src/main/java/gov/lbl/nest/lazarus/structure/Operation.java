package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to describe a call out to an external system.
 *
 * @author patton
 */
public interface Operation extends
                           BaseElement {

    /**
     * Returns the name of the method within the external system that performs this
     * operation.
     *
     * @return the name of the method within the external system that performs this
     *         operation.
     */
    String getImplementationRef();

    /**
     * Returns the {@link Message} instance that defines the input to this object.
     *
     * @return the {@link Message} instance that defines the input to this object.
     */
    Message getInMessageRef();

    /**
     * Returns the {@link Interface} instance to which this object belongs.
     *
     * @return the {@link Interface} instance to which this object belongs.
     */
    Interface getInterface();

    /**
     * Returns the name of this object.
     *
     * @return the name of this object.
     */
    String getName();

    /**
     * Returns the {@link Message} instance, if any, that defines the output to this
     * object.
     *
     * @return the {@link Message} instance, if any, that defines the output to this
     *         object.
     */
    Message getOutMessageRef();
}
