package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to map a set of data onto another data element.
 *
 * @param <S>
 *            the class which defines the type of the sources.
 * @param <T>
 *            the class which defines the type of the target.
 *
 * @author patton
 */
public interface DataOutputAssociation<S extends DataOutputSource, T extends DataOutputTarget> extends
                                      DataAssociation<S, T> {

}
