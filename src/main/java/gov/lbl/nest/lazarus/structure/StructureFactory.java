package gov.lbl.nest.lazarus.structure;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPathFunctionResolver;
import javax.xml.xpath.XPathVariableResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.lazarus.management.ProcessManager;
import gov.lbl.nest.lazarus.management.ProcessManagerImpl;
import gov.lbl.nest.lazarus.structure.Gateway.GatewayDirection;
import gov.lbl.nest.lazarus.structure.SequenceFlow.Source;
import gov.lbl.nest.lazarus.structure.SequenceFlow.Target;

/**
 * This class needs to be extends to create an appropriate instance of the
 * {@link StructureFactory} class.
 *
 * @author patton
 */
public abstract class StructureFactory {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(StructureFactory.class);

    /**
     * true if the implementation class of this Object has already been logged.
     */
    private static AtomicBoolean LOGGING = new AtomicBoolean();

    /**
     * The {@link ServiceLoader} instance that will load the
     * {@link StructureFactory} implementation.
     */
    private final static ServiceLoader<StructureFactory> STRUCTURE_FACTORY_LOADER = ServiceLoader.load(StructureFactory.class,
                                                                                                       StructureFactory.class.getClassLoader());

    /**
     * Creates an instance of the {@link StructureFactory} class.
     *
     * @return the created instance of the {@link StructureFactory} class.
     */
    public static StructureFactory getStructureFactory() {
        for (StructureFactory factory : STRUCTURE_FACTORY_LOADER) {
            final Class<?> clazz = factory.getClass();
            try {
                final Constructor<?> constructor = clazz.getConstructor();
                StructureFactory result = (StructureFactory) constructor.newInstance();
                if (!(LOGGING.get())) {
                    LOG.debug("Using \"" + clazz.getCanonicalName()
                              + "\" as StructureFactory implementation");
                    LOGGING.set(true);
                }
                return result;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    /**
     * Creates a {@link DataInput} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param itemSubject
     *            the {@link ItemDefinition} whose structure defines the type of the
     *            item this object provides.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type. If <code>null</code> will use value from itemSubject.
     *
     * @return the created {@link DataInput} instance.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public abstract DataInput createDataInput(String name,
                                              String identity,
                                              ItemDefinition itemSubject,
                                              Boolean collection) throws ClassNotFoundException;

    /**
     * Creates a {@link DataInputAssociation} instance.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of sources that this object uses to create the
     *            target data.
     * @param target
     *            the {@link DataInputTarget} instance of the target of this object.
     *
     * @return the created {@link DataInputAssociation} instance.
     */
    public abstract DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget> createDataInputAssociation(String identity,
                                                                                                                          Collection<? extends DataInputSource> sources,
                                                                                                                          DataInputTarget target);

    /**
     * Creates a {@link DataObject} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param itemSubject
     *            the {@link ItemDefinition} whose structure defines the type of the
     *            item this object provides.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type. If <code>null</code> will use value from itemSubject.
     *
     * @return the created {@link DataObject} instance.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public abstract DataObject createDataObject(String name,
                                                String identity,
                                                ItemDefinition itemSubject,
                                                Boolean collection) throws ClassNotFoundException;

    /**
     * Creates a {@link DataOutput} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param itemSubject
     *            the {@link ItemDefinition} whose structure defines the type of the
     *            item this object provides.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type. If <code>null</code> will use value from itemSubject.
     *
     * @return the created {@link DataOutput} instance.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public abstract DataOutput createDataOutput(String name,
                                                String identity,
                                                ItemDefinition itemSubject,
                                                Boolean collection) throws ClassNotFoundException;

    /**
     * Creates a {@link DataOutputAssociation} instance.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param sources
     *            the collection of sources that this object uses to create the
     *            target data.
     * @param target
     *            the {@link DataOutputTarget} instance of the target of this
     *            object.
     *
     * @return the created {@link DataOutputAssociation} instance.
     */
    public abstract DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget> createDataOutputAssociation(String identity,
                                                                                                                              Collection<? extends DataOutputSource> sources,
                                                                                                                              DataOutputTarget target);

    /**
     * Creates a {@link EndEvent} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     *
     * @return the created {@link EndEvent} instance.
     */
    public abstract EndEvent createEndEvent(String name,
                                            String identity);

    /**
     * Creates a {@link ExclusiveGateway} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param direction
     *            the {@link GatewayDirection} instance of this object.
     *
     * @return the created {@link ExclusiveGateway} instance.
     */
    public abstract ExclusiveGateway createExclusiveGateway(String name,
                                                            String identity,
                                                            GatewayDirection direction);

    /**
     * Creates a {@link Expression} instance.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param contents
     *            the {@link String} instance from which to create the
     *            {@link Expression} instance.
     * @param language
     *            the URI of the language, if any, in which the contents is written.
     * @param clazz
     *            the {@link Class} instance of the return type of the returned
     *            {@link Expression} instance.
     * 
     * @param <T>
     *            the type of the return type of the returned {@link Expression}
     *            instance.
     *
     * @return the created {@link Expression} instance.
     */
    public abstract <T> Expression<T> createExpression(String identity,
                                                       String contents,
                                                       String language,
                                                       Class<T> clazz);

    /**
     * Creates a {@link InputDataItem} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param loopDataInput
     *            the collection of {@link DataInput} instances that will be used as
     *            inputs to the loop instances.
     * @param itemSubject
     *            the {@link ItemDefinition} whose structure defines the type of the
     *            item this object provides.
     * @param collection
     *            <code>true</code> if this object is a collection of its structure
     *            type. If <code>null</code> will use value from itemSubject.
     *
     * @return the created {@link InputDataItem} instance.
     *
     * @throws ClassNotFoundException
     *             when the specified structure can not be found.
     */
    public abstract InputDataItem createInputDataItem(String name,
                                                      String identity,
                                                      DataInputSource loopDataInput,
                                                      ItemDefinition itemSubject,
                                                      Boolean collection) throws ClassNotFoundException;

    /**
     * Creates a {@link InputOutputSpecification} instance.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param dataInputs
     *            the list of {@link DataInput} instances that define possible input
     *            data.
     * @param dataOutputs
     *            the list of {@link DataOutput} instances that define possible
     *            output data.
     * @param inputSets
     *            the list of {@link InputSet} instances that define possible sets
     *            of input data.
     * @param outputSets
     *            the list of {@link OutputSet} instances that define possible sets
     *            of output data.
     *
     * @return the created {@link InputOutputSpecification} instance.
     */
    public abstract InputOutputSpecification createInputOutputSpecification(String identity,
                                                                            Collection<? extends DataInput> dataInputs,
                                                                            Collection<? extends DataOutput> dataOutputs,
                                                                            Collection<? extends InputSet> inputSets,
                                                                            Collection<? extends OutputSet> outputSets);

    /**
     * Creates a {@link InputSet} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param dataInputs
     *            the collection of {@link ItemAwareElement} instances that make up
     *            this object.
     *
     * @return the created {@link InputSet} instance.
     */
    public abstract InputSet createInputSet(String name,
                                            String identity,
                                            Collection<? extends DataInput> dataInputs);

    /**
     * Creates a {@link Interface} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param operations
     *            the collection of {@link Operation} instances that can be
     *            performed by the external system.
     * @param implementationRef
     *            the name of the external system to which call outs will be made.
     *
     * @return the created {@link Interface} instance.
     *
     * @throws ClassNotFoundException
     *             when the structure class can not be found.
     * @throws NoSuchMethodException
     *             when no external method can be mapped to the supplied
     *             information.
     */
    public abstract Interface createInterface(String name,
                                              String identity,
                                              Collection<? extends Operation> operations,
                                              String implementationRef) throws ClassNotFoundException,
                                                                        NoSuchMethodException;

    /**
     * Creates a {@link ItemDefinition} instance.
     *
     * @param identity
     *            the identity of this Object.
     * @param structure
     *            the name of the {@link Class} whose structure defines the type of
     *            the item this object represents.
     * @param isCollection
     *            <code>true</code> if this object is a collection of its structure
     *            type.
     *
     * @return the created {@link ItemDefinition} instance.
     *
     * @throws ClassNotFoundException
     *             when the structure class can not be found.
     */
    public abstract ItemDefinition createItemDefinition(String identity,
                                                        String structure,
                                                        Boolean isCollection) throws ClassNotFoundException;

    /**
     * Creates a {@link Message} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param itemRef
     *            the {@link ItemDefinition} instance that defines the content of
     *            this object.
     *
     * @return the created {@link Message} instance.
     */
    public abstract Message createMessage(String name,
                                          String identity,
                                          ItemDefinition itemRef);

    /**
     * Creates a {@link MultiInstanceLoopCharacteristics} instance.
     *
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     *
     *
     * @param loopCardinality
     *            The {@link Expression} instance that defines the number of loops
     *            to be executed.
     *
     * @param loopDataInputRef
     *            The collection of {@link DataInput} instance that will be used as
     *            inputs to the loop instances.
     *
     * @param loopDataOutputRef
     *            The collection of {@link DataOutput} instance that will be used a
     *            outputs to the loop instances.
     *
     * @param inputDataItem
     *            The {@link DataInput} instance that will provide the input to each
     *            loop instance and is an element of the loop data input collection.
     *
     * @param outputDataItem
     *            The {@link DataOutput} instance into which each loop instance with
     *            write its output and is an element of the loop data output
     *            collection.
     *
     * @param completionCondition
     *            The {@link Expression} which, when <code>true</code>, will
     *            terminate the remaining loop instances.
     *
     * @return the created {@link MultiInstanceLoopCharacteristics} instance.
     */
    public abstract MultiInstanceLoopCharacteristics createMultiInstanceLoopCharacteristics(String identity,
                                                                                            Expression<Integer> loopCardinality,
                                                                                            DataInputTarget loopDataInputRef,
                                                                                            DataOutputSource loopDataOutputRef,
                                                                                            DataInput inputDataItem,
                                                                                            DataOutput outputDataItem,
                                                                                            Expression<Boolean> completionCondition);

    /**
     * Creates a {@link Operation} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param implementationRef
     *            the name of the method within the external system that performs
     *            this operation.
     * @param inMessageRef
     *            the {@link Message} instance that defines the input to this
     *            object.
     * @param outMessageRef
     *            the {@link Message} instance, if any, that defines the output to
     *            this object.
     *
     * @return the created {@link Operation} instance.
     *
     * @throws NoSuchMethodException
     *             when no external method can be mapped to the supplied
     *             information.
     */
    public abstract Operation createOperation(String name,
                                              String identity,
                                              String implementationRef,
                                              Message inMessageRef,
                                              Message outMessageRef) throws NoSuchMethodException;

    /**
     * Creates a {@link OutputSet} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param dataOutputs
     *            the collection of {@link DataOutput} instances that make up this
     *            object.
     *
     * @return the created {@link OutputSet} instance.
     */
    public abstract OutputSet createOutputSet(String name,
                                              String identity,
                                              Collection<? extends DataOutput> dataOutputs);

    /**
     * Creates a {@link ParallelGateway} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param direction
     *            the {@link GatewayDirection} instance of this object.
     *
     * @return the created {@link ParallelGateway} instance.
     */
    public abstract ParallelGateway createParallelGateway(String name,
                                                          String identity,
                                                          GatewayDirection direction);

    /**
     * Creates a {@link Process} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param start
     *            the {@link StartEvent} instance that begins execution of this
     *            object.
     * @param flowElements
     *            the collection of {@link FlowElement} instances that make up this
     *            object.
     * @param dataObjects
     *            the collection of {@link DataObject} instances in this objects.
     * @param processManager
     *            the {@link ProcessManagerImpl} instance, if any, with which to
     *            control this object.
     *
     * @return the created {@link Operation} instance.
     */
    public abstract Process createProcess(String name,
                                          String identity,
                                          StartEvent start,
                                          Collection<? extends FlowElement> flowElements,
                                          Collection<? extends DataObject> dataObjects,
                                          ProcessManager processManager);

    /**
     * Creates a {@link SequenceFlow} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param source
     *            the source of a flow of control.
     * @param target
     *            the target of a flow of control.
     * @param expression
     *            the {@link Expression}, if any, associated with this Object.
     *
     * @return the created {@link SequenceFlow} instance.
     */
    public abstract SequenceFlow createSequenceFlow(String name,
                                                    String identity,
                                                    Source source,
                                                    Target target,
                                                    Expression<Boolean> expression);

    /**
     * Creates a {@link ServiceTask} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     * @param operation
     *            the {@link Operation} instance used by this object to execute its
     *            responsibilities.
     * @param ioSpecification
     *            the {@link InputOutputSpecification} instance that maps data in
     *            and out of this object.
     * @param inputAssocs
     *            the list of {@link DataInputAssociation} instances used to fill
     *            the {@link DataInput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param outputAssocs
     *            the list of {@link DataOutputAssociation} instances used to fill
     *            the {@link DataOutput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param loopCharacteristics
     *            the {@link LoopCharacteristics} instance, if any, defining who the
     *            created {@link ServiceTask} should loop.
     * @param supplier
     *            the {@link Supplier} instance to which {@link FlowNodeTask}
     *            instance in an executing workflow will be added.
     * @param suspendable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     *
     * @return the created {@link ServiceTask} instance.
     */
    public abstract ServiceTask createServiceTask(String name,
                                                  String identity,
                                                  Operation operation,
                                                  InputOutputSpecification ioSpecification,
                                                  Collection<? extends DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget>> inputAssocs,
                                                  Collection<? extends DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget>> outputAssocs,
                                                  LoopCharacteristics loopCharacteristics,
                                                  Supplier<FlowNodeTask> supplier,
                                                  Suspendable suspendable);

    /**
     * Creates a {@link StartEvent} instance.
     *
     * @param name
     *            the name of this object.
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     *
     * @return the created {@link StartEvent} instance.
     */
    public abstract StartEvent createStartEvent(String name,
                                                String identity);

    /**
     * Sets the {@link NamespaceContext} instance to use if this object does not
     * provide a suitable one.
     *
     * @param context
     *            the {@link NamespaceContext} instance to use if this object does
     *            not provide a suitable one.
     */
    public abstract void setFallbackNamespaceContext(NamespaceContext context);

    /**
     * Sets the {@link XPathFunctionResolver} instance to use if this object does
     * not provide a suitable one.
     *
     * @param resolver
     *            the {@link XPathFunctionResolver} instance to use if this object
     *            does not provide a suitable one.
     */
    public abstract void setFallbackXPathFunctionResolver(XPathFunctionResolver resolver);

    /**
     * Sets the {@link XPathVariableResolver} instance to use if this object does
     * not provide a suitable one.
     *
     * @param resolver
     *            the {@link XPathVariableResolver} instance to use if this object
     *            does not provide a suitable one.
     */
    public abstract void setFallbackXPathVariableResolver(XPathVariableResolver resolver);

}
