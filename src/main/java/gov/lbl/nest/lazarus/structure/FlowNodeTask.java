package gov.lbl.nest.lazarus.structure;

import gov.lbl.nest.common.queued.Queueable;
import gov.lbl.nest.common.queued.Queued;
import gov.lbl.nest.common.tasks.ActiveTask;
import gov.lbl.nest.common.tasks.ordinal.PriorityRankedElement;

/**
 * This interface defines the responsibilities of any task executing in a flow
 * node.
 *
 * @author patton
 */
public interface FlowNodeTask extends
                              PriorityRankedElement<FlowNodeInspector>,
                              ActiveTask<FlowNodeInspector>,
                              Queueable,
                              Queued {
}
