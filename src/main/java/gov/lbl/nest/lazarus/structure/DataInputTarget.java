package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to label objects that can act as a target for
 * {@link DataInputAssociation}.
 *
 * @author patton
 */
public interface DataInputTarget extends
                                 ItemAwareElement {

}
