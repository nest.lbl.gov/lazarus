package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used to map a set of data onto another data element.
 *
 * @param <S>
 *            the class which defines the type of the sources.
 * @param <T>
 *            the class which defines the type of the target.
 *
 * @author patton
 */
public interface DataInputAssociation<S extends DataInputSource, T extends DataInputTarget> extends
                                     DataAssociation<S, T> {

}
