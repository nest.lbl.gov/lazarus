package gov.lbl.nest.lazarus.structure;

/**
 * This interface is used by any element in the structure that, when a control
 * flow arrives, does something and then the control flow is passed on.
 *
 * @author patton
 */
public interface ControlOnlyActivity extends
                                     FlowNode {

}
