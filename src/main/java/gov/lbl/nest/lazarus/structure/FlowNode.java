package gov.lbl.nest.lazarus.structure;

import java.util.Collection;

import gov.lbl.nest.lazarus.digraph.Node;
import gov.lbl.nest.lazarus.structure.SequenceFlow.Source;
import gov.lbl.nest.lazarus.structure.SequenceFlow.Target;

/**
 * This interface is used by any element in the structure that, when a control
 * flow arrives, does something or makes a decision and then the control flow is
 * passed on.
 *
 * @author patton
 */
public interface FlowNode extends
                          Node,
                          FlowElement,
                          Target,
                          Source {

    /**
     * Returns the collection of incoming {@link SequenceFlow} instances.
     *
     * @return the collection of incoming {@link SequenceFlow} instances.
     */
    Collection<? extends SequenceFlow> getIncoming();

    /**
     * Returns the collection of outgoing {@link SequenceFlow} instances.
     *
     * @return the collection of outgoing {@link SequenceFlow} instances.
     */
    Collection<? extends SequenceFlow> getOutgoing();
}
