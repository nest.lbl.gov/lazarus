package gov.lbl.nest.lazarus.xpath;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;

/**
 * This class implements the {@link NamespaceContext} interface for the BPMN
 * namespace.
 *
 * @author patton
 */
public class BpmnNamespaceContext implements
                                  NamespaceContext {

    /**
     * The namespace for the BPMN extensions to XPath.
     */
    public static final String BPMN_NS = "http://www.omg.org/spec/BPMN/20100524/MODEL";

    /**
     * The prefix associated with the BPMN extensions to XPath.
     */
    public static final String BPMN_PREFIX = "bpmn2";

    /**
     * The List of BPMN prefixes.
     */
    private static final Collection<String> BPMN_PREFIXES = Collections.unmodifiableCollection(Arrays.asList(new String[] { BPMN_PREFIX }));

    /**
     * The {@link NamespaceContext} to use if this one does not have a response for
     * an invoked method.
     */
    private final NamespaceContext fallback;

    /**
     * Creates an instance of this class.
     *
     * @param fallback
     *            the {@link NamespaceContext} to use if this one does not have a
     *            response for an invoked method.
     */
    public BpmnNamespaceContext(NamespaceContext fallback) {
        this.fallback = fallback;
    }

    @Override
    public String getNamespaceURI(String prefix) {
        if (null == prefix) {
            throw new IllegalArgumentException("Prefix must not be null");
        }
        if (prefix.equals(BPMN_PREFIX)) {
            return BPMN_NS;
        }
        if (null == fallback) {
            return null;
        }
        return fallback.getNamespaceURI(prefix);
    }

    @Override
    public String getPrefix(String namespaceURI) {
        if (null == namespaceURI) {
            throw new IllegalArgumentException("Namespace must not be null");
        }
        if (namespaceURI.equals(BPMN_NS)) {
            return BPMN_PREFIX;
        }
        if (null == fallback) {
            return null;
        }
        return fallback.getPrefix(namespaceURI);
    }

    @Override
    public Iterator<String> getPrefixes(String namespaceURI) {
        if (null == namespaceURI) {
            throw new IllegalArgumentException("Namespace must not be null");
        }
        if (namespaceURI.equals(BPMN_NS)) {
            return BPMN_PREFIXES.iterator();
        }
        if (null == fallback) {
            return null;
        }
        return fallback.getPrefixes(namespaceURI);
    }

}
