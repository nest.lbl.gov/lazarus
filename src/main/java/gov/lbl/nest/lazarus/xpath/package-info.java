/**
 * This package provides the interfaces and classes that implement XPath
 * functionality independent of the rest of the Lazarus code.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * <li>{@link javax.xml.namespace}</li>
 * </ul>
 */
package gov.lbl.nest.lazarus.xpath;