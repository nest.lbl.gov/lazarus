/**
 * This package provides the classes to persist the state of execution workflows
 * on disk.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * <li>{@link java.util.concurrent}</li>
 * <li>{@link java.io}</li>
 * <li>{@link java.nio.file}</li>
 * <li>{@link jakarta.xml.bind}</li>
 * <li>{@link org.slf4j}</li>
 * <li>{@link gov.lbl.nest.common.configure}</li>
 * <li>{@link gov.lbl.nest.common.xml.item}</li>
 * <li>{@link gov.lbl.nest.lazarus.structure}</li>
 * <li>{@link gov.lbl.nest.lazarus.control}</li>
 * <li>{@link gov.lbl.nest.lazarus.data}</li>
 * <li>{@link gov.lbl.nest.lazarus.process}</li>
 * </ul>
 */
package gov.lbl.nest.lazarus.persist.disk;
