package gov.lbl.nest.lazarus.persist.disk;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import gov.lbl.nest.common.configure.CustomConfigPath;
import gov.lbl.nest.lazarus.control.ControlFlowException;
import gov.lbl.nest.lazarus.control.ControlOnlyProcessImpl;
import gov.lbl.nest.lazarus.control.ControlScope;
import gov.lbl.nest.lazarus.execution.TerminationListener;
import gov.lbl.nest.lazarus.process.ControlledDataFactory;
import gov.lbl.nest.lazarus.process.ProcessDefinitionImpl;

/**
 * This class extends the {@link ControlledDataFactory} class in order to
 * preserve the state in an instance on disk.
 *
 * @author patton
 */
public class ControlledDataImplFactory extends
                                       ControlledDataFactory {

    /**
     * The name of the resource that may contain the path to an alternate
     * <q>suspend<\q> file for SPADE.
     */
    private static final String LAZARUS_ROOT_RESOURCE = "lazarus_root";

    /**
     * True if the {@link #configFileName} value has been loaded.
     */
    private static final CustomConfigPath CONFIG_PATH = new CustomConfigPath(ControlledDataImplFactory.class,
                                                                             LAZARUS_ROOT_RESOURCE);

    /**
     * Creates a {@link Path} instance to use as backing store storage for an
     * object's data.
     *
     * @param fileSystem
     *            the {@link FileSystemManipulations} instance used to create the
     *            backing store.
     * @param process
     *            the name of the process for which to create the path.
     *
     * @return the {@link Path} instance to use as backing store storage for an
     *         object's data.
     *
     * @throws ControlFlowException
     *             when there is a problem creating data in the backing store.
     */
    private static Path createPath(FileSystemManipulations fileSystem,
                                   String process) {
        try {
            return fileSystem.createControlScopePath(process);
        } catch (IOException e) {
            throw new ControlFlowException("Could not create backing store storage for " + (ControlScopeOnDisk.class).getName(),
                                           e);
        }
    }

    /**
     * The {@link FileSystemManipulations} instance used by this object.
     */
    private final DataImplFactory dataFactory = new DataImplFactory();

    /**
     * The {@link FileSystemManipulations} instance used by this object.
     */
    private FileSystemManipulations fileSystem;

    @Override
    public ControlScopeOnDisk createControlledDataScope(ProcessDefinitionImpl process,
                                                        String label,
                                                        Integer priority,
                                                        TerminationListener listener) {
        final Path path = createPath(getFileSystem(),
                                     process.getName());
        final DataScopeOnDisk dataScope = dataFactory.createDataScope(process.getDataObjects(),
                                                                      path);
        return new ControlScopeOnDisk(label,
                                      priority,
                                      process,
                                      listener,
                                      dataScope,
                                      path);
    }

    private FileSystemManipulations getFileSystem() {
        if (null == fileSystem) {
            final Path configPath = CONFIG_PATH.getConfigPath();
            final Path configPathToUse;
            if (null == configPath) {
                configPathToUse = Paths.get("lazarus");
            } else {
                configPathToUse = configPath;
            }
            fileSystem = new FileSystemManipulations(configPathToUse);
        }
        return fileSystem;
    }

    /**
     * Recovers a {@link ControlScope} instance from the supplied Instantiation
     * Path.
     *
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance for which to recover
     *            the instance.
     * @param listener
     *            the {@link TerminationListener} instance to callback when the
     *            recovered instance is terminated.
     * @param paths
     *            the {@link Path} instance to the {@link ControlScope} data to be
     *            recovered.
     *
     * @return the recovered {@link ControlScope} instance.
     *
     * @throws ControlFlowException
     *             when there is a problem with the the backing store so that at
     *             least one {@link ControlScope} instance can not be recovered.
     */
    private ControlScopeOnDisk recoverControlledDataScope(ProcessDefinitionImpl process,
                                                          TerminationListener listener,
                                                          Path path) {
        Path activePath;
        try {
            activePath = FileSystemManipulations.updatedRecoveringToActive(path);
        } catch (IOException e) {
            throw new ControlFlowException("Failed to recover the \"" + path.toString()
                                           + "\" directory",
                                           e);
        }
        final ControlScopeOnDisk result;
        final DataScopeOnDisk dataScope = dataFactory.recoverDataScope(process.getDataObjects(),
                                                                       activePath);
        if (FileSystemManipulations.isTerminating(activePath)) {
            result = new ControlScopeOnDisk(process,
                                            listener,
                                            dataScope,
                                            activePath);
        } else if (FileSystemManipulations.isExecuting(activePath)) {
            result = new ControlScopeOnDisk(activePath,
                                            process,
                                            listener,
                                            dataScope);
        } else if (FileSystemManipulations.isCreated(activePath)) {
            result = new ControlScopeOnDisk(process,
                                            listener,
                                            dataScope,
                                            activePath);
        } else {
            FileSystemManipulations.deleteTree(activePath);
            result = null;
        }
        return result;
    }

    @Override
    public Collection<? extends ControlScopeOnDisk> recoverControlledDataScopes(ProcessDefinitionImpl process,
                                                                                TerminationListener listener) {
        final Collection<Path> instantiationPaths = getFileSystem().recoverControlScopePaths(process.getName());
        final Iterator<Path> iterator = instantiationPaths.iterator();
        while (iterator.hasNext()) {
            final Path instantiationPath = iterator.next();
            if (FileSystemManipulations.isFailed(instantiationPath)) {
                try {
                    /*
                     * Technically there should be a check to see if preservation was required, but
                     * given the difficultly of accessing that information from here and the low
                     * probability of this statement running, the conservative choice is to simply
                     * preserve the state.
                     */
                    // TODO: Add some sort of default setting.
                    FileSystemManipulations.preserveControlScope(instantiationPath);
                } catch (IOException e) {
                    throw new ControlFlowException(e);
                }
                iterator.remove();
            }
        }
        return recoverControlledDataScopes(process,
                                           listener,
                                           instantiationPaths);
    }

    /**
     * Recovers any {@link ControlScope} instances from the supplied Instantiation
     * Paths.
     *
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance for which to recover
     *            instances.
     * @param listener
     *            the {@link TerminationListener} instance to callback when this
     *            object has terminated.
     * @param paths
     *            the collection of the {@link Path} instances to the
     *            {@link ControlScope} data to be recovered.
     *
     * @return the collection of recovered {@link ControlScope} instances.
     *
     * @throws ControlFlowException
     *             when there is a problem with the the backing store so that at
     *             least one {@link ControlScope} instance can not be recovered.
     */
    private Collection<? extends ControlScopeOnDisk> recoverControlledDataScopes(ProcessDefinitionImpl process,
                                                                                 TerminationListener listener,
                                                                                 final Collection<Path> paths) {
        final List<ControlScopeOnDisk> results = new ArrayList<>();
        if (null != paths && !(paths.isEmpty())) {
            for (Path path : paths) {
                final ControlScopeOnDisk created = recoverControlledDataScope(process,
                                                                              listener,
                                                                              path);
                if (null != created) {
                    results.add(created);
                }
            }
        }
        return results;
    }

    @Override
    public Collection<? extends ControlScopeOnDisk> rescueControlledDataScopes(ProcessDefinitionImpl process,
                                                                               TerminationListener listener) {
        final Collection<Path> instantiationPaths = getFileSystem().rescueControlScopePaths(process.getName());
        return recoverControlledDataScopes(process,
                                           listener,
                                           instantiationPaths);
    }
}
