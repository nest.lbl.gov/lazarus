package gov.lbl.nest.lazarus.persist.disk;

import java.util.Collection;

import gov.lbl.nest.lazarus.data.DataFactory;
import gov.lbl.nest.lazarus.data.DataObjectImpl;

/**
 * This class extends the {@link DataFactory} class in order to preserve the
 * state in an instance on disk.
 *
 * @author patton
 */
public class DataImplFactory extends
                             DataFactory {

    @Override
    public DataScopeOnDisk createDataScope(Collection<? extends DataObjectImpl> collection,
                                           Object guidance) {
        return new DataScopeOnDisk(collection,
                                   guidance);
    }

    @Override
    public DataScopeOnDisk recoverDataScope(Collection<? extends DataObjectImpl> dataObjects,
                                            Object guidance) {
        final DataScopeOnDisk result = new DataScopeOnDisk(dataObjects,
                                                           guidance);
        result.recover();
        return result;
    }

}
