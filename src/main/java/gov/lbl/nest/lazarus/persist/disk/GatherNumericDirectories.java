package gov.lbl.nest.lazarus.persist.disk;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class is used to collect all of the directories that have numerical
 * names.
 *
 * @author patton
 *
 * @param <T>
 *            The type of Path being iterated over.
 */
class GatherNumericDirectories<T extends Path> implements
                              FileVisitor<T> {

    private final Comparator<T> NUMERICAL_ORDER = new Comparator<>() {

        @Override
        public int compare(T o1,
                           T o2) {
            final int int1 = Integer.parseInt((o1.getFileName()).toString());
            final int int2 = Integer.parseInt((o2.getFileName()).toString());
            return int1 - int2;
        }
    };

    /**
     * The sequence of {@link Path} instance that have numerical names.
     */
    private final List<T> paths = new ArrayList<>();

    /**
     * Returns the sequence of {@link Path} instance that have numerical names.
     *
     * @return the sequence of {@link Path} instance that have numerical names.
     */
    List<T> getPaths() {
        Collections.sort(paths,
                         NUMERICAL_ORDER);
        return paths;
    }

    @Override
    public FileVisitResult postVisitDirectory(T dir,
                                              IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(T dir,
                                             BasicFileAttributes attrs) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(T file,
                                     BasicFileAttributes attrs) throws IOException {
        try {
            Integer.parseInt((file.getFileName()).toString());
            paths.add(file);
        } catch (NumberFormatException e) {
            // Do nothing, skip.
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(T file,
                                           IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

}
