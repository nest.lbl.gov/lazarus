package gov.lbl.nest.lazarus.persist.disk;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

import gov.lbl.nest.lazarus.control.ControlOwner;
import gov.lbl.nest.lazarus.execution.Cause;
import gov.lbl.nest.lazarus.execution.ThrownFailure;

/**
 * This class added Disk bases information to the {@link ThrownFailure} class
 * and enables it to be represented in XML.
 *
 * @author patton
 */
@XmlRootElement(name = "failure")
@XmlType(propOrder = { "cause",
                       "owner",
                       "rescue" })
public class ThrownFailureOnDisk extends
                                 ThrownFailure {

    /**
     * Creates an instance of this class.
     */
    protected ThrownFailureOnDisk() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param throwable
     *            the {@link Throwable} instance thrown.
     * @param owner
     *            the identity of the {@link ControlOwner} which threw the
     *            {@link Throwable} instance.
     * @param rescue
     *            the {@link String} instance that can be used when a problem file
     *            is being used.
     */
    protected ThrownFailureOnDisk(Throwable throwable,
                                  String owner,
                                  String rescue) {
        super(throwable,
              owner,
              rescue);
    }

    @Override
    @XmlElement
    public Cause getCause() {
        return super.getCause();
    }

    @Override
    @XmlElement
    public String getOwner() {
        return super.getOwner();
    }

    @Override
    @XmlElement
    public String getRescue() {
        return super.getRescue();
    }

}
