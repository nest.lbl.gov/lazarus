package gov.lbl.nest.lazarus.persist.disk;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;

import gov.lbl.nest.lazarus.data.DataCellImpl;
import gov.lbl.nest.lazarus.data.DataOnlyActivityImpl;
import gov.lbl.nest.lazarus.structure.DataObjectException;

/**
 * This class extends the {@link DataCellImpl} class in order to preserve its
 * state on disk.
 *
 * @author patton
 */
public class DataCellOnDisk extends
                            DataCellImpl {

    /**
     * The suffix used by XML files.
     */
    private final static int XML_SUFFIX_LENGTH = FileSystemManipulations.XML_SUFFIX.length();

    /**
     * The {@link Path} instance use to storage this object's data.
     */
    private final Path path;

    /**
     * Creates an instance of this class.
     *
     * @param scope
     *            the {@link DataScopeOnDisk} instance to which the object belongs.
     * @param activity
     *            the {@link DataOnlyActivityImpl} instance with which this object
     *            exists.
     * @param path
     *            the {@link Path} instance use to storage this object's data.
     *
     * @throws IOException
     *             when then is a problem creating the directory.
     * @throws DataObjectException
     *             when the value of a persisted DataOutput instance can not be
     *             returned.
     */
    public DataCellOnDisk(DataScopeOnDisk scope,
                          DataOnlyActivityImpl activity,
                          Path path) throws IOException,
                                     DataObjectException {
        super(scope,
              activity);
        this.path = FileSystemManipulations.createDataCellPath(path);
        final Collection<Path> recoveredIdentityPaths = FileSystemManipulations.recoverIdentityPaths(this.path);
        if (null == recoveredIdentityPaths || recoveredIdentityPaths.isEmpty()) {
            return;
        }

        for (Path recoveredIdentityPath : recoveredIdentityPaths) {
            final String fileName = (recoveredIdentityPath.getFileName()).toString();
            final String identity = fileName.substring(0,
                                                       fileName.length() - XML_SUFFIX_LENGTH);
            final Collection<?> values = (Collection<?>) scope.recoverValue(identity,
                                                                            this.path);
            addRecoveredOutput(identity,
                               values);
        }
    }

    @Override
    protected void createOrReplaceRecoverableOutputValues(String identity,
                                                          Collection<?> values) throws DataObjectException {
        final DataScopeOnDisk scope = (DataScopeOnDisk) getDataScope();
        scope.createOrReplaceCollectionValues(identity,
                                              values,
                                              path);
    }
}
