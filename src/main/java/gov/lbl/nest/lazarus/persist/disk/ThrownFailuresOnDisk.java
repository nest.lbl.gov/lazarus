package gov.lbl.nest.lazarus.persist.disk;

import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is a container for a seuence of {@link ThrownFailureOnDisk}
 * instances.
 *
 * @author patton
 */
@XmlRootElement(name = "failures")
@XmlType(propOrder = { "count",
                       "failures" })
public class ThrownFailuresOnDisk {

    /**
     * The sequence of {@link ThrownFailureOnDisk} instance in this object.
     */
    private List<? extends ThrownFailureOnDisk> failures;

    /**
     * Creates an instance of this class.
     */
    ThrownFailuresOnDisk() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param failures
     *            the sequence of {@link ThrownFailureOnDisk} instance in this
     *            object.
     */
    ThrownFailuresOnDisk(List<? extends ThrownFailureOnDisk> failures) {
        this.failures = failures;
    }

    /**
     * Returns the number of {@link ThrownFailureOnDisk} instances in this object.
     *
     * @return the number of {@link ThrownFailureOnDisk} instances in this object.
     */
    @XmlElement
    protected Integer getCount() {
        if (null == failures) {
            return 0;
        }
        return failures.size();
    }

    /**
     * Returns the sequence of {@link ThrownFailureOnDisk} instance in this object.
     *
     * @return the sequence of {@link ThrownFailureOnDisk} instance in this object.
     */
    @XmlElement(name = "failure")
    protected List<? extends ThrownFailureOnDisk> getFailures() {
        return failures;
    }

    /**
     * The number of {@link ThrownFailureOnDisk} instances in this object. This
     * method does nothing and is included to completeness of XML marshalling.
     *
     * @param count
     *            the number of {@link ThrownFailureOnDisk} instances in this
     *            object.
     */
    protected void setCount(Integer count) {
        // Does nothing.
    }

    /**
     * Sets the sequence of {@link ThrownFailureOnDisk} instance in this object.
     *
     * @param failures
     *            the sequence of {@link ThrownFailureOnDisk} instance in this
     *            object.
     */
    protected void setFailures(List<? extends ThrownFailureOnDisk> failures) {
        this.failures = failures;
    }
}
