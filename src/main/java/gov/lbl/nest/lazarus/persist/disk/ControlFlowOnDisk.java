package gov.lbl.nest.lazarus.persist.disk;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import gov.lbl.nest.lazarus.control.ControlFlow;
import gov.lbl.nest.lazarus.control.ControlFlowException;
import gov.lbl.nest.lazarus.control.ControlFlowImpl;
import gov.lbl.nest.lazarus.control.ControlOwner;
import gov.lbl.nest.lazarus.data.DataCell;
import gov.lbl.nest.lazarus.data.DataOnlyActivityImpl;
import gov.lbl.nest.lazarus.process.ControlledDataFlow;
import gov.lbl.nest.lazarus.structure.DataObjectException;

/**
 * This class extends the {@link ControlFlowImpl} class in order to preserve its
 * state on disk.
 *
 * @author patton
 */
class ControlFlowOnDisk extends
                        ControlFlowImpl implements
                        ControlledDataFlow {

    /**
     * The {@link Path} instance use to storage this object's information.
     */
    private final Path path;

    /**
     * Creates an instance of this class.
     *
     * @param controlScopeOnDisk
     *            the {@link ControlScopeOnDisk} instance to which this object
     *            belongs.
     * @param path
     *            the {@link Path} instance use to storage this object's
     *            information.
     */
    ControlFlowOnDisk(ControlScopeOnDisk controlScopeOnDisk,
                      Path path) {
        this(controlScopeOnDisk,
             path,
             null,
             -1);
    }

    /**
     * Creates an instance of this class.
     *
     * @param controlScopeOnDisk
     *            the {@link ControlScopeOnDisk} instance to which this object
     *            belongs.
     * @param path
     *            the {@link Path} instance use to storage this object's
     *            information.
     * @param fanningIndex
     *            the index into the sequence of {@link ControlFlow} instances when
     *            fanning-out or fanning-in flows, otherwise -1.
     */
    ControlFlowOnDisk(ControlScopeOnDisk controlScopeOnDisk,
                      Path path,
                      ControlOwner owner,
                      int fanningIndex) {
        super(controlScopeOnDisk,
              owner,
              fanningIndex);
        this.path = path;
    }

//    /**
//     * Returns the sequence number of this object.
//     *
//     * @return the sequence number of this object.
//     */
//    private String getNumber() {
//        return (path.getFileName()).toString();
//    }

    @Override
    public DataCell createDataCell(DataOnlyActivityImpl activity) throws DataObjectException {

        final ControlScopeOnDisk controlScope = (ControlScopeOnDisk) getControlScope();
        final DataScopeOnDisk dataScope = (DataScopeOnDisk) (controlScope.getDataScope());
        return dataScope.createDataCell(activity,
                                        this);
    }

    /**
     * Returns the {@link Path} instance use to storage this object's information.
     *
     * @return the {@link Path} instance use to storage this object's information.
     */
    Path getPath() {
        return path;
    }

    @Override
    public List<Integer> getRecoveredBranches() {
        try {
            return FileSystemManipulations.readBranches(path);
        } catch (IOException e) {
            throw new ControlFlowException(e);
        }
    }

    @Override
    protected void purge() {
        FileSystemManipulations.deleteTree(path);
    }

    @Override
    public void setBranchesToRecover(List<Integer> branchesToExecute) throws ControlFlowException {
        try {
            FileSystemManipulations.writeBranches(branchesToExecute,
                                                  path);
        } catch (IOException e) {
            throw new ControlFlowException(e);
        }
    }

    @Override
    protected void updateForkedOwner(ControlOwner owner,
                                     int fanningIndex) {
        try {
            FileSystemManipulations.setOwnerIdentity(ControlScopeOnDisk.FORKED_PREFIX + Integer.toString(fanningIndex)
                                                     + ControlScopeOnDisk.FORKED_SUFFIX
                                                     + owner.getOwnerIdentity(),
                                                     path);
        } catch (IOException e) {
            throw new ControlFlowException("Could no set ControlFlow owner, backing store failed",
                                           e);
        }
    }

    @Override
    protected void updateJoiningOwner(ControlOwner owner,
                                      int fanningIndex) {
        try {
            FileSystemManipulations.setOwnerIdentity(ControlScopeOnDisk.JOINING_PREFIX + Integer.toString(fanningIndex)
                                                     + ControlScopeOnDisk.JOINING_SUFFIX
                                                     + owner.getOwnerIdentity(),
                                                     path);
        } catch (IOException e) {
            throw new ControlFlowException("Could no set ControlFlow owner, backing store failed",
                                           e);
        }
    }

    @Override
    protected void updateOwner(ControlOwner owner) {
        try {
            FileSystemManipulations.setOwnerIdentity(owner.getOwnerIdentity(),
                                                     path);
            Path cellPath = FileSystemManipulations.createDataCellPath(path);
            FileSystemManipulations.deleteTree(cellPath);
        } catch (IOException e) {
            throw new ControlFlowException("Could no set ControlFlow owner, backing store failed",
                                           e);
        }
    }
}
