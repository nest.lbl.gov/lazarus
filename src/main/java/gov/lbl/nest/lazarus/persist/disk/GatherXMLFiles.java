package gov.lbl.nest.lazarus.persist.disk;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This class is used to collect all of the XML files.
 *
 * @author patton
 *
 * @param <T>
 *            The type of Path being iterated over.
 */
class GatherXMLFiles<T extends Path> implements
                    FileVisitor<T> {

    /**
     * The suffix used by XML files.
     */
    private final static String XML_SUFFIX = ".xml";

    /**
     * The collection of {@link Path} instance excluding the invisible files.
     */
    private final List<T> paths = new ArrayList<>();

    /**
     * Creates an instance of this class.
     */
    public GatherXMLFiles() {
    }

    /**
     * Returns the collection of {@link Path} instance excluding the invisible
     * files.
     *
     * @return the collection of {@link Path} instance excluding the invisible
     *         files.
     */
    Collection<T> getPaths() {
        return paths;
    }

    @Override
    public FileVisitResult postVisitDirectory(T dir,
                                              IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(T dir,
                                             BasicFileAttributes attrs) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(T file,
                                     BasicFileAttributes attrs) throws IOException {
        final String fileName = (file.getFileName()).toString();
        if (fileName.endsWith(XML_SUFFIX)) {
            paths.add(file);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(T file,
                                           IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

}
