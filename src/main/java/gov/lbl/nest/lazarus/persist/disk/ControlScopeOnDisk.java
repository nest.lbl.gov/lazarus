package gov.lbl.nest.lazarus.persist.disk;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.xml.bind.JAXBException;

import gov.lbl.nest.lazarus.control.ControlFlow;
import gov.lbl.nest.lazarus.control.ControlFlowException;
import gov.lbl.nest.lazarus.control.ControlFlowImpl;
import gov.lbl.nest.lazarus.control.ControlOnlyProcessImpl;
import gov.lbl.nest.lazarus.control.ControlOwner;
import gov.lbl.nest.lazarus.control.ControlScope;
import gov.lbl.nest.lazarus.control.FlowNodeImpl;
import gov.lbl.nest.lazarus.data.DataScope;
import gov.lbl.nest.lazarus.execution.TerminationListener;
import gov.lbl.nest.lazarus.execution.ThrownFailure;
import gov.lbl.nest.lazarus.process.ControlledDataScope;

/**
 * This class extends the {@link ControlledDataScope} class in order to preserve
 * its state on disk.
 *
 * @author patton
 */
public class ControlScopeOnDisk extends
                                ControlledDataScope {

    /**
     * The String used to prefix the flow index in a forked control flow's owner.
     */
    static final String FORKED_PREFIX = "[";

    /**
     * The String used to suffix the flow index in a forked control flow's owner.
     */
    static final String FORKED_SUFFIX = "]";

    /**
     * The index of the flow index in a forked control flow's owner.
     */
    private static final int FORKED_FLOW_INDEX_INDEX = 0;

    /**
     * The index of the owner index in a forked control flow's owner.
     */
    private static final int FORKED_OWNER_INDEX = FORKED_FLOW_INDEX_INDEX + 1;

    /**
     * The length of the `parts` array of a forked control flow's owner.
     */
    private static final int FORKED_PARTS_LENGTH = FORKED_OWNER_INDEX + 1;

    /**
     * The String used to prefix the flow index in a joining control flow's owner.
     */
    static final String JOINING_PREFIX = "(";

    /**
     * The String used to suffix the flow index in a joining control flow's owner.
     */
    static final String JOINING_SUFFIX = ")";

    /**
     * The index of the flow index in a joining control flow's owner.
     */
    private static final int JOINING_FLOW_INDEX_INDEX = 0;

    /**
     * The index of the owner index in a joining control flow's owner.
     */
    private static final int JOINING_OWNER_INDEX = JOINING_FLOW_INDEX_INDEX + 1;

    /**
     * The length of the `parts` array of a joining control flow's owner.
     */
    private static final int JOINING_PARTS_LENGTH = JOINING_OWNER_INDEX + 1;

    /**
     * Returns the {@link String} instance describing this object from the backing
     * store.
     *
     * @param path
     *            the {@link Path} instance use to storage this object's data.
     * @param label
     *            he {@link String} instance, if already assigned, describing this
     *            object from the backing store.
     *
     * @return the {@link String} instance describing this object from the backing
     *         store.
     *
     * @throws ControlFlowException
     *             when there is a problem reading the backing store.
     */
    private static String readLabel(Path path,
                                    String label) {
        if (null != label) {
            return label;
        }
        try {
            return FileSystemManipulations.getLabel(path);
        } catch (IOException e) {
            throw new ControlFlowException("Could not read back Label",
                                           e);
        }
    }

    /**
     * Returns the {@link String} instance describing this object from the backing
     * store.
     *
     * @param path
     *            the {@link Path} instance use to storage this object's data.
     * @param label
     *            he {@link String} instance, if already assigned, describing this
     *            object from the backing store.
     *
     * @return the {@link String} instance describing this object from the backing
     *         store.
     *
     * @throws ControlFlowException
     *             when there is a problem reading the backing store.
     */
    private static Integer readPriority(Path path,
                                        Integer priority) {
        if (null != priority) {
            return priority;
        }
        try {
            return FileSystemManipulations.getPriority(path);
        } catch (IOException e) {
            throw new ControlFlowException("Could not read back Priority",
                                           e);
        }
    }

    /**
     * The number of {@link ControlFlowOnDisk} instance created by this object.
     */
    private int flowsCreated;

    /**
     * The {@link Path} instance use to storage this object's data.
     */
    private final Path path;

    /**
     * Creates an instance of this object. This constructor is used to recover an
     * already created (but not executing) {@link ControlFlow} instance from the
     * backing store.
     * 
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance of the workflow to be
     *            executed.
     * @param listener
     *            the {@link TerminationListener} instance, if any, to callback when
     *            this object has terminated.
     * @param scope
     *            the {@link DataScope} instance used by this object.
     * @param path
     *            the {@link Path} instance use to storage this object's data.
     *
     * @throws ControlFlowException
     *             when there is a problem recovering data from the backing store.
     */
    ControlScopeOnDisk(ControlOnlyProcessImpl process,
                       TerminationListener listener,
                       DataScopeOnDisk scope,
                       Path path) {
        this(null,
             null,
             path,
             process,
             listener,
             scope);
    }

    /**
     * Creates an instance of this object. This constructor is used to recover an
     * executing {@link ControlFlow} instance from the backing store.
     * 
     * @param path
     *            the {@link Path} instance use to storage this object's data.
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance of the workflow to be
     *            executed.
     * @param listener
     *            the {@link TerminationListener} instance, if any, to callback when
     *            this object has terminated.
     * @param scope
     *            the {@link DataScope} instance used by this object.
     * 
     * @throws ControlFlowException
     *             when there is a problem recovering data from the backing store.
     */
    ControlScopeOnDisk(Path path,
                       ControlOnlyProcessImpl process,
                       TerminationListener listener,
                       DataScopeOnDisk scope) {
        super(readLabel(path,
                        null),
              readPriority(path,
                           null),
              process,
              listener,
              scope);
        this.path = path;
        final List<Path> controls = FileSystemManipulations.recoverControlsPaths(path);
        final List<String> outbounds = new ArrayList<>();
        final Map<String, List<ControlFlowOnDisk>> joins = new HashMap<>();
        final Map<String, ? extends ControlOwner> owners = process.getOwners();
        for (Path control : controls) {
            final int fanningIndex;
            final String ownerToUse;
            final boolean isJoined;
            final boolean isJoining;
            try {
                String owner = FileSystemManipulations.getOwnerIdentity(control);
                if (owner.startsWith(FlowNodeImpl.INCOMING_PREFIX)) {
                    fanningIndex = -1;
                    ownerToUse = owner;
                    isJoined = true;
                    isJoining = false;
                } else if (owner.startsWith(JOINING_PREFIX)) {
                    final String[] parts = (owner.substring(JOINING_PREFIX.length())).split("\\" + JOINING_SUFFIX);
                    if (JOINING_PARTS_LENGTH != parts.length) {
                        throw new ControlFlowException("Could not recover a joined control flow instance");
                    }
                    fanningIndex = Integer.parseInt(parts[JOINING_FLOW_INDEX_INDEX]);
                    ownerToUse = parts[JOINING_OWNER_INDEX];
                    isJoined = false;
                    isJoining = true;
                } else if (owner.startsWith(FlowNodeImpl.OUTGOING_PREFIX)) {
                    fanningIndex = -1;
                    ownerToUse = owner;
                    isJoined = false;
                    isJoining = false;
                    outbounds.add(owner);
                } else if (owner.startsWith(FORKED_PREFIX)) {
                    final String[] parts = (owner.substring(FORKED_PREFIX.length())).split(FORKED_SUFFIX);
                    if (FORKED_PARTS_LENGTH != parts.length) {
                        throw new ControlFlowException("Could not recover a forked control flow instance");
                    }
                    final String outbound = parts[FORKED_OWNER_INDEX];
                    fanningIndex = Integer.parseInt(parts[FORKED_FLOW_INDEX_INDEX]);
                    if (outbounds.contains(outbound)) {
                        ownerToUse = null;
                        FileSystemManipulations.deleteTree(control);
                    } else {
                        ownerToUse = outbound;
                    }
                    isJoined = false;
                    isJoining = false;
                } else {
                    fanningIndex = -1;
                    ownerToUse = owner;
                    isJoined = false;
                    isJoining = false;
                }
            } catch (IOException e) {
                throw new ControlFlowException("Failed to read back ControlOwner identity",
                                               e);
            }
            if (null != ownerToUse) {
                final ControlFlowOnDisk controlFlowOnDisk = new ControlFlowOnDisk(this,
                                                                                  control,
                                                                                  owners.get(ownerToUse),
                                                                                  fanningIndex);
                addControlFlow(controlFlowOnDisk);
                flowsCreated = Integer.parseInt((control.getFileName()).toString()) + 1;
                if (isJoined) {
                    final List<ControlFlowOnDisk> joining = joins.get(ownerToUse);
                    if (null != joining) {
                        for (ControlFlowOnDisk join : joining) {
                            join.destroy();
                        }
                    }
                    joins.remove(ownerToUse);
                } else if (isJoining) {
                    final List<ControlFlowOnDisk> joiningToUse;
                    final List<ControlFlowOnDisk> joining = joins.get(ownerToUse);
                    if (null == joining) {
                        joiningToUse = new ArrayList<>();
                        joins.put(ownerToUse,
                                  joiningToUse);
                    } else {
                        joiningToUse = joining;
                    }
                    joiningToUse.add(controlFlowOnDisk);
                }
            }
        }
    }

    /**
     * Creates an instance of this object. This constructor is used to create a new
     * {@link ControlFlow} instance that does not already exist in the backing
     * store.
     *
     * <b>Note:</b> This has the same arguments as another constructor, but is a
     * <em>different</em> order. This signature is for the creation of a <b>new</b>
     * {@link ControlScope} instance, not a recovered one.
     * 
     * @param label
     *            the {@link String} instance describing this object.
     * @param priority
     *            the {@link Integer} instance containing the priority this object.
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance of the workflow to be
     *            executed.
     * @param listener
     *            the {@link TerminationListener} instance, if any, to callback when
     *            this object has terminated.
     * @param scope
     *            the {@link DataScope} instance used by this object.
     * @param path
     *            the {@link Path} instance use to storage this object's data.
     * 
     * @throws ControlFlowException
     *             when there is a problem recovering data from the backing store.
     */
    ControlScopeOnDisk(String label,
                       Integer priority,
                       ControlOnlyProcessImpl process,
                       TerminationListener listener,
                       DataScopeOnDisk scope,
                       Path path) {
        this(label,
             priority,
             path,
             process,
             listener,
             scope);
        setLabel(label);
        setPriority(priority);

        // Always the last statement of this method!
        setCreated();
    }

    /**
     * Creates an instance of this object.
     *
     * <b>Note:</b> This has the same arguments as another constructor, but is a
     * <em>different</em> order. This is the private constructor used by all the
     * others.
     * 
     * @param priority
     *            the {@link Integer} instance containing the priority this object.
     * @param path
     *            the {@link Path} instance use to storage this object's data.
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance of the workflow to be
     *            executed.
     * @param listener
     *            the {@link TerminationListener} instance, if any, to callback when
     *            this object has terminated.
     * @param scope
     *            the {@link DataScopeOnDisk} instance used by this object.
     * 
     * @throws ControlFlowException
     *             when there is a problem recovering data from the backing store.
     */
    private ControlScopeOnDisk(String label,
                               Integer priority,
                               Path path,
                               ControlOnlyProcessImpl process,
                               TerminationListener listener,
                               DataScopeOnDisk scope) {
        super(readLabel(path,
                        label),
              readPriority(path,
                           priority),
              process,
              listener,
              scope);
        this.path = path;
        FileSystemManipulations.purgeAllControls(path);
    }

    @Override
    public ControlFlowOnDisk createControlFlow() {
        try {
            final Path flowPath = FileSystemManipulations.createControlFlowPath(flowsCreated++,
                                                                                path);
            final ControlFlowOnDisk result = new ControlFlowOnDisk(this,
                                                                   flowPath);
            return result;
        } catch (IOException e) {
            throw new ControlFlowException("Could not create checkpoint storage for " + (ControlFlowOnDisk.class).getName(),
                                           e);
        }
    }

    @Override
    protected ThrownFailure createThrownFailure(ControlFlowImpl control,
                                                Throwable cause) {
        return new ThrownFailureOnDisk(cause,
                                       (control.getOwner()).getOwnerIdentity(),
                                       (path.getFileName()).toString());
    }

    @Override
    protected boolean isFailed() {
        return FileSystemManipulations.isFailed(path);
    }

    @Override
    protected boolean isTerminating() {
        return FileSystemManipulations.isTerminating(path);
    }

    @Override
    protected void preserve() {
        try {
            FileSystemManipulations.preserveControlScope(path);
        } catch (IOException e) {
            throw new ControlFlowException("Could not create perveved storage for " + (ControlFlowOnDisk.class).getName(),
                                           e);
        }
    }

    @Override
    protected void purge() {
        FileSystemManipulations.deleteTree(path);
    }

    @Override
    protected void setCreated() {
        FileSystemManipulations.createCreatedFile(path);
    }

    @Override
    protected void setExecuting() {
        FileSystemManipulations.createExecutingFile(path);
    }

    @Override
    protected void setLabel(String label) {
        try {
            FileSystemManipulations.createLabelFile(label,
                                                    path);
        } catch (IOException e) {
            throw new ControlFlowException("Failed to create \"" + path.toString()
                                           + "\"",
                                           e);
        }
    }

    @Override
    protected void setPriority(Integer priority) {
        try {
            FileSystemManipulations.createPriorityFile(priority,
                                                       path);
        } catch (IOException e) {
            throw new ControlFlowException("Failed to create \"" + path.toString()
                                           + "\"",
                                           e);
        }
    }

    @Override
    protected void setTerminating() {
        FileSystemManipulations.createTerminatingFile(path);
    }

    @Override
    protected void updatedFailingToFailed() {
        try {
            FileSystemManipulations.updatedFailingToFailed(path);
        } catch (IOException e) {
            throw new ControlFlowException("Could move failing file to a failed.",
                                           e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void updateFailures(List<? extends ThrownFailure> failures) {
        try {
            FileSystemManipulations.writeFailing((List<? extends ThrownFailureOnDisk>) failures,
                                                 path);
        } catch (IOException
                 | JAXBException e) {
            throw new ControlFlowException("Could not update the failures backing store.",
                                           e);
        }
    }
}
