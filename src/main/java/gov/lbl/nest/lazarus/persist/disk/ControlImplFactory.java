package gov.lbl.nest.lazarus.persist.disk;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import gov.lbl.nest.lazarus.control.ControlFactory;
import gov.lbl.nest.lazarus.control.ControlFlowException;
import gov.lbl.nest.lazarus.control.ControlOnlyProcessImpl;
import gov.lbl.nest.lazarus.execution.TerminationListener;

/**
 * This class extends the {@link ControlFactory} class in order to preserve the
 * state in an instance on disk.
 *
 * @author patton
 */
public class ControlImplFactory extends
                                ControlFactory {

    /**
     * Creates a {@link Path} instance to use as backing store storage for an
     * object's data.
     *
     * @param fileSystem
     *            the {@link FileSystemManipulations} instance used to create the
     *            backing store.
     * @param process
     *            the name of the process for which to create the path.
     *
     * @return the {@link Path} instance to use as backing store storage for an
     *         object's data.
     *
     * @throws ControlFlowException
     *             when there is a problem creating data in the backing store.
     */
    private static Path createPath(FileSystemManipulations fileSystem,
                                   String process) {
        try {
            return fileSystem.createControlScopePath(process);
        } catch (IOException e) {
            throw new ControlFlowException("Could not create backing store storage for " + (ControlScopeOnDisk.class).getName(),
                                           e);
        }
    }

    /**
     * The {@link FileSystemManipulations} instance used by this object.
     */
    private final FileSystemManipulations fileSystem = new FileSystemManipulations(Paths.get("lazarus"));

    @Override
    public ControlScopeOnDisk createControlScope(ControlOnlyProcessImpl process,
                                                 String label,
                                                 Integer priority,
                                                 TerminationListener listener) {
        return new ControlScopeOnDisk(label,
                                      priority,
                                      process,
                                      listener,
                                      null,
                                      createPath(fileSystem,
                                                 process.getName()));
    }

    @Override
    public Collection<? extends gov.lbl.nest.lazarus.control.ControlScope> recoverControlScopes(ControlOnlyProcessImpl process,
                                                                                                TerminationListener listener) {
        final String name = process.getName();
        if (null == name) {
            throw new NullPointerException("Process is required to have a name");
        }
        final List<ControlScopeOnDisk> results = new ArrayList<>();
        final Collection<Path> instanitationPaths = fileSystem.recoverControlScopePaths(name);
        if (null != instanitationPaths && !(instanitationPaths.isEmpty())) {
            for (Path path : instanitationPaths) {
                if (FileSystemManipulations.isTerminating(path)) {
                    results.add(new ControlScopeOnDisk(process,
                                                       listener,
                                                       null,
                                                       path));
                } else if (FileSystemManipulations.isExecuting(path)) {
                    results.add(new ControlScopeOnDisk(path,
                                                       process,
                                                       listener,
                                                       null));
                } else if (FileSystemManipulations.isCreated(path)) {
                    results.add(new ControlScopeOnDisk(process,
                                                       listener,
                                                       null,
                                                       path));
                } else {
                    FileSystemManipulations.deleteTree(path);
                }
            }
        }
        return results;
    }
}
