package gov.lbl.nest.lazarus.persist.disk;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * This class is used to collect all of the files in a tree excluding ones
 * deemed invisible.
 *
 * @author patton
 *
 * @param <T>
 *            The type of Path being iterated over.
 */
class GatherVisibleFiles<T extends Path> implements
                        FileVisitor<T> {

    /**
     * The collection of file names to exclude when there are none.
     */
    private static final List<Path> NO_INVISIBLES = Collections.unmodifiableList(new ArrayList<>());

    /**
     * The collection of file names to exclude from the result of this object.
     */
    private final Collection<Path> invisible;

    /**
     * The collection of {@link Path} instance excluding the invisible files.
     */
    private final List<T> paths = new ArrayList<>();

    /**
     * Creates an instance of this class.
     * 
     * @param invisible
     *            the list of file names to exclude from the result of this object.
     */
    public GatherVisibleFiles(Collection<Path> invisible) {
        if (null == invisible) {
            this.invisible = NO_INVISIBLES;
        } else {
            this.invisible = invisible;
        }
    }

    /**
     * Returns the collection of {@link Path} instance excluding the invisible
     * files.
     *
     * @return the collection of {@link Path} instance excluding the invisible
     *         files.
     */
    Collection<T> getPaths() {
        return paths;
    }

    @Override
    public FileVisitResult postVisitDirectory(T dir,
                                              IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(T dir,
                                             BasicFileAttributes attrs) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(T file,
                                     BasicFileAttributes attrs) throws IOException {
        if (!invisible.contains((file.getFileName()))) {
            paths.add(file);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(T file,
                                           IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

}
