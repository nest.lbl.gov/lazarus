package gov.lbl.nest.lazarus.persist.disk;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.common.xml.item.Item;
import gov.lbl.nest.common.xml.item.NullValue;
import gov.lbl.nest.common.xml.item.ValueAdapter;
import gov.lbl.nest.common.xml.item.ValueAsString;
import gov.lbl.nest.common.xml.item.ValueCollection;
import gov.lbl.nest.lazarus.control.ControlFlowException;
import gov.lbl.nest.lazarus.data.DataCell;
import gov.lbl.nest.lazarus.data.DataFlowException;
import gov.lbl.nest.lazarus.data.DataObjectImpl;
import gov.lbl.nest.lazarus.data.DataOnlyActivityImpl;
import gov.lbl.nest.lazarus.data.DataScope;
import gov.lbl.nest.lazarus.data.ItemAwareElementImpl;
import gov.lbl.nest.lazarus.data.ItemDefinitionImpl;
import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import jakarta.xml.bind.JAXBException;

/**
 * This class extends the {@link DataScope} class in order to preserve its state
 * on disk.
 *
 * @author patton
 */
public class DataScopeOnDisk extends
                             DataScope {

    /**
     * If the value is a Non-JAXB class then this method wraps it, otherwise it
     * returns a value unchanged.
     *
     * @param value
     *            the {@link Object} to wrap if it is a non-JAXB class.
     *
     * @return the representation of the supplied Object that can be marshalled by
     *         JAXB.
     */
    private static Object createStoreableValue(Object value) {
        if (null == value) {
            return new NullValue();
        }
        final Class<?> clazz = value.getClass();
        final Object valueToUse;
        if ((ValueAdapter.getNonJAXBClasses()).contains(clazz)) {
            valueToUse = new ValueAsString(value);
        } else {
            valueToUse = value;
        }
        return valueToUse;
    }

    /**
     * In the value is a Non-JAXB class then this method wraps it, otherwise it
     * returns a value unchanged.
     *
     * @param value
     *            the {@link Object} to wrap if it is a non-JAXB class.
     *
     * @return the representation of the supplied Object that can be marshalled by
     *         JAXB.
     */
    private static Collection<?> createStoreableValues(Collection<?> values) {
        final List<Object> result = new ArrayList<>();
        for (Object value : values) {
            result.add(createStoreableValue(value));
        }
        return result;
    }

    /**
     * Returns the collection of {@link Class} instances needed to marshall and
     * unmarshall data in a {@link DataObjectAsXML} object.
     * 
     * @param dataObjects
     *            the collection of {@link DataObjectImpl} instance in the
     *            {@link DataObjectAsXML} instance.
     * 
     * @return the collection of {@link Class} instances needed to marshall and
     *         unmarshall data in a {@link DataObjectAsXML} object.
     */
    private static Class<?>[] getPersistentClasses(Collection<? extends DataObjectImpl> dataObjects) {
        final List<Class<?>> classList = new ArrayList<>();
        for (DataObjectImpl dataObject : dataObjects) {
            final ItemDefinitionImpl itemSubject = dataObject.getItemSubject();
            final Class<?> structure = itemSubject.getStructure();
            final Class<?> structureToUse;
            if (itemSubject.isCollection()) {
                structureToUse = ValueCollection.class;
            } else if ((ValueAdapter.getNonJAXBClasses()).contains(structure)) {
                structureToUse = ValueAsString.class;
            } else {
                structureToUse = structure;
            }
            if (!classList.contains(structureToUse)) {
                classList.add(structureToUse);
            }
        }
        return classList.toArray(new Class<?>[0]);
    }

    /**
     * Updates the "live" persistedClasses set with the {@link Class} instance of
     * the supplied value, if necessary, and writes to to the specified directory.
     *
     * @param value
     *            the representation of the {@link Object} instance that will be be
     *            marshalled by JAXB.
     * @param classes
     *            the collection of known persisted classes for the specified
     *            directory.
     * @param directory
     *            the {@link Path} instance of directory for which the set of
     *            persisted classes should be returned.
     *
     * @return the array of the classes that may be written.
     *
     * @throws DataObjectException
     *             when the classes can not be written.
     */
    private static List<Class<?>> updateClassMapping(Object value,
                                                     List<Class<?>> classes,
                                                     Path directory) throws DataObjectException {
        final Class<?> valueClazz = value.getClass();
        if (!classes.contains(valueClazz)) {
            classes.add(valueClazz);
            FileSystemManipulations.writeClasses(classes,
                                                 directory);
        }
        return classes;
    }

    /**
     * The collections of classes that have been persistent in a directory, index by
     * directory.
     */
    private final Map<Path, List<Class<?>>> persistedClassesByDirectory = new HashMap<>();

    /**
     * The {@link Path} instance for the directory in which this object stores its
     * data files.
     */
    private Path path;

    /**
     * The collection of {@link Class} instances need to write this object.
     */
    private Class<?>[] persistentClasses;

    /**
     * Creates an instance of this class.
     *
     * @param dataObjects
     *            the {@link DataObject} instances for the process, indexed by their
     *            name.
     * @param guidance
     *            the {@link Object} instance that provides guidance to the
     *            persistence system on the storage of data items.
     *
     * @throws DataFlowException
     *             when there is a problem creating data in the backing store.
     */
    public DataScopeOnDisk(Collection<? extends DataObjectImpl> dataObjects,
                           Object guidance) {
        super(dataObjects);
        if (!(guidance instanceof Path)) {
            throw new UnsupportedOperationException("The class " + getClass().getName()
                                                    + " does not support "
                                                    + (guidance.getClass()).getCanonicalName()
                                                    + " as Guidance");

        }
        try {
            this.path = FileSystemManipulations.createDataScopePath((Path) guidance);
        } catch (IOException e) {
            throw new DataFlowException("Could not create backing store storage for " + (DataScopeOnDisk.class).getName(),
                                        e);
        }
        persistentClasses = getPersistentClasses(dataObjects);
    }

    /**
     * Returns a new {@link DataCell} instance within the scope of this object.
     *
     * @param activity
     *            the {@link DataOnlyActivityImpl} instance with which this object
     *            exists.
     * @param control
     *            the {@link ControlFlowOnDisk} instance that is active when this
     *            method was invoked.
     *
     * @return the new {@link DataCell} instance within the scope of this object.
     * 
     * @throws DataObjectException
     *             when existing persisted data output value can not be written
     *             successfully.
     */
    DataCell createDataCell(DataOnlyActivityImpl activity,
                            ControlFlowOnDisk control) throws DataObjectException {
        try {
            return new DataCellOnDisk(this,
                                      activity,
                                      control.getPath());
        } catch (IOException e) {
            throw new ControlFlowException("Failed to create Data Cell directory based off \"" + path.toString()
                                           + "\"",
                                           e);
        }
    }

    @Override
    public DataCell createDataCell(DataOnlyActivityImpl activity,
                                   Object guidance) throws DataObjectException {
        if (!(guidance instanceof Path)) {
            throw new UnsupportedOperationException("The class " + getClass().getName()
                                                    + " does not support "
                                                    + (guidance.getClass()).getCanonicalName()
                                                    + " as Guidance");
        }

        try {
            return new DataCellOnDisk(this,
                                      activity,
                                      (Path) guidance);
        } catch (IOException e) {
            throw new ControlFlowException("Failed to create Data Cell directory based off \"" + path.toString()
                                           + "\"",
                                           e);
        }
    }

    @Override
    protected void createOrReplaceCollectionValues(String identity,
                                                   Collection<?> values) throws DataObjectException {
        createOrReplaceCollectionValues(identity,
                                        values,
                                        path);
    }

    /**
     * Stores the set values in the collection as an XML file.
     *
     * @param identity
     *            the identity of the {@link DataObject} instance being stored.
     * @param values
     *            the Collection on values of the Object to store.
     * @param path
     *            the {@link Path} instance of directory into which the file should
     *            be written.
     */
    void createOrReplaceCollectionValues(String identity,
                                         Collection<?> values,
                                         Path path) throws DataObjectException {
        try {
            final List<Class<?>> classes = getPersistedClasses(path);
            final Collection<?> content = createStoreableValues(values);
            for (Object storeableValue : content) {
                final Class<?> storedClass = storeableValue.getClass();
                if (!classes.contains(storedClass)) {
                    updateClassMapping(storeableValue,
                                       classes,
                                       path);
                }
            }
            final Item item = new Item(null,
                                       content);
            final ValueCollection storeableValues = item.getCollection();
            updateClassMapping(storeableValues,
                               classes,
                               path);

            final Path dataPath = path.resolve(identity);
            FileSystemManipulations.writeXML(storeableValues,
                                             classes,
                                             dataPath);
        } catch (JAXBException
                 | IOException e) {
            throw new DataObjectException(e);
        }
    }

    @Override
    protected void createOrReplaceValue(String identity,
                                        Object value) throws DataObjectException {
        createOrReplaceValue(identity,
                             value,
                             path);
    }

    /**
     * Stores the set value as an XML file.
     *
     * @param identity
     *            the identity of the {@link DataObject} instance being stored.
     * @param value
     *            the value of the Object to store.
     * @param directory
     *            the {@link Path} instance of directory into which the file should
     *            be written.
     *
     * @throws DataObjectException
     *             when the value can not be written successfully.
     */
    private void createOrReplaceValue(String identity,
                                      Object value,
                                      Path directory) throws DataObjectException {
        try {
            final List<Class<?>> classes = getPersistedClasses(directory);
            Object storeableValue = createStoreableValue(value);
            updateClassMapping(storeableValue,
                               classes,
                               directory);

            final Path dataPath = directory.resolve(identity);
            FileSystemManipulations.writeXML(storeableValue,
                                             classes,
                                             dataPath);
        } catch (JAXBException
                 | IOException e) {
            throw new DataObjectException(e);
        }
    }

    /**
     * Returns the known collection of persisted classes for the specified
     * directory.
     *
     * @param path
     *            the {@link Path} instance of directory for which the set of
     *            persisted classes should be returned.
     *
     * @return the known collection of persisted classes for the specified
     *         directory.
     */
    private List<Class<?>> getPersistedClasses(Path path) {
        final List<Class<?>> persistedClasses = persistedClassesByDirectory.get(path);
        final List<Class<?>> persistedClassesToUse;
        if (null == persistedClasses) {
            persistedClassesToUse = new ArrayList<>();
            persistedClassesByDirectory.put(path,
                                            persistedClassesToUse);
        } else {
            persistedClassesToUse = persistedClasses;
        }
        return persistedClassesToUse;
    }

    /**
     * Recovers the values of the {@link DataObjectImpl} instances in this object.
     *
     * @throws DataFlowException
     *             when a {@link DataObjectImpl} can not be successfully recovered.
     */
    void recover() {
        final Collection<? extends DataObjectImpl> dataObjects = getDataObjects();
        final Map<String, Object> values = new HashMap<>();
        for (DataObjectImpl dataObject : dataObjects) {
            final String identity = dataObject.getIdentity();
            final Object value = recoverValue(identity);
            if (null != value) {
                values.put(identity,
                           value);
            }
        }
        setRecoveredValues(values);
    }

    /**
     * Returns the recovered value, if any, for the {@link DataObjectImpl} instance
     * specified by the supplied identity.
     *
     * @param identity
     *            the identity of the {@link DataObjectImpl} to be recovered.
     *
     * @return the recovered value, if any, for the {@link DataObjectImpl} instance
     *         specified by the supplied identity.
     *
     * @throws DataFlowException
     *             when the value can not be recovered.
     */
    private Object recoverValue(String identity) {
        return recoverValue(identity,
                            path);

    }

    /**
     * Returns the recovered value, if any, for the {@link ItemAwareElementImpl}
     * instance specified by the supplied identity.
     *
     * @param identity
     *            the identity of the {@link ItemAwareElementImpl} to be recovered.
     * @param path
     *            the {@link Path} instance of directory into which the file should
     *            be written.
     *
     * @return the recovered value, if any, for the {@link ItemAwareElementImpl}
     *         instance specified by the supplied identity.
     *
     * @throws DataFlowException
     *             when the value can not be recovered.
     */
    Object recoverValue(String identity,
                        Path path) {
        final Path dataPath = path.resolve(identity);
        if (!FileSystemManipulations.existingXML(dataPath)) {
            return null;
        }
        try {
            final List<Class<?>> classes = FileSystemManipulations.readClasses(path);
            final Object value = FileSystemManipulations.readXML(classes,
                                                                 dataPath);
            if (value instanceof ValueAsString) {
                return ((ValueAsString) value).getObject();
            } else if (value instanceof ValueCollection) {
//                final List<Value> contents = ((ValueCollection) value).getValues();
//                final int size = contents.size();
//                final List<Object> result = new ArrayList<>(size);
//                for (Value entry : contents) {
//                    result.add(entry.getObject());
//                }
//                return result;
                return ((ValueCollection) value).getObject();
            }
            return value;
        } catch (JAXBException
                 | IOException e) {
            throw new DataFlowException(e);
        }
    }
}
