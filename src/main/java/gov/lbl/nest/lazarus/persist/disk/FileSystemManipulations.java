
package gov.lbl.nest.lazarus.persist.disk;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.xml.item.NullValue;
import gov.lbl.nest.lazarus.control.ControlFlow;
import gov.lbl.nest.lazarus.control.ControlFlowException;
import gov.lbl.nest.lazarus.control.ControlOwner;
import gov.lbl.nest.lazarus.control.ControlScope;
import gov.lbl.nest.lazarus.data.DataFlowException;
import gov.lbl.nest.lazarus.execution.ThrownFailure;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.DataOutput;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

/**
 * This class is used to manipulate a disk's file system as needed by the
 * classes in this package,
 *
 * @author patton
 */
public class FileSystemManipulations {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(FileSystemManipulations.class);

    /**
     * The name of the directory containing the data for all
     * {@link ControlScopeOnDisk} instances using this object.
     */
    protected static final Path CONTROLS_DIRECTORY = Paths.get("controls");

    /**
     * The name of the directory containing the data for all
     * {@link ControlScopeOnDisk} instances using this object.
     */
    protected static final Path DATA_CELL_DATA_DIRECTORY = Paths.get("data_outputs");

    /**
     * The name of the directory containing the data for all
     * {@link ControlScopeOnDisk} instances using this object.
     */
    protected static final Path DATA_SCOPE_DATA_DIRECTORY = Paths.get("data_objects");

    /**
     * The name of the directory containing the data for the active
     * {@link ControlScopeOnDisk} instances using this object.
     */
    protected static final Path ACTIVE_CONTROLS_DIRECTORY = Paths.get("active");

    /**
     * The name of the directory containing the data for the preserved
     * {@link ControlScopeOnDisk} instances that used this object.
     */
    protected static final Path PRESERVED_CONTROLS_DIRECTORY = Paths.get("preserved");

    /**
     * The name of the directory containing the data for the rescued
     * {@link ControlScopeOnDisk} instances using this object.
     */
    protected static final Path RESCUED_CONTROLS_DIRECTORY = Paths.get("rescued");

    /**
     * The suffix to use when writing files.
     */
    private static final String TMP_SUFFIX = ".tmp";

    /**
     * The name of the file in which the number to assign to the next new instance
     * is stored.
     */
    protected static final String NEXT_INSTANCE_FILE = "next_instance.txt";

    /**
     * The name of the file in which the number to assign to the next new instance
     * is stored.
     */
    protected static final String NEXT_INSTANCE_TMP_FILE = NEXT_INSTANCE_FILE + TMP_SUFFIX;

    /**
     * The suffix to use when writing files.
     */
    static final String XML_SUFFIX = ".xml";

    /**
     * The name of the file containing the label of a {@link ControlScopeOnDisk}.
     */
    protected static final Path LABEL_FILE = Paths.get("label.txt");

    /**
     * The name of the file containing the priority of a {@link ControlScopeOnDisk}.
     */
    protected static final Path PRIORITY_FILE = Paths.get("priority.txt");

    /**
     * The name of the file flagging an {@link ControlScopeOnDisk} is being
     * terminated.
     */
    protected static final Path CREATED_FILE = Paths.get("created");

    /**
     * The name of the file flagging an {@link ControlScopeOnDisk} is being
     * terminated.
     */
    protected static final Path EXECUTING_FILE = Paths.get("executing");

    /**
     * The name of the file flagging an {@link ControlScopeOnDisk} is being
     * terminated.
     */
    protected static final Path TERMINATING_FILE = Paths.get("terminating");

    /**
     * The name of the file containing the identity of a {@link ControlFlowOnDisk}
     * instance's owner.
     */
    protected static final Path OWNER_FILE = Paths.get("owner");

    /**
     * The time, in millis, to wait between re-trying the creation of a directory
     * after a FileSystemException exception is thrown.
     */
    private static final long DIRECTORY_RETRY_INTERVAL = 100;

    /**
     * The time, in millis, to wait between re-trying to use a file after it has
     * been flushed but then a FileSystemException exception is thrown.
     */
    private static final long FLUSH_RETRY_INTERVAL = 100;

    /**
     * The number of time an operation will be attempted before being considered a
     * "hard" failure.
     */
    private static final int RETRY_LIMIT = 3;

    /**
     * The possible states an instance can be in, in order.
     */
    private static final List<Path> STATES = Arrays.asList(new Path[] { null,
                                                                        CREATED_FILE,
                                                                        EXECUTING_FILE,
                                                                        TERMINATING_FILE,
                                                                        null });

    /**
     * The string used to separate branch indices for a multi-instance activity.
     */
    private final static String BRANCHES_CONJUNCTION = " ";

    /**
     * The name of the file containing the branch indices for a multi-instance
     * activity.
     */
    private static final String BRANCHES_FILE = "branches.txt";

    /**
     * The collection of branches to return when there are none.
     */
    private static final List<Integer> NO_BRANCHES = Collections.unmodifiableList(new ArrayList<>());

    /**
     * The string used to separate class names.
     */
    private static final String CLASS_CONJUNCTION = ":";

    /**
     * The name of the file containing the class of each data object.
     */
    private static final Path CLASSES_TXT_PATH = Paths.get("classes.txt");

    /**
     * The name of the file in which to write {@link ThrownFailure} instances while
     * a {@link ControlScopeOnDisk} is failing.
     */
    private static final Path FAILING_FILE = Paths.get("failing");

    /**
     * The name of the file in which to write {@link ThrownFailure} instances while
     * a {@link ControlScopeOnDisk} is failing.
     */
    private static final Path FAILED_FILE = Paths.get("failed");

    /**
     * The {@link Class} instance needed to read or write the failure XML objects.
     */
    private static final List<Class<?>> FAILURE_CLASSES = Arrays.asList(new Class[] { ThrownFailureOnDisk.class,
                                                                                      ThrownFailuresOnDisk.class });

    /**
     * Creates a new directory for a new {@link ControlFlowOnDisk} instance to use.
     *
     * @param i
     *            the number of {@link ControlFlowOnDisk} instances already created
     *            by the {@link ControlScopeOnDisk} instance.
     * @param directory
     *            the path of the directory for the {@link ControlScopeOnDisk}
     *            instance creating the new {@link ControlFlowOnDisk} instance.
     *
     * @return the created directory.
     *
     * @throws IOException
     *             when then is a problem creating the directory.
     */
    static Path createControlFlowPath(int i,
                                      Path directory) throws IOException {
        final Path result = (directory.resolve(CONTROLS_DIRECTORY)).resolve(Integer.toString(i));
        return createDirectories(result);
    }

    /**
     * Creates the file that indicates the {@link ControlScopeOnDisk} instance
     * implied by the directory has been created.
     *
     * @param directory
     *            the path of the directory for the {@link ControlScopeOnDisk}
     *            instance that has been created.
     */
    static void createCreatedFile(Path directory) {
        createStateFile(CREATED_FILE,
                        null,
                        directory);
    }

    /**
     * Creates a new directory for a new {@link DataCellOnDisk} instance to use.
     *
     * @param path
     *            the {@link Path} instance of the directory where to create the
     *            {@link DataScopeOnDisk} instance directory.
     *
     * @return the created directory.
     *
     * @throws IOException
     *             when then is a problem creating the directory.
     */

    static Path createDataCellPath(Path path) throws IOException {
        final Path result = path.resolve(DATA_CELL_DATA_DIRECTORY);
        return createDirectories(result);
    }

    /**
     * Creates a new directory for a new {@link DataScopeOnDisk} instance to use.
     *
     * @param path
     *            the {@link Path} instance of the directory where to create the
     *            {@link DataScopeOnDisk} instance directory.
     *
     * @return the created directory.
     *
     * @throws IOException
     *             when then is a problem creating the directory.
     */
    static Path createDataScopePath(Path path) throws IOException {
        final Path result = path.resolve(DATA_SCOPE_DATA_DIRECTORY);
        return createDirectories(result);
    }

    /**
     * Creates, if possible, the specified directory.
     *
     * @param path
     *            the {@link Path} instance of the directory to be created.
     *
     * @return the created directory.
     *
     * @throws IOException
     *             when then is a problem creating the directory.
     */
    private static Path createDirectories(Path path) throws IOException {
        int retries = 0;
        while (true) {
            try {
                return Files.createDirectories(path);
            } catch (IOException e) {
                retries = waitForRetry(retries,
                                       "Attempting retry of directories creation of \"" + path.toString()
                                                + "\" having waited "
                                                + DIRECTORY_RETRY_INTERVAL
                                                + " milliseconds",
                                       "Failed to create directories \"" + path
                                                                   + "\"");
            }
        }
    }

    /**
     * Creates the file that indicates the {@link ControlScopeOnDisk} instance
     * implied by the directory is executing.
     *
     * @param path
     *            the {@link Path} instance of the directory for the
     *            {@link ControlScopeOnDisk} instance that is executing.
     */
    static void createExecutingFile(Path path) {
        createStateFile(EXECUTING_FILE,
                        CREATED_FILE,
                        path);
    }

    /**
     * Create a new file to hold the label of the supplied
     * {@link ControlScopeOnDisk} instance.
     *
     * @param label
     *            the {@link String} to be written to the file.
     * @param path
     *            the {@link Path} instance of the directory for the
     *            {@link ControlScopeOnDisk} instance whose label should be recorded
     *
     * @throws IOException
     *             when the identity can not be written.
     */
    static void createLabelFile(String label,
                                Path path) throws IOException {
        if (null == label) {
            return;
        }
        final Path labelFile = path.resolve(LABEL_FILE);
        writeString(label,
                    labelFile);
    }

    /**
     * Create a new file to hold the priority of the supplied
     * {@link ControlScopeOnDisk} instance.
     *
     * @param label
     *            the {@link String} to be written to the file.
     * @param path
     *            the {@link Path} instance of the directory for the
     *            {@link ControlScopeOnDisk} instance whose priority should be
     *            recorded
     *
     * @throws IOException
     *             when the identity can not be written.
     */
    static void createPriorityFile(Integer priority,
                                   Path path) throws IOException {
        if (null == priority) {
            return;
        }
        final Path priorityFile = path.resolve(PRIORITY_FILE);
        writeString(priority.toString(),
                    priorityFile);
    }

    /**
     * Create a new state file to preserve the new state.
     *
     * @param newState
     *            the new state to be written.
     * @param oldState
     *            the old state, if any, that should be deleted.
     * @param path
     *            the path of the directory for the {@link ControlScopeOnDisk}
     *            instance whose state is to be recorded.
     */
    private static void createStateFile(Path newState,
                                        Path oldState,
                                        Path path) {
        final Path newFile = path.resolve(newState);
        final Path oldFile;
        if (null == oldState) {
            oldFile = null;
        } else {
            oldFile = path.resolve(oldState);
        }
        try {
            Files.createFile(newFile);
            if (null != oldFile) {
                try {
                    Files.delete(oldFile);
                } catch (NoSuchFileException e) {
                    // Do nothing
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to create \"" + newFile.toString()
                                            + "\"");
        }
    }

    /**
     * Creates the file that indicates the {@link ControlScopeOnDisk} instance
     * implied by the directory is terminating.
     *
     * @param directory
     *            the path of the directory for the {@link ControlScopeOnDisk}
     *            instance that is terminating.
     */
    static void createTerminatingFile(Path directory) {
        createStateFile(TERMINATING_FILE,
                        EXECUTING_FILE,
                        directory);
    }

    /**
     * Deletes this specified {@link Path} instance and, if it is a directory, all
     * files it contains.
     *
     * @param file
     *            the root of the tree to be deleted.
     */
    static void deleteTree(Path path) {
        if (Files.notExists(path)) {
            return;
        }

        /*
         * To make sure the directory deletion is atomic, first move it sideways.
         */
        final Path tmpFile = Paths.get((path.getFileName()).toString() + TMP_SUFFIX);
        final Path pathToDelete = (path.getParent()).resolve(tmpFile);
        try {
            Files.move(path,
                       pathToDelete,
                       StandardCopyOption.REPLACE_EXISTING,
                       StandardCopyOption.ATOMIC_MOVE);
        } catch (IOException e1) {
            LOG.warn("The file tree at " + path.toString()
                     + " was not be successfully deleted");
        }

        try (final Stream<Path> paths = Files.walk(pathToDelete)) {
            final Stream<Path> filesFirst = paths.sorted(Comparator.reverseOrder());
            final Stream<File> files = filesFirst.map(Path::toFile);
            files.forEach(File::delete);
        } catch (IOException e) {
            LOG.warn("The file tree at " + path.toString()
                     + " was not successfully deleted");
        }
    }

    /**
     * Returns <code>true</code> if an XML files exists associated with the supplied
     * path.
     *
     * @param path
     *            the {@link Path} instance of the file from which the XML should be
     *            read.
     *
     * @return <code>true</code> if an XML files exists associated with the supplied
     *         path.
     */
    static boolean existingXML(Path path) {
        final Path directory = path.getParent();
        final Path pathToTest = directory.resolve((path.getFileName()).toString() + XML_SUFFIX);
        return Files.exists(pathToTest);
    }

    /**
     * Returns the {@link String} instance that is the label for the supplied
     * {@link ControlScopeOnDisk} instance.
     *
     * @param scope
     *            the path of the {@link ControlScopeOnDisk} instance whose label
     *            should be returned.
     *
     * @returns the {@link String} instance that is the label for the supplied
     *          {@link ControlScopeOnDisk} instance.
     *
     * @throws IOException
     *             when the label can not be read.
     */
    static String getLabel(Path scope) throws IOException {
        final Path labelFile = scope.resolve(LABEL_FILE);
        if (Files.notExists(labelFile)) {
            return null;
        }
        return readString(labelFile);
    }

    /**
     * Returns the identity of the {@link ControlOwner} instance owning the
     * {@link ControlFlowOnDisk} instance whose path is supplied.
     *
     * @param path
     *            the path of the {@link ControlFlowOnDisk} instance whose
     *            {@link ControlOwner} identity should be returned.
     *
     * @return the identity of the {@link ControlOwner} instance owning the
     *         {@link ControlFlowOnDisk} instance whose path is supplied.
     *
     * @throws IOException
     *             when the identity can not be read.
     */
    static String getOwnerIdentity(Path path) throws IOException {
        final Path ownerFile = path.resolve(OWNER_FILE);
        if (Files.notExists(ownerFile)) {
            return null;
        }
        return readString(ownerFile);
    }

    /**
     * Returns the {@link Integer} instance that is the priority for the supplied
     * {@link ControlScopeOnDisk} instance.
     *
     * @param scope
     *            the path of the {@link ControlScopeOnDisk} instance whose label
     *            should be returned.
     *
     * @returns the {@link Integer} instance that is the label for the supplied
     *          {@link ControlScopeOnDisk} instance.
     *
     * @throws IOException
     *             when the label can not be read.
     */
    static Integer getPriority(Path scope) throws IOException {
        final Path priorityFile = scope.resolve(PRIORITY_FILE);
        if (Files.notExists(priorityFile)) {
            return 0;
        }
        return Integer.parseInt((readString(priorityFile)));
    }

    /**
     * Returns <code>true</code> if the supplied {@link ControlScopeOnDisk} instance
     * is in the terminating state.
     *
     * @param path
     *            the {@link Path} instance of the directory for the
     *            {@link ControlScopeOnDisk} instance that is executing.
     *
     * @return <code>true</code> if the supplied {@link ControlScopeOnDisk} instance
     *         is in the terminating state.
     */
    static boolean isCreated(Path path) {
        return stateFileExists(STATES.subList(0,
                                              3),
                               path);
    }

    /**
     * Returns <code>true</code> if the supplied {@link ControlScopeOnDisk} instance
     * is in the executing state.
     *
     * @param path
     *            the {@link Path} instance of the {@link ControlScopeOnDisk}
     *            instance that is executing.
     *
     * @return <code>true</code> if the supplied {@link ControlScopeOnDisk} instance
     *         is in the terminating state.
     */
    static boolean isExecuting(Path path) {
        return stateFileExists(STATES.subList(1,
                                              4),
                               path);
    }

    /**
     * Returns <code>true</code> if the supplied {@link ControlScopeOnDisk} instance
     * has failed.
     *
     * @param path
     *            the {@link Path} instance of the {@link ControlScopeOnDisk}
     *            instance that may have failed
     *
     * @return <code>true</code> if the supplied {@link ControlScopeOnDisk} instance
     *         has failed.
     */
    static boolean isFailed(Path path) {
        final Path failedFile = path.resolve(FAILED_FILE.toString() + XML_SUFFIX);
        return Files.exists(failedFile);
    }

    /**
     * Returns <code>true</code> if the supplied {@link ControlScopeOnDisk} instance
     * is in the terminating state.
     *
     * @param path
     *            the {@link Path} instance of the directory for the
     *            {@link ControlScopeOnDisk} instance that is executing.
     *
     * @return <code>true</code> if the supplied {@link ControlScopeOnDisk} instance
     *         is in the terminating state.
     */
    static boolean isTerminating(Path path) {
        return stateFileExists(STATES.subList(2,
                                              5),
                               path);
    }

    /**
     * Moves a active {@link ControlScopeOnDisk} instance into the preserved area.
     *
     * @param path
     *            the {@link Path} instance for the directory of
     *            {@link ControlScopeOnDisk} to be preserved.
     *
     * @throws IOException
     *             when the preserved area can not be created.
     */
    static void preserveControlScope(Path path) throws IOException {
        final Path activeDirectory = path.getParent();
        if (!ACTIVE_CONTROLS_DIRECTORY.equals(activeDirectory.getFileName())) {
            throw new ControlFlowException("Original ControlScope is not store in the correct directory");
        }
        final Path rootDirectory = activeDirectory.getParent();
        final Path preservedDirectory = rootDirectory.resolve(PRESERVED_CONTROLS_DIRECTORY);
        createDirectories(preservedDirectory);
        final Path result = preservedDirectory.resolve(path.getFileName());
        Files.move(path,
                   result,
                   StandardCopyOption.REPLACE_EXISTING,
                   StandardCopyOption.ATOMIC_MOVE);
    }

    /**
     * Purges the backing store of information for all {@link ControlFlowOnDisk}
     * instances belonging to the implied {@link ControlScopeOnDisk} instance.
     *
     * @param directory
     *            the path of the directory for the {@link ControlScopeOnDisk}
     *            instance that is being queried.
     */
    static void purgeAllControls(Path directory) {
        final Path controls = directory.resolve(CONTROLS_DIRECTORY);
        deleteTree(controls);
    }

    /**
     * Returns the collection of branches, if any, of a multi-instance activity.
     *
     * @param path
     *            the directory from which to read the branches.
     *
     * @return the collection of branches, if any, of a multi-instance activity.
     *
     * @throws IOException
     *             when the branches can not be read successfully.
     */
    static List<Integer> readBranches(Path path) throws IOException {
        final Path branchesFile = path.resolve(BRANCHES_FILE);
        if (!Files.exists(branchesFile)) {
            return null;
        }
        final String branches = readString(branchesFile);
        final String[] strings = branches.split(BRANCHES_CONJUNCTION);
        if (1 == strings.length && "".equals(strings[0])) {
            return NO_BRANCHES;
        }

        final List<Integer> results = new ArrayList<>();
        for (String string : strings) {
            results.add(Integer.valueOf(string));
        }
        return results;
    }

    /**
     * Returns the set of {@link Class} instances in the specified directory.
     *
     * @param path
     *            the directory from which to read the classes.
     *
     * @return the set of {@link Class} instances in the specified directory.
     *
     * @throws IOException
     *             when the classes can not be read successfully.
     * @throws DataFlowException
     *             when a class can not be found.
     */
    static List<Class<?>> readClasses(Path path) throws IOException {
        final Path classesFile = path.resolve(CLASSES_TXT_PATH);
        final String classes = readString(classesFile);
        final String[] names = (classes.trim()).split(CLASS_CONJUNCTION);
        final List<Class<?>> result = new ArrayList<>();
        try {
            for (String name : names) {
                final Class<?> clazz = ((FileSystemManipulations.class).getClassLoader()).loadClass(name);
                result.add(clazz);
            }
            return result;
        } catch (ClassNotFoundException e) {
            throw new DataFlowException(e);
        }
    }

    /**
     * Reads a single {@link String} instance back from a file.
     *
     * @param path
     *            the {@link Path} instance of the file to read.
     *
     * @return the {@link String} instance read, or null if the path is null.
     *
     * @throws IOException
     *             if the {@link String} can not be read successfully.
     */
    static String readString(Path path) throws IOException {
        if (null == path) {
            return null;
        }
        final String result;
        try (final BufferedReader bw = Files.newBufferedReader(path)) {
            result = bw.readLine();
            bw.close();
        }
        return result;
    }

    /**
     * Reads a single {@link Object} instance to a file as XML.
     *
     * @param classes
     *            The collection of classes of {@link Object} instances being read.
     * @param path
     *            the {@link Path} instance of the file from which the XML should be
     *            read.
     *
     * @throws JAXBException
     *             if the {@link Object} can not be unmarshalled into XML.
     * @throws IOException
     *             if the {@link Object} can not be read successfully.
     */
    static Object readXML(List<Class<?>> classes,
                          Path path) throws IOException,
                                     JAXBException {
        final Path directory = path.getParent();
        final Path pathToRead = directory.resolve((path.getFileName()).toString() + XML_SUFFIX);
        if (!Files.exists(pathToRead)) {
            return null;
        }
        JAXBContext jc = JAXBContext.newInstance(classes.toArray(new Class[0]));
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        final Object value = unmarshaller.unmarshal(pathToRead.toFile());
        return value;
    }

    /**
     * Returns the sequence of {@link Path} instances that contain the known
     * {@link ControlFlowOnDisk} instances for the implied
     * {@link ControlScopeOnDisk} instance.
     *
     * @param directory
     *            the path of the directory for the {@link ControlScopeOnDisk}
     *            instance whose path to its {@link ControlFlowOnDisk} instances
     *            should be returned.
     *
     * @return the sequence of {@link Path} instances that contain the known
     *         {@link ControlFlowOnDisk} instances for the implied
     *         {@link ControlScopeOnDisk} instance.
     */
    static List<Path> recoverControlsPaths(Path directory) {
        final Path controlsDirectory = directory.resolve(CONTROLS_DIRECTORY);
        final GatherNumericDirectories<Path> numericFiles = new GatherNumericDirectories<>();
        try {
            Files.walkFileTree(controlsDirectory,
                               new HashSet<FileVisitOption>(),
                               1,
                               numericFiles);
            return numericFiles.getPaths();
        } catch (IOException e) {
            throw new IllegalStateException("Failure in backing store!",
                                            e);
        }
    }

    /**
     * Sets the identity of the {@link ControlOwner} instance owning the
     * {@link ControlFlowOnDisk} instance whose path is supplied.
     *
     * @param owner
     *
     * @param path
     *            the path of the {@link ControlFlowOnDisk} instance whose
     *            {@link ControlOwner} identity should be set.
     *
     * @throws IOException
     *             when the identity can not be written.
     */
    static void setOwnerIdentity(String owner,
                                 Path controlFlow) throws IOException {
        final Path ownerFile = controlFlow.resolve(OWNER_FILE);
        writeString(owner,
                    ownerFile);
    }

    /**
     * Returns <code>true</code> if the requested "current" (the second element in
     * the list) state file exists and the "next" one (the third element) does not.
     *
     * If this will return <code>true</code>, then it also makes sure that the
     * "previous" state file (the first) does not exist, deleting it if necessary.
     * Also if the "next" state file does exist, it makes sure that the "current"
     * state file does not exist, deleting it if necessary
     *
     * @param states
     *            the list of the previous, current, and next states to be tested.
     * @param directory
     *            the path of the directory for the {@link ControlScopeOnDisk}
     *            instance that is being queried.
     *
     *
     * @return <code>true</code> if the requested "current" (the second element in
     *         the list) state file exists and the "next" one (the third element)
     *         does not.
     */
    private static boolean stateFileExists(List<Path> states,
                                           Path directory) {
        final Path currentFile = directory.resolve(states.get(1));
        if (Files.exists(currentFile)) {
            final Path nextState = states.get(2);
            if (null != nextState) {
                final Path nextFile = directory.resolve(nextState);
                if (Files.exists(nextFile)) {
                    try {
                        Files.delete(currentFile);
                    } catch (IOException e) {
                        // No nothing, this was only clean living in any case!
                    }
                    return false;
                }
            }
            final Path previousState = states.get(0);
            if (null != previousState) {
                final Path previousFile = directory.resolve(previousState);
                if (Files.exists(previousFile)) {
                    try {
                        Files.delete(currentFile);
                    } catch (IOException e) {
                        // No nothing, this was only clean living in any case!
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Moves a failing file to be a failed one.
     *
     * @param path
     *            the {@link Path} instance of the {@link ControlScopeOnDisk}
     *            instance whose failures are being moved.
     *
     * @throws IOException
     *             when the file can not be moved.
     */
    static void updatedFailingToFailed(Path path) throws IOException {
        final Path failingFile = path.resolve(FAILING_FILE.toString() + XML_SUFFIX);
        final Path failedFile = path.resolve(FAILED_FILE.toString() + XML_SUFFIX);
        Files.move(failingFile,
                   failedFile,
                   StandardCopyOption.REPLACE_EXISTING,
                   StandardCopyOption.ATOMIC_MOVE);
    }

    /**
     * Moves the recovered directory and a {@link ControlScopeOnDisk} instance to be
     * an active one.
     *
     * @param path
     *            the recover {@link Path} instance of the
     *            {@link ControlScopeOnDisk} instance being recovered.
     * 
     * @return the Path instance of the active {@link ControlScopeOnDisk} instance.
     *
     * @throws IOException
     *             when the file can not be moved.
     */
    static Path updatedRecoveringToActive(Path path) throws IOException {
        final Path recoveredDirectory = path.getParent();
        final Path rootDirectory = recoveredDirectory.getParent();
        final Path activeDirectory = rootDirectory.resolve(ACTIVE_CONTROLS_DIRECTORY);
        createDirectories(activeDirectory);
        final Path result = activeDirectory.resolve(path.getFileName());
        Files.move(path,
                   result,
                   StandardCopyOption.REPLACE_EXISTING,
                   StandardCopyOption.ATOMIC_MOVE);
        return result;
    }

    /**
     * Waits to retry a file system operation.
     *
     * @param retries
     *            the current number of retries that has been attempted.
     * @param warning
     *            the warning to the log file that a retry is being attempted.
     * @param message
     *            the message to include in the {@link IOException} that will be
     *            thrown if the retry limit has been reached.
     *
     * @return the updated number of retries.
     *
     * @throws IOException
     */
    private static int waitForRetry(final int retries,
                                    final String warning,
                                    final String message) throws IOException {
        if (RETRY_LIMIT == retries) {
            throw new IOException(message);
        }
        try {
            Thread.sleep(DIRECTORY_RETRY_INTERVAL);
            LOG.warn(warning);
        } catch (InterruptedException e) {
            // Do nothing and let the retry go ahead
        }
        return retries + 1;
    }

    /**
     * Writes the collection of branches, if any, of a multi-instance activity.
     *
     * @param branches
     *            the collection of branches, if any, that will be recovered by this
     *            object if it is executing a multi-instance activity.
     *
     * @throws IOException
     *             if the collection can not be written successfully.
     */
    static void writeBranches(List<Integer> branches,
                              Path path) throws IOException {
        final Path branchesFile = path.resolve(BRANCHES_FILE);
        final StringBuilder sb = new StringBuilder();
        String conjunction = "";
        for (Integer i : branches) {
            sb.append(conjunction);
            sb.append(i.toString());
            conjunction = BRANCHES_CONJUNCTION;
        }
        FileSystemManipulations.writeString(sb.toString(),
                                            branchesFile);
    }

    /**
     * Writes details of the set of {@link Class} instances into the specified
     * directory.
     *
     * @param classes
     *            the collection of {@link Class} instances to be written.
     * @param path
     *            the directory into which to write the class's file.
     *
     * @throws DataObjectException
     *             when the classes can not be written successfully
     */
    static void writeClasses(List<Class<?>> classes,
                             Path path) throws DataObjectException {
        final Path classesFile = path.resolve(CLASSES_TXT_PATH);
        final StringBuilder sb = new StringBuilder();
        String conjunction = "";
        for (Class<?> clazz : classes) {
            sb.append(conjunction);
            sb.append(clazz.getName());
            conjunction = CLASS_CONJUNCTION;
        }
        try {
            writeString(sb.toString(),
                        classesFile);
        } catch (IOException e1) {
            throw new DataObjectException(e1);
        }
    }

    /**
     * Captures the supplied sequence of the {@link ThrownFailure} instances.
     *
     * @param failures
     *            the {@link Throwable} instances that have been thrown within any
     *            {@link ControlFlow} instance belonging to this object.
     *
     * @param path
     *            the {@link Path} instance of the {@link ControlScopeOnDisk}
     *            instance whose failures are being written.
     *
     * @throws JAXBException
     *             if the {@link Object} can not be marshalled into XML.
     * @throws IOException
     *             if the {@link Object} can not be written successfully.
     */
    static void writeFailing(List<? extends ThrownFailureOnDisk> failures,
                             Path path) throws IOException,
                                        JAXBException {
        final Path failingFile = path.resolve(FAILING_FILE);
        if (1 == failures.size()) {
            writeXML(failures.get(0),
                     FAILURE_CLASSES,
                     failingFile);
        }
        writeXML(new ThrownFailuresOnDisk(failures),
                 FAILURE_CLASSES,
                 failingFile);
    }

    /**
     * Writes a single {@link String} instance to a file.
     *
     * @param string
     *            the {@link String} instance to be written.
     * @param path
     *            the {@link Path} instance of the file into which the
     *            {@link String} instance will be written.
     *
     * @throws IOException
     *             if the {@link String} can not be written successfully.
     */
    static void writeString(String string,
                            Path path) throws IOException {
        if (null != string) {
            Path directory = path.getParent();
            if (!Files.exists(directory)) {
                createDirectories(directory);
            }
            final Path tmpFile = Paths.get((path.getFileName()).toString() + TMP_SUFFIX);
            final Path pathToWrite = (path.getParent()).resolve(tmpFile);
            try (final BufferedWriter bw = Files.newBufferedWriter(pathToWrite)) {
                bw.write(string);
                bw.newLine();
                bw.close();
            }
            try {
                Files.move(pathToWrite,
                           path,
                           StandardCopyOption.REPLACE_EXISTING,
                           StandardCopyOption.ATOMIC_MOVE);
            } catch (IOException e) {
                LOG.warn("The file at " + path.toString()
                         + " could not be successfully written");
            }
        }
    }

    /**
     * Writes a single {@link Object} instance to a file as XML.
     *
     * @param storeableValue
     *            the {@link Object} instance to be written.
     * @param classes
     *            The collection of classes of {@link Object} instances be written.
     * @param path
     *            the {@link Path} instance of the file into which the XML will be
     *            written.
     *
     * @throws JAXBException
     *             if the {@link Object} can not be marshalled into XML.
     * @throws IOException
     *             if the {@link Object} can not be written successfully.
     */
    static void writeXML(Object storeableValue,
                         List<Class<?>> classes,
                         Path path) throws IOException,
                                    JAXBException {
        final Path directory = path.getParent();
        if (!Files.exists(directory)) {
            createDirectories(directory);
        }
        final Path pathToWrite = directory.resolve((path.getFileName()).toString() + XML_SUFFIX
                                                   + TMP_SUFFIX);
        try {
            Class<?>[] clazzes = classes.toArray(new Class<?>[0]);
            JAXBContext jc = JAXBContext.newInstance(clazzes);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.marshal(storeableValue,
                               pathToWrite.toFile());
            Files.move(pathToWrite,
                       directory.resolve((path.getFileName()).toString() + XML_SUFFIX),
                       StandardCopyOption.REPLACE_EXISTING,
                       StandardCopyOption.ATOMIC_MOVE);
        } catch (IOException e) {
            LOG.warn("The file at " + path.toString()
                     + " could not be successfully written");
            throw e;
        }
    }

    /**
     * The number to assign to the next new ControlScope Path.
     */
    private int nextInstance = -1;

    /**
     * The {@link Path} instance of the file in which the number to assign to the
     * next new ControlScope Path is stored.
     */
    private final Path nextControlScopeFile;

    /**
     * The {@link Path} instance of the file in which the number to assign to the
     * next new ControlScope Path is initially written, in order to keep the file
     * change atomic.
     */
    private final Path nextControlScopeFileTmp;

    /**
     * The {@link Path} instance of the root of the tree used by this object.
     */
    private final Path root;

    /**
     * Creates an instance of this class.
     *
     * @param root
     *            the root of the directory tree used by this instance.
     */
    FileSystemManipulations(Path root) {
        this.root = root;
        try {
            createDirectories(root);
        } catch (IOException e) {
            throw new IllegalStateException("Could not create root directory for tree",
                                            e);
        }
        nextControlScopeFile = root.resolve(NEXT_INSTANCE_FILE);
        /*
         * Next block could be rewritten using a SeekableByteChannel when there is
         * nothing else to do.
         */
        try (RandomAccessFile tmpFile = new RandomAccessFile(nextControlScopeFile.toFile(),
                                                             "rwd")) {
            synchronized (nextControlScopeFile) {
                if (-1 == nextInstance) {
                    if (0 == tmpFile.length()) {
                        nextInstance = 0;
                    } else {
                        nextInstance = Integer.parseInt(tmpFile.readLine());
                    }
                }
            }
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Could not create file",
                                            e);
        } catch (IOException e) {
            throw new IllegalStateException("Could not read initial number of next instance",
                                            e);
        }
        nextControlScopeFileTmp = root.resolve(NEXT_INSTANCE_TMP_FILE);
    }

    /**
     * Creates a new directory for a new {@link ControlScopeOnDisk} instance to use.
     * 
     * @param process
     *            the name of the process for which to create a new directory.
     *
     * @return the created directory.
     *
     * @throws IOException
     *             when then is a problem creating the directory.
     */
    Path createControlScopePath(String process) throws IOException {
        final Path rescue = ((root.resolve(process)).resolve(RESCUED_CONTROLS_DIRECTORY));
        if (!Files.exists(rescue)) {
            createDirectories(rescue);
        }
        final Path preserved = ((root.resolve(process)).resolve(PRESERVED_CONTROLS_DIRECTORY));
        if (!Files.exists(preserved)) {
            createDirectories(preserved);
        }
        final Path active = ((root.resolve(process)).resolve(ACTIVE_CONTROLS_DIRECTORY)).resolve(Integer.toString(getNextInstance()));
        if (!Files.exists(active)) {
            createDirectories(active);
        }
        return active;
    }

    /**
     * Reads the number to assign to the next instance's directory, and updated the
     * persistent store of this number.
     *
     * @return the number to assign to the next instance's directory.
     * 
     * @throws IOException
     *             when the next number can not be saved in persistent store.
     */
    private synchronized int getNextInstance() throws IOException {
        int result = nextInstance++;
        int retries = 0;
        while (true) {
            try (final FileOutputStream tmpStream = new FileOutputStream(nextControlScopeFileTmp.toFile())) {
                tmpStream.write((Integer.toString(nextInstance)).getBytes());
                (tmpStream.getChannel()).force(false);
                try {
                    Files.move(nextControlScopeFileTmp,
                               nextControlScopeFile,
                               StandardCopyOption.REPLACE_EXISTING,
                               StandardCopyOption.ATOMIC_MOVE);
                    return result;
                } catch (IOException e) {
                    retries = waitForRetry(retries,
                                           "Attempting retry of the move of \"" + nextControlScopeFileTmp.toString()
                                                    + "\" having waited "
                                                    + FLUSH_RETRY_INTERVAL
                                                    + " milliseconds",
                                           "Failed to save next number");
                }
            } catch (IOException e) {
                // Let IOException propagate up the call stack.
                throw e;
            }
        }
    }

    /**
     * Returns a collection of {@link Path} instance which contain
     * {@link ControlScope} states for {@link ControlScope} instances that were in
     * the process of being executed when the previous instance of this application
     * stopped. active when the application
     * 
     * @param process
     *            the name of the process for which to recover the paths.
     *
     * @return the collection of {@link Path} instance for the recovered
     *         {@link ControlScope} instances.
     */
    Collection<Path> recoverControlScopePaths(String process) {
        final Path flowDirectory = (root.resolve(process)).resolve(ACTIVE_CONTROLS_DIRECTORY);
        return recoverInstantiationPaths(flowDirectory);
    }

    /**
     * Returns a collection of {@link Path} instance which contain persistent
     * {@link ControlScopeOnDisk} instances states.
     * 
     * @param path
     *            the path that contains the the {@link ControlFlowOnDisk} instance
     *            whose {@link ControlOwner} identity should be returned.
     *
     * @return tcollection of {@link Path} instance which contain persistent
     *         {@link ControlScopeOnDisk} instances states.
     */
    private Collection<Path> recoverInstantiationPaths(final Path path) {
        final GatherNumericDirectories<Path> numericFiles = new GatherNumericDirectories<>();
        try {
            Files.walkFileTree(path,
                               new HashSet<FileVisitOption>(),
                               1,
                               numericFiles);
            return numericFiles.getPaths();
        } catch (IOException e) {
            throw new IllegalStateException("Failure in backing store!",
                                            e);
        }
    }

    /**
     * Returns a collection of {@link Path} instance which contain
     * {@link ControlScope} states for {@link ControlScope} instances that were not
     * in the process of being executed, but whose state have been rescued.
     * 
     * @param process
     *            the name of the process for which to rescue the paths.
     *
     * @return the collection of {@link Path} instance for the rescued
     *         {@link ControlScope} instances.
     */
    Collection<Path> rescueControlScopePaths(String process) {
        final Path flowDirectory = (root.resolve(process)).resolve(RESCUED_CONTROLS_DIRECTORY);
        return recoverInstantiationPaths(flowDirectory);
    }

    /**
     * Returns the sequence of {@link Path} instances that contain persistent
     * {@link DataOutput} instances for the implied {@link DataCellOnDisk} instance.
     *
     * @param path
     *            the {@link Path} instance of the directory for the
     *            {@link DataCellOnDisk} instance whose path to its persistent
     *            {@link DataOutput} instances should be returned.
     *
     * @return the sequence of {@link Path} instances that contain persistent
     *         {@link DataOutput} instances for the implied {@link DataCellOnDisk}
     *         instance.
     */
    static Collection<Path> recoverIdentityPaths(Path path) {
        final GatherXMLFiles<Path> namedFiles = new GatherXMLFiles<>();
        try {
            Files.walkFileTree(path,
                               new HashSet<FileVisitOption>(),
                               1,
                               namedFiles);
            return namedFiles.getPaths();
        } catch (IOException e) {
            throw new IllegalStateException("Failure in backing store!",
                                            e);
        }
    }
}
