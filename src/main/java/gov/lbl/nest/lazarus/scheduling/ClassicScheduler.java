package gov.lbl.nest.lazarus.scheduling;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.NoSuchElementException;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.lazarus.control.ControlFlowException;
import gov.lbl.nest.lazarus.control.ControlOnlyExecutableProcess;
import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.execution.ProcessDefinition;
import gov.lbl.nest.lazarus.execution.ProcessScheduler;

/**
 * This class implements the {@link ProcessScheduler} interface using the
 * original mechanism of immediately executing the new {@link ExecutableProcess}
 * instance.
 *
 * @author patton
 */
public class ClassicScheduler implements
                              ProcessScheduler {

    /**
     * The List instance containing the ExecutableProcess instances that are wait to
     * be executed.
     */
    private final Deque<ExecutableProcess> awaitingExecution = new ArrayDeque<>();

    @Override
    public boolean addExecutableProcess(ExecutableProcess executableProcess) {
        try {
            synchronized (awaitingExecution) {
                awaitingExecution.add(executableProcess);
                awaitingExecution.notifyAll();

            }
            startExecution();
            return true;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public ExecutableProcess addProcessDefinition(ProcessDefinition process,
                                                  String name,
                                                  Integer priority,
                                                  Map<String, Object> values) throws InitializingException {
        try {
            final ExecutableProcess executableProcess = process.createExecutableProcess(name,
                                                                                        priority,
                                                                                        values);
            synchronized (awaitingExecution) {
                awaitingExecution.add(executableProcess);
                awaitingExecution.notifyAll();

            }
            startExecution();
            return executableProcess;
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * This method is used simply to decouple the request for execution with the
     * execution itself in order to reflect the more complex implementation of the
     * {@link ProcessScheduler} interface.
     */
    private void startExecution() {
        synchronized (awaitingExecution) {
            try {
                while (!awaitingExecution.isEmpty()) {
                    final ExecutableProcess executableProcess = awaitingExecution.remove();
                    if (executableProcess instanceof ControlOnlyExecutableProcess) {
                        ((ControlOnlyExecutableProcess) executableProcess).execute();
                    } else {
                        throw new ControlFlowException("Only " + ControlOnlyExecutableProcess.class.getName()
                                                       + " instnaces can be executed by "
                                                       + (this.getClass()).getName());
                    }
                }
            } catch (NoSuchElementException e) {
                throw new RuntimeException("Should never (ever) get here",
                                           e);
            }
        }
    }
}
