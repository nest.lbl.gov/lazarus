/**
 * This package provides classes that implement the "out of the box"
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * <li>{@link gov.lbl.nest.common.configure}</li>
 * <li>{@link gov.lbl.nest.lazarus.execution}</li>
 * </ul>
 */
package gov.lbl.nest.lazarus.scheduling;