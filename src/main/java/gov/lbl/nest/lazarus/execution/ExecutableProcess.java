package gov.lbl.nest.lazarus.execution;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.Process;

/**
 * This interface is used to accesses a concrete, executable instance of a
 * {@link Process} instance.
 *
 * @author patton
 */
public interface ExecutableProcess {

    /**
     * Returns the {@link ThrownFailure} instance, if there is one, that cause this
     * process to terminate.
     *
     * @return the {@link ThrownFailure} instance, if there is one, that cause this
     *         process to terminate.
     */
    ThrownFailure getFailure();

    /**
     * Returns the label used when creating this object.
     *
     * @return the label used when creating this object.
     */
    String getLabel();

    /**
     * Returns the {@link AtomicBoolean} instance that will be <code>true</code>
     * when this workflow contained in this object has terminated. The
     * <code>wait()</code> method of the returned instance can be used to detect changes
     * in its value.
     *
     * @return the {@link AtomicBoolean} instance that will be <code>true</code>
     *         when this workflow contained in this object has terminated.
     */
    AtomicBoolean getTerminated();

    /**
     * Returns the current values for the {@link DataObject} instances contained in
     * this object.
     *
     * @return the current values for the {@link DataObject} instances contained in
     *         this object.
     */
    Map<String, Object> getValues();
}
