package gov.lbl.nest.lazarus.execution;

import gov.lbl.nest.lazarus.structure.FlowNode;

/**
 * This abstract class is used to capture a {@link Throwable} instance that
 * cause a {@link ExecutableProcess} instance to fail.
 *
 * @author patton
 */
public abstract class ThrownFailure {

    /**
     * The {@link Cause} instance that captures the {@link Throwable} instance in
     * this object.
     */
    private Cause cause;

    /**
     * The identity of the {@link FlowNode} which threw the {@link Throwable}
     * instance.
     */
    private String owner;

    /**
     * The {@link String} instance that can be used when a problem file is being
     * used.
     */
    private String rescue;

    /**
     * Creates an instance of this class.
     */
    protected ThrownFailure() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param throwable
     *            the {@link Throwable} instance thrown.
     * @param owner
     *            the identity of the {@link FlowNode} which threw the
     *            {@link Throwable} instance.
     * @param rescue
     *            the {@link String} instance that can be used when a problem file
     *            is being used.
     */
    protected ThrownFailure(Throwable throwable,
                            String owner,
                            String rescue) {
        this.cause = new Cause(throwable);
        this.owner = owner;
        this.rescue = rescue;
    }

    /**
     * Returns the {@link Cause} instance that captures the {@link Throwable}
     * instance in this object.
     *
     * @return the {@link Cause} instance that captures the {@link Throwable}
     *         instance in this object.
     */
    public Cause getCause() {
        return cause;
    }

    /**
     * Returns the identity of the {@link FlowNode} which threw the
     * {@link Throwable} instance.
     *
     * @return the identity of the {@link FlowNode} which threw the
     *         {@link Throwable} instance.
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Returns the {@link String} instance that can be used when a problem file is
     * being used.
     *
     * @return the {@link String} instance that can be used when a problem file is
     *         being used.
     */
    public String getRescue() {
        return rescue;
    }

    /**
     * Sets the {@link Cause} instance that captures the {@link Throwable} instance
     * in this object.
     *
     * @param cause
     *            the {@link Cause} instance that captures the {@link Throwable}
     *            instance in this object.
     */
    protected void setCause(Cause cause) {
        this.cause = cause;
    }

    /**
     * Sets the identity of the {@link FlowNode} which threw the {@link Throwable}
     * instance.
     *
     * @param owner
     *            the identity of the {@link FlowNode} which threw the
     *            {@link Throwable} instance.
     */
    protected void setOwner(String owner) {
        this.owner = owner;
    }
}
