package gov.lbl.nest.lazarus.execution;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to capture detail about the {@link Throwable} instance
 * that was the cause of a failure in an XML element.
 *
 * @author patton
 */
@XmlType(propOrder = { "className",
                       "message",
                       "cause" })
public class Cause {

    /**
     * Creates an instance of the original Throwable instance, but without the
     * details!
     *
     * @param className
     *            the class name of the original Throwable instance
     * @param message
     *            the message, if any, associated with the {@link Throwable}.
     *
     * @return
     */
    private static Throwable createThrowable(final String className,
                                             final String message,
                                             final Cause cause) {
        final ClassLoader loader = Cause.class.getClassLoader();
        try {
            @SuppressWarnings("unchecked")
            final Class<Throwable> loadedClass = (Class<Throwable>) loader.loadClass(className);
            final Throwable thrown;
            if (null == cause) {
                thrown = null;
            } else {
                thrown = createThrowable(cause.getClassName(),
                                         cause.getMessage(),
                                         cause.getCause());
            }
            try {
                final Constructor<Throwable> constructor = loadedClass.getConstructor(new Class<?>[] { String.class,
                                                                                                       Throwable.class });
                return constructor.newInstance(message,
                                               thrown);
            } catch (NoSuchMethodException e) {
                try {
                    final Constructor<Throwable> constructor = loadedClass.getConstructor(new Class<?>[] { String.class });
                    return constructor.newInstance(message);
                } catch (NoSuchMethodException e2) {
                    try {
                        final Constructor<Throwable> constructor = loadedClass.getConstructor(new Class<?>[] {});
                        return constructor.newInstance();
                    } catch (NoSuchMethodException e3) {
                        return new Throwable("This is a substitute for a \"" + className
                                             + "\" instance as one could not be constructed");
                    }
                }
            }
        } catch (InvocationTargetException e) {
            throw new IllegalArgumentException("Failed to correctly invoke the constructor of \"" + className
                                               + "\"");
        } catch (SecurityException e) {
            throw new IllegalArgumentException("Constructor of class \"" + className
                                               + "\" is not accessible");
        } catch (ClassNotFoundException e) {
            return new Throwable("This is a substitute for a \"" + className
                                 + "\" instance as one could not be constructed");
        } catch (InstantiationException e) {
            throw new IllegalArgumentException("Can not instantiate class \"" + className
                                               + "\"");
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("Constructor of class \"" + className
                                               + "\" can not be accessed");
        }
    }

    /**
     * The {@link Cause} instance that was the reason this object was generated.
     */
    private Cause cause;

    /**
     * The {@link Throwable} instance that caused the failure.
     */
    private Throwable throwable;

    /**
     * The class of the {@link Throwable} instance in this object.
     */
    private String className;

    /**
     * The message, if any, associated with the {@link Throwable} in this object.
     */
    private String message;

    /**
     * Creates an instance of this class.
     */
    protected Cause() {
        // does nothing.
    }

    /**
     * Creates an instance of this class.
     *
     * @param throwable
     *            the {@link Throwable} instance whose details must be captured.
     */
    Cause(final Throwable throwable) {
        final Throwable thrown = throwable.getCause();
        if (null != thrown && throwable != thrown) {
            cause = new Cause(thrown);
        }
        message = throwable.getMessage();
        className = (throwable.getClass()).getName();
    }

    /**
     * Returns the {@link Cause} instance that was the reason this object was
     * generated.
     *
     * @return he {@link Cause} instance that was the reason this object was
     *         generated.
     */
    @XmlElement
    protected Cause getCause() {
        return cause;
    }

    /**
     * Returns the class of the {@link Throwable} instance in this object.
     *
     * @return the class of the {@link Throwable} instance in this object.
     */
    @XmlElement(name = "class")
    protected String getClassName() {
        return className;
    }

    /**
     * Returns the message, if any, associated with the {@link Throwable} in this
     * object.
     *
     * @return the message, if any, associated with the {@link Throwable} in this
     *         object.
     */
    @XmlElement
    protected String getMessage() {
        return message;
    }

    /**
     * Returns the {@link Throwable} instance that this object represents.
     *
     * @return the {@link Throwable} instance that this object represents.
     */
    @XmlTransient
    public Throwable getThrowable() {
        if (null == throwable && null != className) {
            throwable = createThrowable(className,
                                        message,
                                        cause);
        }
        return throwable;
    }

    /**
     * Sets the {@link Cause} instance that was the reason this object was
     * generated.
     *
     * @param cause
     *            the {@link Cause} instance that was the reason this object was
     *            generated.
     */
    protected void setCause(Cause cause) {
        this.cause = cause;
    }

    /**
     * Sets the class of the {@link Throwable} instance in this object.
     *
     * @param className
     *            the class of the {@link Throwable} instance in this object.
     */
    protected void setClassName(final String className) {
        this.className = className;
    }

    // static member methods (alphabetic)

    /**
     * Sets the message, if any, associated with the {@link Throwable} in this
     * object.
     *
     * @param message
     *            the message, if any, associated with the {@link Throwable} in this
     *            object.
     */
    protected void setMessage(String message) {
        this.message = message;
    }
}
