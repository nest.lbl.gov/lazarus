package gov.lbl.nest.lazarus.execution;

import gov.lbl.nest.common.suspension.Suspendable;

/**
 * This interface extends the {@link FlowNodeExecutions} interface by adding the
 * ability for it to be suspended.
 *
 * @author patton
 */
public interface SuspendableFlowNodeExecutions extends
                                               FlowNodeExecutions,
                                               Suspendable {

}
