package gov.lbl.nest.lazarus.execution;

import java.util.Map;

import gov.lbl.nest.common.configure.InitializingException;

/**
 * This interface is used to schedule a new execution of a
 * {@link ProcessDefinition} instance.
 *
 * @author patton
 */
public interface ProcessScheduler {

    /**
     * Schedules the execution of an existing {@link ExecutableProcess} instance.
     * 
     * @param executableProcess
     *            the {@link ExecutableProcess} instance whose execution should be
     *            scheduled.
     * 
     * @return <code>false</code> if the execution could not be scheduled.
     */
    boolean addExecutableProcess(ExecutableProcess executableProcess);

    /**
     * Schedules a new execution of the supplied {@link ProcessDefinition} instance
     * using the supplied initial conditions.
     *
     * @param process
     *            the {@link ProcessDefinition} instance for which a new execution
     *            should be scheduled.
     * @param name
     *            the name to give the new execution instance.
     * @param priority
     *            the priority to give the new execution instance.
     * @param values
     *            the values of the Data Object for the new execution instance.
     *
     * @return The {@link ExecutableProcess} instance that can be used to access the
     *         resulting execution, or <code>null</code> if the new execution could
     *         not be scheduled.
     *
     * @throws InitializingException
     *             when this object is not ready for executions to be scheduler.
     */
    ExecutableProcess addProcessDefinition(ProcessDefinition process,
                                           String name,
                                           Integer priority,
                                           Map<String, Object> values) throws InitializingException;
}
