package gov.lbl.nest.lazarus.execution;

/**
 * This interface is used to signal the termination of a
 * {@link ExecutableProcess} instance.
 */
public interface TerminationListener {

    /**
     * This method is called when the specified {@link ExecutableProcess} as failed.
     *
     * @param process
     *            the {@link ExecutableProcess} that has failed.
     *
     * @return true if the internal structures of the instance should be preserved
     *         so it can be rescued.
     */
    boolean failure(ExecutableProcess process);

    /**
     * This method is called when the specified {@link ExecutableProcess} has
     * completed successfully.
     *
     * @param process
     *            the {@link ExecutableProcess} that has completed
     */
    void success(ExecutableProcess process);
}
