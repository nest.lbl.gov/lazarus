package gov.lbl.nest.lazarus.execution;

import java.util.Collection;
import java.util.Map;

import gov.lbl.nest.lazarus.management.ProcessManager;
import gov.lbl.nest.lazarus.management.ProcessManagerImpl;
import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.DataObjectException;
import gov.lbl.nest.lazarus.structure.Process;

/**
 * This interface extends the {@link Process} interface for this package.
 *
 * @author patton
 */
public interface ProcessDefinition extends
                                   Process {
    /**
     * Creates a new {@link ExecutableProcess} instance of this object using the
     * supplied parameters. The created {@link ExecutableProcess} instance will use
     * the default {@link TerminationListener} instance if one has been specified.
     *
     * @param label
     *            the label for the created {@link ExecutableProcess} instance.
     * @param priority
     *            the priority of the created {@link ExecutableProcess} instance
     * @param values
     *            the initial values for the {@link DataObject} instances of this
     *            object.
     *
     * @return the created {@link ExecutableProcess} instance.
     *
     * @throws DataObjectException
     *             when the initial values of the {@link DataObject} instances can
     *             not be set.
     *
     * @see #setDefaultTerminationListener(TerminationListener)
     */
    ExecutableProcess createExecutableProcess(String label,
                                              Integer priority,
                                              Map<String, Object> values) throws DataObjectException;

    /**
     * Creates a new {@link ExecutableProcess} instance of this object using the
     * supplied parameters.
     *
     * @param label
     *            the label for the created {@link ExecutableProcess} instance.
     * @param priority
     *            the priority of the created {@link ExecutableProcess} instance
     * @param values
     *            the initial values for the {@link DataObject} instances of this
     *            object.
     * @param listener
     *            the {@link TerminationListener} instance to use with this object,
     *            otherwise the default {@link TerminationListener} instace, if any,
     *            with be used.
     *
     * @return the created {@link ExecutableProcess} instance.
     *
     * @throws DataObjectException
     *             when the initial values of the {@link DataObject} instances can
     *             not be set.
     */
    ExecutableProcess createExecutableProcess(String label,
                                              Integer priority,
                                              Map<String, Object> values,
                                              TerminationListener listener) throws DataObjectException;

    /**
     * Returns the {@link ProcessManagerImpl} instance, if any, with which to
     * control this object.
     *
     * @return the {@link ProcessManagerImpl} instance, if any, with which to
     *         control this object.
     */
    ProcessManager getProcessManager();

    /**
     * Recovers the collection of {@link ExecutableProcess} instances that were not
     * completed when the application terminated.
     *
     * @return the collection of {@link ExecutableProcess} instances that were not
     *         completed when the application terminated.
     */
    public Collection<? extends ExecutableProcess> recoverExecutableProcesses();

    /**
     * Recovers any {@link ExecutableProcess} instances that were not in the process
     * of being executed, but have now been rescued.
     *
     * @return the collection of {@link ExecutableProcess} instances that were not
     *         in the process of being executed, but have now been rescued.
     */
    public Collection<? extends ExecutableProcess> rescueExecutableProcesses();

    /**
     * Sets the {@link TerminationListener} instance the should be used by an
     * {@link ExecutableProcess} instance if not is explicitly specified.
     *
     * @param listener
     *            the {@link TerminationListener} instance the should be used by an
     *            {@link ExecutableProcess} instance if not is explicitly specified.
     */
    public void setDefaultTerminationListener(TerminationListener listener);
}
