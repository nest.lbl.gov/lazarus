package gov.lbl.nest.lazarus.execution;

import java.util.List;

import gov.lbl.nest.lazarus.structure.FlowNodeInspector;

/**
 * This interface is used to report on the counts of a single a single flow
 * node.
 *
 * @author patton
 */
public interface FlowNodeExecutions {

    /**
     * Returns the number of tasks currently executing for the flow node this object
     * is monitoring.
     *
     * @return the number of tasks currently executing for the flow node this object
     *         is monitoring.
     */
    int getExecutingCount();

    /**
     * Returns the collection of {@link FlowNodeInspector} instances currently
     * associated with the flow node this object is monitoring.
     *
     * @return the collection of {@link FlowNodeInspector} instances currently
     *         associated with the flow node this object is monitoring.
     */
    List<? extends FlowNodeInspector> getInspectors();

    /**
     * Returns the name of the flow node this object is monitoring.
     *
     * @return the name of the flow node this object is monitoring.
     */
    String getName();

    /**
     * Returns the number of pending tasks currently awaiting executing for the flow
     * node this object is monitoring.
     *
     * @return the number of pending tasks currently awaiting executing for the flow
     *         node this object is monitoring.
     */
    int getPendingCount();

    /**
     * Returns the total number of tasks assigned the flow node this object is
     * monitoring.
     *
     * @return the total number of tasks assigned the flow node this object is
     *         monitoring.
     */
    int getTotalCount();
}
