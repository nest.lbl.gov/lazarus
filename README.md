[Lazarus](https://bitbucket.org/berkeleylab/nest-lazarus) provides an implementation of a subset of the "Process Execution Conformance" part of the [BPMN 2.0 specification](http://www.omg.org/spec/BPMN/2.0/).

The main focus for this project is to provide an implementation that was robust to failure of the machine on which it is executing. The aim being that if the machine is uncleanly shutdown then, upon restart, the existing workflow will recover by re-starting whatever task or tasks where executing at the time and so none of the results of the workflow that has already completed will be lost and thus will not need to be re-executed.

The subset of the specification is selected to provide the necessary mechanics to glue together a set of Java classes, both via the control and data channels, into a straight-forward workflow. The subset includes the following:

* Execution of a Task(Activity) within the context of a Process. With no inbound flows the Task should start when the Process is started and with no End Event this Process should end when there are no more flows, i.e. when the Task has completed.
