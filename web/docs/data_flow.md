# Data Flow #

This document lays out the concepts behind how the flow of data is managed by Lazarus. Lazarus uses the the BPMN data flow model to manage its data.


## Overview ##

A BPMN Process can have zero, one or more data objects are passed into each Instantiation that is created. Values used in flow nodes within the Process can be mapped onto those data objects by means of an input data association. Similarly resultant values can be mapped onto these data objects by output data associations. The mapping between the values in the Process level data objects and the values used or created by an activity is specified by an I/O specification, union of the input and output data associations.


## Data Flow within a Process Instance ##

An instantiation of a BPMN process, a.k.a. a workflow, occurs by associating a concrete inputs to a instance of a process and that instantiation can be executed. The instantiation of a Process holds the process level data, while when an flow node is executed, i.e. the flow node "owns" a control flow, the data objects needed by that flow node are created, as declared by the input data association and associated with the owned control flow. That way, in case of recovery, the same initial data is available flow node.






## Architecture ##

The flow data behaviour is establish by the collaboration of the following interfaces and classes.


## DataObject ##

The is an object that contains values needed or produced by the execution of an realized instance of the process.


## DataInput ##

This is an object that provides data to an Activity.


## DataOutput ##

This is an object that gathers data to an Activity.


## DataInputAssociation ##

This is an object that maps data from one or more DataObject instances, or parts thereof, onto a DataInput instance.


## DataOutputAssociation ##

This is an object that maps data from a DatOutput instance onto one or more DataObject instances, or parts thereof.


## InputOutputSpecification ##

This is an object that containers all of the data flow information for an activity.
