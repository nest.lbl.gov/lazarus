# Control Flow #

This document lays out the concepts behind how the flow of control is managed by Lazarus. Lazarus uses the the BPMN control flow model to manage its control.


## Overview ##

A BPMN Process captured the structure of a workflow. In terms of control flow, the workflow is made up of flow nodes, which are where decision are made or actions are executed, that are connected by sequence flows that control which flow nodes are executed and when.


## Flow of Control within a Process Instance ##

An instantiation of a BPMN process, a.k.a. a workflow, occurs by associating a concrete inputs to a instance of a process and that instantiation can be executed. The instantiation manages a collection of one or more control flows as they pass through the set of flow nodes that make up a workflow.

At any time a control flow  can only be owned by a single , while a owner  can own zero one or more control flows at any time.


## Control Flows and Owners ##

An owner can also be considered to be a "checkpoint" in the workflow because restarting a recovered workflow is done by invoking the `exercise` method on each recovered control flow , which in turn calls the `exercise` method of its owning owner .

The natural place to checkpoint control flow is when an upstream flow node has passed control to a flow node. A flow node into three parts:

1. Inbound - the part that receives control flows from the inbound sequence flows and, when appropriate, passes a single control flow to the next part.

1. Body - the part that, upon receipt of the single control flow from the Inbound, executes this node's responsibilities, i.e. does something, after which it passes that single control flow to the next part.

1. Outbound - the final part that, upon receipt of the single control flow from the Body, passes on the necessary control flows to the outbound sequence flows.

A control flow is effectively pass into a flow node once there is a single control flow to be passed into the Body. Therefore this is the time to change ownership of a control flow and also this means that the `exercise` method of the owning flow node should start execution of the of the node's responsibilities, i.e. the Body.

When considering a single control flow, the above scenario works fine, however the cases where control flows are created or destroyed requires more care to make sure that the right control flows are recovered for a given checkpoint. In particular the Inbound and Outbound parts that support "parallel and inclusive" behaviour need careful attention.


## Outbound Parallel Behaviour ##

For an Outbound part that suports parallel behaviour (for this discussion and the inbound discussion, inclusive behavior is considered a subset of the parallel behaviour), new control flows are created. It simplifies recovery to consider that there are two states for this part.

1. No new control flows have been created.

1. All new control flows have been created.

with the line between then being whether the original control flow received from the Body has been destroyed or not. This way this part will be recovered into one of those two states, with the first one requiring the entire outbound part to be executed all over again, and the second requiring simply the passing of the new control flows to their outbound sequence flows.

To support those requirements the following for needed.

* An extra control owner from which to take the control flow from the Body. This will stop the body having to re-executes its responsibilities upon recovery as they will have already been completed.

* A mechanism to associate the new control flows with the original one. Thus if the original one exist after recovery the new control flows do not need to be recovered.

* A mechanism to associate the new control flows with their appropriate outbound sequence flows so that if the passing of a control flow via a sequence flow has not completed before recovery it can be completed after recovery.

The first of these requirements can be satisfied by making the Outbound part a control owner that is separate from the flow node's ownership, i.e. the passing to the control flow from the Body to the the Outbound part becomes its own checkpoint.

The second requirement can be satisfied by marking the new control flows as "forked" flows and if the original control flow exists then any recovered forked flow should be destroyed. (Note: all of these control flows will be owned by the Outbound part of the flow node.)

The final requirement can be satisfied by recording the index of the sequence flow to which a control flow should be passed as this order will preserved when the workflow is restored.
 

## Inbound Parallel Behaviour ##

For an Inbound part that suports parallel behaviour, old control flows are destroyed. It simplifies recovery to consider that there are two states for this part.

1. No old control flows have been destroyed.

1. All old control flows have been destroyed.

with the line between then being whether the new control flow to be passed to the Body has been created or not. As with the Outbound behaviour, this part will be recovered into one of those two states, in this case with the first one requiring the entire inbound part to be executed all over again, and the second requiring simply the passing of the new control flow to its associated Body.

To support those requirements the following for needed.

* An extra control owner from which to take the control flow from the incoming sequence flows. This simplifies how to handle the system while waiting for the other sequence flows to delivery their control flows.

* A mechanism to associate the old control flows with the new one. Thus if the new one exist after recovery the old control flows do not need to be recovered.

At the moment tere is _no_ support for multiple controls being received via a single sequence flow, therefore then is no need for a mechanism to associate the old control flows with their appropriate inbound sequence flows. This may be revisited in a later version.


## Architecture ##

The flow control behaviour is establish by the collaboration of the following interfaces and classes.


## Instantiation ##

This is a instantiation of a BPMN process, a.k.a. a workflow, with concrete inputs and it can be executed.


### SequenceFlow ###

This passes a control flow from one flow node to another.


### FlowNode ###

This receives flow on control from with either inbound SequenceFlow instances, or from an external trigger.

A FlowNode itself is made up of three components:

*   An Inbound instance received flow control from inbound SequenceFlows instances and determines whether the Body instance should be executed.
*   a Body instance that executes the responsibilities, if any, of the FlowNode; and
*   an Outbound instance the determines to which outbound SequenceFlows instances should control flow be passed after the Body instance has complete the FlowNode's responsibilities.


### Inbound ###

This manages zero, one or many inbound SequenceFlow instances and its makes the decision, when a flow control is received from an upstream FlowNode via one of these SequenceFlows, whether of not to execute the Body instance that is part of the parent FlowNode.

An Inbound may also accept a control flow when requested to do so by an ExternalTrigger.


### Outbound ###

This manages zero, one or many outbound SequenceFlow inatances and, when the execution of the Body instance that is part of the parent FlowNode has completed, decides to which  SequenceFlows should control flows be passed.


### Body ###

This executes the responsibilities of the FlowNode when it receives a control flow from  the Inbound instance. When those responsibilities are fulfilled then it passes that control onto the Outbound instance.


---

# footnotes #

* XML [ID](https://www.w3.org/TR/xml/#id)s follow the limitation of the [Names](https://www.w3.org/TR/xml/#NT-Name) construction which restricts the characters with which [a Name can start](https://www.w3.org/TR/xml/#NT-NameStartChar), or can be [contained in a Name](https://www.w3.org/TR/xml/#NT-NameChar). This feature can be used to distinguish owner identities derived from a flow node's identity.

